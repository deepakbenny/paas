package com.swiggy.checkout.paytm.service;

import com.swiggy.checkout.data.entities.UserTokenDetailsEntity;
import com.swiggy.checkout.data.model.bo.PaymentConfiguration;
import com.swiggy.checkout.data.model.dao.UserTokenDetailsDao;
import com.swiggy.checkout.exception.SwiggyPaymentException;
import com.swiggy.checkout.extenal.entities.response.BaseResponse;
import com.swiggy.checkout.extenal.entities.response.ErrorResponse;
import com.swiggy.checkout.extenal.entities.response.SuccessResponse;
import com.swiggy.checkout.jackson.JacksonConverterFactory;
import com.swiggy.checkout.paytm.PaytmProperties;
import com.swiggy.checkout.paytm.entities.request.GetOtpRequest;
import com.swiggy.checkout.paytm.entities.request.ValidateOtpRequest;
import com.swiggy.checkout.paytm.entities.response.GetOtpResponse;
import com.swiggy.checkout.paytm.entities.response.GetSsoTokenResponse;
import com.swiggy.checkout.paytm.entities.response.ValidateOtpResponse;
import com.swiggy.checkout.service.WalletTokenMigrationService;
import com.swiggy.checkout.utils.HTTPLogger;
import com.swiggy.checkout.utils.Utility;
import lombok.extern.slf4j.Slf4j;
import okhttp3.Interceptor;
import okhttp3.OkHttpClient;
import okhttp3.Request;
import okhttp3.logging.HttpLoggingInterceptor;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import retrofit2.Call;
import retrofit2.Response;
import retrofit2.Retrofit;

import javax.annotation.PostConstruct;
import java.io.IOException;
import java.sql.Timestamp;
import java.util.concurrent.TimeUnit;

/**
 * Created by Manish on 15/02/17.
 */

@Service
@Slf4j
public class PaytmAuthService {

    private PaytmAuthClient paytmAuthClient;

    @Autowired
    private PaytmProperties paytmProperties;

    @Autowired
    private PayTmRetrofitService retrofitService;

    @Autowired
    private WalletTokenMigrationService walletTokenMigrationService;

    @Autowired
    private UserTokenDetailsDao userTokenDetailsDao;

    @Autowired
    private PaymentConfiguration configuration;

    public static final String PAYTM = "PayTM";
    private static final String USER_TOKEN_BACKFILL_ENABLED_NEW = "user_token_backfill_enabled_new";

    @PostConstruct
    public void postConstruct() {

        OkHttpClient httpClient = new OkHttpClient.Builder()
                .addNetworkInterceptor(new Interceptor() {
                    @Override
                    public okhttp3.Response intercept(Chain chain) throws IOException {
                        Request request = chain
                                .request()
                                .newBuilder()
                                .addHeader("Authorization", paytmProperties.getBasicAuth()) // "Basic YjUzOGIwODJiOGJjMzEwNDRhNmU2YzllZDg0NmRkM2Y6YzcwOTY2OGI5MTExZWFmZmNjMWE3ZGQ1YjM3YzNhMzc="
                                .addHeader("User-Agent", "Swiggy-API")
                                .build();
                        return chain.proceed(request);
                    }
                })
                .addInterceptor(new HttpLoggingInterceptor(HTTPLogger.DEFAULT).setLevel(HttpLoggingInterceptor.Level.BODY))
                .connectTimeout(1000, TimeUnit.MILLISECONDS) // 1 second
                .readTimeout(5000, TimeUnit.MILLISECONDS) // 5 seconds
                .build();

        Retrofit retrofit = new Retrofit.Builder()
                .baseUrl(paytmProperties.getAuthUrl()) // paytmProperties.getAuthUrl() or "https://accounts-uat.paytm.com"
                .addConverterFactory(JacksonConverterFactory.create())
                .client(httpClient)
                .build();

        paytmAuthClient = retrofit.create(PaytmAuthClient.class);

    }


    public BaseResponse getOtpForLinkingWallet() throws SwiggyPaymentException {
        Call<GetOtpResponse> otpForLinkingWallet = paytmAuthClient.getOtpForLinkingWallet(
                new GetOtpRequest(Utility.getUserFromHeader().getMobile(), paytmProperties.getClientId(), "wallet", "token"));
        BaseResponse baseResponse = new SuccessResponse();
        baseResponse.setData(retrofitService.getResponseOrLogWarning(otpForLinkingWallet, "Could not send otp."));
        return baseResponse;

    }


    public BaseResponse validateOtpForLinkingWallet(String otp, String state) throws SwiggyPaymentException {
        Call<ValidateOtpResponse> validateOtpResponseCall = paytmAuthClient.validateOtpForLinkingWallet(new ValidateOtpRequest(otp, state));
        ValidateOtpResponse validateOtpResponse = retrofitService.getResponseOrLogWarning(validateOtpResponseCall, "Otp validation failed. Plz retry");
        saveInDb(validateOtpResponse);
        return new SuccessResponse(validateOtpResponse.getAccessToken());
    }

    public BaseResponse getSsoToken() {
        GetSsoTokenResponse getSsoTokenResponse = new GetSsoTokenResponse();

        String userId = Utility.getUserFromHeader().getCustomerId();
        UserTokenDetailsEntity userTokenDetails = userTokenDetailsDao.findByUserIdAndPaymentMethodName(Utility.getLong(userId), PAYTM);

        //TODO -- to be removed after migration
        if (null == userTokenDetails && !Utility.isEmpty(Utility.getUserFromHeader().getPaytmSsoToken()) && configuration.getConfigurationAsBoolean(USER_TOKEN_BACKFILL_ENABLED_NEW, true)) {
            userTokenDetails = walletTokenMigrationService.saveUserTokenDetails(userId, Utility.getUserFromHeader().getPaytmSsoToken(), PAYTM);
        }

        if (null != userTokenDetails) {
            getSsoTokenResponse.setAccessToken(userTokenDetails.getToken());
            return new SuccessResponse(getSsoTokenResponse);
        }
        return new ErrorResponse("Couldn't find user token");
    }

    /**
     * Delink api is a special api since the response body is null(Void).
     * The only way to distinguish between success or error is response code 200.
     *
     * @return
     * @throws SwiggyPaymentException
     */
    public BaseResponse delink() throws SwiggyPaymentException {

        String userId = Utility.getUserFromHeader().getCustomerId();
        UserTokenDetailsEntity userTokenDetails = userTokenDetailsDao.findByUserIdAndPaymentMethodName(Utility.getLong(userId), PAYTM);

        //TODO -- to be removed after the migration
        if (null == userTokenDetails && Utility.getUserFromHeader().getPaytmSsoToken() != null && configuration.getConfigurationAsBoolean(USER_TOKEN_BACKFILL_ENABLED_NEW, true)) {
            userTokenDetails = walletTokenMigrationService.saveUserTokenDetails(userId, Utility.getUserFromHeader().getPaytmSsoToken(), PAYTM);
        }

        if (null != userTokenDetails) {
            Call<Void> responseBodyCall = paytmAuthClient.delinkAccount(userTokenDetails.getToken());
            try {
                Response<Void> execute = responseBodyCall.execute();
                if (execute.isSuccessful()) {
                    userTokenDetailsDao.delete(userTokenDetails);
                    Utility.getUserFromHeader().setPaytmSsoToken(null);
                    return new SuccessResponse();
                }
            } catch (IOException e) {
                e.printStackTrace();
            }
        } else {
            return new ErrorResponse(1, "User account already de-linked");
        }
        return new ErrorResponse(1, "Couldn't delink your Paytm Account");
    }

    private void saveInDb(ValidateOtpResponse validateOtpResponse) {
        UserTokenDetailsEntity userTokenDetailsEntity = userTokenDetailsDao.findByUserIdAndPaymentMethodName(Utility.getLong(Utility.getUserFromHeader().getCustomerId()), PAYTM);
        if (null != userTokenDetailsEntity) {
            userTokenDetailsDao.delete(userTokenDetailsEntity);
        }
        UserTokenDetailsEntity userTokenDetails = new UserTokenDetailsEntity();
        userTokenDetails.setToken(validateOtpResponse.getAccessToken());
        userTokenDetails.setCreatedOn(new Timestamp(new java.util.Date().getTime()));
        userTokenDetails.setUserId(Utility.getLong(Utility.getUserFromHeader().getCustomerId()));
        userTokenDetails.setPaymentMethodName(PAYTM);
        userTokenDetails.setExpiresOn(new Timestamp(Utility.getLong(validateOtpResponse.getExpires())));
        userTokenDetails.setRefreshToken("");
        userTokenDetailsDao.save(userTokenDetails);
    }
}
