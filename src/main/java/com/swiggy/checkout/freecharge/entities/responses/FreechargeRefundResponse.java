package com.swiggy.checkout.freecharge.entities.responses;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import com.fasterxml.jackson.annotation.JsonInclude;
import lombok.*;

/**
 * Created by Manish on 27/01/17.
 */

@Setter
@Getter
@NoArgsConstructor
@AllArgsConstructor
@Builder
@JsonInclude(JsonInclude.Include.NON_NULL)
@JsonIgnoreProperties(ignoreUnknown = true)
public class FreechargeRefundResponse extends FreechargeBaseResponse{
    private String merchantTxnId;
    private String refundTxnId;
    private String refundedAmount;
    private String checksum;
    private String refundMerchantTxnId;
    private String status;
}
