package com.swiggy.checkout.health;

import com.swiggy.checkout.health.bean.BaseHealthBean;
import lombok.Builder;

import java.util.HashMap;
import java.util.Map;

/**
 * Created by Manish on 17/10/17.
 */

public class HealthCheckerResponse {

    private Map<String, BaseHealthBean> healths = new HashMap<String, BaseHealthBean>();

    public Map<String, BaseHealthBean> getHealths() {
        return healths;
    }

    public void putHealthBean(String healthCheckerName, BaseHealthBean healthBean) {
        this.healths.put(healthCheckerName, healthBean);
    }
}
