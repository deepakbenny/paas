package com.swiggy.checkout.thirdparty;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import com.fasterxml.jackson.databind.annotation.JsonSerialize;
import lombok.*;

/**
 * Created by Manish on 05/02/17.
 */
@Getter
@Setter
@Builder
@JsonSerialize(include=JsonSerialize.Inclusion.NON_DEFAULT)
@JsonIgnoreProperties(ignoreUnknown = true)
@NoArgsConstructor
@AllArgsConstructor
public class ThirdPartyMetaData {
    private double thirdPartyCash=0;

    private String thirdPartySource=null;

    private double thirdPartyCashToRefund=0;

    private String thirdPartyOrderId="";
}
