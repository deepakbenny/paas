package com.swiggy.checkout.paytm.entities.request;

import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.annotation.JsonProperty;
import lombok.*;

/**
 * Created by Manish on 29/01/17.
 */

@NoArgsConstructor
@AllArgsConstructor
@Getter
@Setter
@Builder
@JsonInclude(JsonInclude.Include.NON_NULL)
public class RefundRequest {

    @JsonProperty("MID")
    private String mid;

    @JsonProperty("ORDERID")
    private String orderId;

    @JsonProperty("TXNID")
    private String txnId;

    @JsonProperty("REFUNDAMOUNT")
    private String refundAmount;

    @JsonProperty("TXNTYPE")
    private String txnType;

    @JsonProperty("REFID")
    private String refundId;

    @JsonProperty("CHECKSUM")
    private String checkSum;


}
