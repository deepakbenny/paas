package com.swiggy.checkout.service.paylater;

import com.swiggy.checkout.exception.SwiggyPaymentException;
import com.swiggy.checkout.service.ValidPayLaterMethod;
import com.swiggy.checkout.service.paylater.lazypay.LazyPayService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

/**
 * Created by chirag.rc on 10/13/17.
 */
@Service
public class PayLaterServiceFactory {

    private static final int BAD_REQUEST_ERROR_CODE = 400;
    private static final String INVALID_WALLET_ERROR = "INVALID PAYMENT TYPE ";

    @Autowired
    private LazyPayService lazyPayService;

    public PayLaterBaseService getService(String paylaterType) throws SwiggyPaymentException {

        ValidPayLaterMethod validPayLaterMethod = ValidPayLaterMethod.getPaylaterMethodName(paylaterType);

        if (validPayLaterMethod == null) {
            throw new SwiggyPaymentException(BAD_REQUEST_ERROR_CODE, INVALID_WALLET_ERROR + paylaterType);
        }

        switch (validPayLaterMethod) {

            case LAZYPAY:
                return lazyPayService;

            default:
                throw new SwiggyPaymentException(BAD_REQUEST_ERROR_CODE, INVALID_WALLET_ERROR);
        }
    }
}
