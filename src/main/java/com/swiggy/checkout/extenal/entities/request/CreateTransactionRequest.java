package com.swiggy.checkout.extenal.entities.request;

import com.fasterxml.jackson.annotation.JsonProperty;
import com.swiggy.checkout.pojo.User;
import com.swiggy.checkout.thirdparty.ThirdPartyMetaData;
import lombok.*;

/**
 * Created by Manish on 27/01/17.
 */

@Data
@Builder
@NoArgsConstructor
@AllArgsConstructor
public class CreateTransactionRequest {

    @JsonProperty("order_id")
    private String orderId;

    @JsonProperty("payment_method")
    private String paymentMethod;

    @JsonProperty("payment_netbanking_type")
    private String paymentNetBankingType;

    @JsonProperty("card_bin_number")
    private String binNumber;

    @JsonProperty("amount")
    private String amount;

    @JsonProperty("coupon")
    private String coupon;

    @JsonProperty("third_party_meta_data")
    private ThirdPartyMetaData thirdPartyMetaData;;
}
