package com.swiggy.checkout.wallet;

import com.swiggy.checkout.exception.SwiggyPaymentException;
import com.swiggy.checkout.freecharge.service.FreechargeWalletService;
import com.swiggy.checkout.mobikwik.service.MobikwikWalletService;
import com.swiggy.checkout.paytm.service.PaytmWalletService;
import com.swiggy.checkout.service.ValidWallet;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

/**
 * Created by Manish on 16/02/17.
 */
@Service
public class WalletServiceFactory {

    private static final int BAD_REQUEST_ERROR_CODE = 400;
    private static final String INVALID_WALLET_ERROR = "INVALID WALLET ";

    @Autowired
    PaytmWalletService paytmWalletService;

    @Autowired
    MobikwikWalletService mobikwikWalletService;

    @Autowired
    FreechargeWalletService freechargeWalletService;



    public WalletBaseService getWallet(String wallet) throws SwiggyPaymentException {

        ValidWallet validWallet = ValidWallet.getWalletName(wallet);

        if(validWallet == null){
            throw new SwiggyPaymentException(BAD_REQUEST_ERROR_CODE, INVALID_WALLET_ERROR+wallet);
        }

        switch (ValidWallet.getWalletName(wallet)) {
            case PAYTM:
            case PAYTM_SSO:
                return paytmWalletService;
            case MOBIKWIK:
            case MOBIKWIK_SSO:
                return mobikwikWalletService;
            case FREECHARGE:
            case FREECHARGE_SSO:
                return freechargeWalletService;
        }

        throw new SwiggyPaymentException(BAD_REQUEST_ERROR_CODE, INVALID_WALLET_ERROR);
    }
}
