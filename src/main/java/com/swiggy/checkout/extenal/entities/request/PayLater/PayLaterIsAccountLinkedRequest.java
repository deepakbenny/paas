package com.swiggy.checkout.extenal.entities.request.PayLater;

import com.fasterxml.jackson.annotation.JsonProperty;
import lombok.Data;

/**
 * Created by chirag.rc on 10/13/17.
 */
@Data
public class PayLaterIsAccountLinkedRequest {

    @JsonProperty("user_id")
    private long userId;

    @JsonProperty("paylater_method")
    private String paylaterMethod;
}
