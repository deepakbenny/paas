package com.swiggy.checkout.freecharge.entities.request;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import com.fasterxml.jackson.annotation.JsonInclude;
import lombok.*;

/**
 * Created by Manish on 27/01/17.
 */

@Setter
@Getter
@NoArgsConstructor
@AllArgsConstructor
@Builder
@JsonInclude(JsonInclude.Include.NON_NULL)
@JsonIgnoreProperties(ignoreUnknown = true)
public class RefundRequest {
    private String checksum;
    private String merchantId;
    private String txnId;
    private String refundMerchantTxnId;
    private String merchantTxnId;
    private String refundAmount;
    private String Comments;

}
