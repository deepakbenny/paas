package com.swiggy.checkout.data.entities;

import lombok.*;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.Table;
import java.io.Serializable;
import java.sql.Timestamp;
import java.util.List;

/**
 * Created by Manish on 29/01/17.
 */


@Entity
@Table(name = "payment_transaction")
@Getter
@Setter
@Builder
@NoArgsConstructor
@AllArgsConstructor
@EqualsAndHashCode(callSuper = false)
public class TransactionEntity implements Serializable {

    @Id @Column(name = "id")
    private long id;

    @Column(name = "order_id")
    private long orderId;

    @Column(name = "user_id")
    private long userId;

    @Column(name = "payment_method")
    private String paymentMethod;

    @Column(name = "payment_txn_status")
    private String paymentTxnStatus;

    @Column(name = "amount")
    private int amount;

    @Column(name = "payment_details")
    private String paymentDetails;

    @Column(name = "created_on")
    private Timestamp createdOn;

    @Column(name = "updated_on")
    private Timestamp updatedOn;

}
