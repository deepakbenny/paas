package com.swiggy.checkout.thirdparty;

import com.swiggy.checkout.data.entities.ThirdPartyEntity;
import com.swiggy.checkout.utils.Utility;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.context.annotation.Primary;
import org.springframework.stereotype.Service;

import javax.servlet.http.HttpServletRequest;

/**
 * Created by abhinav.chandel on 9/28/16.
 */
@Primary
@Service
public class ThirdPartyHttpServletImpl implements ThirdPartyClientService{

    public static final String THIRD_PARTY_SOURCE_TOKEN = "Client-Name";

    @Autowired
    @Qualifier(value = "thirdPartyServiceImpl")
    private ThirdPartyClientService thirdPartyClientService;

    @Override
    public ThirdPartyEntity getThirdPartyByToken() {
        HttpServletRequest request = Utility.getCurrentRequest();
        return getThirdPartyByToken(request.getHeader(THIRD_PARTY_SOURCE_TOKEN));
    }

    @Override
    public ThirdPartyEntity getThirdPartyByToken(String token) {
        return thirdPartyClientService.getThirdPartyByToken(token);
    }

    @Override
    public ThirdPartyEntity getThirdPartyBySource(String source) {
        return thirdPartyClientService.getThirdPartyBySource(source);
    }
}
