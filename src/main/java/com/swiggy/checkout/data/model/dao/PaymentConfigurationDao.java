package com.swiggy.checkout.data.model.dao;

import com.swiggy.checkout.data.entities.PaymentConfigurationEntity;
import com.swiggy.checkout.data.entities.TransactionEntity;
import com.swiggy.rest.dao.CrudDao;
import org.springframework.cache.annotation.Cacheable;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;


import java.util.List;

/**
 * Created by Manish on 23/01/17.
 */

public interface PaymentConfigurationDao extends JpaRepository<PaymentConfigurationEntity, Long> {

    @Override
    PaymentConfigurationEntity save(PaymentConfigurationEntity paymentConfigurationEntity);

    @Cacheable(value = "PaymentsConfiguration", cacheManager = "redisCacheManagerPayments")
    List<PaymentConfigurationEntity> findAll();

    @Cacheable(value = "PaymentsConfiguration", cacheManager = "redisCacheManagerPayments")
    List<PaymentConfigurationEntity> findById(long id);

    @Cacheable(value = "PaymentsConfiguration", cacheManager = "redisCacheManagerPayments")
    PaymentConfigurationEntity findOneByKey(String key);
}
