package com.swiggy.checkout.wallet;

import com.swiggy.checkout.exception.SwiggyPaymentException;
import com.swiggy.checkout.extenal.entities.request.*;
import com.swiggy.checkout.extenal.entities.response.BaseResponse;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

/**
 * Created by Manish on 16/02/17.
 */

@Service
@Slf4j
public class WalletService {

    @Autowired
    WalletServiceFactory walletServiceFactory;

    private WalletBaseService walletBaseService;

    /**
     * Depending upon wallet method, consult respective wallet service
     */
    public BaseResponse getWalletBalance(String wallet) throws SwiggyPaymentException {

        walletBaseService = walletServiceFactory.getWallet(wallet);
        return walletBaseService.checkBalance();
    }

    public BaseResponse getOtpForLinkingWallet(String wallet) throws SwiggyPaymentException {

        walletBaseService = walletServiceFactory.getWallet(wallet);
        return walletBaseService.generateOTP();
    }

    public BaseResponse validateOtpForLinkingWallet(OtpValidateRequest otpValidateReq) throws SwiggyPaymentException {

        walletBaseService = walletServiceFactory.getWallet(otpValidateReq.getWallet());
        return walletBaseService.generateToken(otpValidateReq.getOtp(), otpValidateReq.getState());
    }


    public BaseResponse withdrawAmount(WalletWithdrawAmountRequest withdrawAmountRequest) throws SwiggyPaymentException {
        walletBaseService = walletServiceFactory.getWallet(withdrawAmountRequest.getWallet());
        return walletBaseService.debitAmount(withdrawAmountRequest.getAmount(), withdrawAmountRequest.getOrderId(), "");
    }

    public BaseResponse delink(WalletDelinkRequest walletDelinkRequest) throws SwiggyPaymentException {

        walletBaseService = walletServiceFactory.getWallet(walletDelinkRequest.getWallet());
        return walletBaseService.delink();
    }

    public BaseResponse getSSOToken(String wallet) throws SwiggyPaymentException {

        walletBaseService = walletServiceFactory.getWallet(wallet);
        return walletBaseService.getSSOToken();
    }

    public BaseResponse resendOTP(WalletResendOTPRequest walletResendOTPRequest) throws SwiggyPaymentException {

        walletBaseService = walletServiceFactory.getWallet(walletResendOTPRequest.getWallet());
        return walletBaseService.resendOTP(walletResendOTPRequest);
    }

    public BaseResponse generateRegisterUserToken(WalletRegisterNewUserRequest walletRegisterNewUserRequest) throws SwiggyPaymentException {

        walletBaseService = walletServiceFactory.getWallet(walletRegisterNewUserRequest.getWallet());
        return walletBaseService.generateRegisterUserToken(walletRegisterNewUserRequest);
    }

    public BaseResponse generateLoginToken(String wallet) throws SwiggyPaymentException {

        walletBaseService = walletServiceFactory.getWallet(wallet);
        return walletBaseService.generateLoginToken();
    }

    public BaseResponse computeChecksum(WalletCalculateChecksumRequest walletCalculateChecksumRequest) throws SwiggyPaymentException {

        walletBaseService = walletServiceFactory.getWallet(walletCalculateChecksumRequest.getWallet());
        return walletBaseService.computeChecksum(walletCalculateChecksumRequest);
    }
}
