package com.swiggy.checkout.binbasedrouting.binupdatelistener;

import com.swiggy.checkout.email.EmailService;
import lombok.extern.slf4j.Slf4j;
import net.logstash.logback.encoder.org.apache.commons.lang.exception.ExceptionUtils;
import org.springframework.amqp.AmqpException;
import org.springframework.amqp.rabbit.core.RabbitTemplate;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.scheduling.annotation.Async;
import org.springframework.stereotype.Component;

@Component
@Slf4j
public class BinDetailUpdateSender {

    @Autowired
    @Qualifier("binBasedRoutingUpdateRabbitTemplate")
    private RabbitTemplate binBasedRoutingRabbitTemplate;

    @Value("${email_context}")
    private String emailContext;

    @Value("${email_onErrorEmailId}")
    private String emailOnError;

    @Autowired
    private EmailService emailService;

    @Async
    public void sendBinToQueueAsync(long bin) {
        try {
            binBasedRoutingRabbitTemplate.convertAndSend(String.valueOf(bin));
        } catch (Exception e) {
            log.error("Error while sending from rabbit mq", e);
            emailService.sendEmail(emailOnError, String.format("[%s][%s][%s][%s]", emailContext, "DS-API", "Error while sending bin to rabbit_mq", bin)
                    , ExceptionUtils.getStackTrace(e));
        }
        log.info("[ROUTING SERVICE] bin {} send to queue for update", bin);
    }
}
