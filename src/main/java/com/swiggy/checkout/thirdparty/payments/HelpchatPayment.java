package com.swiggy.checkout.thirdparty.payments;


import com.swiggy.checkout.data.entities.TransactionPaymentDetails;
import com.swiggy.checkout.thirdparty.client.exchange.*;
import com.swiggy.checkout.thirdparty.client.services.ThirdPartyPayment;
import com.swiggy.checkout.thirdparty.client.services.ThirdPartyPaymentService;
import com.swiggy.checkout.utils.Utility;
import com.swiggy.commons.Json;
import org.apache.commons.lang3.NotImplementedException;
import org.springframework.stereotype.Component;

import java.util.Map;

/**
 * Created by abhinav.chandel on 9/17/16.
 */
@Component(value="helpchat")
public class HelpchatPayment implements ThirdPartyPayment {
    private static final String SUCCESS_WITHDRAW = "success";
    private static final String SUCCESS_REFUND = "success";
    public String createWithdrawRequest(TransactionPaymentDetails paymentDetails, double thirdPartyClientCash) {
        throw new NotImplementedException("");
    }
    public Map<String, String> createRefundRequest(TransactionPaymentDetails paymentDetails, double thirdPartyClientCash) {
        HelpchatRefundRequest helpchat = new HelpchatRefundRequest();
        helpchat.setOrderId(paymentDetails.getThirdPartyMetaData().getThirdPartyOrderId());
        helpchat.setEvent("REFUND");
        Map<String, String> m = (Map<String, String>) Json.deserialize(Utility.jsonEncode2(helpchat), Object.class);
        return m;
    }
    public boolean processWithdrawResponse(String response) {
        throw new NotImplementedException("");
    }
    public boolean processRefundResponse(String response) {
        HelpchatRefundResponse helpchat = (HelpchatRefundResponse) Utility.jsonDecode(response, HelpchatRefundResponse.class);

        return helpchat.getStatus().stream().filter(status -> {
            return status.equalsIgnoreCase("REFUND_INITIATED");
        }).findAny().isPresent();
    }
    public Map<String, String> createVerifyCashbackRequest(TransactionPaymentDetails paymentDetails) {
        HelpchatVerifyRequest helpchat = new HelpchatVerifyRequest();
        helpchat.setOrderId(paymentDetails.getThirdPartyMetaData().getThirdPartyOrderId());
        Map<String, String> m = (Map<String, String>) Json.deserialize(Utility.jsonEncode2(helpchat), Object.class);
        return m;
    }
    public double processVerifyCashbackResponse(String response) {
        HelpchatVerifyResponse helpchat = (HelpchatVerifyResponse) Utility.jsonDecode(response, HelpchatVerifyResponse.class);
        return helpchat.getHelpchatCashApplied();
    }

    @Override
    public Map<String, String> createVerifyTransactionRequest(TransactionPaymentDetails paymentDetails) {
        HelpchatVerifyRequest helpchat = new HelpchatVerifyRequest();
        helpchat.setOrderId(paymentDetails.getThirdPartyMetaData().getThirdPartyOrderId());
        Map<String, String> m = (Map<String, String>) Json.deserialize(Utility.jsonEncode2(helpchat), Object.class);
        return m;
    }

    @Override
    public double processVerifyTransactionResponse(String response) {
        HelpchatVerifyResponse helpchat = (HelpchatVerifyResponse) Utility.jsonDecode(response, HelpchatVerifyResponse.class);
        return helpchat.getPaymentCollected();
    }

    @Override
    public Map<String, String> createEditRequest(TransactionPaymentDetails paymentDetails) {
        HelpchatEditNotifyRequest helpchat = new HelpchatEditNotifyRequest();
        helpchat.setOrderId(paymentDetails.getThirdPartyMetaData().getThirdPartyOrderId());
        helpchat.setEvent("EDIT");
        Map<String, String> m = (Map<String, String>) Json.deserialize(Utility.jsonEncode2(helpchat), Object.class);
        return m;
    }

    @Override
    public boolean processEditResponse(String response) {
        HelpchatEditResponse helpchat = (HelpchatEditResponse) Utility.jsonDecode(response, HelpchatEditResponse.class);
        return helpchat.getStatus().stream().filter(status -> status.equalsIgnoreCase("REFUND_INITIATED")).findAny().isPresent();
    }

}
