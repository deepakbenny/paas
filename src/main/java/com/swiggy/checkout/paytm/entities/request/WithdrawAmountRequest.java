package com.swiggy.checkout.paytm.entities.request;

import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.annotation.JsonProperty;
import lombok.*;

/**
 * Created by Manish on 29/01/17.
 */

@NoArgsConstructor
@AllArgsConstructor
@Getter
@Setter
@Builder
@JsonInclude(JsonInclude.Include.NON_NULL)
public class WithdrawAmountRequest {

    @JsonProperty("AppIP")
    private String appIp;

    @JsonProperty("AuthMode")
    private String authMode;

    @JsonProperty("Channel")
    private String channel;

    @JsonProperty("Currency")
    private String currency;

    @JsonProperty("IndustryType")
    private String industryType;

    @JsonProperty("MID")
    private String mid;

    @JsonProperty("CustId")
    private String customerId;

    @JsonProperty("DeviceId")
    private String deviceId;

    @JsonProperty("OrderId")
    private String orderId;

    @JsonProperty("PaymentMode")
    private String paymentMode;

    @JsonProperty("ReqType")
    private String reqType;

    @JsonProperty("TxnAmount")
    private String txnAmount;

    @JsonProperty("PREAUTH_ID")
    private String preAuthId;

    @JsonProperty("SSOToken")
    private String ssOToken;

    @JsonProperty("CheckSum")
    private String checkSum;

    @JsonProperty("PROMO_CAMP_ID")
    private String promoCampId;

}
