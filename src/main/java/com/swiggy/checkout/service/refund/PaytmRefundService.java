package com.swiggy.checkout.service.refund;

import com.swiggy.checkout.exception.SwiggyPaymentException;
import com.swiggy.checkout.freecharge.entities.responses.RefundPaasResponse;
import com.swiggy.checkout.paytm.entities.request.PaytmRefundRequest;
import com.swiggy.checkout.paytm.service.PaytmWalletService;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

/**
 * Created by Geetesh on 6/27/17.
 */
@Service
@Slf4j
public class PaytmRefundService implements RefundBaseService {

    @Autowired
    private PaytmWalletService paytmWalletService;


    @Override
    public RefundPaasResponse refund(RefundPaasRequest request) throws SwiggyPaymentException {
        PaytmRefundRequest refundRequest = new PaytmRefundRequest(request.getOrderId(), request.getTxnId(), request.getAmount());
        return paytmWalletService.refund(refundRequest, request.getPgCall()!=null?Boolean.parseBoolean(request.getPgCall()):false);
    }
}
