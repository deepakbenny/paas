package com.swiggy.checkout.extenal.entities.request;

import com.fasterxml.jackson.annotation.JsonProperty;
import lombok.Data;
import lombok.EqualsAndHashCode;

/**
 * Created by chirag.rc on 12/19/17.
 */
@Data
@EqualsAndHashCode(callSuper = false)
public class WalletRegisterNewUserRequest {

    @JsonProperty("wallet")
    private String wallet;

    @JsonProperty("authCode")
    private String authCode;
}
