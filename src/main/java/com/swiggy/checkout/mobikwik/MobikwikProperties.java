package com.swiggy.checkout.mobikwik;

import lombok.Getter;
import lombok.Setter;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.context.annotation.Configuration;

/**
 * Created by Manish on 29/01/17.
 */
@Configuration
@Getter
@Setter
public class MobikwikProperties {

    @Value("${mobikwik.authUrl: https://walletapi.mobikwik.com}")
    private String authUrl;

    @Value("${mobikwik.mid}")
    private String mid;

}
