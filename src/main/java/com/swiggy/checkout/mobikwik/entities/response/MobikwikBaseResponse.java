package com.swiggy.checkout.mobikwik.entities.response;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import com.fasterxml.jackson.annotation.JsonInclude;
import lombok.*;

/**
 * Created by Manish on 29/01/17.
 */

@NoArgsConstructor
@AllArgsConstructor
@Getter
@Setter
@ToString
@JsonIgnoreProperties(ignoreUnknown = true)
@JsonInclude(JsonInclude.Include.NON_NULL)
public class MobikwikBaseResponse {
    private String status;
    private String messagecode;
    private String statuscode;
    private String statusdescription;
    private String balanceamount;
    private String orderid;
    private String refId;
}
