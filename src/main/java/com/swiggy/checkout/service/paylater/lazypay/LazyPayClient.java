package com.swiggy.checkout.service.paylater.lazypay;

import com.swiggy.checkout.extenal.entities.request.PayLater.LazyPay.*;
import com.swiggy.checkout.extenal.entities.response.PayLater.LazyPay.*;
import com.swiggy.checkout.jackson.JacksonConverterFactory;
import com.swiggy.checkout.utils.HTTPLogger;
import com.swiggy.checkout.utils.Utility;
import lombok.extern.slf4j.Slf4j;
import okhttp3.Interceptor;
import okhttp3.OkHttpClient;
import okhttp3.Request;
import okhttp3.logging.HttpLoggingInterceptor;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import retrofit2.Call;
import retrofit2.Response;
import retrofit2.Retrofit;

import javax.annotation.PostConstruct;
import java.io.IOException;
import java.util.List;
import java.util.concurrent.TimeUnit;

/**
 * Created by chirag.rc on 10/13/17.
 */
@Slf4j
@Service
public class LazyPayClient {

    private LazyPayApi lazyPayApi;

    @Autowired
    private LazyPayHelper lazyPayHelper;

    @PostConstruct
    public void postConstruct() {

        OkHttpClient httpClient = new OkHttpClient.Builder()
                .addNetworkInterceptor(new Interceptor() {
                    @Override
                    public okhttp3.Response intercept(Chain chain) throws IOException {
                        Request request = chain
                                .request()
                                .newBuilder()
                                .addHeader("Content-Type", "application/json")
                                .build();
                        return chain.proceed(request);
                    }
                })
//                .addInterceptor(new LoggingInterceptor())
                .addInterceptor(new HttpLoggingInterceptor(HTTPLogger.DEFAULT).setLevel(HttpLoggingInterceptor.Level.BODY))
                .connectTimeout(1000, TimeUnit.MILLISECONDS) // 1 second
                .readTimeout(5000, TimeUnit.MILLISECONDS) // 5 seconds
                .build();

        Retrofit retrofit = new Retrofit.Builder()
                .baseUrl(lazyPayHelper.getBaseUrl())
                .addConverterFactory(JacksonConverterFactory.create())
                .client(httpClient)
                .build();

        lazyPayApi = retrofit.create(LazyPayApi.class);
    }

    LazyPayEligibilityResponse checkEligibility(String signature, String accessKey, LazyPayEligibilityRequest lazyPayEligibilityRequest) {
        try {
            Call<LazyPayEligibilityResponse> call = lazyPayApi.isEligible(signature, accessKey, lazyPayEligibilityRequest);
            Response<LazyPayEligibilityResponse> execute = call.execute();
            if (execute.isSuccessful()) {
                return execute.body();
            }
            String errorResponse = execute.errorBody().string();
            log.error("[LAZYPAY_CLIENT] Got Erroneous response while checking eligibility for request:" +
                            " signature -> {}, LazyPayEligibilityRequest -> {}, Error response -> {}",
                    signature, Utility.jsonEncode2(lazyPayEligibilityRequest), errorResponse);
            return Utility.jsonDecode2(errorResponse, LazyPayEligibilityResponse.class);
        } catch (IOException e) {
            log.error("[LAZYPAY_CLIENT] Error while calling lazyPay for checking eligibility. Error -> {}", e.getMessage());
        }
        return null;
    }


    LazyPayGetOtpResponse getOtpForLinking(String signature, String accessKey, LazyPayGetOtpRequest lazyPayGetOtpRequest) {
        try {
            Call<LazyPayGetOtpResponse> call = lazyPayApi.getOtp(signature, accessKey, lazyPayGetOtpRequest);
            Response<LazyPayGetOtpResponse> execute = call.execute();
            if (execute.isSuccessful()) {
                return execute.body();
            }
            String errorResponse = execute.errorBody().string();
            log.error("[LAZYPAY_CLIENT] Got Erroneous response while requesting Otp for request: signature -> {}, LazyPayGetOtpRequest -> {}, Error response -> {}", signature, lazyPayGetOtpRequest, errorResponse);
            return Utility.jsonDecode2(errorResponse, LazyPayGetOtpResponse.class);
        } catch (IOException e) {
            log.error("[LAZYPAY_CLIENT] Error while calling lazyPay for getting Otp. Error -> {}", e.getMessage());
        }
        return null;
    }


    LazyPayTransactionResponse validate(String signature, LazyPayValidateOtpRequest lazyPayValidateOtpRequest) {
        try {
            Call<LazyPayTransactionResponse> call = lazyPayApi.validateOtp(signature, lazyPayValidateOtpRequest);
            Response<LazyPayTransactionResponse> execute = call.execute();
            if (execute.isSuccessful()) {
                return execute.body();
            }
            String errorResponse = execute.errorBody().string();
            log.error("[LAZYPAY_CLIENT] Got Erroneous response while validating Otp for request: signature -> {}, LazyPayGetOtpRequest -> {}, Error Response -> {}", signature, Utility.jsonEncode2(lazyPayValidateOtpRequest), errorResponse);
            return Utility.jsonDecode2(errorResponse, LazyPayTransactionResponse.class);
        } catch (IOException e) {
            log.error("[LAZYPAY_CLIENT] Error while calling lazyPay for validating Otp. Error -> {}", e.getMessage());
        }
        return null;
    }


    LazyPayTransactionResponse debitAmount(String signature, String token, LazyPayAutoDebitRequest lazyPayAutoDebitRequest) {
        try {
            Call<LazyPayTransactionResponse> call = lazyPayApi.debitAmount(signature, token, lazyPayAutoDebitRequest);
            Response<LazyPayTransactionResponse> execute = call.execute();
            if (execute.isSuccessful()) {
                return execute.body();
            }
            String errorResponse = execute.errorBody().string();
            log.error("[LAZYPAY_CLIENT] Got Erroneous response while auto debiting amount for request: signature -> {}, token -> {}, lazyPayAutoDebitRequest -> {}, Error Response -> {}", signature, token, Utility.jsonEncode2(lazyPayAutoDebitRequest), errorResponse);
            return Utility.jsonDecode2(errorResponse, LazyPayTransactionResponse.class);
        } catch (IOException e) {
            log.error("[LAZYPAY_CLIENT] Error while calling lazyPay for Auto-Debiting amount. Error -> {}", e.getMessage());
        }
        return null;
    }

    LazyPayRefundResponse refund(String signature, String accessKey, LazyPayRefundRequest lazyPayRefundRequest) {
        Call<LazyPayRefundResponse> call = lazyPayApi.refund(signature, accessKey, lazyPayRefundRequest);
        try {
            Response<LazyPayRefundResponse> execute = call.execute();
            if (execute.isSuccessful()) {
                return execute.body();
            }
            String errorResponse = execute.errorBody().string();
            log.error("[LAZYPAY_CLIENT] Got Erroneous response while refunding amount for request: signature -> {}, lazyPayRefundRequest -> {}, Error Response -> {}", signature, Utility.jsonEncode2(lazyPayRefundRequest), errorResponse);
            return Utility.jsonDecode2(errorResponse, LazyPayRefundResponse.class);
        } catch (IOException e) {
            log.error("[LAZYPAY_CLIENT] Error while calling lazyPay for refunding amount. Error -> {}", e.getMessage());
        }
        return null;
    }

    LazyPayResendOtpResponse regenerateOtp(String signature, LazyPayResendOtpRequest lazyPayResendOtpRequest) {
        Call<LazyPayResendOtpResponse> call = lazyPayApi.regenerateOtp(signature, lazyPayResendOtpRequest);
        try {
            Response<LazyPayResendOtpResponse> execute = call.execute();
            if (execute.isSuccessful()) {
                return execute.body();
            }
            String errorResponse = execute.errorBody().string();
            log.error("[LAZYPAY_CLIENT] Got Erroneous response while regenerating otp for request: signature -> {}, lazyPayResendOtpRequest -> {}, Error Response -> {}", signature, Utility.jsonEncode2(lazyPayResendOtpRequest), errorResponse);
            return Utility.jsonDecode2(errorResponse, LazyPayResendOtpResponse.class);
        } catch (IOException e) {
            log.error("[LAZYPAY_CLIENT] Error while calling lazyPay for regenerating otp. Error -> {}", e.getMessage());
        }
        return null;
    }

    List<LazyPayTxnEnquiryResponse> enquireTransaction(String signature, String accessKey, LazyPayTxnEnquiryRequest lazyPayTxnEnquiryRequest) {
        Call<List<LazyPayTxnEnquiryResponse>> call = lazyPayApi.enquireTransaction(signature, accessKey, lazyPayTxnEnquiryRequest.getMerchantTxnId(), lazyPayTxnEnquiryRequest.getIsSale());
        try {
            Response<List<LazyPayTxnEnquiryResponse>> execute = call.execute();
            if (execute.isSuccessful()) {
                return execute.body();
            }
            String errorResponse = execute.errorBody().string();
            log.error("[LAZYPAY_CLIENT] Got Erroneous response while enquiring transaction for request: signature -> {}, lazyPayTxnEnquiryResponse -> {}, ErrorResponse -> {}", signature, Utility.jsonEncode2(lazyPayTxnEnquiryRequest), errorResponse);
        } catch (IOException e) {
            log.error("[LAZYPAY_CLIENT] Error while calling lazyPay for transaction enquiry. Error -> {}", e.getMessage());
        }
        return null;
    }
}
