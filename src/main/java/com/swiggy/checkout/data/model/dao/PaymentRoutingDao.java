package com.swiggy.checkout.data.model.dao;

import com.swiggy.checkout.data.entities.PaymentRoutingEntity;
import org.springframework.cache.annotation.Cacheable;
import org.springframework.data.jpa.repository.JpaRepository;

import java.util.List;

/**
 * Created by Manish on 03/02/17.
 */
public interface PaymentRoutingDao extends JpaRepository<PaymentRoutingEntity, Integer> {

    @Override
    PaymentRoutingEntity save(PaymentRoutingEntity paymentRoutingEntity);

    @Cacheable(value = "PaymentRouting", cacheManager = "redisCacheManagerPayments")
    List<PaymentRoutingEntity> findByBankAndPaymentTypeAndEnabledTrue(String bank, String paymentType);

    @Cacheable(value = "PaymentRouting", cacheManager = "redisCacheManagerPayments")
    List<PaymentRoutingEntity> findByCardTypeAndPaymentTypeAndBankAndEnabledTrue(String cardType, String paymentType, String bank);

    @Cacheable(value = "PaymentRouting", cacheManager = "redisCacheManagerPayments")
    List<PaymentRoutingEntity> findByCardTypeAndPaymentTypeAndBankAndPaymentGateway(String cardType, String paymentType, String bank, String paymentGateway);

}
