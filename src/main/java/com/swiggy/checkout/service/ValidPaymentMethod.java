package com.swiggy.checkout.service;

import com.swiggy.checkout.data.entities.TransactionPaymentDetails;
import com.swiggy.checkout.pojo.PaymentStatusResponse;
import com.swiggy.checkout.service.status.TransactionStatus;

/**
 * Created by Manish on 29/01/17.
 */

public enum ValidPaymentMethod {

    CASH("Cash", false),
    CARD("Card", false),
    PAYU("PayU", false),
    PAYUMONEY("PayUMoney", false),
    JUSPAY("Juspay", false),
    JUSPAY_NB("Juspay-NB", false),
    PAYTM("PayTM", false),
    PAYTM_SSO("PayTM-SSO", true),
    MOBIKWIK("Mobikwik", false),
    MOBIKWIK_SSO("Mobikwik-SSO", true),
    FREECHARGE("Freecharge", false),
    FREECHARGE_SSO("Freecharge-SSO", true),
    THIRD_PARTY_CASH("third-party-cash", false),
    THIRD_PARTY_ONLINE("third-party-online", false),
    LAZYPAY("PayLater_Lazypay", false),
    WALLET("Wallet", true),
    SODEXO("Sodexo", false);

    private String paymentMethod;
    private boolean isWallet;

    private TransactionStatus transactionStatus;

    private ValidPaymentMethod(String paymentMethod, boolean isWallet) {
        this.paymentMethod = paymentMethod;
        this.isWallet = isWallet;
    }

    public String getPaymentMethod() {
        return paymentMethod;
    }

    public void registerStatusHandler(TransactionStatus transactionStatus) {
        this.transactionStatus = transactionStatus;
    }

    public boolean verifyPaymentStatus(TransactionPaymentDetails paymentDetails, PaymentStatusResponse paymentStatusResponse) {
        if (transactionStatus == null) return true; // In case of Cash and Third party cash
        return transactionStatus.verifyPaymentStatus(paymentDetails, paymentStatusResponse);
    }

    public String toString() {
        return paymentMethod;
    }

    public static ValidPaymentMethod getPaymentMethod(String paymentMethod) {
        for (ValidPaymentMethod method : ValidPaymentMethod.values()) {
            if (method.getPaymentMethod().equalsIgnoreCase(paymentMethod)) return method;
        }
        return null;
    }

    public static boolean isPaymentMethodValid(String paymentMethod) {

        for (ValidPaymentMethod method : ValidPaymentMethod.values()) {
            if (method.getPaymentMethod().equalsIgnoreCase(paymentMethod)) return true;
        }

        return false;
    }

    public static boolean isWallet(String paymentMethod) {
        for (ValidPaymentMethod method : ValidPaymentMethod.values()) {
            if (method.getPaymentMethod().equalsIgnoreCase(paymentMethod) && method.isWallet) return true;
        }
        return false;
    }

}
