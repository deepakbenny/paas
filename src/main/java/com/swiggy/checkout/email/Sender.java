package com.swiggy.checkout.email;

import lombok.extern.slf4j.Slf4j;
import org.springframework.stereotype.Component;

import java.util.Map;
import java.util.Properties;

import javax.activation.DataHandler;
import javax.activation.DataSource;
import javax.activation.FileDataSource;
import javax.mail.*;
import javax.mail.internet.InternetAddress;
import javax.mail.internet.MimeBodyPart;
import javax.mail.internet.MimeMessage;
import javax.mail.internet.MimeMultipart;

/**
 * Created by Manish on 06/02/17.
 */
@Slf4j
@Component
public class Sender {

    private static Properties gmailProp ;
    public Sender(){
        gmailProp = getGmailProperties();
    }

    private static Properties getGmailProperties(){
        Properties props = new Properties();
        props.put("mail.smtp.host", "smtp.gmail.com");
        props.put("mail.smtp.socketFactory.port", "465");
        props.put("mail.smtp.socketFactory.class",
                "javax.net.ssl.SSLSocketFactory");
        props.put("mail.smtp.auth", "true");
        props.put("mail.smtp.port", "465");
        return props;
    }

    public void sendGMail(String to,String from,String subject,String body,String username,String password) throws Exception {
        Session session = Session.getDefaultInstance(gmailProp,
                new javax.mail.Authenticator() {
                    protected PasswordAuthentication getPasswordAuthentication() {
                        return new PasswordAuthentication(username,password);
                    }
                });
        Message message = new MimeMessage(session);
        message.setFrom(new InternetAddress(from));
        message.setRecipients(Message.RecipientType.TO,
                InternetAddress.parse(to));
        message.setSubject(subject);
        message.setText(body);
        Transport.send(message);
    }


    public void sendGMailWithAttachments(String to,String from,String subject,String body,String username,String password,Map<String,String> attachFiles) throws Exception {
        Session session = Session.getDefaultInstance(gmailProp,
                new javax.mail.Authenticator() {
                    protected PasswordAuthentication getPasswordAuthentication() {
                        return new PasswordAuthentication(username,password);
                    }
                });
        Message message = new MimeMessage(session);
        message.setFrom(new InternetAddress(from));
        message.setRecipients(Message.RecipientType.TO,
                InternetAddress.parse(to));
        message.setSubject(subject);

        // Create the message part
        BodyPart messageBodyPart = new MimeBodyPart();
        messageBodyPart.setContent(body, "text/html");

        // Create a multipart message
        Multipart multipart = new MimeMultipart();
        multipart.addBodyPart(messageBodyPart);

        if(attachFiles != null && !attachFiles.isEmpty()){
            for (Map.Entry<String, String> attachFile : attachFiles.entrySet()) {
                MimeBodyPart attachPart = new MimeBodyPart();
                attachPart.attachFile(attachFile.getValue());
                attachPart.setFileName(attachFile.getKey());
                multipart.addBodyPart(attachPart);
            }
        }
        message.setContent(multipart);
        Transport.send(message);
    }

    public void sendHtmlContentMail(String to, String from, String subject, String content, String username, String password) throws Exception{
        Session session = Session.getDefaultInstance(gmailProp,
                new javax.mail.Authenticator() {
                    protected PasswordAuthentication getPasswordAuthentication() {
                        return new PasswordAuthentication(username,password);
                    }
                });
        Message message = new MimeMessage(session);
        message.setFrom(new InternetAddress(from));
        message.setRecipients(Message.RecipientType.TO,
                InternetAddress.parse(to));
        message.setSubject(subject);
        message.setContent(content,"text/html");
        Transport.send(message);
    }
}
