package com.swiggy.checkout.exception;

import lombok.Getter;

/**
 * Created by Manish on 23/01/17.
 */

@Getter
public enum  ThirdPartyError {

    AUTH_ERROR(101, "Auth key generation failed."),
    SESSION_NOT_FOUND(420, "Session expired.");

    private int code;
    private String message;

    private ThirdPartyError(int code, String message) {
        this.code = code;
        this.message = message;
    }


}
