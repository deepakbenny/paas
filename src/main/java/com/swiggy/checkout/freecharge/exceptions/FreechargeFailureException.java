package com.swiggy.checkout.freecharge.exceptions;

import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
/**
 * Created by Manish on 27/01/17.
 */

@Getter
@Setter
@NoArgsConstructor
@AllArgsConstructor
public class FreechargeFailureException extends Exception {
    private int code;
    private String message;
}
