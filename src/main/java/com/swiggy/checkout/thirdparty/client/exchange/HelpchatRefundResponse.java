package com.swiggy.checkout.thirdparty.client.exchange;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import lombok.Getter;
import lombok.Setter;

import java.util.List;

/**
 * Created by abhinav.chandel on 9/19/16.
 */
@Getter
@Setter
@JsonIgnoreProperties(ignoreUnknown = true)
public class HelpchatRefundResponse {
    private String message;
    private List<String> status;
}
