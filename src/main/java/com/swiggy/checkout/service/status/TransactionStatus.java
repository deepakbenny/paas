package com.swiggy.checkout.service.status;

import com.swiggy.checkout.data.entities.TransactionPaymentDetails;
import com.swiggy.checkout.pojo.PaymentStatusResponse;

/**
 * Created by Manish on 30/01/17.
 */

public interface TransactionStatus {

    boolean verifyPaymentStatus(TransactionPaymentDetails paymentDetails, PaymentStatusResponse paymentStatusResponse);
}
