package com.swiggy.checkout.freecharge;

import com.fc.co.util.CheckSumUtil;
import com.fc.co.util.JsonUtils;
import com.swiggy.checkout.exception.SwiggyPaymentException;
import lombok.extern.slf4j.Slf4j;
import org.apache.commons.lang3.StringUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import javax.crypto.Cipher;
import javax.crypto.spec.SecretKeySpec;
import java.security.Key;
import java.util.HashMap;
import java.util.Iterator;
import java.util.Map;

/**
 * Created by Manish on 05/02/17.
 */
@Service
@Slf4j
public class FreechargeHelper {

    @Autowired
    private FreechargeProperties freechargeProperties;

    public String createChecksum(Object request) throws SwiggyPaymentException {

        HashMap requestMap = null;
        HashMap checksumMap = null;

        try {
            requestMap = JsonUtils.getObjectToMap(request);
            checksumMap = new HashMap(requestMap);
            checksumMap = this.removeNullAndEmptyFromMap(checksumMap);
            String checksum = CheckSumUtil.generateChecksum(checksumMap, freechargeProperties.getSecretKey());
            return checksum;
        } catch (Exception var7) {
            log.error("[FREECHARGE CHECKSUM]There was an error converting object to hashmap " + var7.getMessage());
            throw new SwiggyPaymentException(1, "Failed to generate checksum");
        }
    }

    public String generateLoginToken(String accessToken) throws SwiggyPaymentException {
        try {
            Cipher cipher = Cipher.getInstance("AES");
            String merchantKey = freechargeProperties.getSecretKey();
            String st = StringUtils.substring(merchantKey, 0, 16);
            Key secretKey = new SecretKeySpec(st.getBytes(), "AES");
            cipher.init(Cipher.ENCRYPT_MODE, secretKey);
            byte[] encryptedByte = cipher.doFinal(accessToken.getBytes());
            // convert the byte to hex format method 1
            StringBuffer sb = new StringBuffer();
            for (int i = 0; i < encryptedByte.length; i++) {
                sb.append(Integer.toString((encryptedByte[i] & 0xff) + 0x100, 16)
                        .substring(1));
            }
            return sb.toString();
        } catch (Exception e) {
            log.error("[FREECHARGE LOGINTOKEN] exception {}", e);
            throw new SwiggyPaymentException(1, "Not able to generate login token");
        }
    }

    private HashMap<String, Object> removeNullAndEmptyFromMap(HashMap<String, Object> map) {
        if(null == map) {
            log.error("map is null");
            return map;
        } else {
            Iterator it = map.entrySet().iterator();

            while(it.hasNext()) {
                Map.Entry e = (Map.Entry)it.next();
                String key = (String)e.getKey();
                Object value = e.getValue();
                if(value == null) {
                    it.remove();
                } else if(value instanceof String && ((String)value).isEmpty()) {
                    it.remove();
                }
            }

            return map;
        }
    }
}
