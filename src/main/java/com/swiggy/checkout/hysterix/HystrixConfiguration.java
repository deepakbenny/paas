package com.swiggy.checkout.hysterix;

import com.netflix.config.ConfigurationManager;
import com.netflix.hystrix.strategy.HystrixPlugins;
import com.soundcloud.prometheus.hystrix.HystrixPrometheusMetricsPublisher;
import io.prometheus.client.CollectorRegistry;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.context.annotation.Configuration;

import javax.annotation.PostConstruct;

@Configuration
@Slf4j
public class HystrixConfiguration {

    @Value("${hystrix.default.concurrent}")
    private int defaultConcurrent;

    //checkout hystrix configuration
    //checkout common config
    @Value("${hystrix.checkout.common.executionStrategy:thread}")
    private String checkoutStrategy;

    @Value("${hystrix.checkout.common.timeoutEnabled:true}")
    private boolean checkoutTimeoutEnabled;

    @Value("${hystrix.checkout.common.timeout:5000}")
    private int checkoutTimeout;

    @Value("${hystrix.checkout.common.interruptOnTimeout:true}")
    private boolean checkoutInterruptOnTimeout;

    @Value("${hystrix.checkout.common.fallbackEnabled:true}")
    private boolean checkoutFallbackEnabled;

    @Value("${hystrix.checkout.common.circuitBreakerEnabled:true}")
    private boolean checkoutCircuitBreakerEnabled;

    @Value("${hystrix.checkout.common.rollingMetricsTime:120000}")
    private int checkoutRollingTime;

    @Value("${hystrix.checkout.common.rollingBuckets:1200}")
    private int checkoutRollingBuckets;

    @Value("${hystrix.checkout.common.sleepWindow:10000}")
    private int checkoutSleepWindow;

    @Value("${hystrix.checkout.common.errorThresholdPercentage:50}")
    private int checkoutErrorThreshold;

    //juspayGetCards config
    @Value("${hystrix.checkout.juspayGetCards.requestVolumeThreshold:40}")
    private int juspayGetCardsRequestThreshold;

    //juspayCreateOrder config
    @Value("${hystrix.checkout.juspayCreateOrder.requestVolumeThreshold:160}")
    private int juspayCreateOrderRequestThreshold;

    //payu config
    @Value("${hystrix.checkout.payu.requestVolumeThreshold:140}")
    private int payuRequestThreshold;

    //paytm config
    @Value("${hystrix.checkout.paytm.requestVolumeThreshold:30}")
    private int paytmRequestThreshold;

    //paytm config
    @Value("${hystrix.checkout.razorpay.requestVolumeThreshold:30}")
    private int razorPayRequestThreshold;

    //order - place/confirm config
    @Value("${hystrix.checkout.order.timeout:10000}")
    private int orderTimeout;

    @Value("${hystrix.checkout.order.circuitBreakerEnabled:false}")
    private boolean orderCircuitBreakerEnabled;

    @PostConstruct
    public void init() {

        HystrixPlugins.getInstance().registerConcurrencyStrategy(new CheckoutConcurrencyStrategy());

        ConfigurationManager.getConfigInstance()
                .setProperty("hystrix.command.default.execution.isolation.strategy", "SEMAPHORE");
        ConfigurationManager.getConfigInstance()
                .setProperty("hystrix.command.default.execution.isolation.semaphore.maxConcurrentRequests", defaultConcurrent);


        //juspayGetCards configuration
        ConfigurationManager.getConfigInstance().setProperty("hystrix.command.juspayGetCards.execution.isolation.strategy", checkoutStrategy);
        ConfigurationManager.getConfigInstance().setProperty("hystrix.command.juspayGetCards.execution.timeout.enabled", checkoutTimeoutEnabled);
        ConfigurationManager.getConfigInstance().setProperty("hystrix.command.juspayGetCards.execution.isolation.thread.timeoutInMilliseconds", checkoutTimeout);
        ConfigurationManager.getConfigInstance().setProperty("hystrix.command.juspayGetCards.execution.isolation.thread.interruptOnTimeout", checkoutInterruptOnTimeout);
        ConfigurationManager.getConfigInstance().setProperty("hystrix.command.juspayGetCards.fallback.enabled", checkoutFallbackEnabled);
        ConfigurationManager.getConfigInstance().setProperty("hystrix.command.juspayGetCards.circuitBreaker.enabled", checkoutCircuitBreakerEnabled);
        ConfigurationManager.getConfigInstance().setProperty("hystrix.command.juspayGetCards.metrics.rollingStats.timeInMilliseconds", checkoutRollingTime);
        ConfigurationManager.getConfigInstance().setProperty("hystrix.command.juspayGetCards.metrics.rollingStats.numBuckets", checkoutRollingBuckets);
        ConfigurationManager.getConfigInstance().setProperty("hystrix.command.juspayGetCards.circuitBreaker.requestVolumeThreshold", juspayGetCardsRequestThreshold);
        ConfigurationManager.getConfigInstance().setProperty("hystrix.command.juspayGetCards.circuitBreaker.sleepWindowInMilliseconds", checkoutSleepWindow);
        ConfigurationManager.getConfigInstance().setProperty("hystrix.command.juspayGetCards.circuitBreaker.errorThresholdPercentage", checkoutErrorThreshold);

        //juspayCreateOrder configuration
        ConfigurationManager.getConfigInstance().setProperty("hystrix.command.juspayCreateOrder.execution.isolation.strategy", checkoutStrategy);
        ConfigurationManager.getConfigInstance().setProperty("hystrix.command.juspayCreateOrder.execution.timeout.enabled", checkoutTimeoutEnabled);
        ConfigurationManager.getConfigInstance().setProperty("hystrix.command.juspayCreateOrder.execution.isolation.thread.timeoutInMilliseconds", checkoutTimeout);
        ConfigurationManager.getConfigInstance().setProperty("hystrix.command.juspayCreateOrder.execution.isolation.thread.interruptOnTimeout", checkoutInterruptOnTimeout);
        ConfigurationManager.getConfigInstance().setProperty("hystrix.command.juspayCreateOrder.fallback.enabled", checkoutFallbackEnabled);
        ConfigurationManager.getConfigInstance().setProperty("hystrix.command.juspayCreateOrder.circuitBreaker.enabled", checkoutCircuitBreakerEnabled);
        ConfigurationManager.getConfigInstance().setProperty("hystrix.command.juspayCreateOrder.metrics.rollingStats.timeInMilliseconds", checkoutRollingTime);
        ConfigurationManager.getConfigInstance().setProperty("hystrix.command.juspayCreateOrder.metrics.rollingStats.numBuckets", checkoutRollingBuckets);
        ConfigurationManager.getConfigInstance().setProperty("hystrix.command.juspayCreateOrder.circuitBreaker.requestVolumeThreshold", juspayCreateOrderRequestThreshold);
        ConfigurationManager.getConfigInstance().setProperty("hystrix.command.juspayCreateOrder.circuitBreaker.sleepWindowInMilliseconds", checkoutSleepWindow);
        ConfigurationManager.getConfigInstance().setProperty("hystrix.command.juspayCreateOrder.circuitBreaker.errorThresholdPercentage", checkoutErrorThreshold);

        //payu configuration
        ConfigurationManager.getConfigInstance().setProperty("hystrix.command.payu.execution.isolation.strategy", checkoutStrategy);
        ConfigurationManager.getConfigInstance().setProperty("hystrix.command.payu.execution.timeout.enabled", checkoutTimeoutEnabled);
        ConfigurationManager.getConfigInstance().setProperty("hystrix.command.payu.execution.isolation.thread.timeoutInMilliseconds", checkoutTimeout);
        ConfigurationManager.getConfigInstance().setProperty("hystrix.command.payu.execution.isolation.thread.interruptOnTimeout", checkoutInterruptOnTimeout);
        ConfigurationManager.getConfigInstance().setProperty("hystrix.command.payu.fallback.enabled", checkoutFallbackEnabled);
        ConfigurationManager.getConfigInstance().setProperty("hystrix.command.payu.circuitBreaker.enabled", checkoutCircuitBreakerEnabled);
        ConfigurationManager.getConfigInstance().setProperty("hystrix.command.payu.metrics.rollingStats.timeInMilliseconds", checkoutRollingTime);
        ConfigurationManager.getConfigInstance().setProperty("hystrix.command.payu.metrics.rollingStats.numBuckets", checkoutRollingBuckets);
        ConfigurationManager.getConfigInstance().setProperty("hystrix.command.payu.circuitBreaker.requestVolumeThreshold", payuRequestThreshold);
        ConfigurationManager.getConfigInstance().setProperty("hystrix.command.payu.circuitBreaker.sleepWindowInMilliseconds", checkoutSleepWindow);
        ConfigurationManager.getConfigInstance().setProperty("hystrix.command.payu.circuitBreaker.errorThresholdPercentage", checkoutErrorThreshold);

        //paytm configuration
        ConfigurationManager.getConfigInstance().setProperty("hystrix.command.paytm.execution.isolation.strategy", checkoutStrategy);
        ConfigurationManager.getConfigInstance().setProperty("hystrix.command.paytm.execution.timeout.enabled", checkoutTimeoutEnabled);
        ConfigurationManager.getConfigInstance().setProperty("hystrix.command.paytm.execution.isolation.thread.timeoutInMilliseconds", checkoutTimeout);
        ConfigurationManager.getConfigInstance().setProperty("hystrix.command.paytm.execution.isolation.thread.interruptOnTimeout", checkoutInterruptOnTimeout);
        ConfigurationManager.getConfigInstance().setProperty("hystrix.command.paytm.fallback.enabled", checkoutFallbackEnabled);
        ConfigurationManager.getConfigInstance().setProperty("hystrix.command.paytm.circuitBreaker.enabled", checkoutCircuitBreakerEnabled);
        ConfigurationManager.getConfigInstance().setProperty("hystrix.command.paytm.metrics.rollingStats.timeInMilliseconds", checkoutRollingTime);
        ConfigurationManager.getConfigInstance().setProperty("hystrix.command.paytm.metrics.rollingStats.numBuckets", checkoutRollingBuckets);
        ConfigurationManager.getConfigInstance().setProperty("hystrix.command.paytm.circuitBreaker.requestVolumeThreshold", paytmRequestThreshold);
        ConfigurationManager.getConfigInstance().setProperty("hystrix.command.paytm.circuitBreaker.sleepWindowInMilliseconds", checkoutSleepWindow);
        ConfigurationManager.getConfigInstance().setProperty("hystrix.command.paytm.circuitBreaker.errorThresholdPercentage", checkoutErrorThreshold);

        //razorpay configuration
        ConfigurationManager.getConfigInstance().setProperty("hystrix.command.razorpay.execution.isolation.strategy", checkoutStrategy);
        ConfigurationManager.getConfigInstance().setProperty("hystrix.command.razorpay.execution.timeout.enabled", checkoutTimeoutEnabled);
        ConfigurationManager.getConfigInstance().setProperty("hystrix.command.razorpay.execution.isolation.thread.timeoutInMilliseconds", checkoutTimeout);
        ConfigurationManager.getConfigInstance().setProperty("hystrix.command.razorpay.execution.isolation.thread.interruptOnTimeout", checkoutInterruptOnTimeout);
        ConfigurationManager.getConfigInstance().setProperty("hystrix.command.razorpay.fallback.enabled", checkoutFallbackEnabled);
        ConfigurationManager.getConfigInstance().setProperty("hystrix.command.razorpay.circuitBreaker.enabled", checkoutCircuitBreakerEnabled);
        ConfigurationManager.getConfigInstance().setProperty("hystrix.command.razorpay.metrics.rollingStats.timeInMilliseconds", checkoutRollingTime);
        ConfigurationManager.getConfigInstance().setProperty("hystrix.command.razorpay.metrics.rollingStats.numBuckets", checkoutRollingBuckets);
        ConfigurationManager.getConfigInstance().setProperty("hystrix.command.razorpay.circuitBreaker.requestVolumeThreshold", razorPayRequestThreshold);
        ConfigurationManager.getConfigInstance().setProperty("hystrix.command.razorpay.circuitBreaker.sleepWindowInMilliseconds", checkoutSleepWindow);
        ConfigurationManager.getConfigInstance().setProperty("hystrix.command.razorpay.circuitBreaker.errorThresholdPercentage", checkoutErrorThreshold);

        HystrixPrometheusMetricsPublisher.register("paas",CollectorRegistry.defaultRegistry, false, true);
    }
}
