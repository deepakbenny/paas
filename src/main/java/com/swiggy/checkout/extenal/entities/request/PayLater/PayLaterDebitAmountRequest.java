package com.swiggy.checkout.extenal.entities.request.PayLater;

import com.fasterxml.jackson.annotation.JsonProperty;
import lombok.Data;

/**
 * Created by chirag.rc on 10/13/17.
 */
@Data
public class PayLaterDebitAmountRequest {

    @JsonProperty("payment_mode")
    private String paymentMode;

    @JsonProperty("order_id")
    private String orderId;

    @JsonProperty("source")
    private String source;

    @JsonProperty("user_id")
    private long userId;

    @JsonProperty("email")
    private String email;

    @JsonProperty("mobile")
    private String mobile;

    @JsonProperty("value")
    private String value;

    @JsonProperty("currency")
    private String currency = "INR";

    @JsonProperty("paylater_method")
    private String paylaterMethod;
}
