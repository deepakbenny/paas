package com.swiggy.checkout.extenal.entities.response.PayLater.LazyPay;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.databind.annotation.JsonSerialize;
import com.swiggy.checkout.pojo.PayLater.LazyPay.LazyPayTokenDetails;
import lombok.Data;

import java.util.HashMap;
import java.util.Map;

/**
 * Created by chirag.rc on 10/24/17.
 */
@Data
@JsonIgnoreProperties(ignoreUnknown = true)
@JsonSerialize(include = JsonSerialize.Inclusion.NON_NULL)
public class LazyPayTransactionResponse extends LazyPayErrorResponse {

    @JsonProperty("status")
    private int status;

    @JsonProperty("transactionId")
    private String transactionId;

    @JsonProperty("merchantOrderId")
    private String merchantOrderId;

    @JsonProperty("amount")
    private String amount;

    @JsonProperty("currency")
    private String currency;

    @JsonProperty("signature")
    private String signature;

    @JsonProperty("responseData")
    private Map<String, Object> responseData;

    @JsonProperty("token")
    private LazyPayTokenDetails lazyPayTokenDetails;
}
