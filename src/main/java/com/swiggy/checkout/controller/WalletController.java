package com.swiggy.checkout.controller;

import com.swiggy.checkout.exception.SwiggyPaymentException;
import com.swiggy.checkout.extenal.entities.request.*;
import com.swiggy.checkout.extenal.entities.response.BaseResponse;
import com.swiggy.checkout.extenal.entities.response.ErrorResponse;
import com.swiggy.checkout.wallet.WalletService;
import io.swagger.annotations.ApiOperation;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.MediaType;
import org.springframework.web.bind.annotation.*;

/**
 * Created by Manish on 10/02/17.
 */

@RestController
@RequestMapping("/v1/wallet")
public class WalletController {

    private static final int ERROR_CODE = 100;
    private static final String ERROR_MSG = "Some error on server side for Wallet Configurations";

    @Autowired
    private WalletService walletService;

    @ApiOperation(value = "link wallet", notes = "Api to link wallet with user.")
    @RequestMapping(
            value = "/link",
            method = RequestMethod.GET,
            produces = MediaType.APPLICATION_JSON_VALUE
    )
    public BaseResponse getOtpForLinkingWallet(@RequestParam(value = "wallet") String wallet) throws SwiggyPaymentException {

        return walletService.getOtpForLinkingWallet(wallet);
    }

    @ApiOperation(value = "validate", notes = "Api to validate otp.")
    @RequestMapping(
            value = "/validate",
            method = RequestMethod.POST,
            produces = MediaType.APPLICATION_JSON_VALUE
    )
    public BaseResponse validate(@RequestBody OtpValidateRequest otpValidateRequest) throws SwiggyPaymentException {

        return walletService.validateOtpForLinkingWallet(otpValidateRequest);
    }

    @ApiOperation(value = "check balance", notes = "Api to check balance of wallet account")
    @RequestMapping(
            value = "/check-balance",
            method = RequestMethod.GET,
            produces = MediaType.APPLICATION_JSON_VALUE
    )
    public BaseResponse checkBalance(@RequestParam(value = "wallet") String wallet) throws SwiggyPaymentException {

        return walletService.getWalletBalance(wallet);
    }

    @ApiOperation(value = "withdraw", notes = "Api to debit amount from wallet account")
    @RequestMapping(
            value = "/withdraw-amount",
            method = RequestMethod.POST,
            produces = MediaType.APPLICATION_JSON_VALUE
    )
    public BaseResponse withdrawAmount(@RequestBody WalletWithdrawAmountRequest withdrawAmountRequest) {
        try {
            return walletService.withdrawAmount(withdrawAmountRequest);
        } catch (Exception e) {
            return new ErrorResponse(ERROR_CODE, ERROR_MSG);
        }
    }

    @ApiOperation(value = "delink", notes = "Api to delink user from wallet account")
    @RequestMapping(
            value = "/delink",
            method = RequestMethod.POST,
            produces = MediaType.APPLICATION_JSON_VALUE
    )
    public BaseResponse delink(@RequestBody WalletDelinkRequest walletDelinkRequest) {
        try {
            return walletService.delink(walletDelinkRequest);
        } catch (Exception e) {
            return new ErrorResponse(ERROR_CODE, ERROR_MSG);
        }
    }

    @ApiOperation(value = "getSSOToken", notes = "Api to get user's token associated with the wallet")
    @RequestMapping(
            value = "/getSSOToken",
            method = RequestMethod.GET,
            produces = MediaType.APPLICATION_JSON_VALUE
    )
    public BaseResponse getSSOToken(@RequestParam(value = "wallet") String wallet) throws SwiggyPaymentException {
        try {
            return walletService.getSSOToken(wallet);
        } catch (Exception e) {
            return new ErrorResponse(ERROR_CODE, ERROR_MSG);
        }
    }

    @ApiOperation(value = "resendOtp", notes = "Api to resend OTP to user's mobile number")
    @RequestMapping(
            value = "/resendOtp",
            method = RequestMethod.POST,
            produces = MediaType.APPLICATION_JSON_VALUE
    )
    public BaseResponse resendOTP(@RequestBody WalletResendOTPRequest walletResendOTPRequest) throws SwiggyPaymentException {
        try {
            return walletService.resendOTP(walletResendOTPRequest);
        } catch (Exception e) {
            return new ErrorResponse(ERROR_CODE, ERROR_MSG);
        }
    }

    @ApiOperation(value = "validate new user", notes = "Api to validate new user by authcode.")
    @RequestMapping(
            value = "/new_user",
            method = RequestMethod.POST,
            produces = MediaType.APPLICATION_JSON_VALUE
    )
    public BaseResponse generateRegisterUserToken(@RequestBody WalletRegisterNewUserRequest walletRegisterNewUserRequest) throws SwiggyPaymentException {
        try {
            return walletService.generateRegisterUserToken(walletRegisterNewUserRequest);
        } catch (Exception e) {
            return new ErrorResponse(ERROR_CODE, ERROR_MSG);
        }
    }

    @ApiOperation(value = "get login token", notes = "Api to get login token from user session details.")
    @RequestMapping(
            value = "/login_token",
            method = RequestMethod.POST,
            produces = MediaType.APPLICATION_JSON_VALUE
    )
    public BaseResponse generateLoginToken(@RequestParam(value = "wallet") String wallet) throws SwiggyPaymentException {
        try {
            return walletService.generateLoginToken(wallet);
        } catch (Exception e) {
            return new ErrorResponse(ERROR_CODE, ERROR_MSG);
        }
    }

    @ApiOperation(value = "calculate checksum", notes = "Api to calculate checksum.")
    @RequestMapping(
            value = "/calculate_checksum",
            method = RequestMethod.POST,
            produces = MediaType.APPLICATION_JSON_VALUE
    )
    public BaseResponse computeChecksum(@RequestBody WalletCalculateChecksumRequest walletCalculateChecksumRequest) throws SwiggyPaymentException {
        try {
            return walletService.computeChecksum(walletCalculateChecksumRequest);
        } catch (Exception e) {
            return new ErrorResponse(ERROR_CODE, ERROR_MSG);
        }
    }

}
