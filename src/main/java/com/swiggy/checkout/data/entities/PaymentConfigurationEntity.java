package com.swiggy.checkout.data.entities;



import lombok.*;

import javax.persistence.*;
import java.io.Serializable;

/**
 * Created by Manish on 23/01/17.
 */

@Entity
@Table(name = "payment_configurations")
@Getter
@Setter
@Builder
@NoArgsConstructor
@AllArgsConstructor
@EqualsAndHashCode(callSuper = false)
@Cacheable
public class PaymentConfigurationEntity implements Serializable {

    @Id
    @Column(name = "id")
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private long id;

    @Column(name = "config_key")
    private String key;

    @Column(name = "config_value")
    private String value;

}
