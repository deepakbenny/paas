package com.swiggy.checkout.mobikwik.service;

import com.swiggy.checkout.mobikwik.entities.response.CheckBalanceResponse;
import com.swiggy.checkout.mobikwik.entities.response.CheckUserExistsResponse;
import com.swiggy.checkout.mobikwik.entities.response.MobikwikBaseResponse;
import com.swiggy.checkout.mobikwik.entities.response.MobikwikTokenResponse;
import retrofit2.Call;
import retrofit2.http.POST;
import retrofit2.http.Query;

/**
 * Created by Manish on 29/01/17.
 */

interface MobikwikClient {

    @POST("querywallet")
    Call<CheckUserExistsResponse> checkUserExists(@Query("merchantname") String merchantName, @Query("mid") String mid,
                                                  @Query("cell") String cell, @Query("action") String action, @Query("msgcode") String msgCode,
                                                  @Query("checksum") String checksum);


    @POST("otpgenerate")
    Call<MobikwikBaseResponse> generateOTP(@Query("merchantname") String merchantName, @Query("mid") String mid,
                                           @Query("cell") String cell, @Query("amount") String amount, @Query("tokentype") String tokenType,
                                           @Query("msgcode") String msgCode, @Query("checksum") String checksum);

    @POST("tokengenerate")
    Call<MobikwikTokenResponse> generateToken(@Query("merchantname") String merchantName, @Query("mid") String mid,
                                              @Query("cell") String cell, @Query("amount") String amount, @Query("otp") String otp,
                                              @Query("tokentype") String tokenType, @Query("msgcode") String msgCode,
                                              @Query("checksum") String checksum);

    @POST("userbalance")
    Call<CheckBalanceResponse> checkBalance(@Query("merchantname") String merchantName, @Query("mid") String mid,
                                            @Query("cell") String cell, @Query("token") String token, @Query("msgcode") String msgCode,
                                            @Query("checksum") String checksum);


    @POST("createwalletuser")
    Call<MobikwikTokenResponse> createWallet(@Query("merchantname") String merchantName, @Query("mid") String mid,
                                             @Query("cell") String cell, @Query("email") String email, @Query("otp") String otp, @Query("msgcode") String msgCode,
                                             @Query("checksum") String checksum);


    @POST("debitwallet")
    Call<MobikwikBaseResponse> debitAmount(@Query("merchantname") String merchantName, @Query("mid") String mid,
                                           @Query("cell") String cell, @Query("token") String token, @Query("amount") String amount,
                                           @Query("comment") String comment, @Query("orderid") String orderId, @Query("txntype") String transactionType,
                                           @Query("msgcode") String msgCode, @Query("checksum") String checksum);

    @POST("tokenregenerate")
    Call<MobikwikTokenResponse> regenerateToken(@Query("merchantname") String merchantName, @Query("mid") String mid,
                                                @Query("cell") String cell, @Query("token") String token, @Query("tokentype") String tokenType,
                                                @Query("msgcode") String msgCode, @Query("checksum") String checksum);

    @POST("walletrefund")
    Call<MobikwikBaseResponse> refund(@Query("merchantname") String merchantName, @Query("mid") String mid,
                                      @Query("txid") String orderId, @Query("amount") String amount,
                                      @Query("checksum") String checksum);
}
