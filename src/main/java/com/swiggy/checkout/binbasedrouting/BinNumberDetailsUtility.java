package com.swiggy.checkout.binbasedrouting;


import com.swiggy.checkout.data.entities.BinNumberDetailsEntity;
import com.swiggy.checkout.utils.Utility;
import lombok.extern.slf4j.Slf4j;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.net.HttpURLConnection;
import java.net.URL;
import java.sql.Timestamp;
import java.time.LocalDateTime;

@Slf4j
public class BinNumberDetailsUtility {

    private static final String JUS_PAY_BIN_URL = "https://api.juspay.in/cardbins/%s";

    private static final String BIN_LIST_BIN_URL = "https://lookup.binlist.net/%s";

    //3 days as expiry for details
    private static final long BIN_DETAIL_EXPIRY_TIME = 259200000L;

    private static final String BIN_NUMBER_DETAILS_UTILITY = BinNumberDetailsUtility.class.getName();


    public static JusPayBinDetails getJusPayBinDetails(String binNumber) {
        String url = String.format(JUS_PAY_BIN_URL, binNumber);
        String response = getResponse(url);
        if (response != null) {
            try {
                return Utility.jsonDecode2(response, JusPayBinDetails.class);
            } catch (IOException e) {
                log.error(BIN_NUMBER_DETAILS_UTILITY + "  :  " + binNumber + "\n" + e.getMessage());
                return null;
            }
        }
        return null;
    }

    public static BinListBinDetails getBinListBinDetails(String binNumber) throws IOException {
        String url = String.format(BIN_LIST_BIN_URL, binNumber);
        String response = getResponse(url);
        return Utility.jsonDecode2(response, BinListBinDetails.class);
    }

    public static String getResponse(String urlString) {

        try {
            URL url = new URL(urlString);
            HttpURLConnection conn = (HttpURLConnection) url.openConnection();
            conn.setRequestMethod("GET");
            conn.setConnectTimeout(1000);
            conn.setReadTimeout(5000);
            int responseCode = conn.getResponseCode();
            // Get the response
            BufferedReader bufferedReader = new BufferedReader(new InputStreamReader(conn.getInputStream()));
            String line;
            String response = "";
            while ((line = bufferedReader.readLine()) != null) {
                response += line;
            }
            try {
                bufferedReader.close();
            } catch (Exception e) {
                log.error(BIN_NUMBER_DETAILS_UTILITY + "  :  " + urlString + "\n" + e.getMessage());
            }
            return response;
        } catch (IOException e) {
            log.error(BIN_NUMBER_DETAILS_UTILITY + "  :  " + urlString + " \n" + e.getMessage());
        }

        return null;
    }

    public static boolean isUpdateRequired(BinNumberDetailsEntity binEntity) {
        return binEntity == null || binEntity.getLastUpdatedTime() == null || isExpired(binEntity);
    }

    private static boolean isExpired(BinNumberDetailsEntity binEntity) {
        try {
            return (Timestamp.valueOf(LocalDateTime.now()).getTime() - binEntity.getLastUpdatedTime().getTime()) > BIN_DETAIL_EXPIRY_TIME;
        } catch (Exception e) {
            log.error(BIN_NUMBER_DETAILS_UTILITY + "  :  " + e.getMessage());
            return true;
        }
    }

    public static BinNumberDetailsEntity getNewBinDetails(long binId) {
        BinNumberDetailsEntity binNumberDetailsEntity = null;
        JusPayBinDetails jusPayBinDetails = getJusPayBinDetails(String.valueOf(binId));
        if (jusPayBinDetails != null) {
            binNumberDetailsEntity = new BinNumberDetailsEntity();
            binNumberDetailsEntity.setBin(binId);
            binNumberDetailsEntity.setBrand(jusPayBinDetails.getBrand());
            binNumberDetailsEntity.setBank(jusPayBinDetails.getBank());
            binNumberDetailsEntity.setType(jusPayBinDetails.getType());
            binNumberDetailsEntity.setIsocountry(jusPayBinDetails.getIsocountry());
            binNumberDetailsEntity.setLastUpdatedTime(Timestamp.valueOf(LocalDateTime.now()));
        }
        return binNumberDetailsEntity;
    }
}
