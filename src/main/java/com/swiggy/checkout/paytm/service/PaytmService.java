package com.swiggy.checkout.paytm.service;

import com.fasterxml.jackson.annotation.JsonProperty;
import com.netflix.hystrix.contrib.javanica.annotation.DefaultProperties;
import com.netflix.hystrix.contrib.javanica.annotation.HystrixCommand;
import com.newrelic.api.agent.Trace;
import com.swiggy.checkout.data.entities.TransactionPaymentDetails;
import com.swiggy.checkout.data.model.bo.PaymentConfiguration;
import com.swiggy.checkout.exception.GlobalControllerExceptionHandler;
import com.swiggy.checkout.paytm.entities.response.PaytmVerifyPaymentResponse;
import com.swiggy.checkout.pojo.PaymentStatusResponse;
import com.swiggy.checkout.service.ValidPaymentStatus;
import com.swiggy.checkout.utils.Utility;
import lombok.Getter;
import lombok.Setter;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Service;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.io.OutputStreamWriter;
import java.net.URL;
import java.net.URLConnection;
import java.time.LocalDateTime;

/**
 * Created by Manish on 30/01/17.
 */

@Service
@Slf4j
@DefaultProperties(defaultFallback = "verifyPaymentStatusFallback")
public class PaytmService {

    public static final String SUCCESS = "TXN_SUCCESS";
    public static final String FAILURE = "TXN_FAILURE";
    public static final String PENDING = "PENDING";
    public static final String PAYTM_PG_MID = "paytm_pg_mid";


    @Value("${paytm.service.url}")
    private String paytmUrl;

    @Value("${paytm.MID}")
    private String paytmMID;

    @Autowired
    private PaymentConfiguration configuration;

    @Autowired
    private GlobalControllerExceptionHandler exceptionHandler;


    /**
     * Check paytm response. If success, returns true, false otherwise
     * @param paymentDetails
     * @return
     */
    @Trace
    @HystrixCommand(commandKey = "paytm")
    public boolean verifyPaymentStatus(TransactionPaymentDetails paymentDetails, boolean pgCall){


        PaytmVerifyPaymentResponse paytmVerifyPaymentResponse = verifyPaytmTxnStatus(paymentDetails, pgCall);

        log.info("paytm response ----------- {}", Utility.jsonEncode2(paytmVerifyPaymentResponse));

        // Not a valid response, return false
        if(paytmVerifyPaymentResponse == null || Utility.isEmpty(paytmVerifyPaymentResponse.getStatus()))
            return false;

        paymentDetails.setPaytmVerifyPaymentResponse(paytmVerifyPaymentResponse);
        paymentDetails.setPgResponseTime(LocalDateTime.now());

        if(SUCCESS.equalsIgnoreCase(paytmVerifyPaymentResponse.getStatus())) {
           // paytm fraud check
            Double txmAmount = Utility.getDouble(paytmVerifyPaymentResponse.getTxnAmount());
            int txnTotal = txmAmount.intValue();
            // case : diff in order total and txnamount
            if (txnTotal != paymentDetails.getAmount()) {
                Long fraudThreshold = configuration.getConfigurationAsLong("paytm_fraud_threshold", 50);
                int diff = Math.abs(paymentDetails.getAmount() - txnTotal);
                if (diff > fraudThreshold) {
                    log.error("Order ID {} failed as paytm txn amount {} is diff. difference {} txn id {}",
                            paymentDetails.getOrderId(), txmAmount, diff, paytmVerifyPaymentResponse.getTxnId());
                    exceptionHandler.sendEmailOnError(Utility.jsonEncode2(paytmVerifyPaymentResponse),
                            "Paytm - Order amount and txn amount mismatch. Threshold breached!! Difference "+diff);
                    paymentDetails.setPaymentTxnStatus(ValidPaymentStatus.FAILURE);
                    return false;
                } else {
                    log.error("Order ID {}  paytm txn amount {} is diff. difference {} txn id {}. less than threshold, passing as success",
                            paymentDetails.getOrderId(), txmAmount, diff, paytmVerifyPaymentResponse.getTxnId());
                }
            }
            if(!pgCall)
                paymentDetails.setPaymentTxnId(paytmVerifyPaymentResponse.getTxnId());
            paymentDetails.setPaymentTxnStatus(ValidPaymentStatus.SUCCESS);
            return true;
        } else{
            paymentDetails.setPaymentTxnStatus(ValidPaymentStatus.FAILURE);
            return false;
        }
    }

    /**
     * Used by payment status check api
     * @param paymentDetails
     * @param pgCall
     * @param paymentStatusResponse
     * @return
     */
    @HystrixCommand(commandKey = "paytm")
    public boolean verifyPaymentStatus(TransactionPaymentDetails paymentDetails, boolean pgCall, PaymentStatusResponse paymentStatusResponse) {
        PaytmVerifyPaymentResponse paytmVerifyPaymentResponse = verifyPaytmTxnStatus(paymentDetails, pgCall);

        // Not a valid response, return false
        if(paytmVerifyPaymentResponse == null || Utility.isEmpty(paytmVerifyPaymentResponse.getStatus())) {
            paymentStatusResponse.setErrorMessage("Paytm is not able to verify the payment");
            return false;
        }
        paymentDetails.setPaytmVerifyPaymentResponse(paytmVerifyPaymentResponse);
        log.info("paytm pg response ----------- {}", Utility.jsonEncode2(paytmVerifyPaymentResponse));
        paymentStatusResponse.setPaymentStatus(paytmVerifyPaymentResponse.getStatus());
        paymentStatusResponse.setAmount(paytmVerifyPaymentResponse.getTxnAmount());
        paymentStatusResponse.setTxnId(paytmVerifyPaymentResponse.getTxnId());
        paymentStatusResponse.setBankRefNumber(paytmVerifyPaymentResponse.getBankTxnId());
        paymentStatusResponse.setGateway(paytmVerifyPaymentResponse.getGatewayName());
        return true;
    }

    public boolean verifyPaymentStatusFallback(){
        log.info("Reached Hystrix fallback in paytm service");
        return false;
    }


    /**
     * Calls PayTM verify payment API and converts the response to PaytmVerifyPaymentResponse and return it.
     * @param paymentDetails
     * @return
     */
    public PaytmVerifyPaymentResponse verifyPaytmTxnStatus(TransactionPaymentDetails paymentDetails, boolean pgCall) {

        String orderId = pgCall?paymentDetails.getPaymentTxnId():paymentDetails.getOrderId();
        try {
            //{"MID":"swiggy97751185767429","ORDERID":"1051209092"}

            String mid = pgCall?configuration.getConfiguration(PAYTM_PG_MID, "swiggy58513599375770"):paytmMID;

            log.info("inside PaytmVerifyPaymentResponse OrderId "+ orderId +" and MID = "+mid);

            PaytmRequestObject paytmRequestObject = new PaytmRequestObject(mid, orderId);

            URL url = new URL(paytmUrl + Utility.jsonEncode(paytmRequestObject));
            URLConnection conn = url.openConnection();
            conn.setDoOutput(true);
            conn.setConnectTimeout(1000);
            conn.setReadTimeout(5000);
            OutputStreamWriter wr = new OutputStreamWriter(conn.getOutputStream());
            wr.flush();

            // Get the response
            BufferedReader rd = new BufferedReader(new InputStreamReader(conn.getInputStream()));
            String line;
            String response = "";
            while ((line = rd.readLine()) != null) {
                response += line;
            }

            PaytmVerifyPaymentResponse paytmVerifyPaymentResponse= Utility.jsonDecode2(response, PaytmVerifyPaymentResponse.class);

            paytmVerifyPaymentResponse.getStatus();

            return paytmVerifyPaymentResponse;

        } catch (IOException e) {
            log.error("Error in verifying paytm transaction for order "+ orderId, e);
        }

        return null;
    }

    @Getter
    @Setter
    public static class PaytmRequestObject {
        @JsonProperty("MID") private String mid;
        @JsonProperty("ORDERID") private String orderId;

        public PaytmRequestObject(String paytmMID, String orderId) {
            this.mid = paytmMID;
            this.orderId = orderId;
        }
    }

}

