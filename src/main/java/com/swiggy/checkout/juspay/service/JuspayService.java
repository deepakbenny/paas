package com.swiggy.checkout.juspay.service;

import com.fasterxml.jackson.core.JsonProcessingException;
import com.netflix.hystrix.contrib.javanica.annotation.HystrixCommand;
import com.swiggy.checkout.data.entities.TransactionPaymentDetails;
import com.swiggy.checkout.data.model.bo.PaymentConfiguration;
import com.swiggy.checkout.extenal.entities.request.CreateTransactionRequest;
import com.swiggy.checkout.service.Gateway;
import com.swiggy.checkout.service.RoutingService;
import com.swiggy.checkout.service.ValidPaymentMethod;
import com.swiggy.checkout.utils.Utility;
import in.juspay.Environment;
import in.juspay.PaymentService;
import in.juspay.request.CreateOrderRequest;
import in.juspay.request.ListCardsRequest;
import in.juspay.response.CreateOrderResponse;
import in.juspay.response.ListCardsResponse;
import in.juspay.response.StoredCard;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.util.StringUtils;



import java.util.*;

/**
 * Created by Manish on 30/01/17.
 */
@Service
public class JuspayService {
    private static final String API_KEY = "C9D3705047F6460EABD66A001D9A6F72";
    private static final String MERCHANT_ID = "com.swiggy";
    private static final String SODEXO = "Sodexo";

    private PaymentService paymentService;

    @Autowired
    private PaymentConfiguration configuration;

    @Autowired
    private RoutingService routingService;

    private static final String PAYTM_GATEWAY_TRAFFIC = "paytm-gateway-traffic";
    private static final String PAYTM_NB = "paytm-nb";
    private static final String PAYTM_CARD = "paytm-card";
    private static final String RAZORPAY_GATEWAY_TRAFFIC = "razorpay-gateway-traffic";
    private static final String RAZORPAY_NB = "razorpay-nb";
    private static final String RAZORPAY_CARD = "razorpay-card";
    private static final String GATEWAY_ROUTING_ENABLED = "gateway-routing-enabled";

    private static final Logger logger = LoggerFactory.getLogger(JuspayService.class);

    public JuspayService() {
        paymentService = (new PaymentService()).withEnvironment(Environment.PRODUCTION)
                .withKey(API_KEY)
                .withMerchantId(MERCHANT_ID);
    }

    @HystrixCommand(commandKey = "juspayGetCards", fallbackMethod = "getCardsFallback")
    public Map<String, Object> getCards(long custId) throws JsonProcessingException {
        Map<String, Object> response = new HashMap<>();
        List<Map<String, Object>> cardData = new ArrayList<>();
        response.put("customer_id", String.valueOf(custId));
        response.put("merchantId", MERCHANT_ID);
        try {
            ListCardsRequest listcardsrequest = (new ListCardsRequest()).withCustomerId(String.valueOf(custId));
            ListCardsResponse listcardsresponse = paymentService.listCards(listcardsrequest);
            logger.debug("listcardsresponse {}", Utility.jsonEncode2(listcardsresponse));
            for (StoredCard s : listcardsresponse.cards) {
                cardData.add(Utility.jsonToMap(Utility.jsonEncode(s)));
            }
        } catch (Exception ex) {
            logger.error("Error while getting the Juspay cards " + ex.getMessage());
        }

        response.put("cards", cardData);
        logger.debug("cards {}", Utility.jsonEncode2(cardData));
        return response;
    }

    private Map<String, Object> getCardsFallback(long custId){
        logger.info("Reached Hystrix fallback");
        Map<String, Object> response = new HashMap<>();
        return response;
    }

    /**
     * creates order on juspay with gateway id.
     *
     * @param createTransactionRequest
     * @param returnUrl
     * @return
     */
    @HystrixCommand(commandKey = "juspayCreateOrder", fallbackMethod = "createOrderFallback")
    public boolean createOrder(CreateTransactionRequest createTransactionRequest, String returnUrl, TransactionPaymentDetails paymentDetails) {

        CreateOrderRequest createOrderRequest = createBasicOrder(createTransactionRequest.getOrderId(), Double.valueOf(createTransactionRequest.getAmount()), returnUrl);

        Gateway gateway = getTheGateway(paymentDetails);
        createOrderRequest.setGatewayId(gateway.name());
        paymentDetails.setPaymentGateway(gateway.name());
        logger.info("[JUSPAY SERVICE] Payment gateway : {}", gateway.name());
        CreateOrderResponse createOrderResponse = null;
        try {
            createOrderResponse = paymentService.createOrder(createOrderRequest);
        } catch (RuntimeException e) {
            createOrderResponse = paymentService.createOrder(createOrderRequest);
        }

        if (createOrderResponse == null || createOrderResponse.status == null) {
            logger.debug("[JUSPAY SERVICE] Payment service response is NULL.");
            return false;
        }

        logger.debug("[JUSPAY SERVICE] Payment service response: {}", createOrderResponse.status);
        // status should be "CREATED" and statusId should be 1 for successful creation of order
        // for retry they send us NEW if order is already created at their end
        return "CREATED".equalsIgnoreCase(createOrderResponse.status) || "NEW".equalsIgnoreCase(createOrderResponse.status);
    }

    private boolean createOrderFallback(CreateTransactionRequest createTransactionRequest, String returnUrl, TransactionPaymentDetails paymentDetails){
        logger.info("Reached Hystrix fallback");
        return false;
    }

    private CreateOrderRequest createBasicOrder(String orderId, double orderTotal, String returnUrl) {
        CreateOrderRequest createOrderRequest = new CreateOrderRequest()
                .withOrderId(orderId)
                .withAmount(orderTotal)
                .withCustomerId(!StringUtils.isEmpty(Utility.getUserFromHeader().getCustomerId()) ? Utility.getUserFromHeader().getCustomerId() : "")
                .withEmail(Utility.getUserFromHeader() != null &&
                        !StringUtils.isEmpty(Utility.getUserFromHeader().getEmail()) ?
                        Utility.getUserFromHeader().getEmail() : "")
                .withPhoneNumber(Utility.getUserFromHeader() != null &&
                        !StringUtils.isEmpty(Utility.getUserFromHeader().getMobile()) ?
                        Utility.getUserFromHeader().getMobile() : "")
                .withReturnUrl(returnUrl);
        createOrderRequest.setUdf6(Utility.getUserAgent());
        return createOrderRequest;
    }


    private Gateway getTheGateway(TransactionPaymentDetails paymentDetails) {
        Gateway gateway;
        if (SODEXO.equalsIgnoreCase(paymentDetails.getPaymentMethod())) {
            return Gateway.PAYU;
        }
        if (configuration.getConfigurationAsBoolean(GATEWAY_ROUTING_ENABLED, false)) {
            if (ValidPaymentMethod.JUSPAY.toString().equalsIgnoreCase(paymentDetails.getPaymentMethod())) {
                gateway = routingService.routeToGateway(Utility.getLong(paymentDetails.getBinNumber()));
            } else {
                gateway = routingService.routeToGateway(paymentDetails.getBankName());
                logger.info("[ROUTING SERVICE] for NB {} : {}", paymentDetails.getBankName(), gateway);
            }
        } else {
            gateway = decidePaymentGateway(paymentDetails.getPaymentMethod());
        }
        return gateway;
    }

    /**
     * Routing logic. How much traffic should be diverted to which payment gateway.
     * PAYTM 1
     * PAYU 2
     * @return
     */
    private Gateway decidePaymentGateway(String paymentMethod) {
        int paytmPercentage = configuration.getConfigurationAsInt(PAYTM_GATEWAY_TRAFFIC, 0);

        //for net banking enable true
        boolean netbanking = configuration.getConfigurationAsBoolean(PAYTM_NB, false);
        boolean card = configuration.getConfigurationAsBoolean(PAYTM_CARD, false);

        int milli = Calendar.getInstance().get(Calendar.MILLISECOND) % 100;
        logger.info("[JUSPAY SERVICE] time in milli%100 : {}", milli);

        boolean flag = ValidPaymentMethod.JUSPAY.toString().equalsIgnoreCase(paymentMethod) ? card
                : ValidPaymentMethod.JUSPAY_NB.toString().equalsIgnoreCase(paymentMethod) && netbanking;

        if (flag && milli < paytmPercentage) {
            return Gateway.PAYTM;
        }

        int razorPayPercentage = configuration.getConfigurationAsInt(RAZORPAY_GATEWAY_TRAFFIC, 0);

        //for net banking enable true
        boolean razorPayNetbanking = configuration.getConfigurationAsBoolean(RAZORPAY_NB, false);
        boolean razorPayCard = configuration.getConfigurationAsBoolean(RAZORPAY_CARD, false);

        boolean razorPayFlag = ValidPaymentMethod.JUSPAY.toString().equalsIgnoreCase(paymentMethod) ? razorPayCard
                : ValidPaymentMethod.JUSPAY_NB.toString().equalsIgnoreCase(paymentMethod) && razorPayNetbanking;

        if (razorPayFlag && milli < razorPayPercentage) {
            return Gateway.RAZORPAY;
        }

        return Gateway.PAYU;
    }

}


