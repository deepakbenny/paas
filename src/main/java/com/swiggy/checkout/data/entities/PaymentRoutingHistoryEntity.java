package com.swiggy.checkout.data.entities;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.Table;
import java.sql.Timestamp;

/**
 * Created by Manish on 03/02/17.
 */

@Entity
@Table(name = "payment_routing_history")
@Data
@Builder
@NoArgsConstructor
@AllArgsConstructor
public class PaymentRoutingHistoryEntity {

    @Id
    @Column(name = "id", nullable = false, insertable = true, updatable = true)
    private long id;
    @Column(name = "card_type", length = 12)
    private String cardType;
    @Column(name = "payment_type", length = 3)
    private String paymentType;
    @Column(name = "bank", length = 20)
    private String bank;
    @Column(name = "payment_gateway", length = 10)
    private String paymentGateway;
    @Column(name = "percentage", length = 3)
    private int percentage;
    @Column(name = "updated_by", length = 60)
    private String updatedBy;
    @Column(name = "from_time", columnDefinition = "NOT NULL DEFAULT '0000-00-00 00:00:00'")
    private Timestamp fromTime;
    @Column(name = "to_time", columnDefinition = "NOT NULL DEFAULT '0000-00-00 00:00:00'")
    private Timestamp toTime;

}
