package com.swiggy.checkout.paytm;

import lombok.Getter;
import lombok.Setter;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.context.annotation.Configuration;

/**
 * Created by Manish on 29/01/17.
 */

//@ConfigurationProperties(prefix = "https://secure.paytm.in/oltppaytm", ignoreUnknownFields = true)
@Configuration
@Getter
@Setter
public class PaytmProperties {

    @Value("${paytm.authUrl}")
    private String authUrl;

    @Value("${paytm.walletUrl}")
    private String walletUrl;

    @Value("${paytm.clientId}")
    private String clientId;

    @Value("${paytm.clientSecret}")
    private String clientSecret;

    @Value("${paytm.basicAuth}")
    private String basicAuth;

    @Value("${paytm.mid}")
    private String mid;

    @Value("${paytm.industryType}")
    private String industryType;

    @Value("${paytm.merchantKey}")
    private String merchantKey;

    @Value("${payTM_merchant_key}")
    private String paytmPgMerchantKey;
}

