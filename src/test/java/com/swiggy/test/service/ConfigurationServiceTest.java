

package com.swiggy.test.service;

import com.swiggy.checkout.data.entities.PaymentConfigurationEntity;
import com.swiggy.checkout.data.model.bo.PaymentConfiguration;
import com.swiggy.checkout.data.model.dao.PaymentConfigurationDao;
import com.swiggy.test.AbstractTest;
import org.junit.Assert;
import org.junit.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.transaction.annotation.Transactional;


/**
 * Created by Manish on 31/01/17.
 */

@Transactional
public class ConfigurationServiceTest extends AbstractTest{

    @Autowired
    private PaymentConfiguration configuration;

    @Autowired
    PaymentConfigurationDao paymentConfigurationDao;


    @Test
    public void ConfigurationsTest() {
        System.out.print("Testing");
        String sampleKey = "test";
        String sampleValue = "abc";
        paymentConfigurationDao.save(PaymentConfigurationEntity.builder().key(sampleKey).value(sampleValue).build());
        String value =  configuration.getConfiguration(sampleKey,"");
        System.out.print("CONFIGURATION VALUE:"+value);
        Assert.assertEquals(sampleValue,value);
    }


}


