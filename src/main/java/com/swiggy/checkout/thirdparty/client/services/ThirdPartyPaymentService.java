package com.swiggy.checkout.thirdparty.client.services;


import com.fasterxml.jackson.databind.util.JSONPObject;
import com.newrelic.api.agent.Trace;
import com.swiggy.checkout.data.entities.ThirdPartyEntity;
import com.swiggy.checkout.data.entities.TransactionPaymentDetails;
import com.swiggy.checkout.service.ValidPaymentMethod;
import com.swiggy.checkout.service.ValidPaymentStatus;
import com.swiggy.checkout.thirdparty.client.http.*;
import com.swiggy.checkout.utils.Utility;
import lombok.Getter;
import lombok.Setter;
import lombok.extern.slf4j.Slf4j;
import org.apache.commons.lang3.NotImplementedException;
import org.json.JSONObject;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import retrofit2.Response;

import java.util.LinkedHashMap;
import java.util.Map;

/**
 * Created by abhinav.chandel on 9/17/16.
 */
@Service
@Slf4j
@Getter
@Setter
public class ThirdPartyPaymentService {


    @Autowired
    private ThirdPartyFactory thirdPartyFactory;

   // @Autowired
   // private TransactionDao transactionDao;

    private static final String WITHDRAW_REASON = "withdraw from third party: ";

    private static final String REFUND_REASON = "refund from third party: ";

    private static final Logger logger = LoggerFactory.getLogger(ThirdPartyPaymentService.class);

    private static final String REFUND_PUSH_NOTIFICATION = "Rs [thirdPartyCash] has been refunded to you for Order Id #[orderId]";

    /**
     * Verify third party cash used
     * @param thirdParty
     * @param paymentDetails
     *
     * @return boolean
     */
    public boolean verifyThirdPartyCash(ThirdPartyEntity thirdParty, TransactionPaymentDetails paymentDetails) {
        try {
            ThirdPartyPayment thirdPartyPayment = thirdPartyFactory.getThirdPartyPayment(thirdParty.getSource());
            Map<String, String> requestMap = thirdPartyPayment.createVerifyCashbackRequest(paymentDetails);
            ThirdPartyVerifyHttpClient thirdPartyVerifyHttpClient = ThirdPartyConfig.getVerifyClient(thirdParty);
            Response<Object> response = thirdPartyVerifyHttpClient.verifyCashback(requestMap).execute();
            boolean verificationSuccessful = false;
            if (response.isSuccessful()) {
                LinkedHashMap<String, ?> map = (LinkedHashMap<String, ?>) response.body();
                String body = new JSONObject(map).toString();
              /*  if (Math.abs(thirdPartyPayment.processVerifyCashbackResponse(body) - orderEntity.getThirdPartyMetaData().getThirdPartyCash()) < 1) {
                    verificationSuccessful = true;
                }*/
            }
            return verificationSuccessful;
        } catch (Exception e){
            logger.error("Third party verify caused execption: {}", e);
            return false;
        }
    }

    /**
     * Verify third party transaction
     * @param thirdParty
     * @param paymentDetails
     *
     * @return boolean
     */
    @Trace
    public boolean verifyThirdPartyTransaction(ThirdPartyEntity thirdParty, TransactionPaymentDetails paymentDetails) {
        try {
            ThirdPartyPayment thirdPartyPayment = thirdPartyFactory.getThirdPartyPayment(thirdParty.getSource());
            Map<String, String> requestMap = thirdPartyPayment.createVerifyTransactionRequest(paymentDetails);
            ThirdPartyVerifyTransactionHttpClient thirdPartyVerifyTransactionHttpClient = ThirdPartyConfig.getVerifyTransactionClient(thirdParty);
            Response<Object> response = thirdPartyVerifyTransactionHttpClient.verifyTransaction(requestMap).execute();
            boolean verificationSuccessful = false;
            if (response.isSuccessful()) {
                LinkedHashMap<String, ?> map = (LinkedHashMap<String, ?>) response.body();
                String body = new JSONObject(map).toString();
                if (Math.abs(thirdPartyPayment.processVerifyTransactionResponse(body) - paymentDetails.getAmount()) < 1) {
                    verificationSuccessful = true;
                }
            }
            return verificationSuccessful;
        } catch (Exception e){
            logger.error("Third party verify caused execption: {}", e);
            return false;
        }
    }

    /**
     * Calls third party api to withdraw third party cash
     * @param thirdParty
     * @param paymentDetails
     * @param thirdPartyClientCash
     *
     * @return boolean
     */
    public boolean withdrawFromThirdParty(ThirdPartyEntity thirdParty, TransactionPaymentDetails paymentDetails, double thirdPartyClientCash) throws NotImplementedException {
        try {
            ThirdPartyPayment thirdPartyPayment = thirdPartyFactory.getThirdPartyPayment(thirdParty.getSource());
            String requestJSON = thirdPartyPayment.createWithdrawRequest(paymentDetails, thirdPartyClientCash);
            ThirdPartyWithdrawHttpClient thirdPartyWithdrawHttpClient = ThirdPartyConfig.getWithdrawClient(thirdParty);
            Response<Object> response = thirdPartyWithdrawHttpClient.withdraw(requestJSON).execute();
            boolean withdrawSuccessful = false;
            if (response.isSuccessful()) {
                LinkedHashMap<String, ?> map = (LinkedHashMap<String, ?>) response.body();
                String body = new JSONObject(map).toString();
                withdrawSuccessful = thirdPartyPayment.processWithdrawResponse(body);

            }
            if (withdrawSuccessful) {
                paymentDetails.getThirdPartyMetaData().setThirdPartyCash(thirdPartyClientCash);
                paymentDetails.getThirdPartyMetaData().setThirdPartySource(thirdParty.getSource());
                createThirdPartyTransaction(thirdParty, paymentDetails, thirdPartyClientCash, WITHDRAW_REASON+thirdParty.getSource(), "debit", null);
                return true;
            }
            else {
                logger.error("Third party withdraw failed for {}: {}",thirdParty.getSource(), response.errorBody());
                logger.error("Third party withdraw failed :{}", response.message());
                return false;
            }
        } catch (NotImplementedException e) {
            throw new NotImplementedException(e);
        } catch (Exception e){
            logger.error("Third party withdraw caused execption: {}", e);
            return false;
        }
    }

    /**
     * Calls third party api to credit third party money in case of refund
     * @param thirdParty
     * @param paymentDetails
     *
     * @return boolean
     */
    public boolean notifyEditToThirdParty(ThirdPartyEntity thirdParty, TransactionPaymentDetails paymentDetails) {
        try {
            ThirdPartyPayment thirdPartyPayment = thirdPartyFactory.getThirdPartyPayment(thirdParty.getSource());
            Map<String, String> editRequest = thirdPartyPayment.createEditRequest(paymentDetails);
            ThirdPartyEditNotifyHttpClient thirdPartyEditNotifyHttpClient = ThirdPartyConfig.getEditNotifyClient(thirdParty);
            Response<Object> response = thirdPartyEditNotifyHttpClient.editNotify(editRequest).execute();

            if (response.isSuccessful()) {
                LinkedHashMap<String, ?> map = (LinkedHashMap<String, ?>) response.body();
                String body = Utility.jsonEncode2(map);
                return thirdPartyPayment.processEditResponse(body);
            }
            else {
                logger.error("Third party edit notify failed for {}: {}",thirdParty.getSource(), response.errorBody());
                logger.error("Third party edit notify failed :{}", response.message());
                return false;
            }
        } catch (NotImplementedException e) {
            logger.info("Third party edit notify not implemented");
            return false;
        } catch (Exception e) {
            logger.error("Third party edit notify caused execption: {}", e);
            return false;
        }
    }


    /**
     * Calls third party api to credit third party money in case of refund
     * @param thirdParty
     * @param paymentDetails
     * @param thirdPartyClientCash
     *
     * @return boolean
     */
    public boolean refundMoneyToThirdParty(ThirdPartyEntity thirdParty, TransactionPaymentDetails paymentDetails, double thirdPartyClientCash) {
        try {
            ThirdPartyPayment thirdPartyPayment = thirdPartyFactory.getThirdPartyPayment(thirdParty.getSource());
            Map<String, String> refundRequest = thirdPartyPayment.createRefundRequest(paymentDetails, thirdPartyClientCash);
            ThirdPartyRefundHttpClient thirdPartyRefundHttpClient = ThirdPartyConfig.getRefundClient(thirdParty);
            Response<Object> response = thirdPartyRefundHttpClient.refund(refundRequest).execute();
            boolean refundSuccessful = false;
            if (response.isSuccessful()) {
                LinkedHashMap<String, ?> map = (LinkedHashMap<String, ?>) response.body();
                String body = new JSONObject(map).toString();
                refundSuccessful = thirdPartyPayment.processRefundResponse(body);
            }
            if (refundSuccessful) {
                return true;
            }
            else {
                logger.error("Third party refund failed for {}: {}",thirdParty.getSource(), Utility.jsonEncode2(response.errorBody()));
                logger.error("Third party refund failed :{}", response.message());
                addThirdPartyRefund(paymentDetails, thirdPartyClientCash);
                return false;
            }
        } catch (NotImplementedException e) {
            logger.info("Third party refund not implemented");
            addThirdPartyRefund(paymentDetails, thirdPartyClientCash);
            return false;
        } catch (Exception e) {
            logger.error("Third party refund caused execption: {}", e);
            addThirdPartyRefund(paymentDetails, thirdPartyClientCash);
            return false;
        }
    }

    private void addThirdPartyRefund(TransactionPaymentDetails paymentDetails, double thirdPartyClientCash) {
        if (ValidPaymentMethod.THIRD_PARTY_CASH.toString().equals(paymentDetails.getPaymentMethod())) {
            paymentDetails.getThirdPartyMetaData().setThirdPartyCashToRefund(thirdPartyClientCash);
            paymentDetails.setPaymentTxnStatus(ValidPaymentStatus.REFUND_INITIATED);
        }
    }

    private void createThirdPartyTransaction(ThirdPartyEntity thirdPartyEntity, TransactionPaymentDetails paymentDetails, double thirdPartyClientCash, String reason, String txnType, String pushNotificationText) {
        long userId = Long.valueOf(Utility.getUserFromHeader().getCustomerId());
        String orderId = paymentDetails.getOrderId();
        String createdBy = "third-party";
        if (pushNotificationText != null) {
            pushNotificationText = pushNotificationText.replace("[thirdPartyCash]", String.valueOf(thirdPartyClientCash));
            pushNotificationText = pushNotificationText.replace("[orderId]", orderId);
        }
       // transactionDao.addTransaction(userId, orderId, reason, txnType, createdBy, thirdPartyClientCash, pushNotificationText);
    }
}
