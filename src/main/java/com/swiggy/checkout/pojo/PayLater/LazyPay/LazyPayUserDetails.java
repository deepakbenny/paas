package com.swiggy.checkout.pojo.PayLater.LazyPay;

import com.fasterxml.jackson.annotation.JsonProperty;
import lombok.Builder;
import lombok.Data;

/**
 * Created by chirag.rc on 10/23/17.
 */
@Data
public class LazyPayUserDetails {
    @JsonProperty("email")
    private String email;
    @JsonProperty("mobile")
    private String mobile;

    @Builder
    public LazyPayUserDetails(String email, String mobile) {
        this.email = email;
        this.mobile = mobile;
    }
}
