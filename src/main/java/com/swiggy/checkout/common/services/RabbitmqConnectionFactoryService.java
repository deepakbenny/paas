package com.swiggy.checkout.common.services;

import com.rabbitmq.client.ConnectionFactory;
import com.swiggy.checkout.email.EmailService;
import org.apache.commons.lang.exception.ExceptionUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Service;

import java.io.IOException;

@Service
public class RabbitmqConnectionFactoryService {

    @Value("${rabbitmq.host}")
    private String rabbitmqHost;

    @Value("${rabbitmq.port}")
    private Integer rabbitmqPort;

    @Value("${rabbitmq.username}")
    private String rabbitmqUser;

    @Value("${rabbitmq.password}")
    private String rabbitmqPassword;

    @Value("${email_context}")
    private String emailContext;

    @Value("${email_onErrorEmailId}")
    private String emailOnError;

    @Autowired
    EmailService emailService;

    public ConnectionFactory getConnectionFactory() throws IOException {
        ConnectionFactory factory = null;
        try {
            factory = new ConnectionFactory();
            factory.setHost(rabbitmqHost);
            factory.setPort(rabbitmqPort);
            factory.setUsername(rabbitmqUser);
            factory.setPassword(rabbitmqPassword);
            return factory;
        } catch (Exception ex) {
            emailService.sendEmail(emailOnError,
                    String.format("[%s][%s][%s][%s]", emailContext,
                            "Rabbitmq Connection Factory", "Exception occurred", ex.getMessage()), ExceptionUtils.getStackTrace(ex));
        }
        return factory;
    }
}
