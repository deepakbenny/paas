package com.swiggy.checkout.exception;

import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

/**
 * Created by Manish on 29/01/17.
 */

@Getter
@Setter
@AllArgsConstructor
@NoArgsConstructor
public class MobikwikEmailRegisteredException extends Exception {

    private int code;
    private String message;
}
