package com.swiggy.checkout.service.paylater.lazypay;

import com.swiggy.checkout.extenal.entities.request.PayLater.LazyPay.*;
import com.swiggy.checkout.extenal.entities.response.PayLater.LazyPay.*;
import retrofit2.Call;
import retrofit2.http.*;

import java.util.List;

/**
 * Created by chirag.rc on 10/23/17.
 */
public interface LazyPayApi {

    @POST("/api/lazypay/v0/payment/eligibility")
    Call<LazyPayEligibilityResponse> isEligible(@Header("signature") String signature,
                                                @Header("accessKey") String accessKey,
                                                @Body LazyPayEligibilityRequest lazyPayEligibilityRequest);

    @POST("/api/lazypay/v0/payment/initiate")
    Call<LazyPayGetOtpResponse> getOtp(@Header("signature") String signature,
                                       @Header("accessKey") String accessKey,
                                       @Body LazyPayGetOtpRequest lazyPayGetOtpRequest);

    @POST("/api/lazypay/v0/payment/pay")
    Call<LazyPayTransactionResponse> validateOtp(@Header("signature") String signature,
                                                 @Body LazyPayValidateOtpRequest lazyPayValidateOtpRequest);

    @POST("/api/lazypay/v0/payment/pay")
    Call<LazyPayTransactionResponse> debitAmount(@Header("signature") String signature,
                                                 @Header("authorization") String token,
                                                 @Body LazyPayAutoDebitRequest lazyPayAutoDebitRequest);

    @POST("/api/lazypay/v0/refund")
    Call<LazyPayRefundResponse> refund(@Header("signature") String signature,
                                       @Header("accessKey") String accessKey,
                                       @Body LazyPayRefundRequest lazyPayRefundRequest);

    @POST("/api/lazypay/v0/resendOtp")
    Call<LazyPayResendOtpResponse> regenerateOtp(@Header("signature") String signature,
                                                 @Body LazyPayResendOtpRequest lazyPayResendOtpRequest);

    @GET("/api/lazypay/v0/enquiry")
    Call<List<LazyPayTxnEnquiryResponse>> enquireTransaction(@Header("signature") String signature,
                                                             @Header("accessKey") String accessKey,
                                                             @Query("merchantTxnId") String merchantTxnId,
                                                             @Query("isSale") Boolean isSale);
}
