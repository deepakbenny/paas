package com.swiggy.checkout.data.model.dao;

import com.swiggy.checkout.data.entities.UserTokenDetailsEntity;
import org.springframework.cache.annotation.CacheEvict;
import org.springframework.cache.annotation.CachePut;
import org.springframework.cache.annotation.Cacheable;
import org.springframework.data.jpa.repository.JpaRepository;

import java.util.List;

/**
 * Created by chirag.rc on 10/30/17.
 */
public interface UserTokenDetailsDao extends JpaRepository<UserTokenDetailsEntity, Long> {

    @Cacheable(value = "UserTokens", cacheManager = "redisCacheManagerPayments", key = "{#p0,#p1}")
    UserTokenDetailsEntity findByUserIdAndPaymentMethodName(long userId, String paymentMethod);

    @Override
    @CachePut(value = "UserTokens", cacheManager = "redisCacheManagerPayments", key = "{#p0.userId, #p0.paymentMethodName}")
    UserTokenDetailsEntity save(UserTokenDetailsEntity userTokenDetailsEntity);

    @Override
    @CacheEvict(value = "UserTokens", cacheManager = "redisCacheManagerPayments", key = "{#p0.userId, #p0.paymentMethodName}")
    void delete(UserTokenDetailsEntity userTokenDetailsEntity);
}
