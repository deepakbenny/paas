package com.swiggy.checkout.razorpay;

import com.netflix.hystrix.contrib.javanica.annotation.DefaultProperties;
import com.netflix.hystrix.contrib.javanica.annotation.HystrixCommand;
import com.newrelic.api.agent.Trace;
import com.razorpay.*;
import com.swiggy.checkout.data.entities.TransactionPaymentDetails;
import com.swiggy.checkout.data.model.bo.PaymentConfiguration;
import com.swiggy.checkout.pojo.PaymentResponseDecisionConstant;
import com.swiggy.checkout.pojo.PaymentStatusResponse;
import com.swiggy.checkout.razorpay.pojo.RazorPayOrderResponse;
import com.swiggy.checkout.razorpay.pojo.RazorRefundResponse;
import com.swiggy.checkout.service.ValidPaymentStatus;
import com.swiggy.checkout.service.ValidRefundMethod;
import com.swiggy.checkout.utils.Utility;
import lombok.extern.slf4j.Slf4j;
import org.apache.commons.lang3.StringUtils;
import org.json.JSONObject;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import javax.annotation.PostConstruct;
import java.time.LocalDateTime;
import java.util.HashMap;
import java.util.List;

/**
 * Created by Manish on 30/01/17.
 */
@Service
@Slf4j
@DefaultProperties(defaultFallback = "verifyPaymentStatusFallback")
public class RazorPayService {

    public static final String RECEIPT_KEY = "receipt";
    public static final String STATUS_KEY = "status";
    public static final String AMOUNT_KEY = "amount";
    public static final String ID_KEY = "id";
    public static final String AMOUNT_PAID_KEY = "amount_paid";
    public static final String PAYMENT_ID_KEY = "payment_id";
    public static final String ENTITY_KEY = "entity";
    public static final String REFUND_ENTITY_TYPE = "refund";
    private RazorpayClient razorpayClient;

    @Autowired
    private PaymentConfiguration configuration;

    // Initialize client
    @PostConstruct
    public void init() throws RazorpayException {
        try {
            razorpayClient = new RazorpayClient("rzp_live_MPePRFInkpE6kn", "7oYWv1hc3cSNhci6MdPgWeMA");
        } catch (RazorpayException e) {
            razorpayClient = new RazorpayClient("rzp_live_MPePRFInkpE6kn", "7oYWv1hc3cSNhci6MdPgWeMA");
        }
    }

    @Trace
    @HystrixCommand(commandKey = "razorpay")
    public boolean verifyPaymentStatus(TransactionPaymentDetails paymentDetails){
        List<Order> orders;
        try {
            JSONObject orderRequest = new JSONObject();
            String paymentTxnId = paymentDetails.getPaymentTxnId();
            // supported option filters (from, to, count, skip)
            orderRequest.put(RECEIPT_KEY, paymentTxnId);
            orders = razorpayClient.Orders.fetchAll(orderRequest);
        } catch (RazorpayException e) {
            log.error("[RAZOR PAY SERVICE] verify payment request failed.", e);
            return false;
        }
        // Not a valid response, return false
        if (orders == null || orders.isEmpty()) {
            log.info("[RAZOR SERVICE] verify payment response is empty.");
            paymentDetails.setPaymentTxnStatus(ValidPaymentStatus.FAILURE);
            paymentDetails.setPgResponseTime(LocalDateTime.now());
            return false;
        }
        // should have only one payment
        Order order = orders.get(0);
        RazorPayOrderResponse razorPayOrderResponse = new RazorPayOrderResponse(order.get(STATUS_KEY),
                order.get(AMOUNT_KEY), order.get(ID_KEY), order.get(AMOUNT_PAID_KEY));

        log.info("[RAZOR SERVICE] payment paymentStatus : {}", order.toString());
        paymentDetails.setRazorPayPaymentDetails(razorPayOrderResponse);
        String paymentStatus = order.get(STATUS_KEY);
        if (ValidPaymentStatus.FAILURE.equalsIgnoreCase(paymentStatus))
            paymentDetails.setPaymentTxnStatus(ValidPaymentStatus.FAILURE);
        else if (ValidPaymentStatus.PAID.equalsIgnoreCase(paymentStatus)) {
            paymentDetails.setPaymentTxnStatus(ValidPaymentStatus.SUCCESS);
            // response is in paise
            int txnTotal = new Double(razorPayOrderResponse.getAmount()/100).intValue();
            if (txnTotal != paymentDetails.getAmount()) {
                Long fraudThreshold = configuration.getConfigurationAsLong("paytm_fraud_threshold", 50);
                int diff = Math.abs(paymentDetails.getAmount() - txnTotal);
                if (diff > fraudThreshold) {
                    log.error("Order ID {} failed as paytm txn amount {} is diff. difference {} txn id {}",
                            paymentDetails.getOrderId(), txnTotal, diff, razorPayOrderResponse.getId());
                    paymentDetails.setPaymentTxnStatus(ValidPaymentStatus.FAILURE);
                    return false;
                } else {
                    log.error("Order ID {}  paytm txn amount {} is diff. difference {} txn id {}. less than threshold, passing as success",
                            paymentDetails.getOrderId(), txnTotal, diff, razorPayOrderResponse.getId());
                }
            }
        }
        else
            paymentDetails.setPaymentTxnStatus(ValidPaymentStatus.PENDING);
        paymentDetails.setPgResponseTime(LocalDateTime.now());
        // Status can be created, authorized, captured, refunded, failed
        log.debug("[RAZOR SERVICE] payment paymentStatus : {}", (Object) paymentStatus);
        return ValidPaymentStatus.PAID.equalsIgnoreCase(paymentStatus);
    }

    @HystrixCommand(commandKey = "razorpay")
    public boolean verifyPaymentStatus(TransactionPaymentDetails paymentDetails, PaymentStatusResponse paymentStatusResponse) {
        List<Order> orders;
        try {
            JSONObject orderRequest = new JSONObject();
            String paymentTxnId = paymentDetails.getPaymentTxnId();
            // supported option filters (from, to, count, skip)
            orderRequest.put(RECEIPT_KEY, paymentTxnId);
            orders = razorpayClient.Orders.fetchAll(orderRequest);
        } catch (RazorpayException e) {
            log.debug("[RAZOR PAY SERVICE] verify payment request failed.", e);
            paymentStatusResponse.setErrorMessage("RazorPay is not able to verify the payment");
            paymentStatusResponse.setRefundable(PaymentResponseDecisionConstant.RETRY);
            return false;
        }
        // Not a valid response, return false
        if (orders == null || orders.isEmpty()) {
            log.debug("[RAZOR SERVICE] verify payment response is empty.");
            paymentStatusResponse.setErrorMessage("RazorPay is not able to verify the payment");
            paymentStatusResponse.setRefundable(PaymentResponseDecisionConstant.RETRY);
            return false;
        }
        // should have only one payment
        Order order = orders.get(0);
        log.debug("RazorPay response ----------- {}", Utility.jsonEncode2(order));
        String status = order.get(STATUS_KEY);
        paymentStatusResponse.setPaymentStatus(status);
        paymentStatusResponse.setAmount(order.get(AMOUNT_KEY).toString());
        paymentStatusResponse.setTxnId(order.get(ID_KEY));
        // TODO if razorpay provides the same
        //paymentStatusResponse.setBankRefNumber(payUTransactionDetails.getBankRefNumber());
        //paymentStatusResponse.setMihPayId(payUTransactionDetails.getMihPayId());
        if (ValidPaymentStatus.PAID.equalsIgnoreCase(status)) {
            paymentStatusResponse.setRefundable(PaymentResponseDecisionConstant.REFUND);
        } else if ("failure".equalsIgnoreCase(status)) {
            paymentStatusResponse.setRefundable(PaymentResponseDecisionConstant.DISCARD);
        } else {
            paymentStatusResponse.setRefundable(PaymentResponseDecisionConstant.RETRY); // queued or pending or requested
        }
        return true;
    }

    public RazorRefundResponse refundToPG(String pgTxnId, String amount) {
        RazorRefundResponse refundResponse = executeRefund(pgTxnId, amount);
        if (refundResponse != null && REFUND_ENTITY_TYPE.equalsIgnoreCase(refundResponse.getEntity())) {
            log.info("Refund in razorpay for order id {} response {} ", pgTxnId, Utility.jsonEncode2(refundResponse));
            return refundResponse;
        }
        return null;
    }

    private RazorRefundResponse executeRefund(String pgTxnId, String amount) {
        // Partial Refund
        List<Payment> payments;
        try {
            payments = razorpayClient.Orders.fetchPayments(pgTxnId);
        } catch (RazorpayException e) {
            log.error("Error in razorpay refund", e);
            return null;
        }
        if (payments == null || payments.isEmpty()) {
            log.error("Empty response, no payments found for order {}", pgTxnId);
            return null;
        }
        Payment payment = payments.get(0);
        String paymentId = payment.get(ID_KEY);
        if (StringUtils.isEmpty(paymentId)) {
            log.error("Empty payment id for order {}", pgTxnId);
            return null;
        }
        JSONObject refundRequest = new JSONObject();
        refundRequest.put(AMOUNT_KEY, Utility.getDouble(amount) * 100); // Amount should be in paise
        try {
            Refund refund = razorpayClient.Payments.refund(paymentId, refundRequest);
            return getRazorRefundResponse(refund);
        } catch (RazorpayException e) {
            log.error("Error in razorpay refund", e);
            return null;
        }
    }

    private RazorRefundResponse getRazorRefundResponse(Refund refund) {
        RazorRefundResponse refundResponse = new RazorRefundResponse();
        refundResponse.setAmount(refund.get(AMOUNT_KEY));
        refundResponse.setRefundId(refund.get(ID_KEY));
        refundResponse.setPaymentId(refund.get(PAYMENT_ID_KEY));
        refundResponse.setEntity(refund.get(ENTITY_KEY));
        return refundResponse;
    }
}