package com.swiggy.checkout.paytm.entities.response;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.annotation.JsonProperty;
import lombok.*;

/**
 * Created by Manish on 29/01/17.
 */

@NoArgsConstructor
@AllArgsConstructor
@Getter
@Setter
@Builder
@JsonIgnoreProperties(ignoreUnknown = true)
@JsonInclude(JsonInclude.Include.NON_NULL)
public class PayURefundResponse {

    @JsonProperty("status")
    private String status;

    @JsonProperty("msg")
    private String msg;

    @JsonProperty("request_id")
    private String request_id;

    @JsonProperty("bank_ref_num")
    private String bankRefNum;

    @JsonProperty("mihpayid")
    private String mihpayid;

    @JsonProperty("error_code")
    private String errorCode;

    public String toString(){
        return "status: '" + status + "', msg: '" + msg + "', request_id: '" + request_id + "', bank_ref_num: '" + bankRefNum + "', mihpayid: '" + mihpayid + "', error_code: '" + errorCode + "'";
    }

}
