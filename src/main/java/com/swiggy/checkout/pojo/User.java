package com.swiggy.checkout.pojo;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.databind.annotation.JsonSerialize;
import com.fasterxml.jackson.databind.ser.std.ToStringSerializer;
import lombok.*;

/**
 * Created by Manish on 29/01/17.
 */

@Getter
@Setter
@JsonIgnoreProperties(ignoreUnknown = true)
@Data
@Builder
@NoArgsConstructor
@AllArgsConstructor
public class User {

    @JsonSerialize(using=ToStringSerializer.class)
    @JsonProperty("customer_id")
    private String customerId;

    @JsonProperty("email")
    private String email;

    @JsonProperty("mobile")
    private String mobile;

    @JsonProperty("paytm_sso_token")
    private String paytmSsoToken;

    @JsonProperty("mobikwik_sso_token")
    private String mobikwikSsoToken;

    @JsonProperty("freecharge_sso_token")
    private String freechargeSsoToken;

    @JsonProperty("customer_ip")
    private String customerIp;

    @JsonProperty("coupon")
    private String coupon;
}
