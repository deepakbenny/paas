package com.swiggy.checkout.service.paylater.lazypay;

import lombok.Getter;
import lombok.Setter;
import lombok.extern.slf4j.Slf4j;
import org.apache.commons.codec.binary.Hex;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Service;

import javax.crypto.Mac;
import javax.crypto.spec.SecretKeySpec;
import java.io.UnsupportedEncodingException;
import java.security.InvalidKeyException;
import java.security.NoSuchAlgorithmException;

/**
 * Created by chirag.rc on 10/25/17.
 */
@Slf4j
@Service
@Getter
@Setter
public class LazyPayHelper {

    private final static String INDIAN_CURRENCY = "INR";

    @Value("${lazypay.base.url}")
    private String baseUrl;

    @Value("${lazypay.secret.key}")
    private String secretKey;

    @Value("${lazypay.access.key}")
    private String accessKey;

    public String getSignatureForEligibility(String orderAmount, String mobile, String email) {
        String data = mobile + email + orderAmount + INDIAN_CURRENCY;
        return getSignature(data);
    }

    public String getSignatureForTransaction(String merchantTxnId, String orderAmount) {
        String data = "merchantAccessKey=" + accessKey + '&' + "transactionId=" + merchantTxnId + '&' + "amount=" + orderAmount;
        return getSignature(data);
    }

    public String getSignatureForOtpValidation(String txnRefNo) {
        String data = "merchantAccessKey=" + accessKey + '&' + "txnRefNo=" + txnRefNo;
        return getSignature(data);
    }

    public String getSignatureForRefund(String merchantTxnId, String amount) {
        String data = "merchantAccessKey=" + accessKey + '&' + "merchantTxnId=" + merchantTxnId + '&' + "amount=" + amount;
        return getSignature(data);
    }

    public String getSignatureForRegeneratingOtp(String txnRefNo) {
        String data = "merchantAccessKey=" + accessKey + '&' + "txnRefNo=" + txnRefNo;
        return getSignature(data);
    }

    public String getSignatureForEnquiry(String merchantTxnId) {
        String data = "merchantAccessKey=" + accessKey + '&' + "merchantTransactionId=" + merchantTxnId;
        return getSignature(data);
    }

    public String getSignature(String data) {
        String signature = "";
        try {
            SecretKeySpec signingKey = new SecretKeySpec(secretKey.getBytes(), "HmacSHA1");
            Mac mac = Mac.getInstance("HmacSHA1");
            mac.init(signingKey);
            byte[] rawHmac = mac.doFinal(data.getBytes());
            Hex hex = new Hex();
            byte[] encode = hex.encode(rawHmac);
            signature = new String(encode, "UTF-8");
            return signature;
        } catch (NoSuchAlgorithmException e) {
            log.error("[LazyPayHelper] Error getting instance of HmacSHA1 -> {}", e.getMessage());
        } catch (InvalidKeyException e) {
            log.error("[LazyPayHelper] Invalid signing key -> {}, Error -> {}", signature, e.getMessage());
        } catch (UnsupportedEncodingException e) {
            log.error("[LazyPayHelper] Expection while encoding to UTF-8 -> {}", e.getMessage());
        }
        return "";
    }

}
