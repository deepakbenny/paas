package com.swiggy.checkout.service.status;

import com.swiggy.checkout.Payu.service.PayuService;
import com.swiggy.checkout.data.entities.TransactionPaymentDetails;
import com.swiggy.checkout.paytm.service.PaytmService;
import com.swiggy.checkout.pojo.PaymentStatusResponse;
import com.swiggy.checkout.razorpay.RazorPayService;
import com.swiggy.checkout.service.Gateway;
import com.swiggy.checkout.service.ValidPaymentMethod;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import javax.annotation.PostConstruct;

/**
 * Created by Manish on 30/01/17.
 */

@Component
public class JuspayTransactionStatus implements TransactionStatus {

    @Autowired
    private PayuService payuService;

    @Autowired
    private PaytmService paytmService;

    @Autowired
    private RazorPayService razorPayService;

    @PostConstruct
    void init() {
        ValidPaymentMethod.JUSPAY.registerStatusHandler(this);
        ValidPaymentMethod.JUSPAY_NB.registerStatusHandler(this);
        ValidPaymentMethod.PAYU.registerStatusHandler(this);
        ValidPaymentMethod.CARD.registerStatusHandler(this);
        ValidPaymentMethod.SODEXO.registerStatusHandler(this);
    }

    @Override
    public boolean verifyPaymentStatus(TransactionPaymentDetails paymentDetails, PaymentStatusResponse paymentStatusResponse) {

        Gateway gateway = Gateway.getGateway(paymentDetails.getPaymentGateway());

        if(gateway == null) {
            paymentStatusResponse.setErrorMessage("No payment handler available for Juspay, Contact Admin");
            return false;
        }

        switch (gateway) {
            case PAYU:
                return payuService.verifyPaymentStatus(paymentDetails, paymentStatusResponse);

            case PAYTM:
                return paytmService.verifyPaymentStatus(paymentDetails, true, paymentStatusResponse);

            case RAZORPAY:
                return razorPayService.verifyPaymentStatus(paymentDetails, paymentStatusResponse);

            default:
                paymentStatusResponse.setErrorMessage("Juspay gateway is not define");
                return false;
        }

    }
}