package com.swiggy.checkout.pojo.PayLater.LazyPay;

import com.fasterxml.jackson.annotation.JsonProperty;
import lombok.Data;

/**
 * Created by chirag.rc on 10/24/17.
 */
@Data
public class LazyPayOtpTransactionDetails {

    @JsonProperty("reason")
    private String reason;

    @JsonProperty("pgResponseCode")
    private String pgResponseCode;

    @JsonProperty("status")
    private String status;
}
