package com.swiggy.checkout.data.model.bo.impl;

import com.swiggy.checkout.data.entities.PaymentConfigurationEntity;
import com.swiggy.checkout.data.guava.ConfigurationCache;
import com.swiggy.checkout.data.guava.ConfigurationMap;
import com.swiggy.checkout.data.model.bo.PaymentConfiguration;
import com.swiggy.checkout.data.model.dao.PaymentConfigurationDao;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import java.util.List;

/**
 * Created by Manish on 23/01/17.
 */
@Component
public class PaymentConfigurationImpl implements PaymentConfiguration {


    @Autowired
    PaymentConfigurationDao paymentConfigurationDao;

    @Autowired
    ConfigurationCache configurationCache;


    @Override
    public String getConfiguration(String key, String defaultObject) {
        ConfigurationMap configurationMap = configurationCache.getEntry();
        String val = configurationMap.getConfiguration(key);
        if(val == null)
            return defaultObject;
        else return val;
    }

    @Override
    public boolean getConfigurationAsBoolean(String s, boolean defaultObject) {
        return Boolean.parseBoolean(getConfiguration(s,String.valueOf(defaultObject)));
    }

    @Override
    public Integer getConfigurationAsInt(String s, int defaultObject) {
        try {
            return Integer.parseInt(getConfiguration(s, String.valueOf(defaultObject)));
        }catch (NumberFormatException e){
            return defaultObject;
        }
    }

    @Override
    public Long getConfigurationAsLong(String s, long defaultObject) {
        try {
            return Long.parseLong(getConfiguration(s, String.valueOf(defaultObject)));
        } catch (NumberFormatException e) {
            return defaultObject;
        }
    }

    @Override
    public Float getConfigurationAsFloat(String s, float defaultObject) {
        try {
            return Float.parseFloat(getConfiguration(s, String.valueOf(defaultObject)));
        } catch (NumberFormatException e) {
            return defaultObject;
        }
    }

    @Override
    public List<PaymentConfigurationEntity> getAllConfigurations(){

        return  paymentConfigurationDao.findAll();

    }
    @Override
   public void saveConfiguration(String key,String name){
       paymentConfigurationDao.save(PaymentConfigurationEntity.builder().key(key).value(name).build());
    }
}
