package com.swiggy.checkout.service;

import com.newrelic.api.agent.Trace;
import com.swiggy.checkout.data.entities.TransactionEntity;
import com.swiggy.checkout.data.entities.TransactionPaymentDetails;
import com.swiggy.checkout.data.model.dao.TransactionDao;
import com.swiggy.checkout.exception.SwiggyPaymentException;
import com.swiggy.checkout.extenal.entities.response.BaseResponse;
import com.swiggy.checkout.extenal.entities.response.ErrorResponse;
import com.swiggy.checkout.extenal.entities.response.SuccessResponse;
import com.swiggy.checkout.freecharge.service.FreechargeService;
import com.swiggy.checkout.mobikwik.service.MobikwikService;
import com.swiggy.checkout.pojo.PaymentStatusResponse;
import com.swiggy.checkout.service.status.PaytmTransactionStatus;
import com.swiggy.checkout.utils.Utility;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.io.IOException;
import java.time.format.DateTimeFormatter;

/**
 * Created by Manish on 30/01/17.
 */

@Service
@Slf4j
public class VerifyTransactionService {

    private static final int ERROR_CODE = 100;
    private static final int ERROR_CODE_INVALID_ORDER_ID = 101;
    private static final int ERROR_CODE_INVALID_PAYMENT_METHOD = 102;
    private static final String ERROR_MSG = "No Transaction found for Order ";

    @Autowired
    private MobikwikService mobikwikService;

    @Autowired
    private PaytmTransactionStatus paytmTransactionStatus;

    @Autowired
    private FreechargeService freechargeService;

    @Autowired
    private PGService pgService;

    @Autowired
    private TransactionDao transactionDao;
    
    @Trace
    public BaseResponse verifyTransaction(String orderId) throws SwiggyPaymentException {
        try {
            TransactionEntity transactionEntity = transactionDao.findOneByOrderId(Long.valueOf(orderId));
            if (transactionEntity != null) {
                return new SuccessResponse(getPaymentStatusResponse(transactionEntity));
            } else {
                return new ErrorResponse(ERROR_CODE_INVALID_ORDER_ID, ERROR_MSG + orderId);
            }
        } catch (IOException e) {
            log.error("Error in verifyPaymentStatus " + e);
            return null;
        }

    }

    @Trace
    private TransactionPaymentDetails getPaymentStatusResponse(TransactionEntity transaction) throws IOException, SwiggyPaymentException {
        TransactionPaymentDetails paymentDetails = Utility.jsonDecode2(transaction.getPaymentDetails(), TransactionPaymentDetails.class);
        paymentDetails.setOrderId(String.valueOf(transaction.getOrderId()));
        paymentDetails.setAmount(transaction.getAmount());
        PaymentStatusResponse paymentStatusResponse = getPaymentStatusResponse(paymentDetails);

        //returning if check is already done
        if (!paymentDetails.getPaymentTxnStatus().equalsIgnoreCase(ValidPaymentStatus.PENDING)) {
            return paymentDetails;
        }
        //update transaction paymnet status
        final boolean paymentStatus = confirmTxnStatusWithPG(paymentDetails, paymentStatusResponse);
        confirmPaymentStatus(paymentDetails, paymentStatus);
        transaction.setPaymentDetails(Utility.jsonEncode2(paymentDetails));
        transaction.setPaymentTxnStatus(paymentDetails.getPaymentTxnStatus());
        transactionDao.save(transaction);

        return paymentDetails;
    }

    private void confirmPaymentStatus(TransactionPaymentDetails paymentDetails, boolean paymentStatus) {
        if (paymentStatus) {
            paymentDetails.setPaymentTxnStatus(ValidPaymentStatus.SUCCESS);
        } else {
            if (ValidPaymentStatus.PENDING.equalsIgnoreCase(paymentDetails.getPaymentTxnStatus())) {
                paymentDetails.setPaymentTxnStatus(ValidPaymentStatus.PENDING);
            } else {
                paymentDetails.setPaymentTxnStatus(ValidPaymentStatus.FAILURE);
            }
        }
    }

    /**
     * Depending upon payment method, consult respective payment service
     *
     * @param paymentDetails
     * @return
     */
    private boolean confirmTxnStatusWithPG(TransactionPaymentDetails paymentDetails, PaymentStatusResponse paymentStatusResponse) throws SwiggyPaymentException {

        switch (ValidPaymentMethod.getPaymentMethod(paymentDetails.getPaymentMethod())) {
            case SODEXO:
            case JUSPAY:
            case CARD:
            case JUSPAY_NB:
                return pgService.verifyPaymentStatus(paymentDetails);
            case PAYTM:
                return paytmTransactionStatus.verifyPaymentStatus(paymentDetails, paymentStatusResponse);
            case PAYTM_SSO:
                return ValidPaymentStatus.SUCCESS.equals(paymentDetails.getPaymentTxnStatus());
            case THIRD_PARTY_CASH:
            case CASH:
                return true;
            case MOBIKWIK:
                return mobikwikService.verifyPaymentStatus(paymentDetails);
            case MOBIKWIK_SSO:
                return ValidPaymentStatus.SUCCESS.equals(paymentDetails.getPaymentTxnStatus());
            case FREECHARGE:
                return freechargeService.verifyPaymentStatus(paymentDetails);
            case FREECHARGE_SSO:
                return ValidPaymentStatus.SUCCESS.equals(paymentDetails.getPaymentTxnStatus());
            case THIRD_PARTY_ONLINE:
                return ValidPaymentStatus.SUCCESS.equals(paymentDetails.getPaymentTxnStatus());
            case LAZYPAY:
                return ValidPaymentStatus.SUCCESS.equals(paymentDetails.getPaymentTxnStatus());
        }
        throw new SwiggyPaymentException(1, "Invalid payment method.");
    }


    public PaymentStatusResponse verifyPaymentStatus(TransactionEntity transaction) throws SwiggyPaymentException {
        try {
            TransactionPaymentDetails paymentDetails = Utility.jsonDecode2(transaction.getPaymentDetails(), TransactionPaymentDetails.class);
            ValidPaymentMethod paymentMethod = ValidPaymentMethod.getPaymentMethod(paymentDetails.getPaymentMethod());
            PaymentStatusResponse paymentStatusResponse = getPaymentStatusResponse(paymentDetails);
            boolean verified = paymentMethod.verifyPaymentStatus(paymentDetails, paymentStatusResponse);
            paymentStatusResponse.setVerified(verified);
            return paymentStatusResponse;
        } catch (IOException e) {
            log.error("Error in verifyPaymentStatus " + e);
            return null;
        }
    }

    private PaymentStatusResponse getPaymentStatusResponse(TransactionPaymentDetails paymentDetails) {
        return PaymentStatusResponse.builder()
                .paymentMethod(paymentDetails.getPaymentMethod())
                .gateway(paymentDetails.getPaymentGateway())
                .refundStatus(paymentDetails.isRefundInitiated() ? paymentDetails.getPaymentTxnStatus() : null)
                .refundMethod(paymentDetails.getRefundMethod())
                .refundTime(paymentDetails.getRefundTime() != null ? paymentDetails.getRefundTime()
                        .format(DateTimeFormatter.ofPattern("yyyy-MM-dd HH:mm:ss")) : null)
                .build();
    }

}
