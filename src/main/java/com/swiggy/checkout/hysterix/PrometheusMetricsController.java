package com.swiggy.checkout.hysterix;

import io.prometheus.client.CollectorRegistry;
import io.prometheus.client.exporter.common.TextFormat;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.io.IOException;
import java.io.StringWriter;
import java.io.Writer;
import java.util.Collections;
import java.util.HashSet;
import java.util.List;
import java.util.Set;

@RequestMapping("/metrics")
@RestController
@Slf4j
public class PrometheusMetricsController {
    private final CollectorRegistry registry;

    @Autowired
    public PrometheusMetricsController(CollectorRegistry registry) {
        this.registry = registry;
    }

    @RequestMapping(
            method = RequestMethod.GET,
            produces = TextFormat.CONTENT_TYPE_004
    )
    @ResponseBody
    public ResponseEntity<String> getMetricsForPrometheus(@RequestParam(required = false) List<String> keys) throws IOException {
        Writer writer = new StringWriter();
        try {
            TextFormat.write004(writer, this.registry.filteredMetricFamilySamples(parse(keys)));
        } catch (IOException e) {
            log.error("Prometheus Metrics -- write error", e);
        }

        return new ResponseEntity<>(writer.toString(), HttpStatus.OK);
    }

    private Set<String> parse(List<String> keys) {
        return (keys == null || keys.isEmpty()) ? Collections.emptySet() : new HashSet<>(keys);
    }
}
