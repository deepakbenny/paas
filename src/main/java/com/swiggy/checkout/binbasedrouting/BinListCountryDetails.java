package com.swiggy.checkout.binbasedrouting;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.annotation.JsonProperty;
import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

@NoArgsConstructor
@AllArgsConstructor
@Getter
@Setter
@JsonIgnoreProperties(ignoreUnknown = true)
@JsonInclude(JsonInclude.Include.NON_NULL)
public class BinListCountryDetails {
    @JsonProperty("alpha2")
    private String alpha2;

    @JsonProperty("name")
    private String name;

    @JsonProperty("numeric")
    private String numeric;

    @JsonProperty("latitude")
    private int latitude;

    @JsonProperty("longitude")
    private int longitude;
}
