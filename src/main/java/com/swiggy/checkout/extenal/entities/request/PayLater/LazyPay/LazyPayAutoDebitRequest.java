package com.swiggy.checkout.extenal.entities.request.PayLater.LazyPay;

import com.fasterxml.jackson.annotation.JsonProperty;
import com.swiggy.checkout.pojo.PayLater.LazyPay.LazyPayAmountDetails;
import com.swiggy.checkout.pojo.PayLater.LazyPay.LazyPayUserDetails;
import lombok.Data;

/**
 * Created by chirag.rc on 10/24/17.
 */
@Data
public class LazyPayAutoDebitRequest {

    @JsonProperty("paymentMode")
    private String paymentMode;

    @JsonProperty("merchantTxnId")
    private String merchantTxnId;

    @JsonProperty("userDetails")
    private LazyPayUserDetails lazyPayUserDetails;

    @JsonProperty("amount")
    private LazyPayAmountDetails lazyPayAmountDetails;

    @JsonProperty("source")
    private String source;
}
