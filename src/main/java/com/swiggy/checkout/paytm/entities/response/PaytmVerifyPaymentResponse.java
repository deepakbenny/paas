package com.swiggy.checkout.paytm.entities.response;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.annotation.JsonProperty;
import lombok.Getter;
import lombok.Setter;

/**
 * Created by Manish on 29/01/17.
 */

@JsonInclude(JsonInclude.Include.NON_EMPTY)
@JsonIgnoreProperties(ignoreUnknown = true)
@Getter
@Setter
public class PaytmVerifyPaymentResponse {

    @JsonProperty("ErrorCode") private String errorCode;
    @JsonProperty("ErrorMsg") private String errorMsg;
    @JsonProperty("TXNID") private String txnId;
    @JsonProperty("BANKTXNID") private String bankTxnId;
    @JsonProperty("ORDERID") private String orderId;
    @JsonProperty("TXNAMOUNT") private String txnAmount;
    @JsonProperty("STATUS") private String status;
    @JsonProperty("TXNTYPE") private String txnType;
    @JsonProperty("GATEWAYNAME") private String gatewayName;
    @JsonProperty("RESPCODE") private String respCode;
    @JsonProperty("RESPMSG") private String respMsg;
    @JsonProperty("BANKNAME") private String bankName;
    @JsonProperty("MID") private String mid;
    @JsonProperty("PAYMENTMODE") private String paymentMode;
    @JsonProperty("REFUNDAMT") private String refundAmount;
    @JsonProperty("TXNDATE") private String txnDate;

}
