package com.swiggy.checkout.extenal.entities.request.PayLater.LazyPay;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.databind.annotation.JsonSerialize;
import lombok.Data;

/**
 * Created by chirag.rc on 11/13/17.
 */
@Data
@JsonIgnoreProperties(ignoreUnknown = true)
@JsonSerialize(include = JsonSerialize.Inclusion.NON_NULL)
public class LazyPayTxnEnquiryRequest {

    @JsonProperty("merchantTxnId")
    private String merchantTxnId;

    @JsonProperty("isSale")
    private Boolean isSale;
}
