package com.swiggy.checkout.freecharge.entities.responses;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import com.fasterxml.jackson.annotation.JsonInclude;
import lombok.*;

/**
 * Created by Manish on 27/01/17.
 */
@Setter
@Getter
@NoArgsConstructor
@AllArgsConstructor
@Builder
@JsonInclude(JsonInclude.Include.NON_NULL)
@JsonIgnoreProperties(ignoreUnknown = true)
public class LoginResponse extends FreechargeBaseResponse {
    private String accessToken;
    private String accessTokenExpiry;
    private String refreshToken;
    private String refreshTokenExpiry;
}
