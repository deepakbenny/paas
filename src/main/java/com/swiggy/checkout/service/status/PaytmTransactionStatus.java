package com.swiggy.checkout.service.status;

import com.newrelic.api.agent.Trace;
import com.swiggy.checkout.data.entities.TransactionPaymentDetails;
import com.swiggy.checkout.paytm.service.PaytmService;
import com.swiggy.checkout.pojo.PaymentStatusResponse;
import com.swiggy.checkout.service.ValidPaymentMethod;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import javax.annotation.PostConstruct;

/**
 * Created by Manish on 30/01/17.
 */

@Component
public class PaytmTransactionStatus implements TransactionStatus {

    @Autowired
    private PaytmService paytmService;

    @PostConstruct
    void init() {
        ValidPaymentMethod.PAYTM.registerStatusHandler(this);
        ValidPaymentMethod.PAYTM_SSO.registerStatusHandler(this);
    }

    @Trace
    @Override
    public boolean verifyPaymentStatus(TransactionPaymentDetails paymentDetails, PaymentStatusResponse paymentStatusResponse) {
        return paytmService.verifyPaymentStatus(paymentDetails, false);
    }
}
