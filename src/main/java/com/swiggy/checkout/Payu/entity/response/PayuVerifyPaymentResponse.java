package com.swiggy.checkout.Payu.entity.response;

import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.annotation.JsonUnwrapped;
import com.swiggy.checkout.Payu.PayUTransactionDetails;
import lombok.Getter;
import lombok.Setter;

import java.util.Map;

/**
 * Created by Manish on 30/01/17.
 */
@JsonInclude(JsonInclude.Include.NON_EMPTY)
@Getter
@Setter
public class PayuVerifyPaymentResponse {

    @JsonProperty("status") private String status;
    @JsonProperty("msg") private String msg;
    @JsonUnwrapped
    @JsonProperty("transaction_details") private Map<String, PayUTransactionDetails> transactionDetailsMap;

}
