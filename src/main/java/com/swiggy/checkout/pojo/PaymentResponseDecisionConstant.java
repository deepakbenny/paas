package com.swiggy.checkout.pojo;

import lombok.Getter;

/**
 * Created by dhaval on 6/8/17.
 */
public enum PaymentResponseDecisionConstant {

    REFUND((byte)1),
    RETRY((byte)2),
    DISCARD((byte)3);

    @Getter
    private byte value;

    private PaymentResponseDecisionConstant(byte value) {
        this.value = value;
    }
}
