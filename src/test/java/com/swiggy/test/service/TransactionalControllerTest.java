package com.swiggy.test.service;

import com.swiggy.checkout.extenal.entities.request.CreateTransactionRequest;
import com.swiggy.checkout.pojo.User;
import com.swiggy.checkout.utils.Utility;
import com.swiggy.test.AbstractControllerTest;
import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;
import org.springframework.http.MediaType;
import org.springframework.test.web.servlet.MvcResult;
import org.springframework.test.web.servlet.request.MockMvcRequestBuilders;
import org.springframework.transaction.annotation.Transactional;

/**
 * Created by Manish on 06/02/17.
 */

@Transactional
public class TransactionalControllerTest extends AbstractControllerTest{

    @Before
    public void setUp() {
        super.setUp();
    }


    @Test
    public void testCreateTransaction() throws Exception {

        String uri = "/v1/payment/transaction/create";
        String orderId = "781710";
        User user = User.builder().customerId("1563").email("manish.kumar@swiggy.in").paytmSsoToken("paytm_sso_token")
                .mobikwikSsoToken("mobikwick-sso-token").freechargeSsoToken("free-charge-token").mobile("8105025755").build();

        CreateTransactionRequest createTransactionRequest= CreateTransactionRequest.builder().amount("300").orderId(orderId).paymentMethod("Cash").build();

        MvcResult result = mvc.perform(MockMvcRequestBuilders.post(uri)
                .header("Content-Type","application/json")
                .header("userAgent","Swiggy-Android")
                .header("user",user)
                .content(Utility.jsonEncode2(createTransactionRequest))
                .accept(MediaType.APPLICATION_JSON)).andReturn();

        String content = result.getResponse().getContentAsString();
        int status = result.getResponse().getStatus();

        Assert.assertEquals("failure - expected HTTP status", 200, status);
        Assert.assertTrue(
                "failure - expected HTTP response body to have a value",
                content.trim().length() > 0);

    }


    @Test
    public void testSaveConfiguration() throws Exception {

        String uri = "/v1/payment/transaction/verify";
        String orderId = "781705";

        MvcResult result = mvc.perform(MockMvcRequestBuilders.get(uri).param("order_id",orderId)
                .accept(MediaType.APPLICATION_JSON)).andReturn();

        String content = result.getResponse().getContentAsString();
        int status = result.getResponse().getStatus();

        Assert.assertEquals("failure - expected HTTP status 200", 200, status);
        Assert.assertTrue(
                "failure - expected HTTP response body to have a value",
                content.trim().length() > 0);

    }





}
