package com.swiggy.checkout.health.impl;


import com.swiggy.checkout.health.IHealthChecker;
import com.swiggy.checkout.health.bean.BaseHealthBean;
import com.swiggy.checkout.health.bean.MySQlHealthBean;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import javax.persistence.Query;
import javax.persistence.TypedQuery;

/**
 * Created by Manish on 17/10/17.
 */
@Slf4j
@Service
public class MySQLHealthChecker implements IHealthChecker {

    public static final String MYSQL = "mysql";

    @PersistenceContext
    public EntityManager em;

    @Override
    public String getHealthCheckerName() {
        return MYSQL;
    }

    @Override
    public BaseHealthBean performHeathCheck() {
        MySQlHealthBean bean = new MySQlHealthBean();
        try {
            Query query = em.createNativeQuery("SELECT 1");
            query.getResultList();
            bean.setHealthy(true);
        } catch (Exception e) {
            log.info("Error in getting session", e);
        }
        return bean;
    }
}
