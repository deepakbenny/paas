package com.swiggy.checkout.service;

/**
 * Created by chirag.rc on 10/29/17.
 */
public enum ValidPayLaterMethod {

    LAZYPAY("PayLater_Lazypay");

    private String payLaterMethod;

    ValidPayLaterMethod(String paylaterMethod) {
        this.payLaterMethod = paylaterMethod;
    }

    private String getPaylaterMethodName() {
        return payLaterMethod;
    }

    public String toString() {
        return payLaterMethod;
    }

    public static ValidPayLaterMethod getPaylaterMethodName(String paymentType) {
        for (ValidPayLaterMethod name : ValidPayLaterMethod.values()) {
            if (name.getPaylaterMethodName().equalsIgnoreCase(paymentType))
                return name;
        }
        return null;
    }
}
