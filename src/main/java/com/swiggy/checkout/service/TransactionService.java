package com.swiggy.checkout.service;

import com.fasterxml.jackson.core.JsonProcessingException;
import com.newrelic.api.agent.Trace;
import com.swiggy.checkout.data.entities.ThirdPartyEntity;
import com.swiggy.checkout.data.entities.TransactionEntity;
import com.swiggy.checkout.data.entities.TransactionPaymentDetails;
import com.swiggy.checkout.data.model.bo.PaymentConfiguration;
import com.swiggy.checkout.data.model.dao.TransactionDao;
import com.swiggy.checkout.exception.SwiggyPaymentException;
import com.swiggy.checkout.extenal.entities.request.CreateTransactionRequest;
import com.swiggy.checkout.extenal.entities.request.PayLater.PayLaterDebitAmountRequest;
import com.swiggy.checkout.extenal.entities.response.BaseResponse;
import com.swiggy.checkout.extenal.entities.response.ErrorResponse;
import com.swiggy.checkout.extenal.entities.response.PayLater.LazyPay.LazyPayTransactionResponse;
import com.swiggy.checkout.extenal.entities.response.SuccessResponse;
import com.swiggy.checkout.freecharge.entities.responses.DebitResponse;
import com.swiggy.checkout.freecharge.service.FreechargeWalletService;
import com.swiggy.checkout.juspay.JuspayConstants;
import com.swiggy.checkout.juspay.service.JuspayService;
import com.swiggy.checkout.mobikwik.entities.response.MobikwikBaseResponse;
import com.swiggy.checkout.mobikwik.service.MobikwikWalletService;
import com.swiggy.checkout.paytm.entities.response.WithdrawAmountResponse;
import com.swiggy.checkout.paytm.service.PaytmWalletService;
import com.swiggy.checkout.pojo.User;
import com.swiggy.checkout.service.paylater.PayLaterService;
import com.swiggy.checkout.thirdparty.ThirdPartyClientService;
import com.swiggy.checkout.thirdparty.client.services.ThirdPartyPaymentService;
import com.swiggy.checkout.utils.Utility;
import lombok.extern.slf4j.Slf4j;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.sql.Timestamp;
import java.time.LocalDateTime;

/**
 * Created by Manish on 29/01/17.
 */

@Service
@Slf4j
public class TransactionService {

    private static final Logger logger = LoggerFactory.getLogger(TransactionService.class);
    private static final int ERROR_CODE = 100;
    private static final String PARSE_ERROR = "Error in Parsing Transaction Payment Details";
    private static final String BAD_REQUEST_ERROR = "MANDATORY FIELDS MISSING ";
    private static final String INDIAN_CURRENCY = "INR";
    private static final String LAZYPAY = "PayLater_Lazypay";

    @Autowired
    TransactionDao transactionDao;

    @Autowired
    private PaymentConfiguration configuration;

    @Autowired
    private MobikwikWalletService mobikwikWalletService;

    @Autowired
    private JuspayService juspayService;

    @Autowired
    private PaytmWalletService paytmWalletService;

    @Autowired
    private FreechargeWalletService freechargeWalletService;

    @Autowired
    private ThirdPartyClientService thirdPartyClientService;

    @Autowired
    private ThirdPartyPaymentService thirdPartyPaymentService;

    @Autowired
    private PayLaterService payLaterService;

    @Trace
    public BaseResponse createTransaction(CreateTransactionRequest createTransactionRequest)  throws SwiggyPaymentException  {
        validateTransactionRequest(createTransactionRequest);
        try {
            TransactionPaymentDetails transactionPaymentDetails = createOrderPaymentDetails(createTransactionRequest);
            TransactionEntity transactionEntity = getTransactionEntityFromTransactionRequest(createTransactionRequest, transactionPaymentDetails);
            transactionDao.save(transactionEntity);
            return new SuccessResponse(transactionPaymentDetails);
        } catch (JsonProcessingException e) {
            return new ErrorResponse(ERROR_CODE, PARSE_ERROR);
        }
    }

    @Trace
    private TransactionEntity getTransactionEntityFromTransactionRequest(CreateTransactionRequest createTransactionRequest, TransactionPaymentDetails transactionPaymentDetails) throws JsonProcessingException {
        return TransactionEntity.builder().
                orderId(Long.valueOf(createTransactionRequest.getOrderId())).
                userId(Long.valueOf(Utility.getUserFromHeader().getCustomerId())).
                amount(Integer.valueOf(createTransactionRequest.getAmount())).
                paymentMethod(createTransactionRequest.getPaymentMethod()).
                paymentTxnStatus(transactionPaymentDetails.getPaymentTxnStatus()).
                paymentDetails(Utility.jsonEncode(transactionPaymentDetails)).
                createdOn(Timestamp.valueOf(LocalDateTime.now())).
                updatedOn(Timestamp.valueOf(LocalDateTime.now())).build();
    }


    /**
     * Basic Validation of the request..
     * 1. Valid payment method Request
     * 2. Validate Header request
     *
     * @param createTransactionRequest
     * @return
     */
    @Trace
    private void validateTransactionRequest(CreateTransactionRequest createTransactionRequest) throws SwiggyPaymentException {


        if (Utility.isEmpty(createTransactionRequest.getOrderId())) {
            logger.info("[PLACE TRANSACTION] [Validation] invalid Order ID");
            throw new SwiggyPaymentException(400, BAD_REQUEST_ERROR);
        }

        if (Utility.getUserFromHeader() == null) {
            logger.info("[PLACE TRANSACTION] [Validation] invalid USER DETAILS");
            throw new SwiggyPaymentException(400, BAD_REQUEST_ERROR);
        }

    }

    /**
     * Creating order related payment details
     *
     * @return
     */
    @Trace
    private TransactionPaymentDetails createOrderPaymentDetails(CreateTransactionRequest createTransactionRequest) {
        String orderId = createTransactionRequest.getOrderId();
        TransactionPaymentDetails paymentDetails = TransactionPaymentDetails.builder().
                paymentMethod(createTransactionRequest.getPaymentMethod()).
                amount(Integer.valueOf(createTransactionRequest.getAmount())).
                thirdPartyMetaData(createTransactionRequest.getThirdPartyMetaData()).
                build();

        paymentDetails.setPaymentNetbankingType(createTransactionRequest.getPaymentNetBankingType());

        switch (ValidPaymentMethod.getPaymentMethod(createTransactionRequest.getPaymentMethod())) {
            case SODEXO:
            case JUSPAY:
                paymentDetails.setBinNumber(createTransactionRequest.getBinNumber());
            case CARD:
            case JUSPAY_NB:
                paymentDetails.setBankName(createTransactionRequest.getPaymentNetBankingType());
                paymentDetails.setPaymentTxnStatus(ValidPaymentStatus.PENDING);
                paymentDetails.setPaymentTxnId(JuspayConstants.MERCHANT_ID + "-" + orderId + "-1"); // fixed format
                String returnUrl = null;
                if (Utility.isCurrentRequestFromWeb(Utility.getHeaderRequest("user-agent"))) {
                    returnUrl = configuration.getConfiguration("_web_juspay_return_url", "https://www.swiggy.com/checkout/order-received");
                }
                juspayService.createOrder(createTransactionRequest, returnUrl, paymentDetails); // create order on juspay
                break;

            case PAYTM:
                paymentDetails.setPaymentTxnStatus(ValidPaymentStatus.PENDING);
                break;

            case MOBIKWIK:
                paymentDetails.setPaymentTxnStatus(ValidPaymentStatus.PENDING);
                break;

            case FREECHARGE:
                paymentDetails.setPaymentTxnStatus(ValidPaymentStatus.PENDING);
                break;

            case THIRD_PARTY_CASH:
            case CASH:
                paymentDetails.setPgResponseTime(LocalDateTime.now());
                paymentDetails.setPaymentTxnStatus(ValidPaymentStatus.SUCCESS);
                break;

            case PAYTM_SSO:
                //Utility.getUserFromHeader().setCoupon(createTransactionRequest.getCoupon());
                String couponId = createTransactionRequest.getCoupon();
                BaseResponse paytmWithdrawResponse = paytmWalletService.debitAmount(String.valueOf(createTransactionRequest.getAmount()), orderId, couponId);

                if (logger.isDebugEnabled())
                    logger.debug(Utility.jsonEncode2(paytmWithdrawResponse));
                logger.info("Paytm-sso response: " + Utility.jsonEncode2(paytmWithdrawResponse));
                if (paytmWithdrawResponse.getStatusCode() == 0) {
                    logger.info("[PLACE ORDER] Withdraw successful. Order Id : {}", orderId);
                    paymentDetails.setPaymentTxnStatus(ValidPaymentStatus.SUCCESS);
                } else {
                    logger.info("[PLACE ORDER] Withdraw Failed. Order Id : {}", orderId);
                    paymentDetails.setPaymentTxnStatus(ValidPaymentStatus.FAILURE);
                }
                paymentDetails.setPgResponseTime(LocalDateTime.now());
                if (paytmWithdrawResponse.getData() instanceof WithdrawAmountResponse) {
                    WithdrawAmountResponse response = (WithdrawAmountResponse) paytmWithdrawResponse.getData();
                    paymentDetails.setPaytmSsoResponse(response);
                    if (response != null)
                        paymentDetails.setPaymentTxnId(response.getTxnId());
                }
                break;

            case MOBIKWIK_SSO:
                handleMobikwikSsoPayment(createTransactionRequest, paymentDetails);
                break;

            case FREECHARGE_SSO:
                logger.info("[PLACE ORDER] freecharge sso payments - trying to deduct money from source : {}", orderId);
                handleFreechargeSsoPayment(createTransactionRequest, paymentDetails);
                break;

            case THIRD_PARTY_ONLINE:
                logger.info("[PLACE ORDER] Third party online - trying to deduct money from source : {}", orderId);
                handleThirdPartyPayment(createTransactionRequest, paymentDetails);
                break;

            case LAZYPAY:
                logger.info("[PLACE ORDER] LazyPay payment - trying to deduct money for orderId: {}", orderId);
                handleLazyPayPayment(createTransactionRequest, paymentDetails);
                break;

            default:
                paymentDetails.setPaymentTxnStatus(ValidPaymentStatus.PENDING);
                break;
        }
        return paymentDetails;
    }


    /**
     * Call Mobikwik debitAmount flow. If there is no exception, then payment went through.
     * Else payment failed and handle the order status and payment txn status appropriately.
     *
     * @param createTransactionRequest
     * @param paymentDetails
     */

    @Trace
    private void handleMobikwikSsoPayment(CreateTransactionRequest createTransactionRequest, TransactionPaymentDetails paymentDetails){
        String orderId = String.valueOf(createTransactionRequest.getOrderId());
        try {
            BaseResponse baseResponse = mobikwikWalletService.debitAmount(createTransactionRequest.getAmount(), orderId, "");
            logger.info("[PLACE ORDER] Mobikwik Withdraw Response : {}" + Utility.jsonEncode2(baseResponse));
            mobikwikWalletService.regenerateToken(paymentDetails);
            paymentDetails.setPaymentTxnStatus(ValidPaymentStatus.SUCCESS);
            // paymentDetails.setWalletSsoToken(Utility.getUserFromHeader().getMobikwikSsoToken());
            if (baseResponse.getData() instanceof MobikwikBaseResponse)
                paymentDetails.setMobikwikSsoWithdrawResponse((MobikwikBaseResponse) baseResponse.getData());
        } catch (SwiggyPaymentException e) {
            logger.info("[PLACE ORDER] Mobikwik Withdraw Failed. Order Id : {}", orderId);
            paymentDetails.setPaymentTxnStatus(ValidPaymentStatus.FAILURE);
        }
        paymentDetails.setPgResponseTime(LocalDateTime.now());
    }


    @Trace
    private void handleFreechargeSsoPayment(CreateTransactionRequest createTransactionRequest, TransactionPaymentDetails paymentDetails){
        String orderId = createTransactionRequest.getOrderId();
        try {
            BaseResponse baseResponse = freechargeWalletService.debitAmount(String.valueOf(
                    createTransactionRequest.getAmount()), orderId, "");
            logger.info("[PLACE ORDER] Freecharge Withdraw Response : {}" + Utility.jsonEncode2(baseResponse));
            paymentDetails.setPaymentTxnStatus(ValidPaymentStatus.SUCCESS);
            if (baseResponse.getData() instanceof DebitResponse)
                paymentDetails.setFreechargeWithdrawResponse((DebitResponse) baseResponse.getData());
        } catch (SwiggyPaymentException e) {
            logger.info("[PLACE ORDER] Freecharge Withdraw Failed. Order Id : {}" + orderId);
            paymentDetails.setPaymentTxnStatus(ValidPaymentStatus.FAILURE);
            paymentDetails.setFreechargeFailureMessage(e.getDetailMessage());
        }
        paymentDetails.setPgResponseTime(LocalDateTime.now());
    }


    @Trace
    private void handleThirdPartyPayment(CreateTransactionRequest createTransactionRequest, TransactionPaymentDetails paymentDetails) {
        boolean thirdPartyOnlineEnable = configuration.getConfigurationAsBoolean("THIRD_PARTY_ONLINE_ENABLE", false);
        if (!thirdPartyOnlineEnable) {
            logger.info("[PLACE ORDER] Third Party disabled, Failed. Order Id : {}" + createTransactionRequest.getOrderId());
            paymentDetails.setPaymentTxnStatus(ValidPaymentStatus.FAILURE);
            paymentDetails.setPgResponseTime(LocalDateTime.now());
            return;
        }
        String orderId = createTransactionRequest.getOrderId();
        ThirdPartyEntity thirdParty = thirdPartyClientService.getThirdPartyByToken();
        if (thirdParty != null) {
            try {
                if (!thirdPartyPaymentService.verifyThirdPartyTransaction(thirdParty, paymentDetails)) {
                    throw new SwiggyPaymentException(3, "Could not validate payment from " + thirdParty.getSource());
                }
                paymentDetails.setPaymentTxnStatus(ValidPaymentStatus.SUCCESS);
            } catch (SwiggyPaymentException e) {
                logger.info("[PLACE ORDER] Third Party Withdraw Failed. Order Id : {}" + orderId);
                paymentDetails.setPaymentTxnStatus(ValidPaymentStatus.FAILURE);
            }
        } else {
            logger.info("[PLACE ORDER] Third Party Entity is empty, Order Id : {}" + orderId);
        }
        paymentDetails.setPgResponseTime(LocalDateTime.now());
    }

    @Trace
    private void handleLazyPayPayment(CreateTransactionRequest createTransactionRequest, TransactionPaymentDetails paymentDetails) {
        String orderId = createTransactionRequest.getOrderId();
        User user = Utility.getUserFromHeader();
        try {
            PayLaterDebitAmountRequest payLaterDebitAmountRequest = new PayLaterDebitAmountRequest();
            payLaterDebitAmountRequest.setMobile(user.getMobile());
            payLaterDebitAmountRequest.setEmail(user.getEmail());
            payLaterDebitAmountRequest.setOrderId(orderId);
            payLaterDebitAmountRequest.setCurrency(INDIAN_CURRENCY);
            payLaterDebitAmountRequest.setUserId(Utility.getLong(user.getCustomerId()));
            payLaterDebitAmountRequest.setValue(createTransactionRequest.getAmount());
            payLaterDebitAmountRequest.setPaylaterMethod(LAZYPAY);

            BaseResponse response = payLaterService.debitAmount(payLaterDebitAmountRequest);

            logger.info("[PLACE ORDER] LazyPay Withdraw Response : {}", Utility.jsonEncode2(response));

            if (response instanceof SuccessResponse) {
                LazyPayTransactionResponse lazyPayTransactionResponse = (LazyPayTransactionResponse) response.getData();
                paymentDetails.setPaymentMethod(LAZYPAY);
                paymentDetails.setPaymentTxnId(lazyPayTransactionResponse.getTransactionId());
                paymentDetails.setPaymentTxnStatus(ValidPaymentStatus.SUCCESS);
                paymentDetails.setLazyPayTransactionResponse(lazyPayTransactionResponse);
            } else {
                paymentDetails.setPaymentTxnStatus(ValidPaymentStatus.FAILURE);
            }
        } catch (SwiggyPaymentException e) {
            logger.info("[PLACE ORDER] LazyPay AutoDebit Failed. Order Id : {}", orderId);
            paymentDetails.setPaymentTxnStatus(ValidPaymentStatus.FAILURE);
        }
        paymentDetails.setPgResponseTime(LocalDateTime.now());
    }

}
