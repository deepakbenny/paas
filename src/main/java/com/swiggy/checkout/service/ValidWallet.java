package com.swiggy.checkout.service;

/**
 * Created by Manish on 10/02/17.
 */
public enum ValidWallet {


    PAYTM("PayTM"),
    PAYTM_SSO("PayTM-SSO"),
    MOBIKWIK("Mobikwik"),
    MOBIKWIK_SSO("Mobikwik-SSO"),
    FREECHARGE("Freecharge"),
    FREECHARGE_SSO("Freecharge-SSO");

    private String walletName;

    private ValidWallet(String walletName) {
        this.walletName = walletName;
    }

    private String getWalletName() {
        return walletName;
    }

    public String toString() {
        return walletName;
    }

    public static ValidWallet getWalletName(String walletName) {
        for(ValidWallet name : ValidWallet.values()) {
            if(name.getWalletName().equalsIgnoreCase(walletName))
                return name;
        }
        return null;
    }

}
