package com.swiggy.checkout.controller;

import com.swiggy.checkout.extenal.entities.request.PaymentRoutingRequest;
import com.swiggy.checkout.extenal.entities.response.BaseResponse;
import com.swiggy.checkout.service.Gateway;
import com.swiggy.checkout.service.PaymentRoutingService;
import com.swiggy.checkout.service.RoutingService;
import io.swagger.annotations.ApiOperation;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.MediaType;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

@RestController
@RequestMapping("/api/v1/payments")
public class PaymentRoutingController {

    @Autowired
    PaymentRoutingService paymentRoutingService;

    @Autowired
    RoutingService routingService;

    @ApiOperation(value = "add payment route", notes = "Api to add payment routing configuration.")
    @RequestMapping(
            value = {"/addRoute"},
            method = RequestMethod.POST,
            produces = MediaType.APPLICATION_JSON_VALUE
    )

    public BaseResponse addRoute(@RequestBody PaymentRoutingRequest paymentRoutingRequest) {
        return paymentRoutingService.addRoutingConfiguration(paymentRoutingRequest);
    }

    @ApiOperation(value = "add payment route", notes = "Api to add payment routing configuration.")
    @RequestMapping(
            value = {"/getRoute"},
            method = RequestMethod.POST,
            produces = MediaType.APPLICATION_JSON_VALUE
    )

    public Gateway addRoute(@RequestBody long value) {
        return routingService.routeToGateway(value);
    }

}

