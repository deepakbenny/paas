package com.swiggy.checkout.mobikwik.entities.request;

import com.fasterxml.jackson.annotation.JsonInclude;
import lombok.*;

/**
 * Created by Manish on 29/01/17.
 */
@NoArgsConstructor
@AllArgsConstructor
@Getter
@Setter
@Builder
@JsonInclude(JsonInclude.Include.NON_NULL)
public class MobikwikCalculateChecksumRequest {

    private String amount;
    private String orderid;
    private String redirectUrl;
}
