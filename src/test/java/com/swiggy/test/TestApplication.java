package com.swiggy.test;

import com.swiggy.checkout.Application;
import com.swiggy.checkout.ApplicationConfig;
import com.swiggy.commons.config.ServerConfig;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.EnableAutoConfiguration;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.cache.annotation.EnableCaching;
import org.springframework.context.annotation.EnableAspectJAutoProxy;
import org.springframework.context.annotation.Import;
import org.springframework.context.annotation.PropertySource;
import org.springframework.transaction.annotation.EnableTransactionManagement;


/**
 * Created by Manish on 05/02/17.
 */

@SpringBootApplication(scanBasePackages = "com.swiggy.checkout")
@EnableCaching
@EnableTransactionManagement
@EnableAutoConfiguration
@EnableAspectJAutoProxy(proxyTargetClass = true)
@PropertySource("classpath:application.properties")
@Import(value = {ServerConfig.class, ApplicationConfig.class})
public class TestApplication {

    private static final Logger log = LoggerFactory.getLogger(TestApplication.class);

    public static void main(String[] args) {
        SpringApplication.run(Application.class, args);
    }

}
