package com.swiggy.checkout.service.refund;

import com.swiggy.checkout.Payu.entity.response.PayURefundResponse;
import com.swiggy.checkout.Payu.service.PayuService;
import com.swiggy.checkout.exception.SwiggyPaymentException;
import com.swiggy.checkout.freecharge.entities.responses.RefundPaasResponse;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

/**
 * Created by Geetesh on 6/27/17.
 */
@Service
@Slf4j
public class PayuRefundService implements RefundBaseService {

    @Autowired
    private PayuService payuService;

    @Override
    public RefundPaasResponse refund(RefundPaasRequest request) throws SwiggyPaymentException {
        PayURefundResponse response = payuService.refundPayuTxn(request.getTxnId(), request.getAmount());
        RefundPaasResponse refundPaasResponse = new RefundPaasResponse();
        refundPaasResponse.getData().setPayURefundResponse(response);
        return refundPaasResponse;
    }
}
