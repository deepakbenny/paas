package com.swiggy.checkout.binbasedrouting;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.annotation.JsonProperty;
import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

@NoArgsConstructor
@AllArgsConstructor
@Getter
@Setter
@JsonIgnoreProperties(ignoreUnknown = true)
@JsonInclude(JsonInclude.Include.NON_NULL)
public class BinListBankDetails {
    @JsonProperty("name")
    private String Name;

    @JsonProperty("logo")
    private String logo;

    @JsonProperty("url")
    private String url;

    @JsonProperty("city")
    private String city;

    @JsonProperty("phone")
    private String phone;
}
