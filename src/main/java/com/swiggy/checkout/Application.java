package com.swiggy.checkout;

import com.swiggy.annotations.audit.GeneralRepository;
import com.swiggy.checkout.config.RequestInterceptor;
import com.swiggy.commons.ApplicationInitializer;
import com.swiggy.commons.GeneralRepositoryImpl;
import com.swiggy.commons.config.*;
import com.swiggy.commons.interceptors.ExecutionTimeInterceptor;
import com.swiggy.commons.interceptors.RequestIdInterceptor;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.EnableAutoConfiguration;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.cache.annotation.EnableCaching;
import org.springframework.context.annotation.*;
import org.springframework.data.redis.connection.RedisConnectionFactory;
import org.springframework.data.redis.connection.jedis.JedisConnectionFactory;
import org.springframework.transaction.annotation.EnableTransactionManagement;
import org.springframework.web.servlet.config.annotation.InterceptorRegistry;
import org.springframework.web.servlet.config.annotation.WebMvcConfigurerAdapter;

/**
 * Created by Manish on 23/01/17.
 */

@SpringBootApplication(scanBasePackages = "com.swiggy.checkout")
@ImportResource("classpath:rabbitMQ.xml")
@EnableCaching
@EnableTransactionManagement
@EnableAutoConfiguration
@EnableAspectJAutoProxy(proxyTargetClass = true)
//@PropertySource("classpath:application.properties")
@Import(value = {ServerConfig.class, ApplicationConfig.class})
@Configuration
public class Application extends WebMvcConfigurerAdapter {


    public static void main(String[] args) {
        SpringApplication.run(Application.class, args);
    }

    @Bean
    public GeneralRepository generalRepository(){
        return new GeneralRepositoryImpl();
    }

    public void addInterceptors(InterceptorRegistry registry) {
        registry.addInterceptor(new ExecutionTimeInterceptor());
        registry.addInterceptor(new RequestInterceptor());
    }

}
