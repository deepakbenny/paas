package com.swiggy.checkout.mobikwik.service;


import com.newrelic.api.agent.Trace;
import com.swiggy.checkout.data.entities.TransactionPaymentDetails;
import com.swiggy.checkout.data.entities.UserTokenDetailsEntity;
import com.swiggy.checkout.data.model.bo.PaymentConfiguration;
import com.swiggy.checkout.data.model.dao.UserTokenDetailsDao;
import com.swiggy.checkout.exception.SwiggyPaymentException;
import com.swiggy.checkout.extenal.entities.request.WalletCalculateChecksumRequest;
import com.swiggy.checkout.extenal.entities.request.WalletRegisterNewUserRequest;
import com.swiggy.checkout.extenal.entities.request.WalletResendOTPRequest;
import com.swiggy.checkout.extenal.entities.response.BaseResponse;
import com.swiggy.checkout.extenal.entities.response.ErrorResponse;
import com.swiggy.checkout.extenal.entities.response.SuccessResponse;
import com.swiggy.checkout.freecharge.entities.responses.RefundPaasResponse;
import com.swiggy.checkout.jackson.JacksonConverterFactory;
import com.swiggy.checkout.mobikwik.MobikwikProperties;
import com.swiggy.checkout.mobikwik.entities.response.*;
import com.swiggy.checkout.pojo.User;
import com.swiggy.checkout.service.WalletTokenMigrationService;
import com.swiggy.checkout.utils.HTTPLogger;
import com.swiggy.checkout.utils.Utility;
import com.swiggy.checkout.wallet.WalletBaseService;
import lombok.extern.slf4j.Slf4j;
import okhttp3.Interceptor;
import okhttp3.OkHttpClient;
import okhttp3.Request;
import okhttp3.logging.HttpLoggingInterceptor;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.util.StringUtils;
import retrofit2.Call;
import retrofit2.Retrofit;

import javax.annotation.PostConstruct;
import java.io.IOException;
import java.security.SignatureException;
import java.sql.Timestamp;
import java.util.concurrent.TimeUnit;

/**
 * Created by Manish on 29/01/17.
 */

@Service
@Slf4j
public class MobikwikWalletService implements WalletBaseService {

    private static final String EXISTING_USER_ACTION = "existingusercheck";
    private static final String MERCHANT_NAME = "Swiggy";
    private static final String DEFAULT_MID = "MBK6868";
    private static final String MSG_CODE_CHECK_USER = "500";
    private static final String MSG_CODE_GENERATE_OTP = "504";
    private static final String TOKEN_TYPE_FOR_TOKEN = "1";
    private static final String DEFAULT_AMOUNT = "10000";
    private static final String TOKEN_TYPE_FOR_OTP = "1";
    private static final String MSG_CODE_FOR_TOKEN = "507";
    private static final String MSG_CODE_FOR_BALANCE = "501";
    private static final String MSG_CODE_FOR_CREATE_USER = "502";
    private static final String MSG_CODE_FOR_DEBIT = "503";
    private static final String MSG_CODE_FOR_REGENERATE = "507";
    private static final String TRANSACTION_TYPE = "debit";
    private static final String MOBIKWIK_SECRET_KEY = "mobikwik_secret_key";
    private static final String MOBIKWIK_SI_SECRET_KEY = "mobikwik_si_secret_key";
    private static final String MOBIKWIK_MID = "mobikwik_mid";
    private static final int NOT_LINKED_STATUS_CODE = 2;
    public static final String SUCCESS = "success";
    private static final String MOBIKWIK = "Mobikwik";
    private static final String USER_TOKEN_BACKFILL_ENABLED_NEW = "user_token_backfill_enabled_new";

    private MobikwikClient mobikwikClient;

    @Autowired
    private MobikwikProperties mobikwikProperties;

    @Autowired
    private MobikwikRetrofitService retrofitService;

    @Autowired
    private PaymentConfiguration configuration;

    @Autowired
    private UserTokenDetailsDao userTokenDetailsDao;

    @Autowired
    private WalletTokenMigrationService walletTokenMigrationService;

    @PostConstruct
    public void postConstruct() {

        OkHttpClient httpClient = new OkHttpClient.Builder()
                .addNetworkInterceptor(new Interceptor() {
                    @Override
                    public okhttp3.Response intercept(Chain chain) throws IOException {
                        Request request = chain
                                .request()
                                .newBuilder()
                                .addHeader("payloadtype", "json")
                                .build();
                        return chain.proceed(request);
                    }
                })
//                .addInterceptor(new LoggingInterceptor())
                .addInterceptor(new HttpLoggingInterceptor(HTTPLogger.DEFAULT).setLevel(HttpLoggingInterceptor.Level.BODY))
                .connectTimeout(1000, TimeUnit.MILLISECONDS) // 1 second
                .readTimeout(5000, TimeUnit.MILLISECONDS) // 5 seconds
                .build();

        Retrofit retrofit = new Retrofit.Builder()
                .baseUrl(mobikwikProperties.getAuthUrl())
                .addConverterFactory(JacksonConverterFactory.create())
                .client(httpClient)
                .build();

        mobikwikClient = retrofit.create(MobikwikClient.class);

    }

    public boolean checkUserExists() throws SwiggyPaymentException {
        String mobile = Utility.getUserFromHeader().getMobile();
        String merchantId = configuration.getConfiguration(MOBIKWIK_MID, DEFAULT_MID);

        String checksum = getChecksumFromArgs(EXISTING_USER_ACTION, mobile, MERCHANT_NAME, merchantId, MSG_CODE_CHECK_USER);
        try {
            Call<CheckUserExistsResponse> userExistsResponse = mobikwikClient.checkUserExists(MERCHANT_NAME, merchantId,
                    mobile, EXISTING_USER_ACTION, MSG_CODE_CHECK_USER, checksum);

            BaseResponse baseResponse = new SuccessResponse();
            baseResponse.setData(retrofitService.getResponseOrLogWarning(userExistsResponse, "Could not check if user exists"));
        } catch (SwiggyPaymentException e) {
            log.info("User does not exist with mobile " + mobile, e);
            return false;
        }
        return true;
    }

    public BaseResponse generateOTP() throws SwiggyPaymentException {

        String mobile = Utility.getUserFromHeader().getMobile();
        String merchantId = configuration.getConfiguration(MOBIKWIK_MID, DEFAULT_MID);

        String checksum = getChecksumFromArgs(DEFAULT_AMOUNT, mobile, MERCHANT_NAME, merchantId, MSG_CODE_GENERATE_OTP, TOKEN_TYPE_FOR_OTP);
        Call<MobikwikBaseResponse> otpForLinkingWallet = mobikwikClient.generateOTP(MERCHANT_NAME, merchantId,
                mobile, DEFAULT_AMOUNT, TOKEN_TYPE_FOR_OTP, MSG_CODE_GENERATE_OTP, checksum);

        // TODO verify checksum
        BaseResponse baseResponse = new SuccessResponse();
        baseResponse.setData(retrofitService.getResponseOrLogWarning(otpForLinkingWallet, "Could not send otp."));
        return baseResponse;
    }


    public BaseResponse generateToken(String otp, String state) throws SwiggyPaymentException {
        if (checkUserExists()) {
            return generateToken(otp);
        } else {
            return createUser(otp);
        }
    }

    public BaseResponse generateToken(String otp) throws SwiggyPaymentException {

        String mobile = Utility.getUserFromHeader().getMobile();
        String merchantId = configuration.getConfiguration(MOBIKWIK_MID, DEFAULT_MID);

        String checksum = getChecksumFromArgs(DEFAULT_AMOUNT, mobile, MERCHANT_NAME, merchantId, MSG_CODE_FOR_TOKEN, otp, TOKEN_TYPE_FOR_TOKEN);
        Call<MobikwikTokenResponse> tokenResponseCall = mobikwikClient.generateToken(MERCHANT_NAME, merchantId, mobile,
                DEFAULT_AMOUNT, otp, TOKEN_TYPE_FOR_TOKEN, MSG_CODE_FOR_TOKEN, checksum);

        MobikwikTokenResponse response = retrofitService.getResponseOrLogWarning(tokenResponseCall, "Could not generate token.");
        saveInDB(response);
        return new SuccessResponse(response.getToken());
    }


    public BaseResponse checkBalance() throws SwiggyPaymentException {
        UserTokenDetailsEntity userTokenDetails = validateToken();
        String mobile = Utility.getUserFromHeader().getMobile();
        String token = userTokenDetails.getToken();
        String merchantId = configuration.getConfiguration(MOBIKWIK_MID, DEFAULT_MID);

        String checksum = getChecksumFromArgs(mobile, MERCHANT_NAME, merchantId, MSG_CODE_FOR_BALANCE, token);
        Call<CheckBalanceResponse> checkBalance = mobikwikClient.checkBalance(MERCHANT_NAME, merchantId, mobile,
                token, MSG_CODE_FOR_BALANCE, checksum);

        BaseResponse baseResponse = new SuccessResponse();
        baseResponse.setData(retrofitService.getResponseOrLogWarning(checkBalance, "Could not check balance."));
        return baseResponse;
    }

    public BaseResponse createUser(String otp) throws SwiggyPaymentException {

        String mobile = Utility.getUserFromHeader().getMobile();
        String email = Utility.getUserFromHeader().getEmail();

        String merchantId = configuration.getConfiguration(MOBIKWIK_MID, DEFAULT_MID);
        String checksum = getChecksumFromArgs(mobile, email, MERCHANT_NAME, merchantId, MSG_CODE_FOR_CREATE_USER, otp);
        Call<MobikwikTokenResponse> createWalletResponse = mobikwikClient.createWallet(MERCHANT_NAME, merchantId, mobile,
                email, otp, MSG_CODE_FOR_CREATE_USER, checksum);

        MobikwikTokenResponse tokenResponse = retrofitService.getResponseOrLogWarning(createWalletResponse, "Could not create user.");
        saveInDB(tokenResponse);
        return new SuccessResponse(tokenResponse.getToken());
    }

    @Trace

    public BaseResponse debitAmount(String amount, String orderId, String couponId) throws SwiggyPaymentException {
        UserTokenDetailsEntity userTokenDetailsEntity = validateToken();
        String orderComments = "SWIGGY-" + orderId;
        User user = Utility.getUserFromHeader();
        String mobile = user.getMobile();
        String token = userTokenDetailsEntity.getToken();
        String merchantId = configuration.getConfiguration(MOBIKWIK_MID, DEFAULT_MID);
        String checksum = getChecksumFromArgs(amount, mobile, orderComments, MERCHANT_NAME, merchantId, MSG_CODE_FOR_DEBIT,
                orderId, token, TRANSACTION_TYPE);
        Call<MobikwikBaseResponse> debitResponse = mobikwikClient.debitAmount(MERCHANT_NAME, merchantId, mobile,
                token, amount, orderComments, orderId, TRANSACTION_TYPE, MSG_CODE_FOR_DEBIT, checksum);
        BaseResponse baseResponse = new SuccessResponse();
        baseResponse.setData(retrofitService.getResponseOrLogWarning(debitResponse, "Could not debit amount."));
        return baseResponse;
    }

    public BaseResponse regenerateToken(TransactionPaymentDetails paymentDetails) throws SwiggyPaymentException {

        String mobile = Utility.getUserFromHeader().getMobile();
        UserTokenDetailsEntity userTokenDetails = validateToken();
        String token = userTokenDetails.getToken();
        String merchantId = configuration.getConfiguration(MOBIKWIK_MID, DEFAULT_MID);
        String checksum = getChecksumFromArgsWithSI(null, true, mobile, MERCHANT_NAME, merchantId, MSG_CODE_FOR_REGENERATE, token, TOKEN_TYPE_FOR_TOKEN);
        Call<MobikwikTokenResponse> tokenResponse = mobikwikClient.regenerateToken(MERCHANT_NAME, merchantId, mobile,
                token, TOKEN_TYPE_FOR_TOKEN, MSG_CODE_FOR_REGENERATE, checksum);
        BaseResponse baseResponse = new SuccessResponse();
        MobikwikTokenResponse response = retrofitService.getResponseOrLogWarning(tokenResponse, "Could not regenerate token.");
        baseResponse.setData(response.getToken());
        log.info("MOBIKWICK NEW TOEKN {}", response.getToken());
        saveInDB(response);
        paymentDetails.setWalletSsoToken(response.getToken());
        return baseResponse;
    }

    public RefundPaasResponse refundAmount(String orderId, String amount) throws SwiggyPaymentException {
        String merchantId = configuration.getConfiguration(MOBIKWIK_MID, DEFAULT_MID);
        String checksum = getChecksumFromArgs(merchantId, orderId, amount);
        Call<MobikwikBaseResponse> mobikwikRefundCall = mobikwikClient.refund(MERCHANT_NAME, merchantId, orderId, amount, checksum);
        RefundPaasResponse refundResponse = new RefundPaasResponse();
        MobikwikBaseResponse response = retrofitService.getResponseOrLogWarning(mobikwikRefundCall, "Could not refund.");
        refundResponse.getData().setMobikwikRefundResponse(response);
        return refundResponse;
    }

    public RefundPaasResponse refundAmountForMid(String orderId, String amount, String merchantId, String secretKey) throws SwiggyPaymentException {
        String checksum = getChecksumFromArgsWithSI(secretKey, false, merchantId, orderId, amount);
        Call<MobikwikBaseResponse> mobikwikRefundCall = mobikwikClient.refund(MERCHANT_NAME, merchantId, orderId, amount, checksum);
        RefundPaasResponse refundResponse = new RefundPaasResponse();
        MobikwikBaseResponse response = retrofitService.getResponseOrLogWarning(mobikwikRefundCall, "Could not refund.");
        refundResponse.getData().setMobikwikRefundResponse(response);
        return refundResponse;
    }

    public BaseResponse delink() throws SwiggyPaymentException {
        UserTokenDetailsEntity userTokenDetailsEntity = validateToken();
        userTokenDetailsDao.delete(userTokenDetailsEntity);
        Utility.getUserFromHeader().setMobikwikSsoToken("");
        return new SuccessResponse();
    }

    public BaseResponse getSSOToken() throws SwiggyPaymentException {
        MobikwikTokenCheckResponse response = new MobikwikTokenCheckResponse();

        String userId = Utility.getUserFromHeader().getCustomerId();
        UserTokenDetailsEntity userTokenDetails = userTokenDetailsDao.findByUserIdAndPaymentMethodName(Utility.getLong(userId), MOBIKWIK);

        //TODO -- to be removed after token migration
        if (null == userTokenDetails && !Utility.isEmpty(Utility.getUserFromHeader().getMobikwikSsoToken()) && configuration.getConfigurationAsBoolean(USER_TOKEN_BACKFILL_ENABLED_NEW, true)) {
            userTokenDetails = walletTokenMigrationService.saveUserTokenDetails(userId, Utility.getUserFromHeader().getMobikwikSsoToken(), MOBIKWIK);
        }
        if (null != userTokenDetails) {
            response.setHasToken(true);
            return new SuccessResponse(response);
        }
        return new ErrorResponse("Couldn't find user token");
    }

    @Override
    public BaseResponse resendOTP(WalletResendOTPRequest walletResendOTPRequest) throws SwiggyPaymentException {
        //Not supported by Mobikwik
        return null;
    }

    @Override
    public BaseResponse generateRegisterUserToken(WalletRegisterNewUserRequest walletRegisterNewUserRequest) throws SwiggyPaymentException {
        //Not supported by Mobikwik
        return null;
    }

    @Override
    public BaseResponse generateLoginToken() throws SwiggyPaymentException {
        //Not supported by Mobikwik
        return null;
    }

    @Override
    public BaseResponse computeChecksum(WalletCalculateChecksumRequest request) throws SwiggyPaymentException {
        UserTokenDetailsEntity userTokenDetails = validateToken();
        validateRequest(request);

        String token = userTokenDetails.getToken();
        String mobile = Utility.getUserFromHeader().getMobile();
        String merchantId = configuration.getConfiguration(MOBIKWIK_MID, DEFAULT_MID);

        String checksum = getChecksumFromArgs(request.getAmount(), mobile, MERCHANT_NAME, merchantId, request.getOrderid(), request.getRedirectUrl(), token);
        BaseResponse baseResponse = new SuccessResponse();
        baseResponse.setData(new MobikwikChecksumResponse(checksum, token));
        return baseResponse;
    }

    private void validateRequest(WalletCalculateChecksumRequest request) throws SwiggyPaymentException {
        if (!StringUtils.hasText(request.getOrderid()) || !StringUtils.hasText(request.getAmount())
                || !StringUtils.hasText(request.getRedirectUrl())) {
            throw new SwiggyPaymentException(1, "Invalid request");
        }
    }

    private String getChecksumFromArgs(Object... values) throws SwiggyPaymentException {
        return getChecksumFromArgsWithSI(null, false, values);
    }

    private String getChecksumFromArgsWithSI(String secretKey, boolean useSIKey, Object... values) throws SwiggyPaymentException {
        if (secretKey == null) {
            if (useSIKey)
                secretKey = configuration.getConfiguration(MOBIKWIK_SI_SECRET_KEY, "R3gYJ46R1XQEJYYateiPqVc4DbIa");
            else
                secretKey = configuration.getConfiguration(MOBIKWIK_SECRET_KEY, "1CgYJ36R1xQEJZYSteixqVc4AbIh");// uat ju6tygh7u7tdg554k098ujd5468o
        }
        String checksum;
        String allParms = "";
        for (Object o : values) {
            if (o != null)
                allParms += "'" + o.toString() + "'";
        }
        log.debug(allParms);
        try {
            checksum = MobikwikService.calculateRFC2104HMAC(allParms, secretKey);
        } catch (SignatureException e) {
            log.error("Failed to generate checksum", e);
            throw new SwiggyPaymentException(1, "Failed to generate checksum");
        }
        log.debug(checksum);
        return checksum;
    }


    public UserTokenDetailsEntity validateToken() throws SwiggyPaymentException {
        String userId = Utility.getUserFromHeader().getCustomerId();
        UserTokenDetailsEntity userTokenDetails = userTokenDetailsDao.findByUserIdAndPaymentMethodName(Utility.getLong(userId), MOBIKWIK);

        //TODO -- to be removed after token migration
        if (null == userTokenDetails && !Utility.isEmpty(Utility.getUserFromHeader().getMobikwikSsoToken()) && configuration.getConfigurationAsBoolean(USER_TOKEN_BACKFILL_ENABLED_NEW, true)) {
            userTokenDetails = walletTokenMigrationService.saveUserTokenDetails(userId, Utility.getUserFromHeader().getMobikwikSsoToken(), MOBIKWIK);
        }

        if (null == userTokenDetails) {
            throw new SwiggyPaymentException(NOT_LINKED_STATUS_CODE, configuration.getConfiguration("mobikwik_account_not_linked", "User not linked with mobikwik"));
        }
        return userTokenDetails;
    }

    private void saveInDB(MobikwikTokenResponse response) {
        String userId = Utility.getUserFromHeader().getCustomerId();
        UserTokenDetailsEntity userTokenDetails = userTokenDetailsDao.findByUserIdAndPaymentMethodName(Utility.getLong(userId), MOBIKWIK);

        if (null != userTokenDetails) {
            userTokenDetailsDao.delete(userTokenDetails);
        }
        UserTokenDetailsEntity userTokenDetailsEntity = new UserTokenDetailsEntity();
        userTokenDetailsEntity.setUserId(Utility.getLong(Utility.getUserFromHeader().getCustomerId()));
        userTokenDetailsEntity.setPaymentMethodName(MOBIKWIK);
        userTokenDetailsEntity.setToken(response.getToken());
        userTokenDetailsEntity.setExpiresOn(new Timestamp(new java.util.Date().getTime()));
        userTokenDetailsEntity.setRefreshToken("");
        userTokenDetailsEntity.setCreatedOn(new Timestamp(new java.util.Date().getTime()));
        userTokenDetailsDao.save(userTokenDetailsEntity);
    }
}