package com.swiggy.checkout.data.model.dao;

import com.swiggy.checkout.data.entities.PaymentConfigurationEntity;
import com.swiggy.checkout.data.entities.PaymentRoutingEntity;
import com.swiggy.checkout.data.entities.ThirdPartyEntity;
import com.swiggy.checkout.data.entities.TransactionEntity;
import org.springframework.cache.annotation.Cacheable;
import org.springframework.data.jpa.repository.JpaRepository;

import java.util.List;

/**
 * Created by Manish on 05/02/17.
 */
public interface ThirdPartyDao extends JpaRepository<ThirdPartyEntity, Integer> {

    @Cacheable(value = "ThirdPartyDao", cacheManager = "redisCacheManagerPayments")
    ThirdPartyEntity findOneByToken(String token);

    @Cacheable(value = "ThirdPartyDao", cacheManager = "redisCacheManagerPayments")
    ThirdPartyEntity findOneBySource(String source);
}
