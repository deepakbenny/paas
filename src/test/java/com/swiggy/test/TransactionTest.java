package com.swiggy.test;

import com.swiggy.checkout.controller.TransactionController;
import com.swiggy.checkout.data.entities.TransactionEntity;
import com.swiggy.checkout.data.model.dao.TransactionDao;
import com.swiggy.checkout.exception.SwiggyPaymentException;
import com.swiggy.checkout.extenal.entities.request.CreateTransactionRequest;
import com.swiggy.checkout.extenal.entities.response.BaseResponse;
import com.swiggy.checkout.pojo.User;
import com.swiggy.checkout.service.TransactionService;
import com.swiggy.checkout.service.VerifyTransactionService;
import com.swiggy.checkout.utils.Utility;
import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.orm.jpa.AutoConfigureDataJpa;
import org.springframework.boot.test.autoconfigure.orm.jpa.AutoConfigureTestEntityManager;
import org.springframework.boot.test.autoconfigure.restdocs.AutoConfigureRestDocs;
import org.springframework.boot.test.autoconfigure.web.servlet.WebMvcTest;
import org.springframework.http.MediaType;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.request.MockHttpServletRequestBuilder;
import org.springframework.transaction.annotation.Transactional;

import javax.servlet.http.HttpServletRequest;
import java.io.IOException;

/**
 * Created by Manish on 05/02/17.
 */


@RunWith(SpringJUnit4ClassRunner.class)
@ContextConfiguration(classes = {TestApplication.class})
@AutoConfigureTestEntityManager
@Transactional
@AutoConfigureDataJpa
public class TransactionTest {

    @Autowired
    HttpServletRequest request;

    @Autowired
    TransactionService transactionService;

    @Autowired
    VerifyTransactionService verifyTransactionService;

    @Autowired
    TransactionDao transactionDao;

    private static String orderId;

    private static final Logger logger = LoggerFactory.getLogger(TransactionController.class);

    @Before
    public void initialize() {
        orderId = "781710";
    }




    @Test
    public void Test(){
        Assert.assertEquals("781710",orderId);
    }


    @Test
    public void createTransactionTest() {

        CreateTransactionRequest createTransactionRequest=CreateTransactionRequest.builder().amount("300").orderId(orderId).paymentMethod("Cash").build();
        request.setAttribute("userAgent","Swiggy-Android");
        request.setAttribute("user", User.builder().customerId("1563").email("manish.kumar@swiggy.in").paytmSsoToken("paytm_sso_token")
              .mobikwikSsoToken("mobikwick-sso-token").freechargeSsoToken("free-charge-token").mobile("8105025755").build());
        try {
            transactionService.createTransaction(createTransactionRequest);
        }
        catch(SwiggyPaymentException e){
            logger.error("Error in Create transaction test Test Case", e);
        }

        TransactionEntity transactionEntity =transactionDao.findOneByOrderId(Long.valueOf(orderId));
        Assert.assertEquals(orderId,transactionEntity.getOrderId());

    }

    @Test
    public void verifyTransactionTest() {

        try {
            CreateTransactionRequest createTransactionRequest=CreateTransactionRequest.builder().amount("300").orderId(orderId).paymentMethod("Cash").build();
            BaseResponse response = verifyTransactionService.verifyTransaction(orderId);
            int code = response.getStatusCode();
            Assert.assertEquals(0,code);
        }catch(Exception e){
            logger.error("Error in Verify Test Case", e);
        }

    }

}
