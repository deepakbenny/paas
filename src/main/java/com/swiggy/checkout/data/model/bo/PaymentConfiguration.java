package com.swiggy.checkout.data.model.bo;

import com.swiggy.checkout.data.entities.PaymentConfigurationEntity;
import org.springframework.stereotype.Service;

import java.util.List;
import java.util.Map;

/**
 * Created by Manish on 23/01/17.
 */
@Service
public interface PaymentConfiguration {

    List<PaymentConfigurationEntity> getAllConfigurations();

    String getConfiguration(String s,String defaultObject);

    boolean getConfigurationAsBoolean(String s, boolean defaultObject);

    Integer getConfigurationAsInt(String s, int defaultObject);

    Long getConfigurationAsLong(String s, long defaultObject);

    Float getConfigurationAsFloat(String s, float defaultObject);

    public void saveConfiguration(String key,String name);
}
