package com.swiggy.checkout.data.guava;

import com.google.common.cache.CacheBuilder;
import com.google.common.cache.CacheLoader;
import com.swiggy.checkout.common.ConfigurationsUtils;
import com.swiggy.checkout.data.entities.PaymentConfigurationEntity;
import com.swiggy.checkout.data.model.dao.PaymentConfigurationDao;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;
import org.slf4j.Logger;
import com.google.common.cache.LoadingCache;

import java.util.HashMap;
import java.util.List;
import java.util.concurrent.TimeUnit;

/**
 * Created by Manish on 24/01/17.
 */

@Component
public class ConfigurationCache {

    private final LoadingCache<String, ConfigurationMap> cache;
    private static final String CONFIGURATION_KEY = "payment_configurations";
    private static final long expiryTime = -1;
    private static final long GUAVA_EXPIRY_TIME = 300;
    private static final long REDIS_EXPIRY_TIME = 600;
    private static final Logger logger = LoggerFactory.getLogger(ConfigurationCache.class);


    @Autowired
    PaymentConfigurationDao paymentConfigurationDao;

    @Autowired
    ConfigurationsUtils configurationsUtils;

    public ConfigurationCache() {
        cache = CacheBuilder.newBuilder()
                .maximumSize(1000)
                .expireAfterWrite(GUAVA_EXPIRY_TIME, TimeUnit.SECONDS)
                .<String, ConfigurationMap>build(new CacheLoader<String, ConfigurationMap>() {
                    public ConfigurationMap load(String k1) throws Exception {
                        logger.info("In Guava Loader");
                        return loadCache();
                    }
                });
    }


    public ConfigurationMap getEntry() {
        return (ConfigurationMap) cache.getUnchecked(CONFIGURATION_KEY);
    }

    private ConfigurationMap loadCache() {
        return new ConfigurationMap(loadFromRedis());
    }

    public HashMap<String, String> loadFromRedis() {
        logger.info("getting from REDIS");
        HashMap<String, String> configuratinsMap = configurationsUtils.getConfigurationFromRedis(CONFIGURATION_KEY);
        if (configuratinsMap == null || configuratinsMap.size() == 0) {
            configuratinsMap = loadFromDB();
            configurationsUtils.setConfigurationForRedis(CONFIGURATION_KEY, configuratinsMap, REDIS_EXPIRY_TIME);
        }
        return configuratinsMap;
    }

    HashMap<String, String> loadFromDB() {
        List<PaymentConfigurationEntity> configurationList = paymentConfigurationDao.findAll();
        HashMap<String, String> configurations = new HashMap<>();
        if (configurationList == null) {
            return configurations;
        }
        for (PaymentConfigurationEntity paymentConfigurationEntity : configurationList) {
            configurations.put(paymentConfigurationEntity.getKey(), paymentConfigurationEntity.getValue());
        }
        return configurations;
    }


}
