package com.swiggy.checkout.data.model.dao;

import com.swiggy.checkout.data.entities.BinBaseEntity;
import com.swiggy.rest.dao.CrudDao;
import org.springframework.cache.annotation.Cacheable;
import org.springframework.data.jpa.repository.JpaRepository;

/**
 * Created by Manish on 02/02/17.
 */
public interface BinBaseDao extends JpaRepository<BinBaseEntity, Long> {

    @Cacheable(value = "BinBase", cacheManager = "redisCacheManagerPayments")
    BinBaseEntity getBinBaseEntityByBin(long bin);
}
