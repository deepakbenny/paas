package com.swiggy.checkout.service;

import com.swiggy.checkout.exception.ThirdPartyError;
import com.swiggy.checkout.exception.ThirdPartyException;
import com.swiggy.checkout.utils.Utility;
import lombok.extern.slf4j.Slf4j;
import org.springframework.stereotype.Service;
import retrofit2.Call;
import retrofit2.Response;

import java.io.IOException;

/**
 * Created by Manish on 23/01/17.
 */

@Slf4j
@Service
public class RetrofitService {

    public <T> T getResponseOrLogWarning(Call<T> call, ThirdPartyError failMessage) throws ThirdPartyException {
        try {
            return getResponse(call);
        } catch (ThirdPartyException e) {
            throw new ThirdPartyException(failMessage, e.getDetailMessage());
        }
    }

    private <T> T getResponse(Call<T> call) throws ThirdPartyException {
        try {
            Response<T> execute = call.execute();

            if (execute.isSuccessful()) {
                log.info(Utility.jsonEncode(execute.body()));
                return execute.body();

            } else {
                throw new ThirdPartyException(1, "Error", Utility.jsonEncode(execute.errorBody()));
            }
        } catch (IOException e) {
            throw new ThirdPartyException(1, e.getMessage());
        }
    }
}