package com.swiggy.checkout.data.model.dao;

import com.swiggy.checkout.data.entities.PaymentConfigurationEntity;
import com.swiggy.checkout.data.entities.TransactionEntity;
import com.swiggy.rest.dao.CrudDao;
import org.springframework.cache.annotation.CacheEvict;
import org.springframework.cache.annotation.CachePut;
import org.springframework.cache.annotation.Cacheable;
import org.springframework.data.jpa.domain.Specification;
import org.springframework.data.jpa.repository.JpaRepository;


import java.util.List;

/**
 * Created by Manish on 29/01/17.
 */

public interface TransactionDao extends JpaRepository<TransactionEntity, Long> {

    @Override
    @CacheEvict(value = "Transactions", cacheManager = "redisCacheManagerPayments", key = "#p0.orderId")
    TransactionEntity save(TransactionEntity transactionEntity);

    @Cacheable(value = "Transactions", cacheManager = "redisCacheManagerPayments", key = "#p0")
    TransactionEntity findOneByOrderId(Long orderId);
}
