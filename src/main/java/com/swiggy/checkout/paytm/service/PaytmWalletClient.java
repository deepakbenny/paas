package com.swiggy.checkout.paytm.service;

import com.swiggy.checkout.paytm.entities.response.BlockAmountResponse;
import com.swiggy.checkout.paytm.entities.response.CheckBalanceResponse;
import com.swiggy.checkout.paytm.entities.response.RefundResponse;
import com.swiggy.checkout.paytm.entities.response.WithdrawAmountResponse;
import retrofit2.Call;
import retrofit2.http.GET;
import retrofit2.http.POST;
import retrofit2.http.Query;

/**
 * Created by Manish on 03/02/17.
 */
public interface PaytmWalletClient {

    @GET("HANDLER_INTERNAL/checkBalance")
    Call<CheckBalanceResponse> checkBalance(@Query("JsonData") String jsonData);

    @POST("HANDLER_FF/withdrawScw")
    Call<WithdrawAmountResponse> withdrawBlockedAmount(@Query(value = "JsonData", encoded = true) String jsonData);

    @POST("HANDLER_INTERNAL/REFUND")
    Call<RefundResponse> refund(@Query(value = "JsonData", encoded = true) String jsonData);
}
