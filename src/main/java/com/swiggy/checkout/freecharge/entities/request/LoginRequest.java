package com.swiggy.checkout.freecharge.entities.request;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import com.fasterxml.jackson.annotation.JsonInclude;
import lombok.*;

/**
 * Created by Manish on 27/01/17.
 */

@Setter
@Getter
@NoArgsConstructor
@AllArgsConstructor
@Builder
@JsonInclude(JsonInclude.Include.NON_NULL)
@JsonIgnoreProperties(ignoreUnknown = true)
public class LoginRequest {
    private String merchantId;
    private String otpId;
    private String otp;
    private String userAgent;
    private String userMachineIdentifier;
    private String checksum;
    private String channel;
}
