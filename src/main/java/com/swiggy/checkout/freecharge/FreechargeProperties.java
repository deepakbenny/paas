package com.swiggy.checkout.freecharge;

import lombok.Getter;
import lombok.Setter;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.context.annotation.Configuration;

/**
 * Created by dhavaljoshi on 8/5/16.
 */
@Configuration
@Getter
@Setter
public class FreechargeProperties {

    @Value("${freecharge.loginEndpoint: https://login.freecharge.in/api}")
    private String loginEndpoint;

    @Value("${freecharge.checkoutEndpoint: https://checkout.freecharge.in/api}")
    private String checkoutEndpoint;

    @Value("${freecharge.SecretKey}")
    private String secretKey;

    @Value("${freecharge.mid}")
    private String mid;
}
