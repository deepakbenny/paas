
package com.swiggy.checkout.service;

import com.swiggy.checkout.binbasedrouting.BinNumberDetailsUtility;
import com.swiggy.checkout.binbasedrouting.binupdatelistener.BinDetailUpdateSender;
import com.swiggy.checkout.data.entities.BinNumberDetailsEntity;
import com.swiggy.checkout.data.entities.PaymentRoutingEntity;
import com.swiggy.checkout.data.model.bo.PaymentConfiguration;
import com.swiggy.checkout.data.model.dao.BinNumberDetailsDao;
import com.swiggy.checkout.data.model.dao.PaymentRoutingDao;
import com.swiggy.checkout.utils.Utility;
import lombok.extern.slf4j.Slf4j;
import org.apache.commons.collections.CollectionUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.*;
import java.util.concurrent.ConcurrentHashMap;


/**
 * Created by Manish on 02/02/17.
 */


@Service
@Slf4j
public class RoutingService {

    private static final String MAP_KEY_FORMAT = "%s_%s_%s";

    private ConcurrentHashMap<String, Random> randomNumberGenMap = new ConcurrentHashMap<>();

    @Autowired
    BinDetailUpdateSender binDetailUpdateSender;

    @Autowired
    private BinNumberDetailsDao binNumberDetailsDao;

    @Autowired
    private PaymentRoutingDao paymentRoutingDao;

    @Autowired
    private PaymentConfiguration configuration;

    //Ordering in reverse order
    private static Comparator<PaymentRoutingEntity> RouteListComparator = new Comparator<PaymentRoutingEntity>() {
        @Override
        public int compare(PaymentRoutingEntity o1, PaymentRoutingEntity o2) {
            if (o1 == null && o2 == null) {
                return 0;
            } else if (o1 == null) {
                return 1;
            } else if (o2 == null) {
                return -1;
            }
            return o2.getPercentage() - o1.getPercentage();

        }
    };

    /*
     * Used for routing to PG for the orders when payment method is Juspay (CARD)
     * 1. Get the bank, payment type, card type using bin_number_details table and route to the specific gateway using routing
     *    configuration given in payment_routing table.
     * 2. Do the cumulative sum of the traffic percentage and define the bucket based on random number generator.
     * @param bin Card first 6 digit number
     * @return Gateway redirection
     */

    public Gateway routeToGateway(long bin) {
        Gateway defaultGateway = Gateway.getGateway(configuration.getConfiguration("default-payment-gateway-card", "PAYU"));
        BinNumberDetailsEntity binEntity = binNumberDetailsDao.getBinNumberDetailsEntitiesByBin(bin);

        if (bin > 0 && BinNumberDetailsUtility.isUpdateRequired(binEntity)) {
            binDetailUpdateSender.sendBinToQueueAsync(bin);
        }

        if (binEntity == null) {
            log.info("[ROUTING SERVICE] Missing bin in bin_number_details {}", bin);
            return defaultGateway;
        }


        //bank and bin type must be there for routing
        log.info("[ROUTING SERVICE] bin bank name {} & type {} & brand {}", binEntity.getBank(),
                binEntity.getType(), binEntity.getBrand());

        String bank = binEntity.getBank();
        String brand = binEntity.getBrand();
        String type = binEntity.getType();



        if (Utility.isEmpty(bank)) {
            log.info("[ROUTING SERVICE] SETTING DEFAULT BANK 1");
            bank = PaymentRoutingEntity.DEFAULT_BANK;
        }


        if (Utility.isEmpty(brand)) {
            log.info("[ROUTING SERVICE] SETTING DEFAULT BRAND 1");
            brand = PaymentRoutingEntity.DEFAULT_BRAND;
        }

        if (Utility.isEmpty(type)) {
            log.info("[ROUTING SERVICE] SETTING DEFAULT CARD_TYPE 1");
            type = PaymentRoutingEntity.DEFAULT_CC_DC;
        }

        List<PaymentRoutingEntity> routeList = paymentRoutingDao.findByCardTypeAndPaymentTypeAndBankAndEnabledTrue(brand,
                type, bank);


        /* For a given bank there is always a brand
          That is why we are setting first bank as Default.
        */
        if (CollectionUtils.isEmpty(routeList)) {
            if (!bank.equals(PaymentRoutingEntity.DEFAULT_BANK)) {
                bank = PaymentRoutingEntity.DEFAULT_BANK;
                routeList =  paymentRoutingDao.findByCardTypeAndPaymentTypeAndBankAndEnabledTrue(brand,
                        type, bank);
            }
        }

        if (CollectionUtils.isEmpty(routeList)) {
            if (!brand.equals(PaymentRoutingEntity.DEFAULT_BRAND)) {
                brand = PaymentRoutingEntity.DEFAULT_BRAND;
                routeList =  paymentRoutingDao.findByCardTypeAndPaymentTypeAndBankAndEnabledTrue(brand,
                        type, bank);
            }
        }

        /*
           CREDIT/DEBIT will be replaced with Card
        */

        if (CollectionUtils.isEmpty(routeList)) {
            if (!type.equals(PaymentRoutingEntity.DEFAULT_CC_DC)) {
                type = PaymentRoutingEntity.DEFAULT_CC_DC;
                routeList = paymentRoutingDao.findByCardTypeAndPaymentTypeAndBankAndEnabledTrue(brand,
                        type, bank);
            }
        }


        return decidePaymentGatewayRandom(routeList, defaultGateway,
                getMapRandomKey(bank, type, brand));
    }


    /*
     *
     * @param bankName
     * @return
     */

    public Gateway routeToGateway(String bankName) {
        Gateway defaultGateway = Gateway.getGateway(configuration.getConfiguration("default-payment-gateway", "PAYU"));
        log.info("[ROUTING SERVICE] net-banking getting called for nb_bank : ", bankName);
        List<PaymentRoutingEntity> routeList = paymentRoutingDao.findByBankAndPaymentTypeAndEnabledTrue(bankName, PaymentRoutingEntity.NET_BANKING);


        if (!CollectionUtils.isEmpty(routeList)) {
            return decidePaymentGatewayRandom(routeList, defaultGateway,
                    getMapRandomKey(bankName, PaymentRoutingEntity.NET_BANKING, PaymentRoutingEntity.DEFAULT_BRAND));
        } else {
            log.info("[ROUTING SERVICE] checking for null route list for nb_bank : {}", bankName);
            routeList = paymentRoutingDao.findByBankAndPaymentTypeAndEnabledTrue(PaymentRoutingEntity.DEFAULT_BANK, PaymentRoutingEntity.NET_BANKING);
        }

        if (!CollectionUtils.isEmpty(routeList)) {
            log.info("[ROUTING SERVICE] Checking NetBanking Default Rule");
            return decidePaymentGatewayRandom(routeList, defaultGateway,
                    getMapRandomKey(PaymentRoutingEntity.DEFAULT_BANK, PaymentRoutingEntity.NET_BANKING, PaymentRoutingEntity.DEFAULT_BRAND));
        }
        log.info("[ROUTING SERVICE] RETURNING DEFAULT GATEWAY {}", defaultGateway);

        return defaultGateway;
    }

    private String getMapRandomKey(String bank, String type, String brand) {
        return String.format(MAP_KEY_FORMAT, bank, type, brand).toLowerCase();
    }

    private Gateway decidePaymentGatewayRandom(List<PaymentRoutingEntity> routeList, Gateway defaultGateway, String randomKey) {
        if (CollectionUtils.isEmpty(routeList)) {
            log.info("[ROUTING SERVICE] RETURNING DEFAULT GATEWAY {}", defaultGateway);
            return defaultGateway;
        }

        //Sorting is in reverse order so that we can override multiple routing mapping to single by assigning value > 100

        routeList.sort(RouteListComparator);

        Random random;
        if (!randomNumberGenMap.containsKey(randomKey)) {
            random = new Random(25);
            Random oldAssignedRandom = randomNumberGenMap.putIfAbsent(randomKey, random);
            if (oldAssignedRandom != null) {
                random = oldAssignedRandom;
            }
        } else {
            random = randomNumberGenMap.get(randomKey);
        }

        int randomValue = random.nextInt(100) + 1;
        log.info("[ROUTING SERVICE] randomValue DEFAULT GATEWAY {}", randomValue);

        for (PaymentRoutingEntity routingEntity : routeList) {
            if (randomValue <= routingEntity.getPercentage()) {
                Gateway gateway = Gateway.getGateway(routingEntity.getPaymentGateway());
                if (gateway != null) {
                    log.info("[ROUTING SERVICE] RETURNING GATEWAY {}: {}", randomKey, gateway);
                    return gateway;
                } else {
                    log.info("[ROUTING SERVICE] RETURNING GATEWAY : WAS NULL for value {} : {}", randomKey, routingEntity.getPaymentGateway());
                }
                break;
            } else {
                randomValue -= routingEntity.getPercentage();
            }
        }

        log.info("[ROUTING SERVICE] RETURNING DEFAULT GATEWAY randomKey:{} gateWay: {}", randomKey, defaultGateway);
        return defaultGateway;
    }

}

