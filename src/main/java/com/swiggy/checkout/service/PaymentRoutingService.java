package com.swiggy.checkout.service;

import com.swiggy.checkout.data.entities.PaymentRoutingEntity;
import com.swiggy.checkout.data.entities.PaymentRoutingHistoryEntity;
import com.swiggy.checkout.data.model.dao.PaymentRoutingDao;
import com.swiggy.checkout.data.model.dao.PaymentRoutingHistoryDao;
import com.swiggy.checkout.extenal.entities.request.PaymentRoutingRequest;
import com.swiggy.checkout.extenal.entities.response.BaseResponse;
import com.swiggy.checkout.extenal.entities.response.SuccessResponse;
import com.swiggy.checkout.utils.Utility;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.sql.Timestamp;
import java.time.LocalDateTime;
import java.util.List;

/**
 * Created by Manish on 03/02/17.
 */

@Service
public class PaymentRoutingService {

    @Autowired
    private PaymentRoutingDao paymentRoutingDao;

    @Autowired
    private PaymentRoutingHistoryDao paymentRoutingHistoryDao;

    public BaseResponse addRoutingConfiguration(PaymentRoutingRequest paymentRoutingRequest) {
        //TODO Handle net banking case

        //Card Type Contains Brand Name
        if (Utility.isEmpty(paymentRoutingRequest.getCardType())) {
            paymentRoutingRequest.setCardType(PaymentRoutingEntity.DEFAULT_BRAND);
        }
        //Payment Type Credit/Debit
        if (Utility.isEmpty(paymentRoutingRequest.getPaymentType())) {
            paymentRoutingRequest.setPaymentType(PaymentRoutingEntity.DEFAULT_CC_DC);
        }
        if (Utility.isEmpty(paymentRoutingRequest.getIssuingBank())) {
            paymentRoutingRequest.setIssuingBank(PaymentRoutingEntity.DEFAULT_BANK);
        }

        if (Utility.isEmpty(paymentRoutingRequest.getGateway())) {
            paymentRoutingRequest.setGateway(PaymentRoutingEntity.DEFAULT_GATEWAY);
        }


        if(paymentRoutingRequest.getPercentage() == null || paymentRoutingRequest.getPercentage() < 0){
            paymentRoutingRequest.setPercentage(0);
        }

        if(paymentRoutingRequest.getEnabled() == null){
            paymentRoutingRequest.setEnabled(false);
        }


        List routeList = paymentRoutingDao.findByCardTypeAndPaymentTypeAndBankAndPaymentGateway(paymentRoutingRequest.getCardType(), paymentRoutingRequest.getPaymentType(),
                paymentRoutingRequest.getIssuingBank(), paymentRoutingRequest.getGateway());

        PaymentRoutingEntity paymentRoutingEntity = PaymentRoutingEntity.builder()
                .cardType(paymentRoutingRequest.getCardType())
                .paymentType(paymentRoutingRequest.getPaymentType())
                .bank(paymentRoutingRequest.getIssuingBank())
                .paymentGateway(paymentRoutingRequest.getGateway())
                .percentage(paymentRoutingRequest.getPercentage())
                .updatedBy(paymentRoutingRequest.getUpdatedBy())
                .createdOn(Timestamp.valueOf(LocalDateTime.now()))
                .enabled(paymentRoutingRequest.getEnabled())
                .build();

        //check if such configuration is already present in DB, if yes then update current table and add entry to the history table,
        //else add entry to the payment_routing table
        if(routeList == null || routeList.size() == 0) {


            paymentRoutingDao.save(paymentRoutingEntity);
        } else {

            PaymentRoutingEntity routingEntity = (PaymentRoutingEntity) routeList.get(0);

            paymentRoutingHistoryDao.save(PaymentRoutingHistoryEntity.builder()
                    .cardType(routingEntity.getCardType())
                    .paymentType(routingEntity.getPaymentType())
                    .bank(routingEntity.getBank())
                    .paymentGateway(routingEntity.getPaymentGateway())
                    .percentage(routingEntity.getPercentage())
                    .updatedBy(routingEntity.getUpdatedBy())
                    .fromTime(routingEntity.getCreatedOn())
                    .toTime(Timestamp.valueOf(LocalDateTime.now()))
                    .build());
            paymentRoutingEntity.setId(routingEntity.getId());
            paymentRoutingDao.save(paymentRoutingEntity);
        }

        return new SuccessResponse();
    }

}
