package com.swiggy.checkout.common;

import com.swiggy.checkout.controller.TransactionController;
import com.swiggy.checkout.utils.Utility;
import com.swiggy.commons.redis.RedisCacheService;
import org.apache.commons.collections.map.HashedMap;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import java.util.HashMap;
import java.util.Map;

/**
 * Created by Manish on 24/01/17.
 */

@Component
public class ConfigurationsUtils {

    private static final Logger logger = LoggerFactory.getLogger(ConfigurationsUtils.class);
    private static final String ERROR_MSG = "Error in Setting Configuration For Redis";

    @Autowired
    private RedisCacheService redisCacheService;

    public HashMap<String,String> getConfigurationFromRedis(String key) {
        try {
            return Utility.jsonToStringMap(redisCacheService.get(key));
        } catch (Exception e) {
            return  null;
        }
    }

    public void setConfigurationForRedis(String key, Map<String,String> value,long time) {
        try {
            redisCacheService.set(key,Utility.jsonEncode(value),time);
        } catch (Exception e) {
            logger.error(ERROR_MSG, e);
        }

    }




}
