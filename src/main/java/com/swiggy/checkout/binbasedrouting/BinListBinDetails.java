package com.swiggy.checkout.binbasedrouting;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.annotation.JsonProperty;
import lombok.*;

@NoArgsConstructor
@AllArgsConstructor
@Getter
@Setter
@Builder
@JsonIgnoreProperties(ignoreUnknown = true)
@JsonInclude(JsonInclude.Include.NON_NULL)
public class BinListBinDetails {

    @JsonProperty("scheme")
    private String brand;

    @JsonProperty("number")
    private BinListNumber binNumber;

    @JsonProperty("type")
    private String type;

    @JsonProperty("brand")
    private String brand2;

    @JsonProperty("prepaid")
    private boolean prepaid;


    @JsonProperty("bank")
    private BinListBankDetails bank;

    @JsonProperty("country")
    private BinListCountryDetails binListCountryDetails;


}

