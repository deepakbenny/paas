package com.swiggy.checkout.service.refund;

import com.swiggy.checkout.Payu.entity.response.PayURefundResponse;
import com.swiggy.checkout.Payu.service.PayuService;
import com.swiggy.checkout.exception.SwiggyPaymentException;
import com.swiggy.checkout.freecharge.entities.responses.RefundPaasResponse;
import com.swiggy.checkout.razorpay.RazorPayService;
import com.swiggy.checkout.razorpay.pojo.RazorRefundResponse;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

/**
 * Created by Geetesh on 6/27/17.
 */
@Service
@Slf4j
public class RazorPayRefundService implements RefundBaseService {

    @Autowired
    private RazorPayService razorPayService;

    @Override
    public RefundPaasResponse refund(RefundPaasRequest request) throws SwiggyPaymentException {
        RazorRefundResponse razorRefundResponse = razorPayService.refundToPG(request.getTxnId(), request.getAmount());
        RefundPaasResponse refundPaasResponse = new RefundPaasResponse();
        refundPaasResponse.getData().setRazorRefundResponse(razorRefundResponse);
        return refundPaasResponse;
    }
}
