package com.swiggy.checkout.pojo.PayLater.LazyPay;

import com.fasterxml.jackson.annotation.JsonProperty;
import lombok.Builder;
import lombok.Data;

/**
 * Created by chirag.rc on 10/23/17.
 */
@Data
public class LazyPayAmountDetails {

    @JsonProperty("value")
    private String value;

    @JsonProperty("currency")
    private String currency;

    @Builder
    public LazyPayAmountDetails(String value, String currency) {
        this.value = value;
        this.currency = currency;
    }
}
