package com.swiggy.checkout.service.refund;

import com.swiggy.checkout.exception.SwiggyPaymentException;
import com.swiggy.checkout.service.ValidPaymentMethod;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

/**
 * Created by Geetesh on 6/27/17.
 */
@Service
@Slf4j
public class RefundServiceFactory {

    @Autowired
    private PayuRefundService payuRefundService;

    @Autowired
    private PaytmRefundService paytmRefundService;

    @Autowired
    private MobikwikRefundService mobikwikRefundService;

    @Autowired
    private MobikwikSsoRefundService mobikwikSsoRefundService;

    @Autowired
    private FreechargeRefundService freechargeRefundService;

    @Autowired
    private RazorPayRefundService razorPayRefundService;

    public  RefundBaseService getRefundService(String paymentMethod, String paymentGateway) throws SwiggyPaymentException{

        ValidPaymentMethod method = ValidPaymentMethod.getPaymentMethod(paymentMethod);
        if (method == null) {
            log.error("[REFUND SERVICE] Not a valid payment method to refund.");
            throw new SwiggyPaymentException(2, "Not a Valid Payment Method.");
        }

        switch (method) {
            case SODEXO:
            case JUSPAY:
            case JUSPAY_NB:
                if(paymentGateway!=null){
                    switch (Gateway.getGateway(paymentGateway)) {
                        case PAYU:
                            return payuRefundService;
                        case PAYTM:
                            return paytmRefundService;
                        case RAZORPAY:
                            return razorPayRefundService;
                        default:
                            log.error("[REFUND SERVICE] Not a valid payment gateway to refund.");
                            throw new SwiggyPaymentException(2, "Not a Valid Payment gateway.");
                    }
                }
                else{
                    log.error("[REFUND SERVICE] Not a valid payment gateway to refund.");
                    throw new SwiggyPaymentException(2, "Not a Valid Payment gateway.");
                }

            case PAYTM:
            case PAYTM_SSO:
                return paytmRefundService;

            case MOBIKWIK:
                return mobikwikRefundService;

            case MOBIKWIK_SSO:
                return mobikwikSsoRefundService;

            case FREECHARGE:
            case FREECHARGE_SSO:
                return freechargeRefundService;

            case CASH:
                throw new SwiggyPaymentException(1, "Not a Valid Payment Method to refund");

            default:
                log.error("[REFUND SERVICE] Not a valid payment method to refund.");
                throw new SwiggyPaymentException(2, "Not a Valid Payment Method.");

        }
    }





}

