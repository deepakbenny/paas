package com.swiggy.checkout.service.refund;

import com.fasterxml.jackson.annotation.JsonProperty;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

/**
 * Created by Geetesh on 6/27/17.
 */
@Data
@Builder
@NoArgsConstructor
@AllArgsConstructor
public class RefundPaasRequest {

    @JsonProperty("order_id")
    private String orderId;

    @JsonProperty("txn_id")
    private String txnId;

    @JsonProperty("refund_id")
    private String refundId;

    @JsonProperty("amount")
    private String amount;

    @JsonProperty("payment_method")
    private String paymentMethod;

    @JsonProperty("payment_gateway")
    private String paymentGateway;

    @JsonProperty("pg_call")
    private String pgCall;

}