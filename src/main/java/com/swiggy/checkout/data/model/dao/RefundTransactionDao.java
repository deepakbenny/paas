package com.swiggy.checkout.data.model.dao;

import com.swiggy.checkout.data.entities.RefundTransactionEntity;
import org.springframework.data.jpa.repository.JpaRepository;

/**
 * Created by Geetesh on 7/12/17.
 */
public interface RefundTransactionDao extends JpaRepository<RefundTransactionEntity, Long> {

    @Override
    RefundTransactionEntity save(RefundTransactionEntity transactionEntity);

}
