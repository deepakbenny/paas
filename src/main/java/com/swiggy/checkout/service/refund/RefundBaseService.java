package com.swiggy.checkout.service.refund;

import com.swiggy.checkout.exception.SwiggyPaymentException;
import com.swiggy.checkout.freecharge.entities.responses.RefundPaasResponse;

/**
 * Created by Geetesh on 6/27/17.
 */
public interface RefundBaseService {

    RefundPaasResponse refund(RefundPaasRequest request) throws SwiggyPaymentException;
}
