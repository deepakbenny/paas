package com.swiggy.checkout.thirdparty.client.http;

import retrofit2.Call;
import retrofit2.http.Body;
import retrofit2.http.POST;


/**
 * Created by chandelabhinav on 21/07/16.
 *
 */
public interface ThirdPartyWithdrawHttpClient {
    @POST("./")
    Call<Object> withdraw(@Body String request);
}

