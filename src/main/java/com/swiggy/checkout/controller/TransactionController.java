package com.swiggy.checkout.controller;

import com.swiggy.checkout.extenal.entities.request.CreateTransactionRequest;
import com.swiggy.checkout.extenal.entities.response.BaseResponse;
import com.swiggy.checkout.extenal.entities.response.ErrorResponse;
import com.swiggy.checkout.service.VerifyTransactionService;
import com.swiggy.checkout.service.TransactionService;
import com.swiggy.checkout.utils.Utility;
import io.swagger.annotations.ApiOperation;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.MediaType;
import org.springframework.web.bind.annotation.*;

/**
 * Created by Manish on 27/01/17.
 */

@RestController
@RequestMapping("/v1/payment/transaction")
public class TransactionController {

    private static final Logger logger = LoggerFactory.getLogger(TransactionController.class);
    private static final int ERROR_CODE = 100;

    private static final String ERROR_MSG = "Some error on server side for payment transaction";

    @Autowired
    private
    TransactionService transactionService;

    @Autowired
    private
    VerifyTransactionService verifyTransactionService;

    @ApiOperation(value = "create transaction", notes = "Api to create payment transaction.")
    @RequestMapping(
            value = {"/create"},
            method = RequestMethod.POST,
            produces = MediaType.APPLICATION_JSON_VALUE
    )
    public BaseResponse createTransaction(@RequestBody CreateTransactionRequest createTransactionRequest){
        try{
            logger.info("Create Transaction Request: {}" , Utility.jsonEncode(createTransactionRequest));
            return  transactionService.createTransaction(createTransactionRequest);
        }
        catch (Exception e) {
            logger.error(ERROR_MSG, e);
            return new ErrorResponse(ERROR_CODE, ERROR_MSG);
        }
    }

    @ApiOperation(value = "verify transaction", notes = "Api to verify payment transaction.")
    @RequestMapping(
            value = {"/verify"},
            method = RequestMethod.GET,
            produces = MediaType.APPLICATION_JSON_VALUE
    )
    public BaseResponse verifyTransaction(@RequestParam(value = "order_id") String orderId){

        logger.info("verify Transaction Request: {}" ,orderId);
        try{
            return  verifyTransactionService.verifyTransaction(orderId);
        }
        catch (Exception e) {
            logger.error(ERROR_MSG, e);
            return new ErrorResponse(ERROR_CODE, ERROR_MSG);
        }
    }

}
