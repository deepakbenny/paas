package com.swiggy.checkout.extenal.entities.response.PayLater.LazyPay;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.databind.annotation.JsonSerialize;
import lombok.Data;

/**
 * Created by chirag.rc on 10/30/17.
 */
@Data
@JsonIgnoreProperties(ignoreUnknown = true)
@JsonSerialize(include = JsonSerialize.Inclusion.NON_NULL)
public class LazyPayResendOtpResponse extends LazyPayErrorResponse {

    @JsonProperty("status")
    private String status;

    @JsonProperty("otpType")
    private String otpType;

    @JsonProperty("attemptsRemaining")
    private int attemptsRemaining;
}
