package com.swiggy.checkout.paytm.entities.response;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.annotation.JsonProperty;
import lombok.AllArgsConstructor;
import lombok.NoArgsConstructor;

/**
 * Created by Manish on 29/01/17.
 */

@NoArgsConstructor
@AllArgsConstructor
@JsonIgnoreProperties(ignoreUnknown = true)
@JsonInclude(JsonInclude.Include.NON_NULL)
public class BlockAmountResponse extends PaytmBaseResponse {

    private String preAuthId;

    private String blockedAmount;

    private String statusMessage;

    private String orderId;

    private String status;

    @JsonProperty("status")
    public String getStatus() {
        return status;
    }

    @JsonProperty("STATUS")
    public void setStatus(String status) {
        this.status = status;
    }

    @JsonProperty("preAuthId")
    public String getPreAuthId() {
        return preAuthId;
    }

    @JsonProperty("PREAUTH_ID")
    public void setPreAuthId(String preAuthId) {
        this.preAuthId = preAuthId;
    }

    @JsonProperty("blockedAmount")
    public String getBlockedAmount() {
        return blockedAmount;
    }

    @JsonProperty("BLOCKEDAMOUNT")
    public void setBlockedAmount(String blockedAmount) {
        this.blockedAmount = blockedAmount;
    }

    @JsonProperty("statusMessage")
    public String getStatusMessage() {
        return statusMessage;
    }

    @JsonProperty("STATUSMESSAGE")
    public void setStatusMessage(String statusMessage) {
        this.statusMessage = statusMessage;
    }

    @JsonProperty("orderId")
    public String getOrderId() {
        return orderId;
    }

    @JsonProperty("ORDER_ID")
    public void setOrderId(String orderId) {
        this.orderId = orderId;
    }

}
