package com.swiggy.checkout.common.services;

import com.rabbitmq.client.Channel;
import com.rabbitmq.client.Connection;
import com.swiggy.checkout.email.EmailService;
import lombok.extern.slf4j.Slf4j;
import net.logstash.logback.encoder.org.apache.commons.lang.exception.ExceptionUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Service;

import java.io.IOException;

@Service
@Slf4j
public class RabbitmqChannelService {

    @Autowired
    EmailService emailService;

    @Autowired
    RabbitmqConnectionFactoryService rabbitmqConnectionFactoryService;

    @Value("${email_context}")
    private String emailContext;

    @Value("${email_onErrorEmailId}")
    private String emailOnError;


    public Channel getRabbitmqChannel(String exchangeName, String exchangeType, String queueName, String routingKey) throws IOException {
        Connection conn;
        Channel channel;
        try {
            conn = rabbitmqConnectionFactoryService.getConnectionFactory().newConnection();
            if (conn != null) {
                channel = conn.createChannel();
                if (channel != null) {
                    channel.exchangeDeclare(exchangeName, exchangeType, true);
                    channel.queueDeclare(queueName, true, false, false, null);
                    channel.queueBind(queueName, exchangeName, routingKey);
                }
                return channel;
            }
        } catch (Exception ex) {
            log.error("Error in getting connection", ex);
            emailService.sendEmail(emailOnError, String.format("[%s][%s][%s][%s]",
                    emailContext, "DS-API", "Exception occurred", "Rabbitmq Channel Creation"), ExceptionUtils.getStackTrace(ex));
        }
        return null;
    }
}
