package com.swiggy.checkout.extenal.entities.request.PayLater;

import com.fasterxml.jackson.annotation.JsonProperty;
import lombok.Data;

/**
 * Created by chirag.rc on 11/13/17.
 */
@Data
public class PayLaterTxnEnquiryRequest {

    @JsonProperty("paylater_method")
    private String paylaterMethod;

    @JsonProperty("is_sale")
    private Boolean isSale;

    @JsonProperty("merchant_order_id")
    private String merchantOrderId;

    @JsonProperty("user_id")
    private long userId;
}
