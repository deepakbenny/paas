package com.swiggy.checkout.utils;

import lombok.extern.slf4j.Slf4j;
import okhttp3.logging.HttpLoggingInterceptor;

/**
 * Created by Manish on 23/01/17.
 */

@Slf4j
public class HTTPLogger implements HttpLoggingInterceptor.Logger {
    public static final HTTPLogger DEFAULT = new HTTPLogger();

    @Override
    public void log(String message) {
        log.info(message);
    }
}
