package com.swiggy.checkout.thirdparty.client.services;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.ApplicationContext;
import org.springframework.stereotype.Service;

/**
 * Created by abhinav.chandel on 9/17/16.
 */
@Service
public class ThirdPartyFactory {

    @Autowired
    private ApplicationContext applicationContext;

    private static final Logger logger = LoggerFactory.getLogger(ThirdPartyFactory.class);

    public ThirdPartyPayment getThirdPartyPayment(String source) {
        try {
            return (ThirdPartyPayment) applicationContext.getBean(source);
        } catch (Exception e) {
            logger.error("Invalid third party: {}", source);
            throw e;
        }
    }
}
