package com.swiggy.checkout.hysterix;

import org.apache.commons.collections.MapUtils;
import org.slf4j.MDC;
import org.springframework.web.context.request.RequestAttributes;
import org.springframework.web.context.request.RequestContextHolder;

import java.util.Map;
import java.util.concurrent.Callable;

public class CheckoutContextCallable<K> implements Callable<K> {

    private final Callable<K> callable;
    private RequestAttributes attributes = null;
    Map<String, String> contextMap;

    public CheckoutContextCallable(Callable<K> actual) {
        this.callable = actual;
        attributes = RequestContextHolder.getRequestAttributes();
        contextMap = MDC.getCopyOfContextMap();
    }

    @Override
    public K call() throws Exception {
        try {
            RequestContextHolder.setRequestAttributes(attributes);
            if (!MapUtils.isEmpty(contextMap)) MDC.setContextMap(contextMap);
            return callable.call();
        }finally {
            RequestContextHolder.setRequestAttributes(attributes);
        }
    }
}
