package com.swiggy.checkout.extenal.entities.request;

import com.fasterxml.jackson.annotation.JsonProperty;
import lombok.*;

/**
 * Created by chirag.rc on 12/19/17.
 */
@Data
@EqualsAndHashCode(callSuper = false)
public class WalletResendOTPRequest {

    @JsonProperty("wallet")
    private String wallet;

    @JsonProperty("otp_id")
    private String otpId;

    @JsonProperty("channel")
    private String channel;
}
