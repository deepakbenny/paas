package com.swiggy.checkout.juspay;

/**
 * Created by Manish on 30/01/17.
 */
public interface JuspayConstants {

    String MERCHANT_NAME = "Swiggy";
    String RETURN_TO_URL = "http://www.swiggy.com/justpay/response.php";
    String MERCHANT_ID = "com.swiggy";
}

