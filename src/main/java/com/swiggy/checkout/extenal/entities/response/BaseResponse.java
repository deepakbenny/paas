package com.swiggy.checkout.extenal.entities.response;

import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

/**
 * Created by Manish on 23/01/17.
 */

@NoArgsConstructor
@Getter
@Setter
public class BaseResponse {

    private int statusCode;
    private String statusMessage;
    private Object data;

    private BaseResponse(int statusCode, String statusMessage) {
        this.statusCode = statusCode;
        this.statusMessage = statusMessage;
    }

    public BaseResponse(int statusCode, String statusMessage, Object data) {
        this(statusCode, statusMessage);
        this.data = data;
    }
}
