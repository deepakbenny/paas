package com.swiggy.checkout.data.entities;

import com.fasterxml.jackson.annotation.JsonIgnore;
import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import com.fasterxml.jackson.databind.annotation.JsonDeserialize;
import com.fasterxml.jackson.databind.annotation.JsonSerialize;
import com.razorpay.Payment;
import com.swiggy.checkout.Payu.PayUTransactionDetails;
import com.swiggy.checkout.Payu.entity.response.PayURefundResponse;
import com.swiggy.checkout.extenal.entities.response.ErrorResponse;
import com.swiggy.checkout.extenal.entities.response.PayLater.LazyPay.LazyPayTransactionResponse;
import com.swiggy.checkout.freecharge.entities.responses.DebitResponse;
import com.swiggy.checkout.freecharge.entities.responses.FreechargeRefundResponse;
import com.swiggy.checkout.jackson.serializer.LocalDateTimeDeserializer;
import com.swiggy.checkout.jackson.serializer.LocalDateTimeSerializer;
import com.swiggy.checkout.jackson.serializer.NumericBooleanDeserializer;
import com.swiggy.checkout.mobikwik.entities.response.MobikwikBaseResponse;
import com.swiggy.checkout.mobikwik.entities.response.MobikwikVerifyPaymentResponse;
import com.swiggy.checkout.paytm.entities.response.PaytmVerifyPaymentResponse;
import com.swiggy.checkout.paytm.entities.response.RefundResponse;
import com.swiggy.checkout.paytm.entities.response.WithdrawAmountResponse;
import com.swiggy.checkout.razorpay.pojo.RazorPayOrderResponse;
import com.swiggy.checkout.thirdparty.ThirdPartyMetaData;
import lombok.*;
import org.springframework.data.annotation.Transient;

import java.time.LocalDateTime;

/**
 * Created by Manish on 29/01/17.
 */
@Getter
@Setter
@JsonSerialize(include = JsonSerialize.Inclusion.NON_NULL)
@JsonIgnoreProperties(ignoreUnknown = true)
@Builder
@NoArgsConstructor
@AllArgsConstructor
public class TransactionPaymentDetails {

    // many fields to derive from this
    // 1. _payment_cod_method
    // 2. order_payment_method - this is original as sent by client
    // 3. payment_method - get_option(order_payment_method_ + order_payment_method)
    // 4. payment_method_title
    private String paymentMethod;
    private String paymentTxnId;
    private String paymentTxnStatus;
    private String paymentNetbankingType;
    private String paymentConfirmationChannel;
    private String binNumber;
    private String bankName;
    @JsonSerialize(using = LocalDateTimeSerializer.class)
    @JsonDeserialize(using = LocalDateTimeDeserializer.class)
    private LocalDateTime pgResponseTime;
    private PayUTransactionDetails payUTransactionDetails;
    private PaytmVerifyPaymentResponse paytmVerifyPaymentResponse;
    private WithdrawAmountResponse paytmSsoResponse;
    private MobikwikBaseResponse mobikwikSsoWithdrawResponse;
    private MobikwikVerifyPaymentResponse mobikwikResponse;
    @JsonSerialize(using = LocalDateTimeSerializer.class)
    @JsonDeserialize(using = LocalDateTimeDeserializer.class)
    private LocalDateTime refundTime;
    private PayURefundResponse payURefundResponse;
    private String payURefund;
    private RefundResponse paytmRefundResponse;
    private MobikwikBaseResponse mobikwikRefundResponse;
    private ErrorResponse paytmRefundErrorResponse;
    @JsonDeserialize(using = NumericBooleanDeserializer.class)
    private boolean refundInitiated;

    //for payment gateway integration
    private String paymentGateway;

    private String walletSsoToken;

    //freecharge
    private DebitResponse freechargeVerifyPaymentResponse;

    //freecharge wallet
    private DebitResponse freechargeWithdrawResponse;
    private FreechargeRefundResponse freechargeRefundResponse;
    private String freechargeFailureMessage;

    private String refundMethod;

    private ThirdPartyMetaData thirdPartyMetaData;

    @JsonIgnore
    private String orderId;

    @JsonIgnore
    private int amount;

    private LazyPayTransactionResponse lazyPayTransactionResponse;

    private RazorPayOrderResponse razorPayPaymentDetails;
}
