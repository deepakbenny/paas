package com.swiggy.checkout.exception;

import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

/**
 * Created by Manish on 23/01/17.
 */
@NoArgsConstructor
@Getter
@Setter
public class ThirdPartyException extends Exception {

    int code;
    String message;
    String detailMessage;

    public ThirdPartyException(int code, String message) {
        this.code = code;
        this.message = message;
    }

    public ThirdPartyException(int code, String message, String detailMessage) {
        this(code, message);
        this.detailMessage = detailMessage;
    }

    public ThirdPartyException(ThirdPartyError error, String detailMessage) {
        this(error.getCode(), error.getMessage(), detailMessage);
    }
}
