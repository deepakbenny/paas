package com.swiggy.checkout.service.refund;

import com.swiggy.checkout.data.entities.RefundTransactionEntity;
import com.swiggy.checkout.data.model.dao.RefundTransactionDao;
import com.swiggy.checkout.exception.SwiggyPaymentException;
import com.swiggy.checkout.freecharge.entities.responses.RefundPaasResponse;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

/**
 * Created by Geetesh on 6/28/17.
 */
@Service
@Slf4j
public class RefundService {

    RefundBaseService refundBaseService;

    @Autowired
    RefundServiceFactory refundServiceFactory;

    @Autowired
    RefundTransactionDao transactionDao;

    private static final String PAAS_SERVICE = "paas_service";

    public RefundPaasResponse refund(RefundPaasRequest request) throws SwiggyPaymentException{
        refundBaseService = refundServiceFactory.getRefundService(request.getPaymentMethod(), request.getPaymentGateway());
        RefundPaasResponse response = refundBaseService.refund(request);
        transactionDao.save(createRefundEntity(request, response));
        return response;
    }

    private RefundTransactionEntity createRefundEntity(RefundPaasRequest request, RefundPaasResponse refundPaasResponse){
//        return RefundTransactionEntity.builder()
//                .orderId(Long.parseLong(request.getOrderId()))
//                .paymentMethod(request.getPaymentMethod())
//                .amount(request.getAmount())
//                .createdBy(PAAS_SERVICE)
//                .refundResponseDetails(refundPaasResponse!=null?refundPaasResponse.getData().toString():"")
//                .build();

        RefundTransactionEntity refundTransactionEntity = new RefundTransactionEntity();
        refundTransactionEntity.setOrderId(Long.parseLong(request.getOrderId()));
        refundTransactionEntity.setPaymentMethod(request.getPaymentMethod());
        refundTransactionEntity.setAmount(request.getAmount());
        refundTransactionEntity.setCreatedBy(PAAS_SERVICE);
        refundTransactionEntity.setRefundResponseDetails(refundPaasResponse!=null?refundPaasResponse.getData().toString():"");
        return refundTransactionEntity;

    }
}
