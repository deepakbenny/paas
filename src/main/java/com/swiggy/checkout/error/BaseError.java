package com.swiggy.checkout.error;

import lombok.Getter;
import lombok.Setter;

/**
 * Created by Manish on 03/02/17.
 */
public class BaseError {
    @Getter
    @Setter
    private int statusCode;
    @Getter @Setter private String statusMessage;

    public BaseError(int statusCode, String statusMessage) {
        this.statusCode = statusCode;
        this.statusMessage = statusMessage;
    }
}
