package com.swiggy.checkout.health;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by Manish on 17/10/17.
 */
public class HealthCheckerRegistry {

    private List<IHealthChecker> healthCheckers;

    public HealthCheckerRegistry() {
        healthCheckers = new ArrayList<IHealthChecker>();
    }

    public void addHealthChecker(IHealthChecker healthChecker) {
        healthCheckers.add(healthChecker);
    }

    public List<IHealthChecker> getAllHealthCheckers() {
        return healthCheckers;
    }
}
