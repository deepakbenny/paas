package com.swiggy.checkout.exception;

import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

/**
 * Created by Manish on 03/02/17.
 */

@Getter
@Setter
@AllArgsConstructor
@NoArgsConstructor
public class PaytmFailureException extends Exception{
    private int code;
    private String message;
}
