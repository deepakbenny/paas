package com.swiggy.checkout.extenal.entities.response.PayLater.LazyPay;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.databind.annotation.JsonSerialize;
import lombok.Data;

import java.util.List;

/**
 * Created by chirag.rc on 10/23/17.
 */
@Data
@JsonIgnoreProperties(ignoreUnknown = true)
@JsonSerialize(include = JsonSerialize.Inclusion.NON_NULL)
public class LazyPayEligibilityResponse extends LazyPayErrorResponse {

    @JsonProperty("status")
    private int status;

    @JsonProperty("txnEligibility")
    private Boolean txnEligibility;

    @JsonProperty("reason")
    private String reason;

    @JsonProperty("code")
    private String code;

    @JsonProperty("userEligibility")
    private Boolean userEligibility;

    @JsonProperty("emailRequired")
    private Boolean emailRequired;

    @JsonProperty("eligibilityResposneId")
    private String eligibilityResposneId;

    @JsonProperty("signUpModes")
    private List<String> signUpModes;

    @JsonProperty("addressReq")
    private Boolean addressReq;

    @JsonProperty("firstNameReq")
    private Boolean firstNameReq;

    @JsonProperty("lastNameReq")
    private Boolean lastNameReq;
}
