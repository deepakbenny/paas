package com.swiggy.checkout.paytm.entities.request;

import com.fasterxml.jackson.annotation.JsonProperty;
import lombok.*;

/**
 * Created by Manish on 29/01/17.
 */

@NoArgsConstructor
@AllArgsConstructor
@Getter
@Setter
@Builder
public class BlockAmountRequest {

    @JsonProperty("MID")
    private String mid;

    @JsonProperty("ORDER_ID")
    private String orderId;

    @JsonProperty("TXN_AMOUNT")
    private String txnAmount;

    @JsonProperty("TOKEN")
    private String token;

    @JsonProperty("DURATIONHRS")
    private String duration;

    @JsonProperty("CHECKSUM")
    private String checksum;

}
