package com.swiggy.checkout.service;

import com.swiggy.checkout.data.entities.UserTokenDetailsEntity;
import com.swiggy.checkout.data.model.dao.UserTokenDetailsDao;
import com.swiggy.checkout.utils.Utility;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.sql.Timestamp;

/**
 * Created by chirag.rc on 12/17/17.
 */
@Service
@Slf4j
public class WalletTokenMigrationService {

    @Autowired
    private UserTokenDetailsDao userTokenDetailsDao;

    public UserTokenDetailsEntity saveUserTokenDetails(String userId, String token, String paymentMethod) {
        UserTokenDetailsEntity userTokenDetails = new UserTokenDetailsEntity();
        userTokenDetails.setUserId(Utility.getLong(userId));
        userTokenDetails.setPaymentMethodName(paymentMethod);
        userTokenDetails.setCreatedOn(new Timestamp(new java.util.Date().getTime()));
        userTokenDetails.setToken(token);
        userTokenDetails.setRefreshToken("");
        userTokenDetails.setExpiresOn(new Timestamp(new java.util.Date().getTime()));
        userTokenDetailsDao.save(userTokenDetails);
        return userTokenDetails;
    }

}
