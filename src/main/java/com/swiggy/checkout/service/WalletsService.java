package com.swiggy.checkout.service;

import com.swiggy.checkout.exception.SwiggyPaymentException;
import com.swiggy.checkout.extenal.entities.response.BaseResponse;
import com.swiggy.checkout.freecharge.service.FreechargeWalletService;
import com.swiggy.checkout.mobikwik.service.MobikwikWalletService;
import com.swiggy.checkout.paytm.service.PaytmAuthService;
import com.swiggy.checkout.paytm.service.PaytmWalletService;
import com.swiggy.checkout.pojo.PaymentStatusResponse;
import com.swiggy.checkout.utils.Utility;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

/**
 * Created by Manish on 10/02/17.
 */

@Service
@Slf4j
 class WalletServices {

    private static final int BAD_REQUEST_ERROR_CODE = 400;
    private static final String ERROR_MSG = "Some error on server side for payment transaction";
    private static final String INVALID_ERROR_MSG = "INVALID WALLET";


    @Autowired
    PaytmWalletService paymenWalletService;

    @Autowired
    MobikwikWalletService mobikwikWalletService;

    @Autowired
    FreechargeWalletService freechargeWalletService;


    /**
     * Depending upon wallet method, consult respective wallet service
     *
     * @param
     * @return
     */
    public BaseResponse getWalletBalance(String wallet) throws SwiggyPaymentException {

        switch (ValidWallet.getWalletName(wallet)) {
            case PAYTM:
            case PAYTM_SSO:
                return paymenWalletService.checkBalance();
            case MOBIKWIK:
            case MOBIKWIK_SSO:
                return mobikwikWalletService.checkBalance();
            case FREECHARGE:
            case FREECHARGE_SSO:
                return freechargeWalletService.checkBalance();
        }

        throw new SwiggyPaymentException(BAD_REQUEST_ERROR_CODE, INVALID_ERROR_MSG);
    }

    public BaseResponse getOtpForLinkingWallet(String wallet) throws SwiggyPaymentException {

        switch (ValidWallet.getWalletName(wallet)) {
            case PAYTM:
            case PAYTM_SSO:
                return paymenWalletService.generateOTP();
            case MOBIKWIK:
            case MOBIKWIK_SSO:
                return mobikwikWalletService.generateOTP();
            case FREECHARGE:
            case FREECHARGE_SSO:
                return freechargeWalletService.generateOTP();
        }

        throw new SwiggyPaymentException(BAD_REQUEST_ERROR_CODE, INVALID_ERROR_MSG);
    }


    public BaseResponse validateOtpForLinkingWallet(String wallet,String otp, String state) throws SwiggyPaymentException {

        switch (ValidWallet.getWalletName(wallet)) {
            case PAYTM:
            case PAYTM_SSO:
                return paymenWalletService.generateToken(otp,state);
            case MOBIKWIK:
            case MOBIKWIK_SSO:
                 return mobikwikWalletService.generateToken(otp,state);
            case FREECHARGE:
            case FREECHARGE_SSO:
                return freechargeWalletService.generateToken(otp,state);
        }

        throw new SwiggyPaymentException(BAD_REQUEST_ERROR_CODE, INVALID_ERROR_MSG);
    }


    public BaseResponse withdrawAmount(String wallet, String amount,String orderId) throws SwiggyPaymentException {

        switch (ValidWallet.getWalletName(wallet)) {
            case PAYTM:
            case PAYTM_SSO:
                return paymenWalletService.debitAmount(amount,orderId,"");
            case MOBIKWIK:
            case MOBIKWIK_SSO:
                return mobikwikWalletService.debitAmount(amount,orderId,"");
            case FREECHARGE:
            case FREECHARGE_SSO:
                return freechargeWalletService.debitAmount(amount,orderId,"");
        }

        throw new SwiggyPaymentException(BAD_REQUEST_ERROR_CODE, INVALID_ERROR_MSG);
    }


}
