package com.swiggy.checkout.extenal.entities.response.PayLater.LazyPay;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import com.fasterxml.jackson.annotation.JsonProperty;

import com.fasterxml.jackson.databind.annotation.JsonSerialize;
import lombok.Data;

import java.util.List;

/**
 * Created by chirag.rc on 10/24/17.
 */

@Data
@JsonIgnoreProperties(ignoreUnknown = true)
@JsonSerialize(include = JsonSerialize.Inclusion.NON_NULL)
public class LazyPayGetOtpResponse extends LazyPayErrorResponse {

    @JsonProperty("status")
    private int status;

    @JsonProperty("txnRefNo")
    private String txnRefNo;

    @JsonProperty("paymentModes")
    private List<String> paymentModes;

    @JsonProperty("lpTxnId")
    private String lpTxnId;

    @JsonProperty("checkoutPageUrl")
    private String checkoutPageUrl;
}
