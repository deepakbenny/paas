package com.swiggy.checkout.paytm.entities.response;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.annotation.JsonProperty;
import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

/**
 * Created by Manish on 29/01/17.
 */

@Getter
@Setter
@NoArgsConstructor
@AllArgsConstructor
@JsonIgnoreProperties(ignoreUnknown = true)
@JsonInclude(JsonInclude.Include.NON_NULL)
public class WithdrawAmountResponse extends PaytmBaseResponse {

    @JsonProperty("TxnId")
    private String txnId;

    @JsonProperty("MID")
    private String mid;

    @JsonProperty("OrderId")
    private String orderId;

    @JsonProperty("TxnAmount")
    private String txnAmount;

    @JsonProperty("BankTxnId")
    private String bankTxnId;

    @JsonProperty("ResponseCode")
    private String responseCode;

    @JsonProperty("ResponseMessage")
    private String responseMessage;

    @JsonProperty("Status")
    private String status;

    @JsonProperty("PaymentMode")
    private String paymentMode;

    @JsonProperty("BankName")
    private String bankName;

    @JsonProperty("CheckSum")
    private String checkSum;

    @JsonProperty("CustId")
    private String custId;

    @JsonProperty("MBID")
    private String mbid;

    @JsonProperty("Error")
    private String error;

    @JsonProperty("PROMO_CAMP_ID")
    private String promoCampId;

    @JsonProperty("PROMO_STATUS")
    private String promoStatus;

    @JsonProperty("PROMO_RESPCODE")
    private String promoResponseCode;


}
