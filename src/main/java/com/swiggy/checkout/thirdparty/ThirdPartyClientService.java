package com.swiggy.checkout.thirdparty;


import com.swiggy.checkout.data.entities.ThirdPartyEntity;

/**
 * Created by abhinav.chandel on 9/28/16.
 *
 * Identifying the third party from the request/order
 */
public interface ThirdPartyClientService {
    /**
     * Get third party from the token within request header
     *
     * @return ThirdPartyEntity
     */
    ThirdPartyEntity getThirdPartyByToken();

    /**
     * Get third party given third party token
     * @param token
     *
     * @return ThirdPartyEntity
     */
    ThirdPartyEntity getThirdPartyByToken(String token);

    /**
     * Get third party given third party source
     * @param source
     *
     * @return ThirdPartyEntity
     */
    ThirdPartyEntity getThirdPartyBySource(String source);
}
