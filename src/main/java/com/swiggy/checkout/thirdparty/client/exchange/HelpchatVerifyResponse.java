package com.swiggy.checkout.thirdparty.client.exchange;

import lombok.Getter;
import lombok.Setter;

/**
 * Created by abhinav.chandel on 10/21/16.
 */
@Getter
@Setter
public class HelpchatVerifyResponse {
    private double helpchatCashApplied;
    private double paymentCollected;
}
