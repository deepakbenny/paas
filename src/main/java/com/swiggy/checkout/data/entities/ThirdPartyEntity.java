package com.swiggy.checkout.data.entities;

import lombok.*;

import javax.persistence.*;
import java.io.Serializable;

/**
 * Created by Manish on 29/01/17.
 */
@Entity
@Getter
@Setter
@Table(name = "third_parties")
@EqualsAndHashCode
@AllArgsConstructor
@NoArgsConstructor
public class ThirdPartyEntity implements Serializable {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private int id;

    /**
     * API to withdraw money from third party
     */
    @Column(name = "withdraw_api", nullable = true)
    private String withdrawApi;

    /**
     * API to refund money from third party
     */
    @Column(name = "refund_api", nullable = true)
    private String refundApi;

    /**
     * API to refund money from third party
     */
    @Column(name = "verify_api", nullable = true)
    private String verifyApi;

    /**
     * To identify third party from request header
     */
    @Column(name = "token", nullable = false)
    private String token;

    /**
     * Name of third party
     */
    @Column(name = "source", nullable = false)
    private String source;

    /**
     * Metadata for making request to third party
     */
    @Column(name = "metadata", nullable = false)
    private String metadata;

    /**
     * API to verify transaction from third party
     */
    @Column(name = "verify_transaction_api", nullable = true)
    private String verifyTransactionApi;

    /**
     * API to verify transaction from third party
     */
    @Column(name = "edit_notify_api", nullable = true)
    private String editNotifyApi;
}

