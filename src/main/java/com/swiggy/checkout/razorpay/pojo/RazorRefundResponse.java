package com.swiggy.checkout.razorpay.pojo;

import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.annotation.JsonInclude.Include;
import com.fasterxml.jackson.annotation.JsonProperty;
import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

/**
 * Created by IntelliJ IDEA.
 * User: bothravinit
 * Date: 1/22/16
 * Time: 2:56 PM
 */

@JsonInclude(Include.NON_EMPTY)
@Getter
@Setter
@AllArgsConstructor
@NoArgsConstructor
public class RazorRefundResponse {
    @JsonProperty("amount") private int amount;
    @JsonProperty("payment_id") private String paymentId;
    @JsonProperty("id") private String refundId;
    @JsonProperty("entity") private String entity;
}
