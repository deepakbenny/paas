package com.swiggy.checkout.thirdparty.client.exchange;

import com.fasterxml.jackson.databind.annotation.JsonSerialize;
import lombok.Getter;
import lombok.Setter;

/**
 * Created by abhinav.chandel on 9/19/16.
 */
@Getter
@Setter
@JsonSerialize(include= JsonSerialize.Inclusion.NON_NULL)
public class HelpchatRefundRequest {
    private String thirdPartyCash;
    private String orderId;
    private String event;
}
