package com.swiggy.checkout.extenal.entities.request.PayLater.LazyPay;

import com.fasterxml.jackson.annotation.JsonProperty;
import lombok.Data;

/**
 * Created by chirag.rc on 10/30/17.
 */
@Data
public class LazyPayResendOtpRequest {

    @JsonProperty("txnRefNo")
    private String txnRefNo;
}
