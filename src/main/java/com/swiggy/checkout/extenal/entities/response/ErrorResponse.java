package com.swiggy.checkout.extenal.entities.response;

import com.swiggy.checkout.error.BaseError;
import com.swiggy.checkout.exception.ThirdPartyError;

/**
 * Created by Manish on 23/01/17.
 */
public class ErrorResponse extends BaseResponse {

    public ErrorResponse(Object data){
        super(1,"error occured",data);
    }

    public ErrorResponse(String message) {
        super(1, message, null);
    }

    public ErrorResponse(String status,Object data){
        super(1,status,data);
    }

    public ErrorResponse(BaseError e, Object data){
        super(e.getStatusCode(),e.getStatusMessage(),data);
    }

    public ErrorResponse(BaseError e){
        this(e,null);
    }


    public ErrorResponse(int statusCode, String message, Object data) {
        super(statusCode, message, data);
    }

    public ErrorResponse(int errorCode, String errorMessage) {
        super(errorCode, errorMessage, null);
    }

    public ErrorResponse(ThirdPartyError error) {
        this(error.getCode(), error.getMessage());
    }
}
