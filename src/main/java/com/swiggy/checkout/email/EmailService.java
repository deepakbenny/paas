package com.swiggy.checkout.email;

import com.swiggy.checkout.data.model.bo.PaymentConfiguration;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.scheduling.annotation.Async;
import org.springframework.stereotype.Service;

import java.util.Map;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;

/**
 * Created by Manish on 06/02/17.
 */
@Service
public class EmailService {

    public static final String PROD_ENV = "PROD";

    @Value("${email_from}")
    private String fromEmail;

    @Value("${email_username}")
    private String username;

    @Value("${email_password}")
    private String password;

    @Value("${email_context}")
    private String emailContext;

    @Autowired
    private Sender sender;

    @Autowired
    private PaymentConfiguration configuration;

    private static ExecutorService executorService = Executors.newFixedThreadPool(10);

    private static final String VERIFICATION_SUBJECT = "Swiggy Email Verification";

    private static final org.slf4j.Logger logger = LoggerFactory.getLogger(EmailService.class);

    @Async
    public void sendEmail(String to, String subject, String body) {
        try {
            sender.sendGMail(to, fromEmail, subject, body, username, password);
        } catch (Exception e) {
            logger.error("Error while Sending Mail body {} subject {}", e, body, subject);
        }

    }

    @Async
    public void sendEmailWithAttachments(String to, String subject, String body,Map<String,String> attachFiles) {

        if (!emailContext.equals(PROD_ENV)) {
            return ;
        }
        try {
            sender.sendGMailWithAttachments(to, fromEmail, subject, body, username, password,attachFiles);
        } catch (Exception e) {
            logger.error("Error while Sending Mail with Attachments body {} subject {}", e, body, subject);
        }
    }

    public void sendVerificationEmail(String emailId, String emailContent) {
        String subject = configuration.getConfiguration("swiggy_email_verify_subject", VERIFICATION_SUBJECT);
        executorService.execute(() -> {
            try {
                sender.sendHtmlContentMail(emailId, fromEmail, subject, emailContent, username, password);
            } catch (Exception e) {
                e.printStackTrace();
            }
        });
    }
}

