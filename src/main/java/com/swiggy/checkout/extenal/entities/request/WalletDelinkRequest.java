package com.swiggy.checkout.extenal.entities.request;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

/**
 * Created by Manish on 25/09/17.
 */

@Data
@Builder
@NoArgsConstructor
@AllArgsConstructor
public class WalletDelinkRequest {
    private String wallet;
}
