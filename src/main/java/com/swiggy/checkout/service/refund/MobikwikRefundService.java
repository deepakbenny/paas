package com.swiggy.checkout.service.refund;

import com.swiggy.checkout.data.model.bo.PaymentConfiguration;
import com.swiggy.checkout.exception.SwiggyPaymentException;
import com.swiggy.checkout.freecharge.entities.responses.RefundPaasResponse;
import com.swiggy.checkout.mobikwik.service.MobikwikWalletService;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

/**
 * Created by Geetesh on 6/27/17.
 */
@Service
@Slf4j
public class MobikwikRefundService implements RefundBaseService {

    @Autowired
    private MobikwikWalletService mobikwikWalletService;

    @Autowired
    private PaymentConfiguration configuration;

    @Override
    public RefundPaasResponse refund(RefundPaasRequest request) throws SwiggyPaymentException {
        String mid = configuration.getConfiguration("mobikwik_mid_redirect", "MBK3451");
        String secretKey = configuration.getConfiguration("mobikwik_secret_key_redirect", "x6YWOuZSsSRsOmoTOP4Sv9rpy0gF");// uat ju6tygh7u7tdg554k098ujd5468o
        return mobikwikWalletService.refundAmountForMid(request.getOrderId(), request.getAmount(), mid, secretKey);
    }
}
