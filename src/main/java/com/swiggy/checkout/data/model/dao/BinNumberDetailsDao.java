package com.swiggy.checkout.data.model.dao;

import com.swiggy.checkout.data.entities.BinNumberDetailsEntity;
import org.springframework.cache.annotation.CachePut;
import org.springframework.cache.annotation.Cacheable;
import org.springframework.data.jpa.repository.JpaRepository;


public interface BinNumberDetailsDao extends JpaRepository<BinNumberDetailsEntity, Long> {

    @Cacheable(value = "BinNumberDetails", cacheManager = "redisCacheManagerPayments", key="#p0")
    BinNumberDetailsEntity getBinNumberDetailsEntitiesByBin(long bin);

    @CachePut(value = "BinNumberDetails", cacheManager = "redisCacheManagerPayments", key="#p0.bin")
    @Override
    BinNumberDetailsEntity save(BinNumberDetailsEntity binNumberDetailsEntity);

}
