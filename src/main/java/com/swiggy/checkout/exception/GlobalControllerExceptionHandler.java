
package com.swiggy.checkout.exception;

import com.fasterxml.jackson.core.JsonParseException;
import com.swiggy.checkout.email.EmailService;
import com.swiggy.checkout.extenal.entities.response.ErrorResponse;
import com.swiggy.checkout.utils.Utility;
import org.apache.catalina.connector.ClientAbortException;
import org.apache.commons.lang3.exception.ExceptionUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.TypeMismatchException;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.http.MediaType;
import org.springframework.http.converter.HttpMessageNotReadableException;
import org.springframework.web.HttpRequestMethodNotSupportedException;
import org.springframework.web.bind.MissingServletRequestParameterException;
import org.springframework.web.bind.annotation.ControllerAdvice;
import org.springframework.web.bind.annotation.ExceptionHandler;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.net.InetAddress;


/**
 * Created by Manish on 05/02/17.
 */

@ControllerAdvice
public class GlobalControllerExceptionHandler {

    @Autowired
    private EmailService emailService;
    @Value("${email_onErrorEmailId}") private String errorEmail;
    @Value("${email_context}") private String emailContext;

    public static final String ATTRIBUTE_NAME = "com.swiggy.request-id";


    private static final Logger logger = LoggerFactory
            .getLogger(GlobalControllerExceptionHandler.class);

    @ExceptionHandler(BadRequestException.class)
    public void exceptionHandler(HttpServletRequest request,
                                 HttpServletResponse response, BadRequestException exception) throws IOException {
        exception.printStackTrace();
        logger.error("Exception thrown: ", exception);
        response.setStatus(exception.getHttpStatusCode());
        response.getWriter().write(exception.getMessage());
    }

    @ExceptionHandler(ClientAbortException.class)
    public void  clientAbortExceptionHandler(HttpServletRequest request,
                                             HttpServletResponse response, ClientAbortException exception) throws IOException {
        exception.printStackTrace();
        logger.error("Exception thrown: ", exception);
        response.setStatus(500);
        response.getWriter().write(exception.getMessage());
    }

    @ExceptionHandler(LoginConstraintViolationException.class)
    public void  loginConstrainViolationExceptionHandler(HttpServletRequest request,
                                                         HttpServletResponse response, LoginConstraintViolationException exception) throws IOException {
        logger.error("Login Constraint Violation Exception thrown: " + exception.getMessage());
        response.setStatus(500);
        response.getWriter().write(Utility.jsonEncode(new ErrorResponse(500, "Duplicate Login Request.")));
    }

    @ExceptionHandler(JsonParseException.class)
    public void jsonParseExceptionHandler(HttpServletRequest request,
                                          HttpServletResponse response, JsonParseException exception) throws IOException {
        exception.printStackTrace();
        logger.error("Exception thrown: ", exception);
        response.setStatus(400);
        response.getWriter().write(exception.getMessage());
    }

    @ExceptionHandler(HttpMessageNotReadableException.class)
    public void httpMessageExceptionHandler(HttpServletRequest request,
                                            HttpServletResponse response, HttpMessageNotReadableException exception) throws IOException {
        exception.printStackTrace();
        logger.error("Exception thrown: ", exception);
        if(exception.getMessage().contains("value not one of declared Enum instance")) {
            response.setStatus(400);
            response.getWriter().write(Utility.jsonEncode(new ErrorResponse("Invalid Input")));
        }else {
            response.setStatus(500);
            response.getWriter().write("Http message is not readable : " + exception.getMessage());
        }
    }

    @ExceptionHandler(HttpRequestMethodNotSupportedException.class)
    public void httpRequestNotFoundExceptionHandler(HttpServletRequest request,
                                                    HttpServletResponse response, HttpRequestMethodNotSupportedException exception) throws IOException {
        exception.printStackTrace();
        logger.error("Exception thrown: ", exception);
        response.setStatus(500);
        response.getWriter().write(exception.getMessage());
    }

    @ExceptionHandler(Exception.class)
    public void genericExceptionHandler(HttpServletRequest request,
                                        HttpServletResponse response, Exception exception) throws Exception {
        exception.printStackTrace();
        logger.error("Exception: ", exception);
        sendEmailOnException(request, exception);
        response.setStatus(500);
        response.getWriter().write(Utility.jsonEncode(new ErrorResponse(500, "Internal Server Error")));
    }

    @ExceptionHandler(MissingServletRequestParameterException.class)
    public void missingParamsHandler(HttpServletResponse response, Exception exception) throws IOException {
        exception.printStackTrace();
        logger.error("Exception:",exception);
        response.setStatus(500);
        response.getWriter().write("Required parameter(s) are not present.");
    }

    @ExceptionHandler(TypeMismatchException.class)
    public void typeMisMatchHandler(HttpServletResponse response, Exception exception) throws IOException {
        logger.error("Exception:",exception);
        response.setStatus(500);
        response.getWriter().write("Type mismatch. Please check parameter values.");
    }

    @ExceptionHandler(SwiggyPaymentException.class)
    public void swiggyPaymentExceptionHandler(HttpServletResponse response, SwiggyPaymentException e) throws IOException {

        response.setStatus(200);
        response.setContentType(MediaType.APPLICATION_JSON_VALUE);

        response.getWriter().write(Utility.jsonEncode(new ErrorResponse(e.getCode(), e.getMessage())));
    }

    public void sendEmailOnException(HttpServletRequest request, Exception exception) {
        if(!emailContext.equals("LOCAL")) {
            String emailHeader = String.format("[ %s ][ %s ][ %s ][ %s ][ %s ][ %s ]", emailContext, getHost(), "DS-API", Utility.getRequestPath(), exception.getClass().getCanonicalName(), exception.getLocalizedMessage());
            String emailBody =  "Request ID: " + request.getAttribute(ATTRIBUTE_NAME) + "\n" +
                    "TID: " + request.getAttribute("tid") + "\n" +
                    ExceptionUtils.getStackTrace(exception);
            emailService.sendEmail(errorEmail, emailHeader, emailBody);
        }
    }

    public void sendEmailOnException(Exception exception, String subject) {
        if(!emailContext.equals("LOCAL")) {
            String emailHeader = String.format("[ %s ][ %s ][ %s ][ %s ][ %s ][ %s ]", emailContext, getHost(), "DS-API", exception.getClass().getCanonicalName(), exception.getLocalizedMessage(), subject);
            String emailBody = ExceptionUtils.getStackTrace(exception);
           // emailService.sendEmail(errorEmail, emailHeader, emailBody);
        }
    }

    public void sendEmailOnError(String error, String subject) {
        if(!emailContext.equals("LOCAL")) {
            String emailHeader = String.format("[ %s ][ %s ][ %s ][ %s ]", emailContext, getHost(), "DS-API", subject);
            emailService.sendEmail(errorEmail, emailHeader, error);
        }
    }

    public String getHost(){
        try {
            return InetAddress.getLocalHost().getHostName();
        }
        catch (Exception e) {
            return "unknown";
        }
    }
}



