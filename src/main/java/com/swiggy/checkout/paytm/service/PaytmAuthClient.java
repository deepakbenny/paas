package com.swiggy.checkout.paytm.service;

import com.swiggy.checkout.paytm.entities.request.GetOtpRequest;
import com.swiggy.checkout.paytm.entities.request.ValidateOtpRequest;
import com.swiggy.checkout.paytm.entities.response.GetOtpResponse;
import com.swiggy.checkout.paytm.entities.response.ValidateOtpResponse;
import retrofit2.Call;
import retrofit2.http.Body;
import retrofit2.http.DELETE;
import retrofit2.http.POST;
import retrofit2.http.Path;

/**
 * Created by Manish on 15/02/17.
 */
public interface PaytmAuthClient {

    @POST("signin/otp")
    Call<GetOtpResponse> getOtpForLinkingWallet(@Body GetOtpRequest getOtpRequest);

    @POST("signin/validate/otp")
    Call<ValidateOtpResponse> validateOtpForLinkingWallet(@Body ValidateOtpRequest validateOtpRequest);

    @DELETE("oauth2/accessToken/{sso_token}")
    Call<Void> delinkAccount(@Path("sso_token") String ssoToken);

}
