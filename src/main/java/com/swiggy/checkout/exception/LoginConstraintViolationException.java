package com.swiggy.checkout.exception;

/**
 * Created by Manish on 06/02/17.
 */
public class LoginConstraintViolationException extends Exception{

    public LoginConstraintViolationException(String message, Throwable cause) {
        super(message, cause);
    }
}
