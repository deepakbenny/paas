package com.swiggy.checkout.freecharge.service;


import com.newrelic.api.agent.Trace;
import com.swiggy.checkout.data.entities.UserTokenDetailsEntity;
import com.swiggy.checkout.data.model.bo.PaymentConfiguration;
import com.swiggy.checkout.data.model.dao.UserTokenDetailsDao;
import com.swiggy.checkout.exception.SwiggyPaymentException;
import com.swiggy.checkout.extenal.entities.request.WalletCalculateChecksumRequest;
import com.swiggy.checkout.extenal.entities.request.WalletRegisterNewUserRequest;
import com.swiggy.checkout.extenal.entities.request.WalletResendOTPRequest;
import com.swiggy.checkout.extenal.entities.response.BaseResponse;
import com.swiggy.checkout.extenal.entities.response.ErrorResponse;
import com.swiggy.checkout.extenal.entities.response.SuccessResponse;
import com.swiggy.checkout.freecharge.FreechargeHelper;
import com.swiggy.checkout.freecharge.FreechargeProperties;
import com.swiggy.checkout.freecharge.entities.request.*;
import com.swiggy.checkout.freecharge.entities.responses.*;
import com.swiggy.checkout.jackson.JacksonConverterFactory;
import com.swiggy.checkout.service.WalletTokenMigrationService;
import com.swiggy.checkout.utils.HTTPLogger;
import com.swiggy.checkout.utils.Utility;
import com.swiggy.checkout.wallet.WalletBaseService;
import lombok.extern.slf4j.Slf4j;
import okhttp3.OkHttpClient;
import okhttp3.Request;
import okhttp3.logging.HttpLoggingInterceptor;
import org.apache.commons.collections.map.HashedMap;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.util.StringUtils;
import retrofit2.Call;
import retrofit2.Retrofit;

import javax.annotation.PostConstruct;
import java.io.UnsupportedEncodingException;
import java.net.URLEncoder;
import java.sql.Timestamp;
import java.util.HashMap;
import java.util.Map;
import java.util.concurrent.TimeUnit;

/**
 * Created by Manish on 05/02/17.
 */

@Service
@Slf4j
public class FreechargeWalletService implements WalletBaseService {

    private static final int NOT_LINKED_STATUS_CODE = 2;
    private static final String FREECHARGE_MID = "freecharge_mid";
    private static final String DEFAULT_MID = "QSEV80O5nBjPQh";   //need to set
    private static final String USER_EXIST = "VERIFY";
    private static final String SMS = "sms";
    private static final String GRANT_TYPE = "AUTHORIZATION_CODE";
    private static final String USER_AGENT = "user_agent";
    private static final String SWIGGY_IOS = "Swiggy-iOS";
    private static final String SWIGGY_ANDROID = "Swiggy-Android";
    private static final String SWIGGY_WEB = "web";
    private static final String IOS = "IOS";
    private static final String ANDROID = "ANDROID";
    private static final String WEB = "WEB";
    private static final String TXN_TYPE = "CUSTOMER_PAYMENT";
    public static final String SUCCESS = "INITIATED";
    public static final String FREECHARGE = "Freecharge";


    private static final String GENERATE_OTP_FAILURE_MESSAGE = "freecharge-generate-otp-failure-message";
    private static final String RESEND_OPT_FAILURE_MESSAGE = "freecharge-resend-otp-failure-message";
    private static final String VERIFY_OTP_FAILURE_MESSAGE = "freecharge-verify-otp-failure-message";
    private static final String CHECK_BALANCE_FAILURE_MESSAGE = "freecharge-check-balance-failure-message";
    private static final String NEW_USER_FAILURE_MESSAGE = "freecharge-new-user-failure-message";
    private static final String USER_TOKEN_BACKFILL_ENABLED_NEW = "user_token_backfill_enabled_new";

    @Autowired
    private FreechargeProperties freechargeProperties;

    @Autowired
    private PaymentConfiguration configuration;

    @Autowired
    private FreechargeHelper freechargeHelper;

    @Autowired
    private UserTokenDetailsDao userTokenDetailsDao;

    @Autowired
    private WalletTokenMigrationService walletTokenMigrationService;


    @Autowired
    private FreechargeRetrofitService retrofitService;

    private FreechargeClient freechargeClient;

    private FreechargeCheckoutClient checkoutClient;

    @PostConstruct
    public void postConstruct() {
        OkHttpClient httpClient = new OkHttpClient.Builder()
                .addNetworkInterceptor(chain -> {
                    Request request = chain
                            .request()
                            .newBuilder()
                            .addHeader("payloadtype", "json")
                            .build();
                    return chain.proceed(request);
                })
                .addInterceptor(new HttpLoggingInterceptor(HTTPLogger.DEFAULT).setLevel(HttpLoggingInterceptor.Level.BODY))
                .connectTimeout(1000, TimeUnit.MILLISECONDS)
                .readTimeout(5000, TimeUnit.MILLISECONDS)
                .build();


        Retrofit retrofit = new Retrofit.Builder()
                .baseUrl(freechargeProperties.getLoginEndpoint())
                .addConverterFactory(JacksonConverterFactory.create())
                .client(httpClient)
                .build();

        freechargeClient = retrofit.create(FreechargeClient.class);

        Retrofit retrofit1 = new Retrofit.Builder()
                .baseUrl(freechargeProperties.getCheckoutEndpoint())
                .addConverterFactory(JacksonConverterFactory.create())
                .client(httpClient)
                .build();

        checkoutClient = retrofit1.create(FreechargeCheckoutClient.class);
    }

    public BaseResponse generateOTP() throws SwiggyPaymentException {
        Call<OtpResponse> otpResponseCall = getOtpResponse();

        return new SuccessResponse(retrofitService.getResponseOrLogWarning(otpResponseCall, configuration.getConfiguration(GENERATE_OTP_FAILURE_MESSAGE, "Could not generate otp.")));
    }

    private Call<OtpResponse> getOtpResponse() throws SwiggyPaymentException {

        String mobile = Utility.getUserFromHeader().getMobile();
        String merchantId = configuration.getConfiguration(FREECHARGE_MID, DEFAULT_MID);

        OtpRequest otpRequest = OtpRequest.builder().mobileNumber(mobile).merchantId(merchantId).build();
        String checksum = freechargeHelper.createChecksum(otpRequest);
        otpRequest.setChecksum(checksum);

        return freechargeClient.generateOtp(otpRequest);
    }

    public BaseResponse generateToken(String otp, String otpId) throws SwiggyPaymentException {
        String merchantId = configuration.getConfiguration(FREECHARGE_MID, DEFAULT_MID);

        LoginRequest loginRequest = LoginRequest.builder().merchantId(merchantId)
                .otp(otp)
                .otpId(otpId)
                .build();
        String checksum = freechargeHelper.createChecksum(loginRequest);
        loginRequest.setChecksum(checksum);

        Call<LoginResponse> loginResponseCall = freechargeClient.verifyUser(loginRequest);
        LoginResponse response = retrofitService.getResponseOrLogWarning(loginResponseCall, configuration.getConfiguration(VERIFY_OTP_FAILURE_MESSAGE, "Could not verify otp"));
        saveInDB(response);
        return new SuccessResponse(response.getAccessToken());
    }

    public BaseResponse delink() throws SwiggyPaymentException {
        UserTokenDetailsEntity userTokenDetailsEntity = validateToken();
        userTokenDetailsDao.delete(userTokenDetailsEntity);
        Utility.getUserFromHeader().setFreechargeSsoToken(null);
        return new SuccessResponse();
    }

    @Override
    public BaseResponse getSSOToken() throws SwiggyPaymentException {

        FreechargeTokenCheckResponse response = new FreechargeTokenCheckResponse();

        String userId = Utility.getUserFromHeader().getCustomerId();
        UserTokenDetailsEntity userTokenDetails = userTokenDetailsDao.findByUserIdAndPaymentMethodName(Utility.getLong(userId), FREECHARGE);

        //TODO -- to be removed after token migration
        if (null == userTokenDetails && !Utility.isEmpty(Utility.getUserFromHeader().getFreechargeSsoToken()) && configuration.getConfigurationAsBoolean(USER_TOKEN_BACKFILL_ENABLED_NEW, true)) {
            userTokenDetails = walletTokenMigrationService.saveUserTokenDetails(userId, Utility.getUserFromHeader().getFreechargeSsoToken(), FREECHARGE);
        }
        if (null != userTokenDetails) {
            response.setHasToken(true);
            return new SuccessResponse(response);
        }
        return new ErrorResponse("Couldn't find user token");
    }

    @Override
    public BaseResponse resendOTP(WalletResendOTPRequest walletResendOTPRequest) throws SwiggyPaymentException {
        String merchantId = configuration.getConfiguration(FREECHARGE_MID, DEFAULT_MID);
        String smsOrCall = SMS.equalsIgnoreCase(walletResendOTPRequest.getChannel()) ? "THROUGH_SMS" : "THROUGH_CALL";

        OtpResendRequest request = OtpResendRequest.builder().otpId(walletResendOTPRequest.getOtpId()).merchantId(merchantId).channel(smsOrCall).build();
        String checksum = freechargeHelper.createChecksum(request);
        request.setChecksum(checksum);
        Call<OtpResponse> otpResponseCall = freechargeClient.resendOtp(request);
        return new SuccessResponse(retrofitService.getResponseOrLogWarning(otpResponseCall, configuration.getConfiguration(RESEND_OPT_FAILURE_MESSAGE, "Could not resend otp")));
    }

    @Override
    public BaseResponse generateRegisterUserToken(WalletRegisterNewUserRequest walletRegisterNewUserRequest) throws SwiggyPaymentException {
        String merchantId = configuration.getConfiguration(FREECHARGE_MID, DEFAULT_MID);
        RegisterRequest registerRequest = RegisterRequest.builder().merchantId(merchantId)
                .authCode(walletRegisterNewUserRequest.getAuthCode())
                .grantType(GRANT_TYPE)
                .build();

        String checksum = freechargeHelper.createChecksum(registerRequest);
        registerRequest.setChecksum(checksum);

        Call<LoginResponse> loginResponseCall = freechargeClient.registerUser(registerRequest);
        LoginResponse response = retrofitService.getResponseOrLogWarning(loginResponseCall, configuration.getConfiguration(NEW_USER_FAILURE_MESSAGE, "Could not verify new user"));
        saveInDB(response);
        Utility.getUserFromHeader().setFreechargeSsoToken(null);
        return new SuccessResponse();
    }

    @Override
    public BaseResponse generateLoginToken() throws SwiggyPaymentException {
        UserTokenDetailsEntity userTokenDetailsEntity = validateToken();
        String token = userTokenDetailsEntity.getToken();
        String loginToken = freechargeHelper.generateLoginToken(token);
        return new SuccessResponse(new HashedMap() {{
            put("login_token", loginToken);
        }});
    }

    @Override
    public BaseResponse computeChecksum(WalletCalculateChecksumRequest walletCalculateChecksumRequest) throws SwiggyPaymentException {
        //Not supported by Freecharge
        return null;
    }

    private UserTokenDetailsEntity validateToken() throws SwiggyPaymentException {
        String userId = Utility.getUserFromHeader().getCustomerId();
        UserTokenDetailsEntity userTokenDetails = userTokenDetailsDao.findByUserIdAndPaymentMethodName(Utility.getLong(userId), FREECHARGE);

        //TODO -- to be removed after token migration
        if (null == userTokenDetails && !Utility.isEmpty(Utility.getUserFromHeader().getFreechargeSsoToken()) && configuration.getConfigurationAsBoolean(USER_TOKEN_BACKFILL_ENABLED_NEW, true)) {
            userTokenDetails = walletTokenMigrationService.saveUserTokenDetails(userId, Utility.getUserFromHeader().getFreechargeSsoToken(), FREECHARGE);
        }
        if (null == userTokenDetails) {
            throw new SwiggyPaymentException(NOT_LINKED_STATUS_CODE, configuration.getConfiguration("freecharge_account_not_linked", "User not linked with freecharge"));
        }
        return userTokenDetails;
    }

    public BaseResponse checkBalance() throws SwiggyPaymentException {
        UserTokenDetailsEntity userTokenDetails = validateToken();
        String token = userTokenDetails.getToken();
        String merchantId = configuration.getConfiguration(FREECHARGE_MID, DEFAULT_MID);
        Call<FreechargeBalance> balanceCall = checkoutClient.checkBalance(merchantId, token);
        return new SuccessResponse(retrofitService.getResponseOrLogWarning(balanceCall, configuration.getConfiguration(CHECK_BALANCE_FAILURE_MESSAGE, "Could not check balance.")));
    }

    @Trace
    public BaseResponse debitAmount(String amount, String orderId, String couponId) throws SwiggyPaymentException {
        if (!StringUtils.hasText(amount) || !StringUtils.hasText(orderId))
            throw new SwiggyPaymentException(1, "Invalid request");

        UserTokenDetailsEntity userTokenDetailsEntity = validateToken();

        String token = userTokenDetailsEntity.getToken();
        String merchantId = configuration.getConfiguration(FREECHARGE_MID, DEFAULT_MID);

        DebitRequest debitRequest = DebitRequest.builder().accessToken(token).currency("INR")
                .amount(amount)
                .merchantId(merchantId)
                .merchantTxnId(orderId)
                .channel(getUserAgent())
                .build();

        String checksum = freechargeHelper.createChecksum(debitRequest);
        debitRequest.setChecksum(checksum);

        //TODO verify checksum
        Call<DebitResponse> debitResponseCall = checkoutClient.debitAmount(debitRequest);
        return new SuccessResponse(retrofitService.getResponseOrLogWarning(debitResponseCall, "Could not debit amount."));
    }

    public RefundPaasResponse refundAmount(String txnId, String amount, String refundId) throws SwiggyPaymentException {
        String merchantId = configuration.getConfiguration(FREECHARGE_MID, DEFAULT_MID);
        RefundRequest refundRequest = RefundRequest.builder().merchantId(merchantId)
                .refundAmount(amount)
                .merchantTxnId(txnId)
                .refundMerchantTxnId(refundId)
                .build();
        String checksum = freechargeHelper.createChecksum(refundRequest);
        refundRequest.setChecksum(checksum);
        Call<FreechargeRefundResponse> responseCall = checkoutClient.refundAmount(refundRequest);
        RefundPaasResponse response = new RefundPaasResponse();
        response.getData().setFreechargeRefundResponse(retrofitService.getResponseOrLogWarning(responseCall, "Could not refund."));
        return response;
    }

    public DebitResponse transactionStatus(String txnId) throws SwiggyPaymentException {
        String merchantId = configuration.getConfiguration(FREECHARGE_MID, DEFAULT_MID);

        Map<String, String> request = new HashMap<String, String>() {{
            put("merchantId", merchantId);
            put("merchantTxnId", txnId);
            put("txnType", TXN_TYPE);
        }};
        String checksum = freechargeHelper.createChecksum(request);
        try {
            checksum = URLEncoder.encode(checksum, "UTF-8");
        } catch (UnsupportedEncodingException e) {
            log.error("[FREECHARGE] transaction status checksum encoding failed.");
            e.printStackTrace();
        }
        Call<DebitResponse> statusResponse = checkoutClient.transactionStatus(merchantId, txnId, checksum, TXN_TYPE);
        return retrofitService.getResponseOrLogWarning(statusResponse, "Could not confirm the transaction status.");

    }

    private String getUserAgent() {
        String userAgent = Utility.getUserAgent();
        if (SWIGGY_IOS.equalsIgnoreCase(userAgent))
            return IOS;
        else if (SWIGGY_ANDROID.equalsIgnoreCase(userAgent))
            return ANDROID;
        else {
            return WEB;
        }
    }

    private void saveInDB(LoginResponse response) {
        String userId = Utility.getUserFromHeader().getCustomerId();
        UserTokenDetailsEntity userTokenDetails = userTokenDetailsDao.findByUserIdAndPaymentMethodName(Utility.getLong(userId), FREECHARGE);
        if (null != userTokenDetails) {
            userTokenDetailsDao.delete(userTokenDetails);
        }

        UserTokenDetailsEntity userTokenDetailsEntity = new UserTokenDetailsEntity();
        userTokenDetailsEntity.setPaymentMethodName(FREECHARGE);
        userTokenDetailsEntity.setUserId(Utility.getLong(Utility.getUserFromHeader().getCustomerId()));
        userTokenDetailsEntity.setToken(response.getAccessToken());
        //TODO need to see the format in which it returns the expiry time
        userTokenDetailsEntity.setExpiresOn(new Timestamp(new java.util.Date().getTime()));
        userTokenDetailsEntity.setCreatedOn(new Timestamp(new java.util.Date().getTime()));
        userTokenDetailsEntity.setRefreshToken(response.getRefreshToken());
        userTokenDetailsDao.save(userTokenDetailsEntity);
    }
}

