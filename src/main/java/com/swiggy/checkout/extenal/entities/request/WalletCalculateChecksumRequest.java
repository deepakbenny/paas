package com.swiggy.checkout.extenal.entities.request;

import com.fasterxml.jackson.annotation.JsonProperty;
import lombok.Data;
import lombok.EqualsAndHashCode;

/**
 * Created by chirag.rc on 12/19/17.
 */
@Data
@EqualsAndHashCode(callSuper = false)
public class WalletCalculateChecksumRequest {

    @JsonProperty("amount")
    private String amount;

    @JsonProperty("order_id")
    private String orderid;

    @JsonProperty("redirect_url")
    private String redirectUrl;

    @JsonProperty("wallet")
    private String wallet;
}
