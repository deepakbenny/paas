package com.swiggy.checkout.health;

import com.swiggy.checkout.health.bean.BaseHealthBean;

/**
 * Created by Manish on 17/10/17.
 */
public interface IHealthChecker {

    public String getHealthCheckerName();

    public BaseHealthBean performHeathCheck();
}
