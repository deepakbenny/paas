package com.swiggy.checkout.data.entities;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import com.fasterxml.jackson.databind.annotation.JsonSerialize;
import com.swiggy.checkout.Payu.entity.response.PayURefundResponse;
import com.swiggy.checkout.extenal.entities.response.ErrorResponse;
import com.swiggy.checkout.freecharge.entities.responses.FreechargeRefundResponse;
import com.swiggy.checkout.mobikwik.entities.response.MobikwikBaseResponse;
import com.swiggy.checkout.paytm.entities.response.RefundResponse;
import com.swiggy.checkout.razorpay.pojo.RazorRefundResponse;
import lombok.Getter;
import lombok.Setter;

/**
 * Created by Geetesh on 7/5/17.
 */
@Getter
@Setter
@JsonSerialize(include=JsonSerialize.Inclusion.NON_NULL)
@JsonIgnoreProperties(ignoreUnknown = true)
public class RefundPaymentDetails {

    private PayURefundResponse payURefundResponse;
    private RefundResponse paytmRefundResponse;
    private MobikwikBaseResponse mobikwikRefundResponse;
    private FreechargeRefundResponse freechargeRefundResponse;
    private ErrorResponse paytmRefundErrorResponse;
    private RazorRefundResponse razorRefundResponse;
}