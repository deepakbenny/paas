package com.swiggy.checkout.mobikwik.service;

import com.newrelic.api.agent.Trace;
import com.swiggy.checkout.data.entities.TransactionPaymentDetails;
import com.swiggy.checkout.data.model.bo.PaymentConfiguration;
import com.swiggy.checkout.mobikwik.entities.response.MobikwikVerifyPaymentResponse;
import com.swiggy.checkout.pojo.PaymentStatusResponse;
import com.swiggy.checkout.service.ValidPaymentStatus;
import com.swiggy.checkout.utils.Utility;
import lombok.extern.slf4j.Slf4j;
import org.apache.commons.codec.binary.Hex;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import javax.crypto.Mac;
import javax.crypto.spec.SecretKeySpec;
import java.io.BufferedReader;
import java.io.InputStreamReader;
import java.io.OutputStreamWriter;
import java.net.URL;
import java.net.URLConnection;
import java.security.SignatureException;
import java.time.LocalDateTime;

/**
 * Created by Manish on 29/01/17.
 */

@Service
@Slf4j
public class MobikwikService {

    private static final String MOBIKWIK_URL = "https://walletapi.mobikwik.com/checkstatus?mid=";

    @Autowired
    private PaymentConfiguration configuration;

    @Trace
    public boolean verifyPaymentStatus(TransactionPaymentDetails paymentDetails){

        try {
            MobikwikVerifyPaymentResponse responseObj = getMobikwikResponse(paymentDetails, true);
            if (responseObj != null ) {
                paymentDetails.setMobikwikResponse(responseObj);
                paymentDetails.setPgResponseTime(LocalDateTime.now());
                String statuscode = responseObj.getStatuscode();
                paymentDetails.setPaymentTxnId(responseObj.getRefid());
                if (statuscode != null && Long.parseLong(statuscode) == 0) {
                    paymentDetails.setPaymentTxnStatus(ValidPaymentStatus.SUCCESS);
                    return true;
                }
            }
            paymentDetails.setPaymentTxnStatus(ValidPaymentStatus.FAILURE);
        } catch (Exception e) {
            log.error("Mobikwik validation failed for order id {}", paymentDetails.getOrderId());
            log.error("",e);
        }
        return false;
    }

   public boolean verifyPaymentStatus(TransactionPaymentDetails paymentDetails, PaymentStatusResponse paymentStatusResponse, boolean isRedirection) {
        try {
            MobikwikVerifyPaymentResponse responseObj = getMobikwikResponse(paymentDetails, isRedirection);
            if (responseObj == null || Utility.isEmpty(responseObj.getStatuscode())) {
                paymentStatusResponse.setErrorMessage("Mobikwik response is empty");
                return false;
            }

            log.debug("mobikwik -------- {}", Utility.jsonEncode2(responseObj));
            paymentStatusResponse.setAmount(responseObj.getAmount());
            paymentStatusResponse.setPaymentStatus(Long.parseLong(responseObj.getStatuscode()) == 0? ValidPaymentStatus.SUCCESS :
                    ValidPaymentStatus.FAILURE);
            paymentStatusResponse.setTxnId(responseObj.getRefid());
        } catch (Exception e) {
            log.error("Mobikwik status check failed for order id {}", paymentDetails.getOrderId());
            log.error("",e);
            return false;
        }
        return true;
    }

    public static String calculateRFC2104HMAC(String data, String key)
            throws java.security.SignatureException
    {
        String result;
        try {
            SecretKeySpec signingKey = new SecretKeySpec(key.getBytes(), "HmacSHA256");
            // get an hmac_sha1 Mac instance and initialize with the signing key
            Mac mac = Mac.getInstance("HmacSHA256");
            mac.init(signingKey);
            byte[] rawHmac = mac.doFinal(data.getBytes());
            Hex hex = new Hex();
            byte[] encode = hex.encode(rawHmac);
            result = new String (encode);
        } catch (Exception e) {
            throw new SignatureException("Failed to generate HMAC : " + e.getMessage());
        }
        return result;
    }

   private MobikwikVerifyPaymentResponse getMobikwikResponse(TransactionPaymentDetails paymentDetails, boolean isRedirection) throws Exception {
        try {

            String merchantId = isRedirection ? configuration.getConfiguration("mobikwik_mid_redirect", "MBK3451") : configuration.getConfiguration("mobikwik_mid", "MBK6868");
            String secretKey = isRedirection ? configuration.getConfiguration("mobikwik_secret_key_redirect", "x6YWOuZSsSRsOmoTOP4Sv9rpy0gF")
                    : configuration.getConfiguration("mobikwik_secret_key", "1CgYJ36R1xQEJZYSteixqVc4AbIh") ;// uat ju6tygh7u7tdg554k098ujd5468o
            String checksum = calculateRFC2104HMAC("'" + merchantId + "'" + "'" + paymentDetails.getOrderId() + "'", secretKey);
            String apiURL = MOBIKWIK_URL + merchantId + "&orderid=" + paymentDetails.getOrderId() + "&checksum=" + checksum;
            URL url = new URL(apiURL);
            URLConnection conn = url.openConnection();
            conn.setDoOutput(true);
            conn.setRequestProperty("payloadtype", "json");
            conn.setConnectTimeout(configuration.getConfigurationAsInt("_mobikwik_connect_timeout", 2) * 1000);
            conn.setReadTimeout(configuration.getConfigurationAsInt("_mobikwik_timeout", 2) * 1000);
            log.info("Calling mobikwik status check api for order id {} api {} ", paymentDetails.getOrderId(), apiURL);
            OutputStreamWriter wr = new OutputStreamWriter(conn.getOutputStream());
            wr.flush();
            // Get the response
            BufferedReader rd = new BufferedReader(new InputStreamReader(conn.getInputStream()));
            String line;
            String response = "";
            while ((line = rd.readLine()) != null) {
                response += line;
            }
            MobikwikVerifyPaymentResponse responseObj = Utility.jsonDecode2(response, MobikwikVerifyPaymentResponse.class);
            log.debug("Mobikwik response {}", response);
            return responseObj;
        } catch (Exception e) {
            throw new Exception(e);
        }
    }
}

