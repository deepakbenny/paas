package com.swiggy.checkout.freecharge.entities.responses;

import com.swiggy.checkout.data.entities.RefundPaymentDetails;
import lombok.*;
import lombok.extern.slf4j.Slf4j;

/**
 * Created by Geetesh on 7/5/17.
 */
@Getter
@Setter
@ToString
@NoArgsConstructor
@Slf4j
public class RefundPaasResponse {

    private int statusCode;
    private String statusMessage;
    private RefundPaymentDetails data = new RefundPaymentDetails();
}
