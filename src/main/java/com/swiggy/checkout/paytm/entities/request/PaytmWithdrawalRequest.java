package com.swiggy.checkout.paytm.entities.request;

import com.fasterxml.jackson.annotation.JsonInclude;
import lombok.*;

/**
 * Created by Manish on 03/02/17.
 */

@Getter
@Setter
@JsonInclude(JsonInclude.Include.NON_NULL)
@Builder
@AllArgsConstructor
@NoArgsConstructor
public class PaytmWithdrawalRequest {

    private String ssoToken;
    private String preAuthId; // this is present in case of BlockedAmount request
    private String orderId;
    private String amount;
    private String customerId;
    private String deviceId;
    private String customerIP;
    private String promoCampId;
}
