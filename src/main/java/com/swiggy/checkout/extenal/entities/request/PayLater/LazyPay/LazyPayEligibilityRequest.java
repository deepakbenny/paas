package com.swiggy.checkout.extenal.entities.request.PayLater.LazyPay;

import com.fasterxml.jackson.annotation.JsonProperty;
import com.swiggy.checkout.pojo.PayLater.LazyPay.LazyPayAmountDetails;
import com.swiggy.checkout.pojo.PayLater.LazyPay.LazyPayUserDetails;
import lombok.Data;

/**
 * Created by chirag.rc on 10/23/17.
 */
@Data
public class LazyPayEligibilityRequest {

    @JsonProperty("userDetails")
    private LazyPayUserDetails lazyPayUserDetails;

    @JsonProperty("amount")
    private LazyPayAmountDetails lazyPayAmountDetails;

    @JsonProperty("source")
    private String merchantName;

}
