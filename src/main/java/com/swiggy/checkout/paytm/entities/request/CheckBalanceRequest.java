package com.swiggy.checkout.paytm.entities.request;

import com.fasterxml.jackson.annotation.JsonProperty;
import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

/**
 * Created by Manish on 29/01/17.
 */

@NoArgsConstructor
@AllArgsConstructor
@Getter
@Setter
public class CheckBalanceRequest {

    @JsonProperty("MID")
    private String mid;

    @JsonProperty("TOKEN")
    private String token;
}
