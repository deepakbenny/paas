package com.swiggy.checkout.service.paylater;

import com.swiggy.checkout.extenal.entities.request.PayLater.*;
import com.swiggy.checkout.extenal.entities.response.BaseResponse;
import com.swiggy.checkout.extenal.entities.response.PayLater.PayLaterRefundResponse;
import com.swiggy.checkout.extenal.entities.response.PayLater.PayLaterTxnEnquiryResponse;

import java.util.List;

/**
 * Created by chirag.rc on 10/13/17.
 */
public interface PayLaterBaseService {

    BaseResponse checkEligibility(PayLaterEligibilityRequest payLaterEligibilityRequest);

    BaseResponse getOtpForLinking(PayLaterGetOtpRequest payLaterGetOtpRequest);

    BaseResponse validate(PayLaterOtpValidationRequest payLaterOtpValidationRequest);

    BaseResponse debitAmount(PayLaterDebitAmountRequest payLaterDebitAmountRequest);

    BaseResponse delink(PayLaterDelinkRequest payLaterDelinkRequest);

    BaseResponse isLinked(PayLaterIsAccountLinkedRequest payLaterIsAccountLinkedRequest);

    PayLaterRefundResponse refund(PayLaterRefundRequest payLaterRefundRequest);

    BaseResponse resendOtp(PayLaterResendOtpRequest payLaterResendOtpRequest);

    List<PayLaterTxnEnquiryResponse> enquiry(PayLaterTxnEnquiryRequest payLaterTxnEnquiryRequest);
}
