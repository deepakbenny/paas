package com.swiggy.checkout.extenal.entities.request.PayLater;

import com.fasterxml.jackson.annotation.JsonProperty;
import lombok.Data;

/**
 * Created by chirag.rc on 10/18/17.
 */
@Data

public class PayLaterEligibilityRequest {

    @JsonProperty("user_id")
    private String userId;

    @JsonProperty("email")
    private String email;

    @JsonProperty("mobile")
    private String mobile;

    @JsonProperty("value")
    private String value;

    @JsonProperty("currency")
    private String currency = "INR";

    @JsonProperty("paylater_method")
    private String paylaterMethod;
}
