package com.swiggy.checkout.thirdparty.client.http;


import com.swiggy.checkout.data.entities.ThirdPartyEntity;
import com.swiggy.checkout.jackson.JacksonConverterFactory;
import com.swiggy.checkout.utils.HTTPLogger;
import com.swiggy.commons.Json;
import lombok.AccessLevel;
import lombok.NoArgsConstructor;
import okhttp3.Interceptor;
import okhttp3.logging.HttpLoggingInterceptor;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import retrofit2.Retrofit;

import java.io.IOException;
import java.util.LinkedHashMap;
import java.util.Map;
import java.util.concurrent.ConcurrentHashMap;
import java.util.concurrent.TimeUnit;

/**
 * Created by abhinav.chandel on 23/09/16.
 */
@NoArgsConstructor(access = AccessLevel.PRIVATE)
public final class ThirdPartyConfig {

    private static final int CONNECT_TIMEOUT_MS = 2000;
    private static final int READ_TIMEOUT_MS = 2000;
    private static final int WRITE_TIMEOUT_MS = 2000;

    private static final Logger logger = LoggerFactory.getLogger(ThirdPartyConfig.class);

    private static Map<String, ThirdPartyWithdrawHttpClient> withdrawMap = new ConcurrentHashMap<>();

    private static Map<String, ThirdPartyRefundHttpClient> refundMap = new ConcurrentHashMap<>();

    private static Map<String, ThirdPartyVerifyHttpClient> verifyMap = new ConcurrentHashMap<>();

    private static Map<String, ThirdPartyVerifyTransactionHttpClient> verifyTransactionMap = new ConcurrentHashMap<>();

    private static Map<String, ThirdPartyEditNotifyHttpClient> editNotifyMap = new ConcurrentHashMap<>();

    public static ThirdPartyWithdrawHttpClient getWithdrawClient(ThirdPartyEntity thirdParty){
        String source = thirdParty.getSource();

        if(withdrawMap.containsKey(source)) {
            return withdrawMap.get(source);
        } else {
            withdrawMap.putIfAbsent(source, thirdPartyWithdrawHttpClient(thirdParty));
            return withdrawMap.get(source);
        }
    }

    public static ThirdPartyVerifyHttpClient getVerifyClient(ThirdPartyEntity thirdParty) {
        String source = thirdParty.getSource();

        if(verifyMap.containsKey(source)) {
            return verifyMap.get(source);
        } else {
            verifyMap.putIfAbsent(source, thirdPartyVerifyHttpClient(thirdParty));
            return verifyMap.get(source);
        }
    }

    public static ThirdPartyVerifyTransactionHttpClient getVerifyTransactionClient(ThirdPartyEntity thirdParty) {
        String source = thirdParty.getSource();

        if(verifyTransactionMap.containsKey(source)) {
            return verifyTransactionMap.get(source);
        } else {
            verifyTransactionMap.putIfAbsent(source, thirdPartyVerifyTransactionHttpClient(thirdParty));
            return verifyTransactionMap.get(source);
        }
    }

    public static ThirdPartyRefundHttpClient getRefundClient(ThirdPartyEntity thirdParty){
        String source = thirdParty.getSource();
        if(refundMap.containsKey(source))
            return refundMap.get(source);
        else {
            refundMap.putIfAbsent(source, thirdPartyRefundHttpClient(thirdParty));
            return refundMap.get(source);
        }
    }

    public static ThirdPartyEditNotifyHttpClient getEditNotifyClient(ThirdPartyEntity thirdParty){
        String source = thirdParty.getSource();
        if(editNotifyMap.containsKey(source))
            return editNotifyMap.get(source);
        else {
            editNotifyMap.putIfAbsent(source, thirdPartyEditNotifyHttpClient(thirdParty));
            return editNotifyMap.get(source);
        }
    }

    private static ThirdPartyWithdrawHttpClient thirdPartyWithdrawHttpClient(ThirdPartyEntity thirdParty) throws NullPointerException {
        Retrofit retrofit = new Retrofit.Builder()
                .baseUrl(thirdParty.getWithdrawApi())
                .addConverterFactory(JacksonConverterFactory.create())
                .client(getHttpClientForWithdraw(thirdParty))
                .build();

        return retrofit.create(ThirdPartyWithdrawHttpClient.class);
    }


    private static ThirdPartyRefundHttpClient thirdPartyRefundHttpClient(ThirdPartyEntity thirdParty) throws NullPointerException{
        Retrofit retrofit = new Retrofit.Builder()
                .baseUrl(thirdParty.getRefundApi())
                .addConverterFactory(JacksonConverterFactory.create())
                .client(getHttpClientForRefund(thirdParty))
                .build();

        return retrofit.create(ThirdPartyRefundHttpClient.class);
    }

    private static ThirdPartyEditNotifyHttpClient thirdPartyEditNotifyHttpClient(ThirdPartyEntity thirdParty) throws NullPointerException{
        Retrofit retrofit = new Retrofit.Builder()
                .baseUrl(thirdParty.getEditNotifyApi())
                .addConverterFactory(JacksonConverterFactory.create())
                .client(getHttpClientForRefund(thirdParty))
                .build();

        return retrofit.create(ThirdPartyEditNotifyHttpClient.class);
    }

    private static ThirdPartyVerifyHttpClient thirdPartyVerifyHttpClient(ThirdPartyEntity thirdParty) throws NullPointerException{
        Retrofit retrofit = new Retrofit.Builder()
                .baseUrl(thirdParty.getVerifyApi())
                .addConverterFactory(JacksonConverterFactory.create())
                .client(getHttpClientForVerify(thirdParty))
                .build();

        return retrofit.create(ThirdPartyVerifyHttpClient.class);
    }

    private static ThirdPartyVerifyTransactionHttpClient thirdPartyVerifyTransactionHttpClient(ThirdPartyEntity thirdParty) throws NullPointerException{
        Retrofit retrofit = new Retrofit.Builder()
                .baseUrl(thirdParty.getVerifyTransactionApi())
                .addConverterFactory(JacksonConverterFactory.create())
                .client(getHttpClientForVerify(thirdParty))
                .build();

        return retrofit.create(ThirdPartyVerifyTransactionHttpClient.class);
    }

    private static okhttp3.OkHttpClient getHttpClientForVerify(ThirdPartyEntity thirdParty){
        okhttp3.OkHttpClient httpClient = new okhttp3.OkHttpClient.Builder()
                .addNetworkInterceptor(new Interceptor() {
                    @Override
                    public okhttp3.Response intercept(Chain chain) throws IOException {
                        okhttp3.Request.Builder builder = chain
                                .request()
                                .newBuilder();
                        String val  = thirdParty.getMetadata();
                        LinkedHashMap<String, String> map = (LinkedHashMap<String, String>)Json.deserialize(val, Object.class);
                        for (String key : map.keySet()) {
                            builder.addHeader(key, map.get(key));
                        }
                        okhttp3.Request request = builder.build();
                        return chain.proceed(request);
                    }
                })
                .addInterceptor(new HttpLoggingInterceptor(HTTPLogger.DEFAULT).setLevel(HttpLoggingInterceptor.Level.BODY))
                .connectTimeout(CONNECT_TIMEOUT_MS, TimeUnit.MILLISECONDS) // 1 second
                .readTimeout(READ_TIMEOUT_MS, TimeUnit.MILLISECONDS) // 10 seconds
                .writeTimeout(WRITE_TIMEOUT_MS, TimeUnit.MILLISECONDS)
                .build();

        return httpClient;
    }

    private static okhttp3.OkHttpClient getHttpClientForRefund(ThirdPartyEntity thirdParty){
        okhttp3.OkHttpClient httpClient = new okhttp3.OkHttpClient.Builder()
                .addNetworkInterceptor(new Interceptor() {
                    @Override
                    public okhttp3.Response intercept(Chain chain) throws IOException {
                        okhttp3.Request.Builder builder = chain
                                .request()
                                .newBuilder();
                        String val  = thirdParty.getMetadata();
                        LinkedHashMap<String, String> map = (LinkedHashMap<String, String>)Json.deserialize(val, Object.class);
                        for (String key : map.keySet()) {
                            builder.addHeader(key, map.get(key));
                        }
                        okhttp3.Request request = builder.build();
                        return chain.proceed(request);
                    }
                })
                .addInterceptor(new HttpLoggingInterceptor(HTTPLogger.DEFAULT).setLevel(HttpLoggingInterceptor.Level.BODY))
                .connectTimeout(CONNECT_TIMEOUT_MS, TimeUnit.MILLISECONDS) // 1 second
                .readTimeout(READ_TIMEOUT_MS, TimeUnit.MILLISECONDS) // 10 seconds
                .writeTimeout(WRITE_TIMEOUT_MS, TimeUnit.MILLISECONDS)
                .build();

        return httpClient;
    }


    private static okhttp3.OkHttpClient getHttpClientForWithdraw(ThirdPartyEntity thirdParty){
        okhttp3.OkHttpClient httpClient = new okhttp3.OkHttpClient.Builder()
                .addNetworkInterceptor(new Interceptor() {
                    @Override
                    public okhttp3.Response intercept(Chain chain) throws IOException {
                        okhttp3.Request.Builder builder = chain
                                .request()
                                .newBuilder();
                        String val  = thirdParty.getMetadata();
                        LinkedHashMap<String, String> map = (LinkedHashMap<String, String>)Json.deserialize(val, Object.class);
                        for (String key : map.keySet()) {
                            builder.addHeader(key, map.get(key));
                        }
                        okhttp3.Request request = builder.build();
                        return chain.proceed(request);
                    }
                })
                .addInterceptor(new HttpLoggingInterceptor(HTTPLogger.DEFAULT).setLevel(HttpLoggingInterceptor.Level.BODY))
                .connectTimeout(CONNECT_TIMEOUT_MS, TimeUnit.MILLISECONDS) // 1 second
                .readTimeout(READ_TIMEOUT_MS, TimeUnit.MILLISECONDS) // 10 seconds
                .writeTimeout(WRITE_TIMEOUT_MS, TimeUnit.MILLISECONDS)
                .build();

        return httpClient;
    }

}

