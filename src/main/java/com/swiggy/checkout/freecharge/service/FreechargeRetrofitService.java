package com.swiggy.checkout.freecharge.service;

import com.swiggy.checkout.exception.SwiggyPaymentException;
import com.swiggy.checkout.freecharge.entities.responses.FreechargeBaseResponse;
import com.swiggy.checkout.freecharge.exceptions.FreechargeFailureException;
import com.swiggy.checkout.utils.Utility;
import lombok.extern.slf4j.Slf4j;
import org.springframework.stereotype.Service;
import org.springframework.util.StringUtils;
import retrofit2.Call;
import retrofit2.Response;

import java.io.IOException;

/**
 * Created by Manish on 05/02/17.
 */

@Service
@Slf4j
public class FreechargeRetrofitService {

    public <T extends FreechargeBaseResponse> T getResponseOrLogWarning(Call<T> call, String failMessage) throws SwiggyPaymentException {
        try {
            return getResponse(call);
        } catch (FreechargeFailureException e) {
            log.warn(" {} fails with {}", failMessage, e.getMessage());
            throw new SwiggyPaymentException(1, failMessage, e.getMessage());
        }
    }

    private <T extends FreechargeBaseResponse> T getResponse(Call<T> call) throws FreechargeFailureException {
        try {
            Response<T> execute = call.execute();

            if (execute.isSuccessful()) {
                log.info(execute.body().toString());

                if(!StringUtils.isEmpty(execute.body().getErrorCode()))
                    throw new FreechargeFailureException(1, "freecharge api error response - " + Utility.jsonEncode(execute.body()));

                return execute.body();
            }
            throw new FreechargeFailureException(1, "freecharge apis replied failure - " + execute.errorBody().string());
        } catch (IOException e) {
            throw new FreechargeFailureException(1, e.getMessage());
        }
    }

}
