package com.swiggy.checkout.thirdparty.client.exchange;

import lombok.Getter;
import lombok.Setter;

/**
 * Created by abhinav.chandel on 9/19/16.
 */
@Getter
@Setter
public class HelpchatWithdrawResponse {
    private String message;
}
