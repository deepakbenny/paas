package com.swiggy.checkout.binbasedrouting;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.annotation.JsonProperty;
import lombok.*;

@NoArgsConstructor
@AllArgsConstructor
@Getter
@Setter
@Builder
@JsonIgnoreProperties(ignoreUnknown = true)
@JsonInclude(JsonInclude.Include.NON_NULL)
public class JusPayBinDetails {
    @JsonProperty("id")
    private String binNumber;

    @JsonProperty("brand")
    private String brand;

    @JsonProperty("bank")
    private String bank;

    @JsonProperty("country")
    private String isocountry;

    @JsonProperty("type")
    private String type;

}
