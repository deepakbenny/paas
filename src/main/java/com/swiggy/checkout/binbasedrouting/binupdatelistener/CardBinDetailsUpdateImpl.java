package com.swiggy.checkout.binbasedrouting.binupdatelistener;

import com.swiggy.checkout.binbasedrouting.BinNumberDetailsUtility;
import com.swiggy.checkout.data.entities.BinNumberDetailsEntity;
import com.swiggy.checkout.data.model.dao.BinNumberDetailsDao;
import com.swiggy.checkout.exception.CardPaymentException;
import com.swiggy.checkout.utils.Utility;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

@Service
@Slf4j
public class CardBinDetailsUpdateImpl implements CardBinDetailsUpdate {

    @Autowired
    private BinNumberDetailsDao binNumberDetailsDao;

    @Override
    public void updateBinDetails(String binId) throws CardPaymentException {
        long binNumber = Utility.getLong(binId);
        if(binNumber <= 0){
            return;
        }
        BinNumberDetailsEntity oldEntity = binNumberDetailsDao.getBinNumberDetailsEntitiesByBin(binNumber);
        if (BinNumberDetailsUtility.isUpdateRequired(oldEntity)) {
            BinNumberDetailsEntity newBinEntity = BinNumberDetailsUtility.getNewBinDetails(binNumber);
            //BinNumber is Invalid or JustPay returned
            if(newBinEntity == null){
                return;
            }
            if (oldEntity == null) {
                newBinEntity.setCreatedTime(newBinEntity.getLastUpdatedTime());
            } else {
                newBinEntity.setCreatedTime(oldEntity.getCreatedTime());
            }
            binNumberDetailsDao.save(newBinEntity);
        }
    }
}

