package com.swiggy.test.service;

import com.swiggy.test.AbstractControllerTest;
import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;
import org.springframework.http.MediaType;
import org.springframework.test.web.servlet.MvcResult;
import org.springframework.test.web.servlet.request.MockMvcRequestBuilders;
import org.springframework.transaction.annotation.Transactional;

/**
 * Created by Manish on 06/02/17.
 */

@Transactional
public class ConfigurationalControllerTest extends AbstractControllerTest{

    @Before
    public void setUp() {
        super.setUp();
    }


    @Test
    public void testGetAllConfigurations() throws Exception {

        String uri = "/v1/configuration/all";

        MvcResult result = mvc.perform(MockMvcRequestBuilders.get(uri)
                .accept(MediaType.APPLICATION_JSON)).andReturn();

        String content = result.getResponse().getContentAsString();
        int status = result.getResponse().getStatus();

        Assert.assertEquals("failure - expected HTTP status", 200, status);
        Assert.assertTrue(
                "failure - expected HTTP response body to have a value",
                content.trim().length() > 0);

    }


    @Test
    public void testSaveConfiguration() throws Exception {

        String uri = "/v1/configuration/save";
        String key = "name";
        String value = "val";

        MvcResult result = mvc.perform(MockMvcRequestBuilders.get(uri).param("key",key).param("value",value)
                .accept(MediaType.APPLICATION_JSON)).andReturn();

        String content = result.getResponse().getContentAsString();
        int status = result.getResponse().getStatus();

        Assert.assertEquals("failure - expected HTTP status 200", 200, status);
        Assert.assertTrue(
                "failure - expected HTTP response body to have a value",
                content.trim().length() > 0);

    }





}
