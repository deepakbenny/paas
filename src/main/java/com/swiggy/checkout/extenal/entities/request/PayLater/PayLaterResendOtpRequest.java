package com.swiggy.checkout.extenal.entities.request.PayLater;

import com.fasterxml.jackson.annotation.JsonProperty;
import lombok.Data;

/**
 * Created by chirag.rc on 10/30/17.
 */
@Data
public class PayLaterResendOtpRequest {

    @JsonProperty("txnRefNo")
    private String txnRefNo;

    @JsonProperty("paylater_method")
    private String paylaterMethod;
}
