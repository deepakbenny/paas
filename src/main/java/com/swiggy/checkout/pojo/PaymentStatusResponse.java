package com.swiggy.checkout.pojo;

import com.fasterxml.jackson.annotation.JsonIgnore;
import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.annotation.JsonProperty;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

/**
 * Created by Manish on 30/01/17.
 */
@Builder
@NoArgsConstructor
@AllArgsConstructor
@Data
@JsonInclude(JsonInclude.Include.NON_NULL)
public class PaymentStatusResponse {

    @JsonProperty("error_message")
    private String errorMessage;

    @JsonProperty("payment_verified")
    private boolean verified;

    @JsonProperty("payment_method")
    private String paymentMethod;

    @JsonProperty("payment_gateway")
    private String gateway;

    @JsonProperty("amount")
    private String amount;

    @JsonProperty("payment_status")
    private String paymentStatus;

    @JsonProperty("txn_id")
    private String txnId;

    @JsonProperty("bank_ref_num")
    String bankRefNumber;

    @JsonProperty("mihpayid")
    String mihPayId;

    @JsonProperty("refund_status")
    String refundStatus;

    @JsonProperty("refund_time")
    String refundTime;

    @JsonProperty("refund_method")
    String refundMethod;

    @JsonIgnore
    private PaymentResponseDecisionConstant refundable; //1 -> has to be refunded, 2 -> has to be queued again for later check, 3 -> failed

}
