package com.swiggy.checkout.freecharge.entities.request;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import com.fasterxml.jackson.annotation.JsonInclude;
import lombok.*;

/**
 * Created by Manish on 27/01/17.
 */
@Setter
@Getter
@NoArgsConstructor
@AllArgsConstructor
@Builder
@JsonInclude(JsonInclude.Include.NON_NULL)
@JsonIgnoreProperties(ignoreUnknown = true)
public class DebitRequest {

    private String amount;
    private String merchantId;
    private String merchantTxnId;
    private String channel;
    private String accessToken;
    private String currency = "INR";  //default
    private String checksum;
}
