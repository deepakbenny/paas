package com.swiggy.checkout.data.model.dao.impl;

import java.lang.reflect.ParameterizedType;
import java.lang.reflect.Type;

/**
 * Created by Manish on 24/01/17.
 */
public abstract class TypeReference<T> {
    private final Type type;

    protected TypeReference() {
        Type superclass = getClass().getGenericSuperclass();
        if (superclass instanceof Class<?>) {
            throw new RuntimeException("Missing type parameter.");
        }
        this.type = ((ParameterizedType) superclass).getActualTypeArguments()[0];
    }
    public Type getType() {
        return this.type;
    }
}
