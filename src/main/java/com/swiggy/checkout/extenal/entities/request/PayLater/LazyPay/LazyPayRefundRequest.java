package com.swiggy.checkout.extenal.entities.request.PayLater.LazyPay;

import com.fasterxml.jackson.annotation.JsonProperty;
import com.swiggy.checkout.pojo.PayLater.LazyPay.LazyPayAmountDetails;
import lombok.Data;

/**
 * Created by chirag.rc on 10/25/17.
 */
@Data
public class LazyPayRefundRequest {

    @JsonProperty("merchantTxnId")
    private String merchantTxnId;

    @JsonProperty("amount")
    private LazyPayAmountDetails lazyPayAmountDetails;
}
