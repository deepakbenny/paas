package com.swiggy.checkout.controller;

import com.swiggy.checkout.exception.SwiggyPaymentException;
import com.swiggy.checkout.extenal.entities.request.PayLater.*;
import com.swiggy.checkout.extenal.entities.response.BaseResponse;
import com.swiggy.checkout.extenal.entities.response.PayLater.PayLaterRefundResponse;
import com.swiggy.checkout.extenal.entities.response.PayLater.PayLaterTxnEnquiryResponse;
import com.swiggy.checkout.service.paylater.PayLaterService;
import io.swagger.annotations.ApiOperation;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.MediaType;
import org.springframework.web.bind.annotation.*;

import java.util.List;

/**
 * Created by chirag.rc on 10/13/17.
 */
@RestController
@RequestMapping("/paylater")
@Slf4j
public class PayLaterController {

    @Autowired
    private PayLaterService payLaterService;

    @ApiOperation(value = "check eligibility for paylater payment method", notes = "Api to check eligibility for a particular paylater payment option for the user.")
    @RequestMapping(
            value = "/v1/checkEligibility",
            method = RequestMethod.POST,
            produces = MediaType.APPLICATION_JSON_VALUE
    )
    public BaseResponse checkEligibility(@RequestBody PayLaterEligibilityRequest payLaterEligibilityRequest) throws SwiggyPaymentException {
        return payLaterService.checkEligibility(payLaterEligibilityRequest);
    }

    @ApiOperation(value = "link paylater payment method", notes = "Api to link a particular paylater payment option with user.")
    @RequestMapping(
            value = "/v1/link",
            method = RequestMethod.POST,
            produces = MediaType.APPLICATION_JSON_VALUE
    )
    public BaseResponse getOtpForLinking(@RequestBody PayLaterGetOtpRequest payLaterGetOtpRequest) throws SwiggyPaymentException {
        return payLaterService.getOtpForLinking(payLaterGetOtpRequest);
    }


    @ApiOperation(value = "validate", notes = "Api to validate otp.")
    @RequestMapping(
            value = "/v1/validate",
            method = RequestMethod.POST,
            produces = MediaType.APPLICATION_JSON_VALUE
    )
    public BaseResponse validate(@RequestBody PayLaterOtpValidationRequest payLaterOtpValidationRequest) throws SwiggyPaymentException {
        return payLaterService.validate(payLaterOtpValidationRequest);
    }

    @ApiOperation(value = "check credit", notes = "Api to check credit available in a particular payment option")
    @RequestMapping(
            value = "/v1/check-credit",
            method = RequestMethod.GET,
            produces = MediaType.APPLICATION_JSON_VALUE
    )
    public BaseResponse checkBalance(@RequestParam(value = "paylater_method") String paylaterMethod) throws SwiggyPaymentException {
        return payLaterService.checkBalance(paylaterMethod);
    }

    @ApiOperation(value = "debit", notes = "Api to debit amount from a particular paylater payment option")
    @RequestMapping(
            value = "/v1/debit-amount",
            method = RequestMethod.POST,
            produces = MediaType.APPLICATION_JSON_VALUE
    )
    public BaseResponse debitAmount(@RequestBody PayLaterDebitAmountRequest payLaterDebitAmountRequest) throws SwiggyPaymentException {
        return payLaterService.debitAmount(payLaterDebitAmountRequest);
    }

    @ApiOperation(value = "delink", notes = "Api to delink user from a particular paylater payment option")
    @RequestMapping(
            value = "/v1/delink",
            method = RequestMethod.DELETE,
            produces = MediaType.APPLICATION_JSON_VALUE
    )
    public BaseResponse delink(@RequestBody PayLaterDelinkRequest payLaterDelinkRequest) throws SwiggyPaymentException {
        return payLaterService.delink(payLaterDelinkRequest);
    }

    @ApiOperation(value = "isLinked", notes = "Api to check if a particular paylater payment option is linked to user's swiggy account")
    @RequestMapping(
            value = "/v1/isLinked",
            method = RequestMethod.POST,
            produces = MediaType.APPLICATION_JSON_VALUE
    )
    public BaseResponse isLinked(@RequestBody PayLaterIsAccountLinkedRequest payLaterIsAccountLinkedRequest) throws SwiggyPaymentException {
        return payLaterService.isLinked(payLaterIsAccountLinkedRequest);
    }

    @ApiOperation(value = "resend Otp", notes = "Api to resend otp.")
    @RequestMapping(
            value = "/v1/resendOtp",
            method = RequestMethod.POST,
            produces = MediaType.APPLICATION_JSON_VALUE
    )
    public BaseResponse resendOtp(@RequestBody PayLaterResendOtpRequest payLaterResendOtpRequest) throws SwiggyPaymentException {
        return payLaterService.resendOtp(payLaterResendOtpRequest);
    }


    @ApiOperation(value = "transaction enquiry", notes = "Api to check transaction status.")
    @RequestMapping(
            value = "/v1/enquire",
            method = RequestMethod.POST,
            produces = MediaType.APPLICATION_JSON_VALUE
    )
    public List<PayLaterTxnEnquiryResponse> checkTransactionStatus(@RequestBody PayLaterTxnEnquiryRequest payLaterTxnEnquiryRequest) throws SwiggyPaymentException {
        return payLaterService.enquiry(payLaterTxnEnquiryRequest);
    }

    @ApiOperation(value = " refund", notes = "Api for refund.")
    @RequestMapping(
            value = "/v1/refund",
            method = RequestMethod.POST,
            produces = MediaType.APPLICATION_JSON_VALUE
    )
    public PayLaterRefundResponse refund(@RequestBody PayLaterRefundRequest payLaterRefundRequest) throws SwiggyPaymentException {
        return payLaterService.refund(payLaterRefundRequest);
    }
}
