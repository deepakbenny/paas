package com.swiggy.checkout.freecharge.service;


import com.swiggy.checkout.freecharge.entities.request.LoginRequest;
import com.swiggy.checkout.freecharge.entities.request.OtpRequest;
import com.swiggy.checkout.freecharge.entities.request.OtpResendRequest;
import com.swiggy.checkout.freecharge.entities.request.RegisterRequest;
import com.swiggy.checkout.freecharge.entities.responses.LoginResponse;
import com.swiggy.checkout.freecharge.entities.responses.OtpResponse;
import retrofit2.Call;
import retrofit2.http.Body;
import retrofit2.http.POST;

/**
 * Created by Manish on 05/02/17.
 */

public interface FreechargeClient {

    @POST("v2/co/oauth/user/generate/otp")
    Call<OtpResponse> generateOtp(@Body OtpRequest otpRequest);

    @POST("v2/co/oauth/user/resend/otp")
    Call<OtpResponse> resendOtp(@Body OtpResendRequest otpResendRequest);

    @POST("v2/co/oauth/user/login")
    Call<LoginResponse> verifyUser(@Body LoginRequest loginRequest);

    @POST("v2/co/oauth/exchange/token")
    Call<LoginResponse> registerUser(@Body RegisterRequest registerRequest);

}
