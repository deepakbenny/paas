package com.swiggy.checkout.thirdparty.client.exchange;

import lombok.Getter;
import lombok.Setter;

import java.util.List;

/**
 * Created by abhinav.chandel on 9/19/16.
 */
@Getter
@Setter
public class HelpchatEditResponse {
    private String message;
    private List<String> status;
}
