package com.swiggy.checkout.paytm.service;

import com.fasterxml.jackson.core.JsonProcessingException;
import com.newrelic.api.agent.Trace;
import com.swiggy.checkout.data.entities.UserTokenDetailsEntity;
import com.swiggy.checkout.data.model.bo.PaymentConfiguration;
import com.swiggy.checkout.data.model.dao.UserTokenDetailsDao;
import com.swiggy.checkout.exception.SwiggyPaymentException;
import com.swiggy.checkout.extenal.entities.request.WalletCalculateChecksumRequest;
import com.swiggy.checkout.extenal.entities.request.WalletRegisterNewUserRequest;
import com.swiggy.checkout.extenal.entities.request.WalletResendOTPRequest;
import com.swiggy.checkout.extenal.entities.response.BaseResponse;
import com.swiggy.checkout.extenal.entities.response.ErrorResponse;
import com.swiggy.checkout.extenal.entities.response.SuccessResponse;
import com.swiggy.checkout.freecharge.entities.responses.RefundPaasResponse;
import com.swiggy.checkout.jackson.JacksonConverterFactory;
import com.swiggy.checkout.paytm.PaytmProperties;
import com.swiggy.checkout.paytm.entities.request.*;
import com.swiggy.checkout.paytm.entities.response.CheckBalanceResponse;
import com.swiggy.checkout.paytm.entities.response.RefundResponse;
import com.swiggy.checkout.paytm.entities.response.WithdrawAmountResponse;
import com.swiggy.checkout.pojo.User;
import com.swiggy.checkout.service.WalletTokenMigrationService;
import com.swiggy.checkout.utils.HTTPLogger;
import com.swiggy.checkout.utils.Utility;
import com.swiggy.checkout.wallet.WalletBaseService;
import lombok.extern.slf4j.Slf4j;
import okhttp3.Interceptor;
import okhttp3.OkHttpClient;
import okhttp3.Request;
import okhttp3.logging.HttpLoggingInterceptor;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.util.StringUtils;
import retrofit2.Call;
import retrofit2.Retrofit;

import javax.annotation.PostConstruct;
import java.io.IOException;
import java.net.URLEncoder;
import java.util.TreeMap;
import java.util.concurrent.TimeUnit;

import com.paytm.merchant.CheckSumServiceHelper;

/**
 * Created by Manish on 03/02/17.
 */

@Service
@Slf4j
public class PaytmWalletService implements WalletBaseService {

    // Fixed set of parameters required for querying paytm withdrawal APIs
    private static final String PAYTM_AUTH_MODE = "USRPWD";
    private static final String PAYTM_CURRENCY = "INR";
    private static final String PAYTM_PAYMENT_MODE = "PPI";
    private static final String PAYTM_PRE_AUTH_WITHDRAWAL_PAYMENT_REQUEST_TYPE = "CAPTURE";
    private static final String PAYTM_DIRECT_DEBIT_PAYMENT_REQUEST_TYPE = "WITHDRAW";
    private static final String PAYTM_PAYMENT_CHANNEL = "WEB"; // paytm API will respond error if channel is anything other than WEB
    private static final String PAYTM_REFUND_TXN_TYPE = "REFUND";
    private static final String PAYTM_REFUND_INIT = "Refund-";

    private static final String BLOCKED_AMOUNT_SUCCESS = "TXN_SUCCESS";
    public static final String PAYTM_PG_MID = "paytm_pg_mid";
    private static final String PAYTM = "PayTM";
    private static final String USER_TOKEN_BACKFILL_ENABLED_NEW = "user_token_backfill_enabled_new";


    private PaytmWalletClient paytmWalletClient;

    @Autowired
    private PaytmProperties paytmProperties;

    @Autowired
    private PayTmRetrofitService retrofitService;

    @Autowired
    private PaymentConfiguration configuration;

    @Autowired
    private PaytmAuthService paytmAuthService;

    @Autowired
    private UserTokenDetailsDao userTokenDetailsDao;

    @Autowired
    private WalletTokenMigrationService walletTokenMigrationService;

    @PostConstruct
    public void postConstruct() {

        OkHttpClient httpClient = new OkHttpClient.Builder()
                .addNetworkInterceptor(new Interceptor() {
                    @Override
                    public okhttp3.Response intercept(Chain chain) throws IOException {
                        Request request = chain
                                .request()
                                .newBuilder()
                                .addHeader("Authorization", paytmProperties.getBasicAuth()) // "Basic YjUzOGIwODJiOGJjMzEwNDRhNmU2YzllZDg0NmRkM2Y6YzcwOTY2OGI5MTExZWFmZmNjMWE3ZGQ1YjM3YzNhMzc="
                                .addHeader("User-Agent", "Swiggy-API")
                                .build();
                        return chain.proceed(request);
                    }
                })
                .addInterceptor(new HttpLoggingInterceptor(HTTPLogger.DEFAULT).setLevel(HttpLoggingInterceptor.Level.BODY))
                .connectTimeout(1000, TimeUnit.MILLISECONDS) // 1 second
                .readTimeout(5000, TimeUnit.MILLISECONDS) // 5 seconds
                .build();

        Retrofit retrofit = new Retrofit.Builder()
                .baseUrl(paytmProperties.getWalletUrl()) // paytmProperties.getAuthUrl() or "https://accounts-uat.paytm.com"
                .addConverterFactory(JacksonConverterFactory.create())
                .client(httpClient)
                .build();

        paytmWalletClient = retrofit.create(PaytmWalletClient.class);

    }

    /**
     * Method to get balance of the current logged in user
     *
     * @return
     * @throws SwiggyPaymentException
     */
    public BaseResponse checkBalance() throws SwiggyPaymentException {

        String userId = Utility.getUserFromHeader().getCustomerId();
        UserTokenDetailsEntity userTokenDetails = userTokenDetailsDao.findByUserIdAndPaymentMethodName(Utility.getLong(userId), PAYTM);

        // TODO to be removed after token migration
        if (null == userTokenDetails && Utility.getUserFromHeader().getPaytmSsoToken() != null && configuration.getConfigurationAsBoolean(USER_TOKEN_BACKFILL_ENABLED_NEW, true)) {
            userTokenDetails = walletTokenMigrationService.saveUserTokenDetails(userId, Utility.getUserFromHeader().getPaytmSsoToken(), PAYTM);
        }

        if (null != userTokenDetails) {
            try {
                Call<CheckBalanceResponse> checkBalanceResponseCall = paytmWalletClient.checkBalance(Utility.jsonEncode(new CheckBalanceRequest(paytmProperties.getMid(), userTokenDetails.getToken())));
                BaseResponse baseResponse = new SuccessResponse();
                baseResponse.setData(retrofitService.getResponseOrLogWarning(checkBalanceResponseCall, "Couldnt retrieve balance"));
                return baseResponse;
            } catch (JsonProcessingException e) {
                e.printStackTrace();
            }
        }
        throw new SwiggyPaymentException(1, "Couldnt retrieve balance");
    }


    @Trace
    public BaseResponse debitAmount(String amount, String orderId, String couponId) {
        PaytmWithdrawalRequest paytmWithdrawalRequest = buildPaytmWithdrawalRequest(amount, orderId);

        // If preAuth id is there, it means we are withdrawing money for previously blocked amount
        // Else we are directly deducting money from user's account
        String requestType = StringUtils.isEmpty(paytmWithdrawalRequest.getPreAuthId())
                ? PAYTM_DIRECT_DEBIT_PAYMENT_REQUEST_TYPE
                : PAYTM_PRE_AUTH_WITHDRAWAL_PAYMENT_REQUEST_TYPE;

        TreeMap<String, String> parameters = new TreeMap<>();
        parameters.put("AppIP", paytmWithdrawalRequest.getCustomerIP());
        parameters.put("AuthMode", PAYTM_AUTH_MODE);
        parameters.put("Channel", PAYTM_PAYMENT_CHANNEL);
        parameters.put("Currency", PAYTM_CURRENCY);
        parameters.put("IndustryType", paytmProperties.getIndustryType());
        parameters.put("MID", paytmProperties.getMid());
        parameters.put("CustId", paytmWithdrawalRequest.getCustomerId());
        parameters.put("DeviceId", paytmWithdrawalRequest.getDeviceId());
        parameters.put("OrderId", paytmWithdrawalRequest.getOrderId());
        parameters.put("PaymentMode", PAYTM_PAYMENT_MODE);
        parameters.put("ReqType", requestType);
        parameters.put("TxnAmount", paytmWithdrawalRequest.getAmount());

        if (configuration.getConfiguration("paytmsso_promo_code_send_in_request", "true").equalsIgnoreCase("true") && couponId != null)
            parameters.put("PROMO_CAMP_ID", couponId);


        if (!StringUtils.isEmpty(paytmWithdrawalRequest.getPreAuthId()))
            parameters.put("PREAUTH_ID", paytmWithdrawalRequest.getPreAuthId());

        parameters.put("SSOToken", paytmWithdrawalRequest.getSsoToken());

        try {
            String checkSum = CheckSumServiceHelper.getCheckSumServiceHelper().genrateCheckSum(paytmProperties.getMerchantKey(), parameters);
            checkSum = URLEncoder.encode(checkSum, "UTF-8");

            WithdrawAmountRequest withdrawAmountRequest = WithdrawAmountRequest.builder()
                    .appIp(paytmWithdrawalRequest.getCustomerIP())
                    .authMode(PAYTM_AUTH_MODE)
                    .channel(PAYTM_PAYMENT_CHANNEL)
                    .currency(PAYTM_CURRENCY)
                    .industryType(paytmProperties.getIndustryType())
                    .mid(paytmProperties.getMid())
                    .customerId(paytmWithdrawalRequest.getCustomerId())
                    .deviceId(paytmWithdrawalRequest.getDeviceId())
                    .orderId(paytmWithdrawalRequest.getOrderId())
                    .paymentMode(PAYTM_PAYMENT_MODE)
                    .reqType(requestType)
                    .txnAmount(paytmWithdrawalRequest.getAmount())
                    .preAuthId(paytmWithdrawalRequest.getPreAuthId())
                    .ssOToken(paytmWithdrawalRequest.getSsoToken())
                    .checkSum(checkSum)
                    .promoCampId(couponId)
                    .build();
            log.info("PAYTM WITHDRAW REQUEST {}", Utility.jsonEncode2(withdrawAmountRequest));

            Call<WithdrawAmountResponse> withdrawAmountResponseCall = paytmWalletClient.withdrawBlockedAmount(Utility.jsonEncode(withdrawAmountRequest));
            WithdrawAmountResponse withdrawAmountResponse = retrofitService.getResponseOrLogWarning(withdrawAmountResponseCall, "Couldn't withdraw the amount");

            if (BLOCKED_AMOUNT_SUCCESS.equals(withdrawAmountResponse.getStatus())) {
                return new SuccessResponse(withdrawAmountResponse);
            } else {
                return new ErrorResponse(withdrawAmountResponse.getResponseMessage(), withdrawAmountResponse);
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
        return new ErrorResponse(1, "Couldn't withdraw the amount");
    }

    public RefundPaasResponse refund(PaytmRefundRequest paytmRefundRequest, boolean pgCall) {

        TreeMap<String, String> parameters = new TreeMap<String, String>();
        String mid = pgCall ? configuration.getConfiguration(PAYTM_PG_MID, "swiggy58513599375770") : paytmProperties.getMid();
        String merchantKey = pgCall ? paytmProperties.getPaytmPgMerchantKey() : paytmProperties.getMerchantKey();
        parameters.put("MID", mid);
        parameters.put("ORDERID", paytmRefundRequest.getOrderId());
        parameters.put("TXNID", paytmRefundRequest.getTxnId());
        parameters.put("REFUNDAMOUNT", paytmRefundRequest.getRefundAmount());
        parameters.put("TXNTYPE", PAYTM_REFUND_TXN_TYPE);
        parameters.put("REFID", PAYTM_REFUND_INIT + paytmRefundRequest.getOrderId());
        RefundPaasResponse refundPaasResponse = new RefundPaasResponse();

        try {
            String checkSum = CheckSumServiceHelper.getCheckSumServiceHelper().genrateCheckSum(merchantKey, parameters);

            //encoding this due to refund failure (checksum mismatch error), no need to encode other params as no special characters excepted there
            checkSum = URLEncoder.encode(checkSum, "UTF-8");

            RefundRequest refundRequest = RefundRequest.builder()
                    .mid(mid)
                    .orderId(paytmRefundRequest.getOrderId())
                    .refundAmount(paytmRefundRequest.getRefundAmount())
                    .txnId(paytmRefundRequest.getTxnId())
                    .txnType(PAYTM_REFUND_TXN_TYPE)
                    .refundId(PAYTM_REFUND_INIT + paytmRefundRequest.getOrderId())
                    .checkSum(checkSum)
                    .build();

            Call<RefundResponse> refundResponseCall = paytmWalletClient.refund(Utility.jsonEncode(refundRequest));
            RefundResponse refundResponse = retrofitService.getResponseOrLogWarning(refundResponseCall, "Couldn't refund the amount");

            if (BLOCKED_AMOUNT_SUCCESS.equals(refundResponse.getSTATUS())) {
                refundPaasResponse.getData().setPaytmRefundResponse(refundResponse);
            } else {
                refundPaasResponse.getData().setPaytmRefundErrorResponse(new ErrorResponse(refundResponse.getSTATUS(), refundResponse));
            }
            return refundPaasResponse;

        } catch (Exception e) {
            e.printStackTrace();
        }
        refundPaasResponse.getData().setPaytmRefundErrorResponse(new ErrorResponse(1, "Couldn't refund the amount"));
        return refundPaasResponse;
    }


    public PaytmWithdrawalRequest buildPaytmWithdrawalRequest(String amount, String orderId) {
        User user = Utility.getUserFromHeader();
        UserTokenDetailsEntity userTokenDetailsEntity = userTokenDetailsDao.findByUserIdAndPaymentMethodName(Utility.getLong(user.getCustomerId()), PAYTM);
        //TODO -- to be removed after token migration
        if (null == userTokenDetailsEntity && configuration.getConfigurationAsBoolean(USER_TOKEN_BACKFILL_ENABLED_NEW, true)) {
            userTokenDetailsEntity = walletTokenMigrationService.saveUserTokenDetails(user.getCustomerId(), user.getPaytmSsoToken(), PAYTM);
        }

        return PaytmWithdrawalRequest.builder()
                .ssoToken((null != userTokenDetailsEntity) ? userTokenDetailsEntity.getToken() : Utility.getUserFromHeader().getPaytmSsoToken())
                .orderId(orderId)
                .amount(amount)
                .customerId(user.getCustomerId())
                .deviceId(orderId)
                .customerIP(user.getCustomerIp())
                .promoCampId(user.getCoupon())
                .build();
    }


    public BaseResponse generateOTP() throws SwiggyPaymentException {
        return paytmAuthService.getOtpForLinkingWallet();
    }

    public BaseResponse generateToken(String otp, String state) throws SwiggyPaymentException {
        return paytmAuthService.validateOtpForLinkingWallet(otp, state);
    }

    public BaseResponse delink() throws SwiggyPaymentException {
        return paytmAuthService.delink();
    }

    public BaseResponse getSSOToken() throws SwiggyPaymentException {
        return paytmAuthService.getSsoToken();
    }

    @Override
    public BaseResponse resendOTP(WalletResendOTPRequest walletResendOTPRequest) throws SwiggyPaymentException {
        //Not supported by PayTM
        return null;
    }

    @Override
    public BaseResponse generateRegisterUserToken(WalletRegisterNewUserRequest walletRegisterNewUserRequest) throws SwiggyPaymentException {
        //Not supported by PayTM
        return null;
    }

    @Override
    public BaseResponse generateLoginToken() throws SwiggyPaymentException {
        //Not supported by PayTM
        return null;
    }

    @Override
    public BaseResponse computeChecksum(WalletCalculateChecksumRequest walletCalculateChecksumRequest) throws SwiggyPaymentException {
        //Not supported by PayTM
        return null;
    }
}
