package com.swiggy.checkout.paytm.entities.response;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.annotation.JsonProperty;
import lombok.AllArgsConstructor;
import lombok.NoArgsConstructor;

/**
 * Created by Manish on 29/01/17.
 */

@NoArgsConstructor
@AllArgsConstructor
@JsonIgnoreProperties(ignoreUnknown = true)
@JsonInclude(JsonInclude.Include.NON_NULL)
public class CheckBalanceResponse extends PaytmBaseResponse {

    private String walletBalance;

    private String status;

    private String responseCode;

    @JsonProperty("walletBalance")
    public String getWalletBalance() {
        return walletBalance;
    }

    @JsonProperty("WALLETBALANCE")
    public void setWalletBalance(String walletBalance) {
        this.walletBalance = walletBalance;
    }

    @JsonProperty("status")
    public String getStatus() {
        return status;
    }

    @JsonProperty("STATUS")
    public void setStatus(String status) {
        this.status = status;
    }

    @JsonProperty("responseCode")
    public String getResponseCode() {
        return responseCode;
    }

    @JsonProperty("RESPONSECODE")
    public void setResponseCode(String responseCode) {
        this.responseCode = responseCode;
    }
}
