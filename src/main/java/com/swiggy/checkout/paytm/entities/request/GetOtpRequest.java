package com.swiggy.checkout.paytm.entities.request;

import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

/**
 * Created by Manish on 29/01/17.
 */

@NoArgsConstructor
@AllArgsConstructor
@Getter
@Setter
public class GetOtpRequest {
//    private String email;
    private String phone;
    private String clientId;
    private String scope;
    private String responseType;
}
