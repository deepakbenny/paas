package com.swiggy.checkout.data.entities;

import lombok.*;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.Table;
import java.io.Serializable;
import java.sql.Timestamp;

/**
 * Created by Manish on 03/02/17.
 */

@Entity
@Table(name = "payment_routing")
@Data
@Builder
@NoArgsConstructor
@AllArgsConstructor
@Getter
@Setter
public class PaymentRoutingEntity implements Serializable {

    public static final String DEFAULT_CC_DC = "CARD";
    public static final String DEFAULT_PAYMENT_TYPE = "All";
    public static final String DEFAULT_BANK = "All";
    public static final String DEFAULT_BRAND = "All";
    public static final String DEFAULT_GATEWAY = "PayU";
    public static final String NET_BANKING = "NB";

    @Id
    @Column(name = "id", nullable = false, insertable = true, updatable = true)
    private long id;
    //this is used for brand
    @Column(name = "card_type", length = 50)
    private String cardType;
    //this is used for credit/debit/nb/card
    @Column(name = "payment_type", length = 20)
    private String paymentType;
    @Column(name = "bank", length = 100)
    private String bank;
    @Column(name = "payment_gateway", length = 50)
    private String paymentGateway;
    @Column(name = "percentage", length = 3)
    private int percentage;
    @Column(name = "updated_by", length = 60)
    private String updatedBy;
    @Column(name = "created_on", columnDefinition = "NOT NULL DEFAULT '0000-00-00 00:00:00'")
    private Timestamp createdOn;
    @Column(name = "enabled", length = 1)
    private boolean enabled;
}
