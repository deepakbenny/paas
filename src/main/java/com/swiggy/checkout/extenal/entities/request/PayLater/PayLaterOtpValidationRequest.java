package com.swiggy.checkout.extenal.entities.request.PayLater;

import com.fasterxml.jackson.annotation.JsonProperty;
import lombok.Data;

/**
 * Created by chirag.rc on 10/13/17.
 */
@Data
public class PayLaterOtpValidationRequest {

    @JsonProperty("user_id")
    private long userId;

    @JsonProperty("paymentMode")
    private String paymentMode;

    @JsonProperty("txnRefNo")
    private String txnRefNo;

    @JsonProperty("otp")
    private String otp;

    @JsonProperty("paylater_method")
    private String paylaterMethod;
}
