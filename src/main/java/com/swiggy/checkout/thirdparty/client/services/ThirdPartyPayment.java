package com.swiggy.checkout.thirdparty.client.services;

import com.swiggy.checkout.data.entities.TransactionPaymentDetails;

import java.util.Map;

/**
 * Created by abhinav.chandel on 9/17/16.
 */
public interface ThirdPartyPayment {
    /**
     * Return json dump for withdraw request
     * @param paymentDetails
     * @param thirdPartyClientCash
     *
     * @return String
     */
    public String createWithdrawRequest(TransactionPaymentDetails paymentDetails, double thirdPartyClientCash);

    /**
     * Return json dump for refund request
     * @param paymentDetails
     * @param thirdPartyClientCash
     *
     * @return String
     */
    public Map<String, String> createRefundRequest(TransactionPaymentDetails paymentDetails, double thirdPartyClientCash);

    /**
     * Return json dump for verify cashback request
     * @param paymentDetails
     *
     * @return String
     */
    public Map<String, String> createVerifyCashbackRequest(TransactionPaymentDetails paymentDetails);

    /**
     * Return whether withdraw is successful given request
     * @param requestBody
     *
     * @return boolean
     */
    public boolean processWithdrawResponse(String requestBody);

    /**
     * Return whether refund is successful given request
     * @param requestBody
     *
     * @return boolean
     */
    public boolean processRefundResponse(String requestBody);

    /**
     * Return whether verify request is successful
     * @param response
     *
     * @return boolean
     */
    public double processVerifyCashbackResponse(String response);

    /**
     * Return json dump for verify cashback request
     * @param paymentDetails
     *
     * @return String
     */
    public Map<String, String> createVerifyTransactionRequest(TransactionPaymentDetails paymentDetails);

    /**
     * Return whether verify request is successful
     * @param response
     *
     * @return boolean
     */
    public double processVerifyTransactionResponse(String response);


    /**
     * Return json dump for edit notify request
     * @param paymentDetails
     *
     * @return String
     */
    public Map<String, String> createEditRequest(TransactionPaymentDetails paymentDetails);

    /**
     * Return whether refund is successful given request
     * @param requestBody
     *
     * @return boolean
     */
    public boolean processEditResponse(String requestBody);

}
