package com.swiggy.checkout.mobikwik.service;

import com.swiggy.checkout.data.model.bo.PaymentConfiguration;
import com.swiggy.checkout.exception.*;
import com.swiggy.checkout.mobikwik.entities.response.MobikwikBaseResponse;
import com.swiggy.checkout.utils.Utility;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import retrofit2.Call;
import retrofit2.Response;

import java.io.IOException;

/**
 * Created by Manish on 29/01/17.
 */

@Service
@Slf4j
public class MobikwikRetrofitService {

    private static final String MOBIKWIK_API_SUCCESS = "0";
    private static final String MOBIKWIK_API_TOKEN_TIMEOUT_1 = "198";
    private static final String MOBIKWIK_API_TOKEN_TIMEOUT_2 = "199";
    private static final String MOBIKWIK_API_INVALID_OTP = "155";
    private static final String MOBIKWIK_EMAIL_ALREADY_EXISTS = "152";

    @Autowired
    private PaymentConfiguration configuration;

    <T extends MobikwikBaseResponse> T getResponseOrLogWarning(Call<T> call, String failMessage) throws SwiggyPaymentException {

        try {

            return getResponse(call);
        } catch (MobikwikFailureException failureException) {
            failureException.printStackTrace();
            log.warn(" Reason - " + failureException.getMessage());
            throw new SwiggyPaymentException(1, failMessage);
        } catch (MobikwikTokenTimeoutException e) {
            log.error("Token timed out", e);
            throw new SwiggyPaymentException(e.getCode(), e.getMessage());
        } catch (MobikwikInvalidOTPException e) {
            log.error("Invalid OTP", e);
            throw new SwiggyPaymentException(e.getCode(), e.getMessage());
        } catch (MobikwikEmailRegisteredException e) {
            log.error("Email already registered", e);
            throw new SwiggyPaymentException(e.getCode(), e.getMessage());
        }
    }


    private <T extends MobikwikBaseResponse> T getResponse(Call<T> call) throws MobikwikFailureException, MobikwikTokenTimeoutException, MobikwikInvalidOTPException, MobikwikEmailRegisteredException {

        try {
            Response<T> execute = call.execute();

            if (execute.isSuccessful()) {
                log.info(execute.body().toString());

                // handling of 198 and 199 error code - this will mean token is expired
                if (MOBIKWIK_API_TOKEN_TIMEOUT_1.equals(execute.body().getStatuscode()) || MOBIKWIK_API_TOKEN_TIMEOUT_2.equals(execute.body().getStatuscode()))
                    throw new MobikwikTokenTimeoutException(3, "mobikwik token expired. Please relink account");

                // handling of 152 - this will mean customer email id already registered
                if (MOBIKWIK_EMAIL_ALREADY_EXISTS.equals(execute.body().getStatuscode()))
                    throw new MobikwikEmailRegisteredException(3, configuration.getConfiguration("mobikwik_email_exists_error",
                            "Email already registered with mobikwik"));

                if (MOBIKWIK_API_INVALID_OTP.equals(execute.body().getStatuscode()))
                    throw new MobikwikInvalidOTPException(4, "Invalid OTP");

                if (!MOBIKWIK_API_SUCCESS.equals(execute.body().getStatuscode()))
                    throw new MobikwikFailureException(1, "mobikwik api replied with failure response - " + Utility.jsonEncode(execute.body()));

                return execute.body();
            }
            throw new MobikwikFailureException(1, "mobikwik apis replied failure - " + Utility.jsonEncode(execute.errorBody()));
        } catch (IOException e) {
            e.printStackTrace();
            throw new MobikwikFailureException(1, e.getMessage());
        }
    }
}
