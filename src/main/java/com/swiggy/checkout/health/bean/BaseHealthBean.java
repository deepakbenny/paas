package com.swiggy.checkout.health.bean;

import lombok.*;

/**
 * Created by Manish on 17/10/17.
 */

public class BaseHealthBean {

    private boolean healthy;

    public boolean isHealthy() {
        return healthy;
    }

    public void setHealthy(boolean healthy) {
        this.healthy = healthy;
    }

}
