package com.swiggy.checkout.Payu.service;

import com.netflix.hystrix.contrib.javanica.annotation.DefaultProperties;
import com.netflix.hystrix.contrib.javanica.annotation.HystrixCommand;
import com.newrelic.api.agent.Trace;
import com.swiggy.checkout.Payu.PayUTransactionDetails;
import com.swiggy.checkout.Payu.entity.response.PayURefundResponse;
import com.swiggy.checkout.Payu.entity.response.PayuVerifyPaymentResponse;
import com.swiggy.checkout.data.entities.TransactionPaymentDetails;
import com.swiggy.checkout.pojo.PaymentStatusResponse;
import com.swiggy.checkout.service.ValidPaymentStatus;
import com.swiggy.checkout.utils.Utility;
import com.swiggy.commons.redis.CacheService;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.io.OutputStreamWriter;
import java.net.URL;
import java.net.URLConnection;
import java.security.MessageDigest;
import java.security.NoSuchAlgorithmException;
import java.time.LocalDateTime;
import java.util.UUID;

/**
 * Created by Manish on 30/01/17.
 */
@Service
@Slf4j
@DefaultProperties(defaultFallback = "verifyPaymentStatusFallback")
public class PayuService {

    @Autowired
    protected CacheService<String> cacheService;

    public String hashCal(String type,String str){
        byte[] hashseq=str.getBytes();
        StringBuilder hexString = new StringBuilder();
        try{
            MessageDigest algorithm = MessageDigest.getInstance(type);
            algorithm.reset();
            algorithm.update(hashseq);
            byte messageDigest[] = algorithm.digest();
            for (byte aMessageDigest : messageDigest) {
                String hex = Integer.toHexString(0xFF & aMessageDigest);
                if (hex.length() == 1) hexString.append("0");
                hexString.append(hex);
            }

        }catch(NoSuchAlgorithmException ignored){ }

        return hexString.toString();
    }

    @Trace
    @HystrixCommand(commandKey = "payu")
    public boolean verifyPaymentStatus(TransactionPaymentDetails paymentDetails){

        String pgTxnId = paymentDetails.getPaymentTxnId();
        PayuVerifyPaymentResponse payuVerifyPaymentResponse = verifyPayuTxnStatus(pgTxnId);

        // Not a valid response, return false
        if(payuVerifyPaymentResponse == null || payuVerifyPaymentResponse.getTransactionDetailsMap() == null
                || payuVerifyPaymentResponse.getTransactionDetailsMap().get(pgTxnId) == null){
            log.debug("[PAYU SERVICE] verify payment response is empty.");
            return false;
        }

        PayUTransactionDetails payUTransactionDetails = payuVerifyPaymentResponse.getTransactionDetailsMap().get(pgTxnId);
        paymentDetails.setPayUTransactionDetails(payUTransactionDetails);
        if("failure".equalsIgnoreCase(payUTransactionDetails.getStatus()))
            paymentDetails.setPaymentTxnStatus(ValidPaymentStatus.FAILURE);
        else
            paymentDetails.setPaymentTxnStatus(payUTransactionDetails.getStatus());
        paymentDetails.setPgResponseTime(LocalDateTime.now());

        // Status can be "success", "pending" or "failure"
        log.debug("[PAYU SERVICE] payment status : {}", payUTransactionDetails.getStatus());
        return ValidPaymentStatus.SUCCESS.equalsIgnoreCase(payUTransactionDetails.getStatus());
    }

    @Trace
    @HystrixCommand(commandKey = "payu")
    public boolean verifyPaymentStatus(TransactionPaymentDetails paymentDetails, PaymentStatusResponse paymentStatusResponse) {
        String pgTxnId = paymentDetails.getPaymentTxnId();
        PayuVerifyPaymentResponse payuVerifyPaymentResponse = verifyPayuTxnStatus(pgTxnId);

        // Not a valid response, return false
        if(payuVerifyPaymentResponse == null || payuVerifyPaymentResponse.getTransactionDetailsMap() == null
                || payuVerifyPaymentResponse.getTransactionDetailsMap().get(pgTxnId) == null){
            paymentStatusResponse.setErrorMessage("PayU is not able to verify the payment");
            return false;
        }

        log.debug("PayU response ----------- {}", Utility.jsonEncode2(payuVerifyPaymentResponse));
        PayUTransactionDetails payUTransactionDetails = payuVerifyPaymentResponse.getTransactionDetailsMap().get(pgTxnId);
        paymentStatusResponse.setPaymentStatus(payUTransactionDetails.getStatus());
        paymentStatusResponse.setAmount(payUTransactionDetails.getAmount());
        paymentStatusResponse.setTxnId(payUTransactionDetails.getPgTxnId());
        paymentStatusResponse.setBankRefNumber(payUTransactionDetails.getBankRefNumber());
        paymentStatusResponse.setMihPayId(payUTransactionDetails.getMihPayId());
        return true;
    }



    /**
     * Call PayU api, convert the response and return the PayuVerifyPaymentResponse
     * @param pgTxnId
     * @return
     */
    public PayuVerifyPaymentResponse verifyPayuTxnStatus(String pgTxnId){
        return executeCommand(pgTxnId, "verify_payment");
    }

    public PayURefundResponse refundPayuTxn(String pgTxnId, String amount){
        return executeCancelCommand(pgTxnId, "cancel_refund_transaction", amount);
    }

    private PayuVerifyPaymentResponse executeCommand(String pgTxnId, String command){
        String payuUrl = "https://info.payu.in/merchant/postservice.php?form=2";

        String salt = "BzLPb2sy";
        String key = "Kw8o8g";
        String toHash = key + "|" + command + "|" + pgTxnId + "|" + salt;
        String hashed = hashCal("SHA-512",toHash);
        String postString = "key=" + key +  "&command=" + command +  "&hash=" + hashed + "&var1=" + pgTxnId ;
        log.debug("[POSTSTRING FOR CONFIRM ORDER]:" + postString);
        try {
            URL url = new URL(payuUrl);
            URLConnection conn = url.openConnection();
            conn.setDoOutput(true);
            conn.setConnectTimeout(1000);
            conn.setReadTimeout(5000);

            OutputStreamWriter wr = new OutputStreamWriter(conn.getOutputStream());
            wr.write(postString);
            wr.flush();

            // Get the response
            BufferedReader rd = new BufferedReader(new InputStreamReader(conn.getInputStream()));
            String line;
            String response = "";
            while ((line = rd.readLine()) != null) {
                response += line;
            }
            return Utility.jsonDecode2(response, PayuVerifyPaymentResponse.class);

        } catch (IOException e) {
            log.error("Failed to verify payu for pg transaction id {}", pgTxnId);
            log.error("", e);
        }
        return null;

    }

    private PayURefundResponse executeCancelCommand(String pgTxnId, String command, String amount){
        String payuUrl = "https://info.payu.in/merchant/postservice?form=2";

        String salt = "BzLPb2sy";
        String key = "Kw8o8g";
        String toHash = key + "|" + command + "|" + pgTxnId + "|" + salt;
        String hashed = hashCal("SHA-512",toHash);
        String postString = "key=" + key +  "&command=" + command +  "&hash=" + hashed + "&var1=" + pgTxnId + "&var2=" + UUID.randomUUID() + "&var3=" + amount;
        log.debug("[POSTSTRING FOR CONFIRM ORDER]:" + postString);
        try {
            URL url = new URL(payuUrl);
            URLConnection conn = url.openConnection();
            conn.setDoOutput(true);
            conn.setConnectTimeout(1000);
            conn.setReadTimeout(5000);

            OutputStreamWriter wr = new OutputStreamWriter(conn.getOutputStream());
            wr.write(postString);
            wr.flush();

            // Get the response
            BufferedReader rd = new BufferedReader(new InputStreamReader(conn.getInputStream()));
            String line;
            String response = "";
            while ((line = rd.readLine()) != null) {
                response += line;
            }
            return Utility.jsonDecode2(response, PayURefundResponse.class);

        } catch (IOException e) {
            log.error("Failed to verify payu for pg transaction id {}", pgTxnId);
            log.error("", e);
        }
        return null;

    }

    private boolean verifyPaymentStatusFallback() {
        log.info("reached hystrix fallback in Payu service");
        return false;
    }

   /* public String getOrderIdForTransactionId (String transactionID) {
        return cacheService.get(transactionID, String.class);
    }*/
}

