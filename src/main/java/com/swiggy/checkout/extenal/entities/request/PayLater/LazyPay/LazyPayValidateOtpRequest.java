package com.swiggy.checkout.extenal.entities.request.PayLater.LazyPay;

import com.fasterxml.jackson.annotation.JsonProperty;
import lombok.Data;

/**
 * Created by chirag.rc on 10/24/17.
 */
@Data
public class LazyPayValidateOtpRequest {

    @JsonProperty("paymentMode")
    private String paymentMode;

    @JsonProperty("txnRefNo")
    private String txnRefNo;

    @JsonProperty("otp")
    private String otp;

}
