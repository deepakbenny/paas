package com.swiggy.checkout.extenal.entities.request;

import com.fasterxml.jackson.annotation.JsonProperty;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

/**
 * Created by Manish on 10/03/17.
 */
@Data
@Builder
@NoArgsConstructor
@AllArgsConstructor
public class WalletWithdrawAmountRequest {

    private String wallet;
    private String orderId;
    private String amount;

}
