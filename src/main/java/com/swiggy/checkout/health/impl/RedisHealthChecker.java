package com.swiggy.checkout.health.impl;

import com.swiggy.checkout.health.IHealthChecker;
import com.swiggy.checkout.health.bean.BaseHealthBean;
import com.swiggy.checkout.health.bean.RedisHealthBean;
import com.swiggy.commons.redis.RedisCacheService;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.redis.connection.RedisConnection;
import org.springframework.stereotype.Service;

/**
 * Created by Manish on 17/10/17.
 */
@Service
@Slf4j
public class RedisHealthChecker implements IHealthChecker {

    public static final String REDIS = "redis";

    @Autowired
    private RedisCacheService cacheService;

    @Override
    public String getHealthCheckerName() {
        return REDIS;
    }

    @Override
    public BaseHealthBean performHeathCheck() {
        RedisHealthBean bean = new RedisHealthBean();
        RedisConnection redisConnection = cacheService.getConnectionFactory().getConnection();
        try {
            String value = redisConnection.ping();
            if (value != null && value.equals("PONG")) {
                bean.setHealthy(true);
            } else {
                bean.setHealthy(false);
            }
        } catch (Exception e) {
            log.error("Error in reading from redis cache", e);
        }
        return bean;
    }
}
