package com.swiggy.checkout.wallet;

import com.swiggy.checkout.exception.SwiggyPaymentException;
import com.swiggy.checkout.extenal.entities.request.WalletCalculateChecksumRequest;
import com.swiggy.checkout.extenal.entities.request.WalletRegisterNewUserRequest;
import com.swiggy.checkout.extenal.entities.request.WalletResendOTPRequest;
import com.swiggy.checkout.extenal.entities.response.BaseResponse;

/**
 * Created by Manish on 15/02/17.
 */
public interface WalletBaseService {

    BaseResponse checkBalance() throws SwiggyPaymentException;

    BaseResponse generateOTP() throws SwiggyPaymentException;

    BaseResponse generateToken(String otp, String otpId) throws SwiggyPaymentException;

    BaseResponse debitAmount(String amount, String orderId, String couponId) throws SwiggyPaymentException;

    BaseResponse delink() throws SwiggyPaymentException;

    BaseResponse getSSOToken() throws SwiggyPaymentException;

    BaseResponse resendOTP(WalletResendOTPRequest walletResendOTPRequest) throws SwiggyPaymentException;

    BaseResponse generateRegisterUserToken(WalletRegisterNewUserRequest walletRegisterNewUserRequest) throws SwiggyPaymentException;

    BaseResponse generateLoginToken() throws SwiggyPaymentException;

    BaseResponse computeChecksum(WalletCalculateChecksumRequest walletCalculateChecksumRequest) throws SwiggyPaymentException;
}
