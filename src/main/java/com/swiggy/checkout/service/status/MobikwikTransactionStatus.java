package com.swiggy.checkout.service.status;

import com.swiggy.checkout.data.entities.TransactionPaymentDetails;
import com.swiggy.checkout.mobikwik.service.MobikwikService;
import com.swiggy.checkout.pojo.PaymentStatusResponse;
import com.swiggy.checkout.service.ValidPaymentMethod;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import javax.annotation.PostConstruct;

/**
 * Created by dhaval on 11/23/16.
 */
@Component
public class MobikwikTransactionStatus implements TransactionStatus {

    @Autowired
    private MobikwikService mobikwikService;

    @PostConstruct
    void init() {
        ValidPaymentMethod.MOBIKWIK.registerStatusHandler(this);
        ValidPaymentMethod.MOBIKWIK_SSO.registerStatusHandler(this);
    }

    @Override
    public boolean verifyPaymentStatus(TransactionPaymentDetails paymentDetails, PaymentStatusResponse paymentStatusResponse) {
        ValidPaymentMethod paymentMethod = ValidPaymentMethod.getPaymentMethod(paymentDetails.getPaymentMethod());

        switch (paymentMethod) {
            case MOBIKWIK:
                return mobikwikService.verifyPaymentStatus(paymentDetails, paymentStatusResponse, true);

            case MOBIKWIK_SSO:
                return mobikwikService.verifyPaymentStatus(paymentDetails, paymentStatusResponse, false);

            default:
                return false;
        }

    }
}
