package com.swiggy.checkout.service;

import com.newrelic.api.agent.Trace;
import com.swiggy.checkout.Payu.service.PayuService;
import com.swiggy.checkout.data.entities.TransactionPaymentDetails;
import com.swiggy.checkout.paytm.service.PaytmService;
import com.swiggy.checkout.razorpay.RazorPayService;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

/**
 * Created by Manish on 30/01/17.
 */
@Service
@Slf4j
public class PGService {

    @Autowired
    private PayuService payuService;
    @Autowired private PaytmService paytmService;
    @Autowired private RazorPayService razorPayService;

    @Trace
    public boolean verifyPaymentStatus(TransactionPaymentDetails paymentDetails){
        if(Gateway.PAYU.name().equalsIgnoreCase(paymentDetails.getPaymentGateway())) {
            return payuService.verifyPaymentStatus(paymentDetails);
        } else if(Gateway.PAYTM.name().equalsIgnoreCase(paymentDetails.getPaymentGateway())) {
            return paytmService.verifyPaymentStatus(paymentDetails, true);
        } else if(Gateway.RAZORPAY.name().equalsIgnoreCase(paymentDetails.getPaymentGateway())) {
            return razorPayService.verifyPaymentStatus(paymentDetails);
        }
        return false;
    }

}