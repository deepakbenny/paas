package com.swiggy.checkout.binbasedrouting.binupdatelistener;

import com.swiggy.checkout.exception.CardPaymentException;

public interface CardBinDetailsUpdate {

    void updateBinDetails(String binNumber) throws CardPaymentException;
}
