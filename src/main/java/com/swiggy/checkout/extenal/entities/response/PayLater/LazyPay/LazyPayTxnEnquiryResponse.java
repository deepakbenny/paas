package com.swiggy.checkout.extenal.entities.response.PayLater.LazyPay;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.databind.annotation.JsonSerialize;
import lombok.Data;

/**
 * Created by chirag.rc on 11/13/17.
 */
@Data
@JsonIgnoreProperties(ignoreUnknown = true)
@JsonSerialize(include = JsonSerialize.Inclusion.NON_NULL)
public class LazyPayTxnEnquiryResponse extends LazyPayErrorResponse{

    @JsonProperty("status")
    private String status;

    @JsonProperty("respMessage")
    private String respMessage;

    @JsonProperty("lpTxnId")
    private String lpTxnId;

    @JsonProperty("txnType")
    private String txnType;

    @JsonProperty("txnDateTime")
    private String txnDateTime;

    @JsonProperty("amount")
    private String amount;
}