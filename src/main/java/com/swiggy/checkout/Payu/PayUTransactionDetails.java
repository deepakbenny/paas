package com.swiggy.checkout.Payu;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.databind.annotation.JsonSerialize;
import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

/**
 * Created by Manish on 29/01/17.
 */

@JsonIgnoreProperties(ignoreUnknown = true)
@Getter
@Setter
@NoArgsConstructor
@AllArgsConstructor
@JsonSerialize(include=JsonSerialize.Inclusion.NON_NULL)
public class PayUTransactionDetails {
    @JsonProperty("firstname")
    String firstName;
    @JsonProperty("mihpayid")
    String mihPayId;
    @JsonProperty("transaction_amount")
    double txnAmount;
    @JsonProperty("amt")
    String amount;
    @JsonProperty("status")
    String status;
    @JsonProperty("mode")
    String mode;
    @JsonProperty("Settled_At")
    String settledAt;
    @JsonProperty("error_Message")
    String errorMessage;
    @JsonProperty("bank_ref_num")
    String bankRefNumber;
    @JsonProperty("addedon")
    String addedOn;
    @JsonProperty("card_no")
    String cardNo;
    @JsonProperty("additional_charges")
    String additionalCharges;
    @JsonProperty("error_code")
    String errorCode;
    @JsonProperty("bankcode")
    String bankcode;
    @JsonProperty("txnid")
    String pgTxnId;
    @JsonProperty("unmappedstatus")
    String unmappedStatus;
    @JsonProperty("net_amount_debit")
    String netAmountDebit;
    @JsonProperty("card_type")
    String cardType;
    @JsonProperty("Merchant_UTR")
    String merchantUtr;
    @JsonProperty("disc")
    String disc;
    @JsonProperty("productinfo")
    String productInfo;
    @JsonProperty("request_id")
    String requestId;
    @JsonProperty("PG_TYPE")
    String pgType;
    @JsonProperty("udf1")
    String udf1;
    @JsonProperty("udf2")
    String udf2;
    @JsonProperty("udf3")
    String udf3;
    @JsonProperty("udf4")
    String udf4;
    @JsonProperty("udf5")
    String udf5;
    @JsonProperty("field9")
    String field9;
}

