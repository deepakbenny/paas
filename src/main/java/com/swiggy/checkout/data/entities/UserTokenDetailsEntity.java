package com.swiggy.checkout.data.entities;

import lombok.*;
import org.hibernate.annotations.GenericGenerator;

import javax.persistence.*;
import java.io.Serializable;
import java.sql.Timestamp;

/**
 * Created by chirag.rc on 10/30/17.
 */
@Entity
@Table(name = "token_details")
@Getter
@Setter
@NoArgsConstructor
@AllArgsConstructor
@Builder
public class UserTokenDetailsEntity implements Serializable {

    @Id
    @Column(name = "id", nullable = false, insertable = true, updatable = true)
    @GeneratedValue(strategy = GenerationType.AUTO)
    private long id;

    @Column(name = "user_id")
    private long userId;

    @Column(name = "payment_method_name")
    private String paymentMethodName;

    @Column(name = "token")
    private String token;

    @Column(name = "expires_on")
    private Timestamp expiresOn;

    @Column(name = "refresh_token")
    private String refreshToken;

    @Column(name = "created_on")
    private Timestamp createdOn;

}