package com.swiggy.checkout.utils;

import com.fasterxml.jackson.core.JsonFactory;
import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.core.type.TypeReference;
import com.fasterxml.jackson.databind.DeserializationFeature;
import com.fasterxml.jackson.databind.JavaType;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.swiggy.checkout.controller.TransactionController;
import com.swiggy.checkout.pojo.User;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.web.context.request.RequestContextHolder;
import org.springframework.web.context.request.ServletRequestAttributes;

import javax.servlet.http.HttpServletRequest;
import java.io.IOException;
import java.nio.charset.StandardCharsets;
import java.util.Base64;
import java.util.HashMap;

/**
 * Created by Manish on 23/01/17.
 */

public class Utility {

    private static final String USER_HEADER_KEY = "user-encoded";
    private static final String USER_AGENT = "user-agent";

    private static final Logger logger = LoggerFactory.getLogger(Utility.class);


    public static final ObjectMapper objectMapper = new ObjectMapper().configure(DeserializationFeature.FAIL_ON_UNKNOWN_PROPERTIES, false)
            .configure(DeserializationFeature.ACCEPT_SINGLE_VALUE_AS_ARRAY, true);
    public static final String NEWRELIC_IGNORE = "com.newrelic.agent.IGNORE";

    public static String jsonEncode(Object o) throws JsonProcessingException {

        if (o == null)
            return "";
        return objectMapper.writeValueAsString(o);
    }

    public static <T> T jsonDecode(String s, Class<?> clazz) {
        if (Utility.isEmpty(s)) return null;
        JavaType classType = objectMapper.getTypeFactory().constructType(clazz);
        try {
            return objectMapper.readerFor(classType)
                    .readValue(s);
        } catch (IOException e) {
            return null;
        }
    }

    public static String jsonEncode2(Object o) {
        try {
            return jsonEncode(o);
        } catch (JsonProcessingException e) {
            e.printStackTrace();
            return "";
        }
    }

    public static String getRequestPath() {
        HttpServletRequest request = getCurrentRequest();
        String context = request.getContextPath();
        String fullURL = request.getRequestURI();
        String path = fullURL.substring(fullURL.indexOf(context) + context.length());
        return path;
    }


    public static <T> T jsonDecode2(String s, Class<?> clazz) throws IOException {
        if (Utility.isEmpty(s)) return null;
        JavaType classType = objectMapper.getTypeFactory().constructType(clazz);
        return objectMapper.reader(classType)
                .readValue(s);
    }

    public static boolean isEmpty(String s) {
        if (s == null || "".equals(s.trim()))
            return true;
        else return false;
    }

    public static Long getLong(String value) {
        try {
            return Long.parseLong(value);
        } catch (Exception e) {
            return 0l;
        }
    }


    public static HashMap<String, String> jsonToStringMap(String jsonString) throws IOException {
        JsonFactory factory = new JsonFactory();
        ObjectMapper mapper = new ObjectMapper(factory);

        TypeReference<HashMap<String, String>> typeRef
                = new TypeReference<HashMap<String, String>>() {
        };

        return mapper.readValue(jsonString, typeRef);
    }

    public static Double getDouble(String value) {
        try {
            return Double.parseDouble(value);
        } catch (Exception e) {
            return 0d;
        }
    }

    public static HashMap<String, Object> jsonToMap(String jsonString) throws IOException {
        JsonFactory factory = new JsonFactory();
        ObjectMapper mapper = new ObjectMapper(factory);

        TypeReference<HashMap<String, Object>> typeRef
                = new TypeReference<HashMap<String, Object>>() {
        };

        return mapper.readValue(jsonString, typeRef);
    }


    public static HttpServletRequest getCurrentRequest() {
        final ServletRequestAttributes attr = (ServletRequestAttributes) RequestContextHolder
                .currentRequestAttributes();
        final HttpServletRequest request = attr.getRequest();
        return request;
    }

    public static boolean isCurrentRequestFromWeb(String userAgent) {
        if (userAgent == null) {
            return true;
        } else {
            return !(userAgent.equals("Swiggy-Android") || (userAgent.equals("Swiggy-iOS")));
        }
    }

    public static String getHeaderRequest(String headerName) {
        final HttpServletRequest request = getCurrentRequest();
        return request.getHeader(headerName);
    }

    public static User getUserFromHeader() {
        String encodedUser  = getHeaderRequest(USER_HEADER_KEY);
        String user = decodeBase64(encodedUser);
        return jsonDecode(user,User.class);
    }

    public static String getUserAgent() {
        String userAgent = Utility.getHeaderRequest(USER_AGENT);
        if(userAgent == null){
            userAgent = "";
        }
        return userAgent;
    }

    public static void ignoreCurrentRequestNPM() {
        HttpServletRequest currentRequest = Utility.getCurrentRequest();
        if (currentRequest != null) {
            currentRequest.setAttribute(NEWRELIC_IGNORE, true);
        }
    }

    public static String decodeBase64(String encodedString) {
        try {
            byte[] decodedBytes = Base64.getDecoder().decode(encodedString);
            return new String(decodedBytes, StandardCharsets.UTF_8);
        } catch (IllegalArgumentException e) {
            logger.error("Invalid base 64 string {}", encodedString);
            return encodedString;
        }
    }

}
