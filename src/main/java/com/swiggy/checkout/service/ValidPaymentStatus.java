package com.swiggy.checkout.service;

/**
 * Created by Manish on 29/01/17.
 */

public class ValidPaymentStatus {

    public static final String SUCCESS= "success";
    public static final String PENDING= "pending";
    public static final String FAILURE = "failed";
    public static final String PAID= "paid";
    public static final String REFUND_INITIATED = "refund-initiated";
}

