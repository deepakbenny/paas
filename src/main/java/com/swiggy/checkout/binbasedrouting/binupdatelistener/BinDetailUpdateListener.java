package com.swiggy.checkout.binbasedrouting.binupdatelistener;

import com.rabbitmq.client.*;
import com.swiggy.checkout.common.services.RabbitmqChannelService;
import com.swiggy.checkout.email.EmailService;
import com.swiggy.checkout.utils.Utility;
import lombok.extern.slf4j.Slf4j;
import net.logstash.logback.encoder.org.apache.commons.lang.exception.ExceptionUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Component;

import javax.annotation.PostConstruct;
import java.io.IOException;


@Component
@Slf4j
public class BinDetailUpdateListener {

    @Value("${bin_detail_update_exchange_name}")
    private String exchangeName;

    @Value("${bin_detail_update_queue_name}")
    private String queueName;

    @Value("${bin_detail_update_routing_key}")
    private String routingKey;

    @Autowired
    private CardBinDetailsUpdate cardBinDetailsUpdate;

    @Autowired
    private RabbitmqChannelService rabbitmqChannelService;

    @Autowired
    private EmailService emailService;

    @Value("${email_context}")
    private String emailContext;

    @Value("${email_onErrorEmailId}")
    private String emailOnError;


    @PostConstruct
    public void init() {
        try {
            Channel rabbitmqChannel = rabbitmqChannelService.getRabbitmqChannel(exchangeName, "fanout",
                    queueName, routingKey);
            rabbitmqChannel.basicQos(1);
            rabbitmqChannel.basicConsume(queueName, false, "", false, false, null, new DefaultConsumer(rabbitmqChannel) {
                @Override
                public void handleDelivery(String consumerTag,
                                           Envelope envelope,
                                           AMQP.BasicProperties properties,
                                           byte[] body) throws IOException {
                    String binId = null;
                    try {
                        binId = new String(body);
                        log.info("[BIN NUMBER MESSAGE] {}", binId);
                        if (!Utility.isEmpty(binId)) {
                            cardBinDetailsUpdate.updateBinDetails(binId);
                        }
                        rabbitmqChannel.basicAck(envelope.getDeliveryTag(), false);
                    } catch (Exception e) {
                        log.error("Error in processing card order message", e);
                        emailService.sendEmail(emailOnError, String.format("[%s][%s][%s][%s]", emailContext, "DS-API",
                                "Error", "Error in processing card order message, orderId " + binId), new String(body) + "\n\n" + ExceptionUtils.getStackTrace(e));
                        rabbitmqChannel.basicAck(envelope.getDeliveryTag(), false);
                    }
                }
            });

        } catch (IOException e) {
            log.error("Error while reading from rabbit mq", e);
            emailService.sendEmail(emailOnError, String.format("[%s][%s][%s][%s]", emailContext, "DS-API", "Error", "Error while reading from rabbit mq card listener"), ExceptionUtils.getStackTrace(e));
        }
    }
}
