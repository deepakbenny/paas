package com.swiggy.checkout.health;

import com.swiggy.checkout.health.bean.BaseHealthBean;
import com.swiggy.checkout.health.impl.MySQLHealthChecker;
import com.swiggy.checkout.health.impl.RedisHealthChecker;
import com.swiggy.checkout.utils.Utility;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

import javax.annotation.PostConstruct;
import javax.servlet.http.HttpServletRequest;
import java.util.List;

/**
 * Created by Manish on 16/10/17.
 */
@RestController
@RequestMapping("")
public class HealthController {

    private  HealthCheckerRegistry registry = new HealthCheckerRegistry();

    @Autowired
    private MySQLHealthChecker mySQLHealthChecker;

    @Autowired
    private RedisHealthChecker redisHealthChecker;

    @PostConstruct
    public  void init () {
        registry.addHealthChecker(mySQLHealthChecker);
        registry.addHealthChecker(redisHealthChecker);
    }



    @RequestMapping(
            value = "/healthChecker",
            method = RequestMethod.GET,
            produces = MediaType.APPLICATION_JSON_VALUE
    )
    public ResponseEntity getAllHealth() {
        Utility.ignoreCurrentRequestNPM();
        return getHealthCheckResponse(registry);
    }

    private ResponseEntity getHealthCheckResponse(HealthCheckerRegistry registry){
        HttpStatus statusCode = HttpStatus.OK;
        List<IHealthChecker> allHealthCheckers = registry.getAllHealthCheckers();
        HealthCheckerResponse response = new HealthCheckerResponse();
        for (IHealthChecker hc : allHealthCheckers) {
            if (hc != null) {
                BaseHealthBean healthBean = hc.performHeathCheck();
                response.putHealthBean(hc.getHealthCheckerName(),healthBean);
                if (!healthBean.isHealthy()) {
                    statusCode = HttpStatus.INTERNAL_SERVER_ERROR;
                }
            }
        }
        return new ResponseEntity<HealthCheckerResponse>(response, statusCode);
    }


    @RequestMapping(
            value = {"/ping"},
            method = RequestMethod.GET,
            produces = MediaType.TEXT_PLAIN_VALUE
    )
    public String verifyTransaction() {
        Utility.ignoreCurrentRequestNPM();
        return "pong";
    }


}
