package com.swiggy.checkout.paytm.entities.response;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.annotation.JsonProperty;
import com.swiggy.checkout.paytm.entities.response.PaytmBaseResponse;
import lombok.*;

/**
 * Created by Manish on 29/01/17.
 */

@NoArgsConstructor
@AllArgsConstructor
@Getter
@Setter
@Builder
@JsonIgnoreProperties(ignoreUnknown = true)
@JsonInclude(JsonInclude.Include.NON_NULL)
public class RefundResponse extends PaytmBaseResponse {

    @JsonProperty("ErrorCode")
    private String ErrorCode;

    @JsonProperty("ErrorMsg")
    private String ErrorMsg;

    @JsonProperty("MID")
    private String MID;

    @JsonProperty("ORDERID")
    private String ORDERID;

    @JsonProperty("TXNID")
    private String TXNID;

    @JsonProperty("BANKTXNID")
    private String BANKTXNID;
    @JsonProperty("TXNAMOUNT")
    private String TXNAMOUNT;
    @JsonProperty("REFUNDAMOUNT")
    private String REFUNDAMOUNT;
    @JsonProperty("TXNDATE")
    private String TXNDATE;
    @JsonProperty("RESPCODE")
    private String RESPCODE;
    @JsonProperty("RESPMSG")
    private String RESPMSG;
    @JsonProperty("STATUS")
    private String STATUS;
    @JsonProperty("REFUNDID")
    private String REFUNDID;
}
