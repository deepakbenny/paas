package com.swiggy.checkout.service.refund;

import com.swiggy.checkout.exception.SwiggyPaymentException;
import com.swiggy.checkout.freecharge.entities.responses.RefundPaasResponse;
import com.swiggy.checkout.freecharge.service.FreechargeWalletService;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

/**
 * Created by Geetesh on 6/27/17.
 */
@Service
@Slf4j
public class FreechargeRefundService implements RefundBaseService{

    @Autowired
    private FreechargeWalletService freechargeWalletService;

    @Override
    public RefundPaasResponse refund(RefundPaasRequest request) throws SwiggyPaymentException {
        return freechargeWalletService.refundAmount(request.getOrderId(), request.getAmount(), request.getRefundId());
    }
}
