package com.swiggy.checkout.extenal.entities.mapper;


import com.swiggy.checkout.utils.Utility;
import ma.glasnost.orika.*;
import ma.glasnost.orika.converter.BidirectionalConverter;
import ma.glasnost.orika.impl.ConfigurableMapper;
import ma.glasnost.orika.impl.generator.specification.StringToEnum;
import ma.glasnost.orika.metadata.Type;
import org.springframework.stereotype.Component;

import java.sql.Timestamp;

/**
 * Created by Manish on 23/01/17.
 */

@Component
public class CustomMapper extends ConfigurableMapper{

    protected void configure(MapperFactory factory) {



        factory.getConverterFactory().
                registerConverter("longToStringConverter", new CustomConverter<Long, String>() {
                    @Override
                    public String convert(Long source, Type<? extends String> destinationType, MappingContext mappingContext) {
                        return String.valueOf(source);
                    }
                });

        factory.getConverterFactory()
                .registerConverter("timestampToString", new CustomConverter<Timestamp, String>() {
                    @Override
                    public String convert(Timestamp source, Type<? extends String> destinationType, MappingContext mappingContext) {
                        return source.toString();
                    }
                });

    }
}
