package com.swiggy.checkout.service;

import com.swiggy.checkout.exception.CardPaymentException;
import lombok.extern.slf4j.Slf4j;

/**
 * Created by Manish on 30/01/17.
 */
@Slf4j
public enum Gateway {

    PAYTM(1, "PAYTM"),
    PAYU(2, "PAYU"),
    RAZORPAY(3, "RAZORPAY");

    private final int gatewayId;
    private String gateway;
   // private PaymentGatewayHandler handler;

    private Gateway(int gatewayId, String gateway) {
        this.gatewayId = gatewayId;
        this.gateway = gateway;
    }

    public int getGatewayId() {
        return this.gatewayId;
    }

    public static Gateway getGateway(String gateway) {
        Gateway[] gateways = values();
        for(Gateway gateway1 : gateways) {
            if(gateway1.name().equalsIgnoreCase(gateway)) {
                return gateway1;
            }
        }
        log.error("unknown payment gateway {}", gateway);
        return null;
    }

  /*  public void setGatewayHandler(PaymentGatewayHandler handler) {
        this.handler = handler;
    }

    public void handlePaymentGatewayDetails(OrderPaymentDetails paymentDetails, long orderId) throws CardPaymentException {
        handler.handlePaymentGatewayDetails(paymentDetails, orderId);
    }*/
}