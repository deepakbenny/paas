package com.swiggy.checkout.freecharge.service;

import com.newrelic.api.agent.Trace;
import com.swiggy.checkout.data.entities.TransactionPaymentDetails;
import com.swiggy.checkout.data.model.bo.PaymentConfiguration;
import com.swiggy.checkout.exception.SwiggyPaymentException;
import com.swiggy.checkout.freecharge.entities.responses.DebitResponse;
import com.swiggy.checkout.pojo.PaymentStatusResponse;
import com.swiggy.checkout.service.ValidPaymentStatus;
import com.swiggy.checkout.utils.Utility;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.time.LocalDateTime;

/**
 * Created by Manish on 05/02/17.
 */


@Service
@Slf4j
public class FreechargeService {

    @Autowired
    private PaymentConfiguration configuration;

    @Autowired
    private FreechargeWalletService freechargeWalletService;

  /*  @Autowired
    private GlobalControllerExceptionHandle exceptionHandler;*/

    public static final String SUCCESS = "SUCCESS";
    public static final String FAILURE = "FAILED";
    public static final String PENDING = "PENDING";
    public static final String INITIATED = "INITIATED";

    @Trace
    public boolean verifyPaymentStatus(TransactionPaymentDetails paymentDetails) {
        try {
            //TODO : put retry logic
            DebitResponse response = freechargeWalletService.transactionStatus(paymentDetails.getOrderId());

            // Not a valid response, return false
            if(response == null || Utility.isEmpty(response.getStatus()))
                return false;

            paymentDetails.setFreechargeVerifyPaymentResponse(response);
            paymentDetails.setPgResponseTime(LocalDateTime.now());

            if(SUCCESS.equalsIgnoreCase(response.getStatus())) {
                Double txmAmount = Utility.getDouble(response.getAmount());
                int txnTotal = txmAmount.intValue();

                if (txnTotal != paymentDetails.getAmount()) {
                    Long fraudThreshold = configuration.getConfigurationAsLong("freecharge_fraud_threshold", 50);
                    int diff = Math.abs(paymentDetails.getAmount() - txnTotal);
                    if (diff > fraudThreshold) {
                        log.error("Order ID {} failed as freecharge txn amount {} is diff. difference {} txn id {}",
                                paymentDetails.getOrderId(), txmAmount, diff, response.getTxnId());
                       // exceptionHandler.sendEmailOnError(Utility.jsonEncode2(response),
                         //       "Freecharge - Order amount and txn amount mismatch. Threshold breached!! Difference "+diff);
                        paymentDetails.setPaymentTxnStatus(ValidPaymentStatus.FAILURE);
                        return false;
                    } else {
                        log.error("Order ID {}  freecharge txn amount {} is diff. difference {} txn id {}. less than threshold, passing as success",
                                paymentDetails.getOrderId(), txmAmount, diff, response.getTxnId());
                    }
                }
                paymentDetails.setPaymentTxnId(response.getTxnId());
                paymentDetails.setPaymentTxnStatus(ValidPaymentStatus.SUCCESS);
                return true;
            } else {
                paymentDetails.setPaymentTxnStatus(ValidPaymentStatus.FAILURE);
                return false;
            }
        } catch (SwiggyPaymentException e) {
            log.info("Freecharge verify transaction Failed. Order Id - " + paymentDetails.getOrderId());
            paymentDetails.setFreechargeFailureMessage(e.getDetailMessage());
        }
        return false;
    }

    public boolean verifyPaymentStatus(TransactionPaymentDetails paymentDetails, PaymentStatusResponse paymentStatusResponse) {
        try {
            DebitResponse response = freechargeWalletService.transactionStatus(paymentDetails.getOrderId());

            if(response == null || Utility.isEmpty(response.getStatus())) {
                paymentStatusResponse.setErrorMessage("Not able to get response from freecharge");
                return false;
            }

            log.debug("Freecharge Verify Status ----------- {}", Utility.jsonEncode2(response));
            paymentStatusResponse.setAmount(response.getAmount());
            paymentStatusResponse.setPaymentStatus(response.getStatus());
            paymentStatusResponse.setTxnId(response.getTxnId());
            return true;

        } catch (SwiggyPaymentException e) {
            log.info("Freecharge verify payment status Failed. Order Id - " + paymentDetails.getOrderId());
            paymentStatusResponse.setErrorMessage(e.getDetailMessage());
            return false;
        }

    }

}

