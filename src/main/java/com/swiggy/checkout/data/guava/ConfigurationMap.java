package com.swiggy.checkout.data.guava;


import java.util.HashMap;

/**
 * Created by Manish on 24/01/17.
 */

public class ConfigurationMap {

    private HashMap<String,String> configurations;

    public ConfigurationMap(HashMap<String, String> configurations){

        this.configurations = configurations;
    }

    public String getConfiguration(String key){

        if(configurations.containsKey(key))
            return configurations.get(key);
        return null;

    }
}
