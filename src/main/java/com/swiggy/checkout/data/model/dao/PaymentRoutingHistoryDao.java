package com.swiggy.checkout.data.model.dao;

import com.swiggy.checkout.data.entities.PaymentRoutingEntity;
import com.swiggy.checkout.data.entities.PaymentRoutingHistoryEntity;
import com.swiggy.checkout.data.entities.TransactionEntity;
import com.swiggy.rest.dao.CrudDao;
import org.springframework.data.jpa.repository.JpaRepository;

/**
 * Created by Manish on 03/02/17.
 */
public interface PaymentRoutingHistoryDao extends JpaRepository<PaymentRoutingHistoryEntity, Long> {

    @Override
    PaymentRoutingHistoryEntity save(PaymentRoutingHistoryEntity paymentRoutingHistoryEntity);
}
