package com.swiggy.checkout.exception;

import lombok.Getter;
import lombok.Setter;

/**
 * Created by Manish on 06/02/17.
 */
@Getter
@Setter
public class BadRequestException extends RuntimeException {


    private String message;
    private int httpStatusCode;

    public BadRequestException() {
        super();
        message = "";
        this.httpStatusCode = 400;
    }

    public BadRequestException(String message) {
        super(message);
        this.message = message;
        this.httpStatusCode = 400;

    }

    public String toString(){
        return message;
    }

}