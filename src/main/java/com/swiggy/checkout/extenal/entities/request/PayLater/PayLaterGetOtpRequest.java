package com.swiggy.checkout.extenal.entities.request.PayLater;

import com.fasterxml.jackson.annotation.JsonProperty;
import lombok.Data;

/**
 * Created by chirag.rc on 10/28/17.
 */
@Data
public class PayLaterGetOtpRequest {

    @JsonProperty("email")
    private String email;

    @JsonProperty("mobile")
    private String mobile;

    @JsonProperty("merchant_txn_id")
    private String merchantTxnId;

    @JsonProperty("amount")
    private String amount;

    @JsonProperty("currency")
    private String currency;

    @JsonProperty("paylater_method")
    private String paylaterMethod;
}
