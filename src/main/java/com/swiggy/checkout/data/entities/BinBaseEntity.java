package com.swiggy.checkout.data.entities;

import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.Getter;
import lombok.Setter;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.Table;
import java.io.Serializable;

/**
 * Created by Manish on 02/02/17.
 */

@Entity
@Table(name = "binbase")
@Data
public class BinBaseEntity implements Serializable {

    @Id
    @Column(name = "id", nullable = false, insertable = false, updatable = false)
    private long id;
    @Column(name = "bin", nullable = false, insertable = false, updatable = false)
    private long bin;
    @Column(name = "brand", nullable = true, length = 20, insertable = false, updatable = false)
    private String brand;
    @Column(name = "bank", nullable = true, length = 20, insertable = false, updatable = false)
    private String bank;
    @Column(name = "type", nullable = true, length = 20, insertable = false, updatable = false)
    private String type;
    @Column(name = "level", nullable = true, length = 20, insertable = false, updatable = false)
    private String level;
    @Column(name = "isocountry", nullable = true, length = 20, insertable = false, updatable = false)
    private String isocountry;
    @Column(name = "isoa2", nullable = true, length = 20, insertable = false, updatable = false)
    private String isoa2;
    @Column(name = "isoa3", nullable = true, length = 20, insertable = false, updatable = false)
    private String isoa3;
    @Column(name = "isonumber", nullable = false, insertable = false, updatable = false)
    private long isonumber;
    @Column(name = "www", nullable = true, length = 20, insertable = false, updatable = false)
    private String www;
    @Column(name = "phone", nullable = true, length = 20, insertable = false, updatable = false)
    private String phone;

}
