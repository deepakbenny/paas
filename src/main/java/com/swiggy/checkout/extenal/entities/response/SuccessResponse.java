package com.swiggy.checkout.extenal.entities.response;

/**
 * Created by Manish on 23/01/17.
 */

public class SuccessResponse extends BaseResponse {

    public SuccessResponse(Object data) {
        super(0, "Executed Successfully", data);
    }

    public SuccessResponse() {
        super(0, "done successfully", null);
    }

}
