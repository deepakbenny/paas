package com.swiggy.checkout.controller;

import com.swiggy.checkout.exception.SwiggyPaymentException;
import com.swiggy.checkout.freecharge.entities.responses.RefundPaasResponse;
import com.swiggy.checkout.service.refund.RefundPaasRequest;
import com.swiggy.checkout.service.refund.RefundService;
import io.swagger.annotations.ApiOperation;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.MediaType;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

/**
 * Created by Geetesh on 6/14/17.
 */
@RestController
@RequestMapping("/v1/payment/transaction")
public class RefundController {

    private static final int ERROR_CODE = 400;
    private static final String ERROR_MSG = "Some error on server side for payment refund";

    @Autowired
    private RefundService refundService;



    @ApiOperation(value = "refund", notes = "Api to refund for a transaction")
    @RequestMapping(
            value = "/refund",
            method = RequestMethod.POST,
            produces = MediaType.APPLICATION_JSON_VALUE
    )
    public RefundPaasResponse refund(@RequestBody RefundPaasRequest request) throws Exception{
        try {
            return refundService.refund(request);
        }
        catch (SwiggyPaymentException spe){
            return null;
        }
        catch (Exception e) {
            return null;
        }
    }
}
