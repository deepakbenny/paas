CREATE TABLE `payment_transaction` (
  `id` bigint(20) NOT NULL AUTO_INCREMENT,
  `amount` int(11) DEFAULT NULL,
  `created_on` datetime DEFAULT NULL,
  `order_id` bigint(20) DEFAULT NULL,
  `payment_details` mediumtext,
  `payment_method` varchar(255) DEFAULT NULL,
  `payment_txn_status` varchar(255) DEFAULT NULL,
  `updated_on` datetime DEFAULT NULL,
  `user_id` bigint(20) DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `order_id` (`order_id`),
  KEY `created_on` (`created_on`),
  KEY `user_id` (`user_id`)
) ENGINE=InnoDB AUTO_INCREMENT=132 DEFAULT CHARSET=latin1;


CREATE TABLE `refund_transaction` (
  `id` bigint(20) NOT NULL AUTO_INCREMENT,
  `amount` varchar(255) DEFAULT NULL,
  `created_by` varchar(255) DEFAULT NULL,
  `created_on` datetime DEFAULT NULL,
  `order_id` bigint(20) DEFAULT NULL,
  `payment_method` varchar(255) DEFAULT NULL,
  `refund_response_details` varchar(255) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=3 DEFAULT CHARSET=latin1;


CREATE TABLE `payment_configurations` (
  `id` bigint(20) NOT NULL AUTO_INCREMENT,
  `config_key` varchar(255) DEFAULT NULL,
  `config_value` varchar(255) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=2 DEFAULT CHARSET=latin1;

CREATE TABLE `third_parties` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `edit_notify_api` varchar(255) DEFAULT NULL,
  `metadata` varchar(255) NOT NULL,
  `refund_api` varchar(255) DEFAULT NULL,
  `source` varchar(255) NOT NULL,
  `token` varchar(255) NOT NULL,
  `verify_api` varchar(255) DEFAULT NULL,
  `verify_transaction_api` varchar(255) DEFAULT NULL,
  `withdraw_api` varchar(255) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=2 DEFAULT CHARSET=latin1;

CREATE TABLE `payment_routing` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `card_type` varchar(12) DEFAULT NULL,
  `payment_type` varchar(10) DEFAULT NULL,
  `bank` varchar(20) DEFAULT 'Any',
  `payment_gateway` varchar(10) DEFAULT NULL,
  `percentage` int(3) DEFAULT NULL,
  `updated_by` varchar(60) DEFAULT NULL,
  `created_on` datetime DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=28 DEFAULT CHARSET=latin1;


CREATE TABLE `binbase` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `bin` int(6) DEFAULT NULL,
  `brand` varchar(128) DEFAULT NULL,
  `bank` varchar(128) DEFAULT NULL,
  `type` varchar(32) DEFAULT NULL,
  `level` varchar(32) DEFAULT NULL,
  `isocountry` varchar(64) DEFAULT NULL,
  `isoa2` varchar(32) DEFAULT NULL,
  `isoa3` varchar(32) DEFAULT NULL,
  `isonumber` int(32) DEFAULT NULL,
  `www` varchar(128) DEFAULT NULL,
  `phone` varchar(128) DEFAULT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `bin` (`bin`)
) ENGINE=InnoDB AUTO_INCREMENT=3405529 DEFAULT CHARSET=utf8;

CREATE TABLE `payment_routing_history` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `card_type` varchar(12) DEFAULT NULL,
  `payment_type` varchar(10) DEFAULT NULL,
  `bank` varchar(20) DEFAULT 'Any',
  `payment_gateway` varchar(10) DEFAULT NULL,
  `percentage` int(3) DEFAULT NULL,
  `updated_by` varchar(60) DEFAULT NULL,
  `from_time` datetime DEFAULT NULL,
  `to_time` datetime DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;







