package com.swiggy.checkout.thirdparty.client.http;

import retrofit2.Call;
import retrofit2.http.GET;
import retrofit2.http.QueryMap;

import java.util.Map;


/**
 * Created by chandelabhinav on 21/07/16.
 *
 */
public interface ThirdPartyVerifyHttpClient {
    @GET("./")
    Call<Object> verifyCashback(@QueryMap Map<String, String> request);
}

