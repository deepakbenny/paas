/*
package com.swiggy.checkout.service.status;

import com.swiggy.api.data.entities.order.OrderEntity;
import com.swiggy.api.data.entities.payment.PaymentStatusResponse;
import com.swiggy.api.services.order.ValidPaymentMethod;
import com.swiggy.api.services.payments.FreechargeService;
import com.swiggy.checkout.pojo.PaymentStatusResponse;
import com.swiggy.checkout.service.ValidPaymentMethod;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import javax.annotation.PostConstruct;

*/
/**
 * Created by Manish on 30/01/17.
 *//*


@Component
public class FreechargeTransactionStatus implements TransactionStatus {

    @Autowired
    FreechargeService freechargeService;

    @PostConstruct
    void init() {
        ValidPaymentMethod.FREECHARGE.registerStatusHandler(this);
        ValidPaymentMethod.FREECHARGE_SSO.registerStatusHandler(this);
    }

    @Override
    public boolean verifyPaymentStatus(OrderEntity orderEntity, PaymentStatusResponse paymentStatusResponse) {
        return freechargeService.verifyPaymentStatus(orderEntity, paymentStatusResponse);
    }
}
*/
