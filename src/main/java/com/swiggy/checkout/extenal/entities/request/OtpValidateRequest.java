package com.swiggy.checkout.extenal.entities.request;

import com.fasterxml.jackson.annotation.JsonProperty;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

/**
 * Created by Manish on 09/03/17.
 */

@Data
@Builder
@NoArgsConstructor
@AllArgsConstructor
public class OtpValidateRequest {
    private String wallet;
    private String otp;
    private String state;
}
