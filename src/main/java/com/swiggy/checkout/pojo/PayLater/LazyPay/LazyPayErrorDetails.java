package com.swiggy.checkout.pojo.PayLater.LazyPay;

import com.fasterxml.jackson.annotation.JsonProperty;
import lombok.Data;

/**
 * Created by chirag.rc on 11/1/17.
 */
@Data
public class LazyPayErrorDetails {

    @JsonProperty("code")
    private String code;

    @JsonProperty("message")
    private String message;
}
