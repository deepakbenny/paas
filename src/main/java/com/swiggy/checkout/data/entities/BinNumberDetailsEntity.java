package com.swiggy.checkout.data.entities;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.Table;
import java.io.Serializable;
import java.sql.Timestamp;

@Entity
@Table(name = "bin_number_details", schema = "paas")
public class BinNumberDetailsEntity implements Serializable {
    private long bin;
    private String brand;
    private String bank;
    private String type;
    private String isocountry;
    private Timestamp createdTime;
    private Timestamp lastUpdatedTime;

    @Id
    @Column(name = "bin", nullable = false, unique = true)
    public long getBin() {
        return bin;
    }

    public void setBin(long bin) {
        this.bin = bin;
    }

    @Column(name = "brand", nullable = false, length = 128)
    public String getBrand() {
        return brand;
    }

    public void setBrand(String brand) {
        this.brand = brand;
    }

    @Column(name = "bank", nullable = false, length=128)
    public String getBank() {
        return bank;
    }

    public void setBank(String bank) {
        this.bank = bank;
    }

    @Column(name = "type", nullable = false, length = 128)
    public String getType() {
        return type;
    }

    public void setType(String type) {
        this.type = type;
    }


    @Column(name = "isocountry", length = 64)
    public String getIsocountry() {
        return isocountry;
    }

    public void setIsocountry(String isocountry) {
        this.isocountry = isocountry;
    }

    @Column(name = "created_time", nullable = false)
    public Timestamp getCreatedTime() {
        return createdTime;
    }

    public void setCreatedTime(Timestamp createdTime) {
        this.createdTime = createdTime;
    }

    @Column(name = "last_updated_time", nullable = false)
    public Timestamp getLastUpdatedTime() {
        return lastUpdatedTime;
    }

    public void setLastUpdatedTime(Timestamp lastUpdatedTime) {
        this.lastUpdatedTime = lastUpdatedTime;
    }

}
