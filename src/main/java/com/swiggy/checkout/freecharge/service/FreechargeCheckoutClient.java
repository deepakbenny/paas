package com.swiggy.checkout.freecharge.service;

import com.swiggy.checkout.freecharge.entities.request.DebitRequest;
import com.swiggy.checkout.freecharge.entities.request.RefundRequest;
import com.swiggy.checkout.freecharge.entities.responses.DebitResponse;
import com.swiggy.checkout.freecharge.entities.responses.FreechargeBalance;
import com.swiggy.checkout.freecharge.entities.responses.FreechargeRefundResponse;
import retrofit2.Call;
import retrofit2.http.Body;
import retrofit2.http.GET;
import retrofit2.http.POST;
import retrofit2.http.Query;

/**
 * Created by Manish on 05/02/17.
 */

public interface FreechargeCheckoutClient {

    @GET("v1/co/oauth/wallet/balance")
    Call<FreechargeBalance> checkBalance(@Query("merchantId") String merchantId, @Query("accessToken") String accessToken);

    @POST("v1/co/oauth/wallet/debit")
    Call<DebitResponse> debitAmount(@Body DebitRequest loginRequest);

    @POST("v1/co/refund")
    Call<FreechargeRefundResponse> refundAmount(@Body RefundRequest refundRequest);

    @GET("v1/co/transaction/status")
    Call<DebitResponse> transactionStatus(@Query("merchantId") String merchantId, @Query("merchantTxnId") String txnId,
                                          @Query("checksum") String checksum, @Query("txnType") String txnType);

}
