package com.swiggy.checkout.thirdparty;


import com.swiggy.checkout.data.entities.ThirdPartyEntity;
import com.swiggy.checkout.data.model.dao.ThirdPartyDao;
import org.apache.commons.lang3.NotImplementedException;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

/**
 * Created by IntelliJ IDEA.
 * User: chandelabhinav
 * Date: 09/07/16
 */
@Service(value = "thirdPartyServiceImpl")
public class ThirdPartyClientServiceImpl implements ThirdPartyClientService
{
    private static final Logger logger = LoggerFactory.getLogger(ThirdPartyClientServiceImpl.class);

    public static final String SUCCESS = "success";
    public static final String FAILURE = "failure";
    public static final String PENDING = "pending";
    public static final String HELPCHAT_PG_MID = "helpchat_pg_mid";
    public static final String THIRD_PARTY_SOURCE_TOKEN = "third_party_token";


    @Autowired
    private ThirdPartyDao thirdPartyDao;

    @Override
    public ThirdPartyEntity getThirdPartyByToken() {
        throw new  NotImplementedException("");
    }

    @Override
    public ThirdPartyEntity getThirdPartyByToken(String token){
        if (token != null) {
            return thirdPartyDao.findOneByToken(token);
        } else {
            return null;
        }
    }

    public ThirdPartyEntity getThirdPartyBySource(String source) {
        return thirdPartyDao.findOneBySource(source);
    }



}
