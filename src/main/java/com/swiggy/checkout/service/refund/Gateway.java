package com.swiggy.checkout.service.refund;

import lombok.extern.slf4j.Slf4j;

/**
 * Created by Geetesh on 6/28/17.
 */
@Slf4j
public enum Gateway {

    PAYTM(1, "PAYTM"),
    PAYU(2, "PAYU"),
    RAZORPAY(3, "RAZORPAY");

    private final int gatewayId;
    private String gateway;

    Gateway(int gatewayId, String gateway) {
        this.gatewayId = gatewayId;
        this.gateway = gateway;
    }

    public static Gateway getGateway(String gateway) {
        Gateway[] gateways = values();
        for(Gateway gateway1 : gateways) {
            if(gateway1.name().equalsIgnoreCase(gateway)) {
                return gateway1;
            }
        }
        log.error("unknown payment gateway {}", gateway);
        return null;
    }

}
