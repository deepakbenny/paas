package com.swiggy.checkout.freecharge.entities.responses;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import com.fasterxml.jackson.annotation.JsonInclude;
import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

/**
 * Created by Manish on 27/01/17.
 */

@Setter
@Getter
@NoArgsConstructor
@AllArgsConstructor
@JsonInclude(JsonInclude.Include.NON_NULL)
@JsonIgnoreProperties(ignoreUnknown = true)
public class DebitResponse extends FreechargeBaseResponse{

    private String txnId;
    private String merchantTxnId;
    private String amount;
    private String checksum;
    private String status;
    private String errorCode;
    private String errorMessage;
    private String metadata;
}
