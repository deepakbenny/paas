package com.swiggy.checkout.paytm.entities.request;

import com.fasterxml.jackson.annotation.JsonInclude;
import lombok.*;

/**
 * Created by Manish on 03/02/17.
 */
@NoArgsConstructor
@AllArgsConstructor
@Getter
@Setter
@Builder
@JsonInclude(JsonInclude.Include.NON_NULL)
public class PaytmRefundRequest {
    private String orderId;
    private String txnId;
    private String refundAmount;
}
