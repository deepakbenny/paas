package com.swiggy.checkout.thirdparty.client.http;

import retrofit2.Call;
import retrofit2.http.Body;
import retrofit2.http.POST;

import java.util.Map;


/**
 * Created by chandelabhinav on 21/07/16.
 *
 */
public interface ThirdPartyEditNotifyHttpClient {
    @POST("./")
    Call<Object> editNotify(@Body Map<String, String> request);
}

