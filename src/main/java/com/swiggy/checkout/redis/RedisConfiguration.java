package com.swiggy.checkout.redis;

import com.swiggy.commons.redis.AbstractRedisCacheService;
import com.swiggy.commons.redis.RedisCacheService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.cache.CacheManager;
import org.springframework.cache.annotation.CachingConfigurerSupport;
import org.springframework.cache.annotation.EnableCaching;
import org.springframework.cache.interceptor.KeyGenerator;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.context.annotation.Primary;
import org.springframework.data.redis.cache.RedisCacheManager;
import org.springframework.data.redis.connection.RedisConnectionFactory;
import org.springframework.data.redis.connection.jedis.JedisConnectionFactory;
import org.springframework.data.redis.serializer.GenericToStringSerializer;
import org.springframework.data.redis.serializer.StringRedisSerializer;

import java.lang.reflect.Method;

/**
 * Created by Manish on 23/01/17.
 */

@Configuration
@EnableCaching
public class RedisConfiguration extends CachingConfigurerSupport {

    @Value("${redis.host}")
    private String host;

    @Value("${redis.port}")
    private String port;

    @Bean
    @Primary
    public RedisCacheService redisCacheService(){
        RedisCacheService redisCacheService =  new RedisCacheService();
        redisCacheService.setConnectionFactory(jedisConnectionFactory(0));
        redisCacheService.setEnableDefaultSerializer(false);
        redisCacheService.setHashValueSerializer(new GenericToStringSerializer<>(Object.class));
        redisCacheService.setValueSerializer(new GenericToStringSerializer<>(Object.class));
        return redisCacheService;
    }


    private RedisConnectionFactory jedisConnectionFactory(int index) {
        JedisConnectionFactory factory = new JedisConnectionFactory();
        factory.setHostName(host);
        factory.setPort(Integer.valueOf(port));
        factory.setDatabase(index);
        factory.setUsePool(true);
        return factory;
    }

    @Bean
    @Qualifier(value = "redisCacheManagerPayments")
    public CacheManager redisCacheManagerPayments() {
        RedisCacheManager cacheManager = new RedisCacheManager(redisCacheService());

        // Number of seconds before expiration. Defaults to unlimited (0)
        cacheManager.setDefaultExpiration(300);
        return cacheManager;
    }

    @Bean
    public KeyGenerator keyGenerator() {
        return new KeyGenerator() {
            @Override
            public Object generate(Object o, Method method, Object... objects) {
                // This will generate a unique key of the class name, the method name,
                // and all method parameters appended.
                StringBuilder sb = new StringBuilder();
                sb.append(o.getClass().getName());
                sb.append(method.getName());
                for (Object obj : objects) {
                    if (obj != null) {
                        sb.append(obj.toString());
                    } else {
                        sb.append("null");
                    }
                }
                return sb.toString();
            }
        };
    }
}