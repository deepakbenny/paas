package com.swiggy.checkout.exception;

import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

/**
 * Created by Manish on 30/01/17.
 */
@Getter
@Setter
@AllArgsConstructor
@NoArgsConstructor
public class CardPaymentException extends Exception {

    private int code;
    private String message;
}
