package com.swiggy.checkout.thirdparty.client.exchange;

import lombok.Getter;
import lombok.Setter;

/**
 * Created by abhinav.chandel on 9/17/16.
 */
@Getter
@Setter
public class HelpchatWithdrawRequest {
    private double thirdPartyCash;
}

