package com.swiggy.checkout.extenal.entities.request;

import com.fasterxml.jackson.annotation.JsonProperty;
import lombok.Data;

/**
 * Created by Manish on 03/02/17.
 */
@Data
public class PaymentRoutingRequest {

    @JsonProperty("card_type")
    private String cardType;

    @JsonProperty("payment_type")
    private String paymentType;

    @JsonProperty("issuing_bank")
    private String issuingBank;

    @JsonProperty("payment_gateway")
    private String gateway;

    @JsonProperty("traffic_percentage")
    private Integer percentage;

    @JsonProperty("updated_by")
    private String updatedBy;

    @JsonProperty("enabled")
    private Boolean enabled;


}
