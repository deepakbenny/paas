package com.swiggy.checkout.service.paylater;

import com.swiggy.checkout.exception.SwiggyPaymentException;
import com.swiggy.checkout.extenal.entities.request.PayLater.*;
import com.swiggy.checkout.extenal.entities.response.BaseResponse;
import com.swiggy.checkout.extenal.entities.response.PayLater.PayLaterRefundResponse;
import com.swiggy.checkout.extenal.entities.response.PayLater.PayLaterTxnEnquiryResponse;
import com.swiggy.checkout.extenal.entities.response.SuccessResponse;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;

/**
 * Created by chirag.rc on 10/13/17.
 */
@Service
public class PayLaterService {

    @Autowired
    private PayLaterServiceFactory payLaterServiceFactory;

    private PayLaterBaseService payLaterBaseService;

    public BaseResponse checkEligibility(PayLaterEligibilityRequest payLaterEligibilityRequest) throws SwiggyPaymentException {
        payLaterBaseService = payLaterServiceFactory.getService(payLaterEligibilityRequest.getPaylaterMethod());
        return payLaterBaseService.checkEligibility(payLaterEligibilityRequest);
    }

    public BaseResponse getOtpForLinking(PayLaterGetOtpRequest payLaterGetOtpRequest) throws SwiggyPaymentException {
        payLaterBaseService = payLaterServiceFactory.getService(payLaterGetOtpRequest.getPaylaterMethod());
        return payLaterBaseService.getOtpForLinking(payLaterGetOtpRequest);
    }

    public BaseResponse validate(PayLaterOtpValidationRequest payLaterOtpValidationRequest) throws SwiggyPaymentException {
        payLaterBaseService = payLaterServiceFactory.getService(payLaterOtpValidationRequest.getPaylaterMethod());
        return payLaterBaseService.validate(payLaterOtpValidationRequest);
    }

    public BaseResponse checkBalance(String payLaterMethod) throws SwiggyPaymentException {
        // TODO when the support is available from the third party
        return new SuccessResponse();
    }

    public BaseResponse debitAmount(PayLaterDebitAmountRequest payLaterDebitAmountRequest) throws SwiggyPaymentException {
        payLaterBaseService = payLaterServiceFactory.getService(payLaterDebitAmountRequest.getPaylaterMethod());
        return payLaterBaseService.debitAmount(payLaterDebitAmountRequest);
    }

    public BaseResponse delink(PayLaterDelinkRequest payLaterDelinkRequest) throws SwiggyPaymentException {
        payLaterBaseService = payLaterServiceFactory.getService(payLaterDelinkRequest.getPaylaterMethod());
        return payLaterBaseService.delink(payLaterDelinkRequest);
    }

    public BaseResponse isLinked(PayLaterIsAccountLinkedRequest payLaterIsAccountLinkedRequest) throws SwiggyPaymentException {
        payLaterBaseService = payLaterServiceFactory.getService(payLaterIsAccountLinkedRequest.getPaylaterMethod());
        return payLaterBaseService.isLinked(payLaterIsAccountLinkedRequest);
    }

    public BaseResponse resendOtp(PayLaterResendOtpRequest payLaterResendOtpRequest) throws SwiggyPaymentException {
        payLaterBaseService = payLaterServiceFactory.getService(payLaterResendOtpRequest.getPaylaterMethod());
        return payLaterBaseService.resendOtp(payLaterResendOtpRequest);
    }

    public List<PayLaterTxnEnquiryResponse> enquiry(PayLaterTxnEnquiryRequest payLaterTxnEnquiryRequest) throws SwiggyPaymentException {
        payLaterBaseService = payLaterServiceFactory.getService(payLaterTxnEnquiryRequest.getPaylaterMethod());
        return payLaterBaseService.enquiry(payLaterTxnEnquiryRequest);
    }

    public PayLaterRefundResponse refund(PayLaterRefundRequest payLaterRefundRequest) throws SwiggyPaymentException {
        payLaterBaseService = payLaterServiceFactory.getService(payLaterRefundRequest.getPaylaterMethod());
        return payLaterBaseService.refund(payLaterRefundRequest);
    }
}
