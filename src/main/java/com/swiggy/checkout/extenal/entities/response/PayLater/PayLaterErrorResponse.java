package com.swiggy.checkout.extenal.entities.response.PayLater;

import com.fasterxml.jackson.annotation.JsonProperty;
import lombok.Data;

/**
 * Created by chirag.rc on 11/15/17.
 */
@Data
public class PayLaterErrorResponse  {

    @JsonProperty("timestamp")
    private long timestamp;

    @JsonProperty("error")
    private String error;

    @JsonProperty("message")
    private String message;

    @JsonProperty("path")
    private String path;

    @JsonProperty("errorCode")
    private String errorCode;
}
