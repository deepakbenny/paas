package com.swiggy.checkout.paytm.service;

import com.swiggy.checkout.exception.PaytmFailureException;
import com.swiggy.checkout.exception.SwiggyPaymentException;
import com.swiggy.checkout.paytm.entities.response.PaytmBaseResponse;
import com.swiggy.checkout.utils.Utility;
import lombok.extern.slf4j.Slf4j;
import org.springframework.stereotype.Service;
import retrofit2.Call;
import retrofit2.Response;

import java.io.IOException;

/**
 * Created by Manish on 03/02/17.
 */
@Service
@Slf4j
public class PayTmRetrofitService {

    private static final String PAYTM_API_FAILURE = "FAILURE";

    <T extends PaytmBaseResponse> T getResponseOrLogWarning(Call<T> call, String failMessage) throws SwiggyPaymentException {
        try {
            return getResponse(call);
        } catch (PaytmFailureException paytmFailureException) {
            log.warn(" Reason - " + paytmFailureException.getMessage());
            throw new SwiggyPaymentException(1, failMessage);
        }
    }

    private <T extends PaytmBaseResponse> T getResponse(Call<T> call) throws PaytmFailureException {
        try {
            Response<T> execute = call.execute();

            if (execute.isSuccessful()) {
                log.info(execute.body().toString());

                if (PAYTM_API_FAILURE.equalsIgnoreCase(execute.body().getStatus()))
                    throw new PaytmFailureException(1, "Paytm api replied with failure response - " + Utility.jsonEncode(execute.body()));

                return execute.body();
            }
            throw new PaytmFailureException(1, "Paytm apis replied failure - " + Utility.jsonEncode(execute.errorBody()));
        } catch (IOException e) {
            throw new PaytmFailureException(1, e.getMessage());
        }
    }
}
