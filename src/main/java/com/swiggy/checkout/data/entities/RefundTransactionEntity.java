package com.swiggy.checkout.data.entities;

import javax.persistence.*;
import java.sql.Timestamp;

/**
 * Created by Geetesh on 7/12/17.
 */
@Entity
@Table(name = "refund_transaction")
public class RefundTransactionEntity {


    private Long id;
    private long orderId;
    private String paymentMethod;
    private String amount;
    private String refundResponseDetails;
    private Timestamp createdOn;
    private String createdBy;




    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    @Column(name = "id", nullable = false)
    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    @Basic
    @Column(name = "order_id")
    public long getOrderId() {
        return orderId;
    }

    public void setOrderId(long orderId) {
        this.orderId = orderId;
    }

    @Basic
    @Column(name = "payment_method")
    public String getPaymentMethod() {
        return paymentMethod;
    }

    public void setPaymentMethod(String paymentMethod) {
        this.paymentMethod = paymentMethod;
    }

    @Basic
    @Column(name = "amount")
    public String getAmount() {
        return amount;
    }

    public void setAmount(String amount) {
        this.amount = amount;
    }

    @Basic
    @Column(name = "refund_response_details")
    public String getRefundResponseDetails() {
        return refundResponseDetails;
    }

    public void setRefundResponseDetails(String refundResponseDetails) {
        this.refundResponseDetails = refundResponseDetails;
    }

    @Basic
    @Column(name = "created_on")
    public Timestamp getCreatedOn() {
        return createdOn;
    }

    public void setCreatedOn(Timestamp createdOn) {
        this.createdOn = createdOn;
    }

    @Basic
    @Column(name = "created_by")
    public String getCreatedBy() {
        return createdBy;
    }

    public void setCreatedBy(String createdBy) {
        this.createdBy = createdBy;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (!(o instanceof RefundTransactionEntity)) return false;

        RefundTransactionEntity that = (RefundTransactionEntity) o;

        if (id != that.id) return false;
        if (orderId != that.orderId) return false;
        if (!paymentMethod.equals(that.paymentMethod)) return false;
        if (!amount.equals(that.amount)) return false;
        if (!refundResponseDetails.equals(that.refundResponseDetails)) return false;
        if (!createdOn.equals(that.createdOn)) return false;
        return createdBy.equals(that.createdBy);

    }

    @Override
    public int hashCode() {
        int result = (int) (id ^ (id >>> 32));
        result = 31 * result + (int) (orderId ^ (orderId >>> 32));
        result = 31 * result + paymentMethod.hashCode();
        result = 31 * result + amount.hashCode();
        result = 31 * result + refundResponseDetails.hashCode();
        result = 31 * result + createdOn.hashCode();
        result = 31 * result + createdBy.hashCode();
        return result;
    }
}
