package com.swiggy.checkout.mobikwik.entities.response;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.annotation.JsonProperty;
import lombok.AllArgsConstructor;
import lombok.NoArgsConstructor;

/**
 * Created by Manish on 29/01/17.
 */
@NoArgsConstructor
@AllArgsConstructor
@JsonIgnoreProperties(ignoreUnknown = true)
@JsonInclude(JsonInclude.Include.NON_NULL)
public class CheckBalanceResponse extends MobikwikBaseResponse {

    private String walletBalance;

    @JsonProperty("balanceamount")
    public String getWalletBalance() {
        return walletBalance;
    }

    @JsonProperty("balanceamount")
    public void setWalletBalance(String walletBalance) {
        this.walletBalance = walletBalance;
    }
}
