package com.swiggy.checkout.service.paylater.lazypay;

import com.swiggy.checkout.data.entities.UserTokenDetailsEntity;
import com.swiggy.checkout.data.model.dao.UserTokenDetailsDao;
import com.swiggy.checkout.extenal.entities.request.PayLater.*;
import com.swiggy.checkout.extenal.entities.request.PayLater.LazyPay.*;
import com.swiggy.checkout.extenal.entities.response.BaseResponse;
import com.swiggy.checkout.extenal.entities.response.ErrorResponse;
import com.swiggy.checkout.extenal.entities.response.PayLater.LazyPay.*;
import com.swiggy.checkout.extenal.entities.response.PayLater.PayLaterRefundResponse;
import com.swiggy.checkout.extenal.entities.response.PayLater.PayLaterTxnEnquiryResponse;
import com.swiggy.checkout.extenal.entities.response.SuccessResponse;
import com.swiggy.checkout.pojo.PayLater.LazyPay.*;
import com.swiggy.checkout.service.paylater.PayLaterBaseService;
import com.swiggy.checkout.utils.Utility;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.util.CollectionUtils;

import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

/**
 * Created by chirag.rc on 10/25/17.
 */
@Slf4j
@Service
public class LazyPayService implements PayLaterBaseService {

    private static final String MERCHANT_NAME = "SWIGGY";
    private static final String LAZYPAY = "PayLater_Lazypay";
    private static final String SWIGGY_ID_ = "SW_";
    private static final String LAZYPAY_ELIGIBILITY_CODE = "LP_ELIGIBLE";
    private static final String AUTO_DEBIT = "AUTO_DEBIT";
    private static final String BEARER = "Bearer";

    @Autowired
    private LazyPayHelper lazyPayHelper;

    @Autowired
    private LazyPayClient lazyPayClient;

    @Autowired
    private UserTokenDetailsDao userTokenDetailsDao;

    @Override
    public BaseResponse checkEligibility(PayLaterEligibilityRequest eligibilityRequest) {
        LazyPayEligibilityRequest lazyPayEligibilityRequest = new LazyPayEligibilityRequest();
        lazyPayEligibilityRequest.setLazyPayUserDetails(getLazyPayFormattedUserDetails(eligibilityRequest.getEmail(), eligibilityRequest.getMobile()));
        lazyPayEligibilityRequest.setLazyPayAmountDetails(getLazyPayFormattedAmountDetails(eligibilityRequest.getValue(), eligibilityRequest.getCurrency()));
        lazyPayEligibilityRequest.setMerchantName(MERCHANT_NAME);

        String signature = lazyPayHelper.getSignatureForEligibility(eligibilityRequest.getValue(), eligibilityRequest.getMobile(), eligibilityRequest.getEmail());

        String accessKey = lazyPayHelper.getAccessKey();

        LazyPayEligibilityResponse lazyPayEligibilityResponse = lazyPayClient.checkEligibility(signature, accessKey, lazyPayEligibilityRequest);

        LazyPayErrorDetails lazyPayErrorDetails = new LazyPayErrorDetails();

        if (null != lazyPayEligibilityResponse) {
            if (LAZYPAY_ELIGIBILITY_CODE.equalsIgnoreCase(lazyPayEligibilityResponse.getCode())) {
                return new SuccessResponse();
            }
            lazyPayErrorDetails.setCode(lazyPayEligibilityResponse.getCode());
            lazyPayErrorDetails.setMessage(lazyPayEligibilityResponse.getReason());
        }
        return new ErrorResponse(lazyPayErrorDetails);
    }

    @Override
    public BaseResponse getOtpForLinking(PayLaterGetOtpRequest payLaterGetOtpRequest) {
        LazyPayGetOtpRequest lazyPayGetOtpRequest = new LazyPayGetOtpRequest();
        lazyPayGetOtpRequest.setMerchantName(MERCHANT_NAME);
        lazyPayGetOtpRequest.setLazyPayAmountDetails(getLazyPayFormattedAmountDetails(payLaterGetOtpRequest.getAmount(), payLaterGetOtpRequest.getCurrency()));
        lazyPayGetOtpRequest.setLazyPayUserDetails(getLazyPayFormattedUserDetails(payLaterGetOtpRequest.getEmail(), payLaterGetOtpRequest.getMobile()));
        lazyPayGetOtpRequest.setMerchantTxnId(payLaterGetOtpRequest.getMerchantTxnId());

        String signature = lazyPayHelper.getSignatureForTransaction(payLaterGetOtpRequest.getMerchantTxnId(), payLaterGetOtpRequest.getAmount());

        String accessKey = lazyPayHelper.getAccessKey();

        LazyPayGetOtpResponse response = lazyPayClient.getOtpForLinking(signature, accessKey, lazyPayGetOtpRequest);

        LazyPayErrorDetails lazyPayErrorDetails = new LazyPayErrorDetails();

        if (null != response) {
            if (Utility.isEmpty(response.getErrorCode())) {
                Map<String, String> data = new HashMap<>();
                data.put("txnRefNo", response.getTxnRefNo());
                return new SuccessResponse(data);
            }
            lazyPayErrorDetails.setCode(response.getErrorCode());
            lazyPayErrorDetails.setMessage(response.getMessage());
        }
        return new ErrorResponse(lazyPayErrorDetails);
    }

    @Override
    public BaseResponse validate(PayLaterOtpValidationRequest payLaterOtpValidationRequest) {
        LazyPayValidateOtpRequest lazyPayValidateOtpRequest = new LazyPayValidateOtpRequest();
        lazyPayValidateOtpRequest.setPaymentMode(payLaterOtpValidationRequest.getPaymentMode());
        lazyPayValidateOtpRequest.setTxnRefNo(payLaterOtpValidationRequest.getTxnRefNo());
        lazyPayValidateOtpRequest.setOtp(payLaterOtpValidationRequest.getOtp());

        String signature = lazyPayHelper.getSignatureForOtpValidation(payLaterOtpValidationRequest.getTxnRefNo());

        LazyPayTransactionResponse response = lazyPayClient.validate(signature, lazyPayValidateOtpRequest);

        LazyPayErrorDetails lazyPayErrorDetails = new LazyPayErrorDetails();

        if (null != response) {
            if (null != response.getLazyPayTokenDetails() && Utility.isEmpty(response.getErrorCode())) {
                saveUserTokenDetails(payLaterOtpValidationRequest.getUserId(), response.getLazyPayTokenDetails());
                //refund the amount used up in linking process
                PayLaterRefundRequest payLaterRefundRequest = new PayLaterRefundRequest();
                payLaterRefundRequest.setUserId(String.valueOf(payLaterOtpValidationRequest.getUserId()));
                payLaterRefundRequest.setCurrency(response.getCurrency());
                payLaterRefundRequest.setMerchantTxnId(response.getMerchantOrderId());
                payLaterRefundRequest.setAmount(response.getAmount());

                PayLaterRefundResponse payLaterRefundResponse = refund(payLaterRefundRequest);

                if (null != payLaterRefundResponse && Utility.isEmpty(payLaterRefundResponse.getErrorCode())) {
                    return new SuccessResponse(null);
                } else if (null == payLaterRefundResponse) {
                    return new ErrorResponse(payLaterRefundResponse);
                } else {
                    lazyPayErrorDetails.setCode(payLaterRefundResponse.getErrorCode());
                    lazyPayErrorDetails.setMessage(payLaterRefundResponse.getMessage());
                }
            } else {
                lazyPayErrorDetails.setCode(response.getErrorCode());
                lazyPayErrorDetails.setMessage(response.getMessage());
            }
        }
        return new ErrorResponse(lazyPayErrorDetails);
    }


    @Override
    public BaseResponse debitAmount(PayLaterDebitAmountRequest payLaterDebitAmountRequest) {
        LazyPayAutoDebitRequest lazyPayAutoDebitRequest = new LazyPayAutoDebitRequest();
        lazyPayAutoDebitRequest.setPaymentMode(payLaterDebitAmountRequest.getPaymentMode());
        lazyPayAutoDebitRequest.setLazyPayUserDetails(getLazyPayFormattedUserDetails(payLaterDebitAmountRequest.getEmail(), payLaterDebitAmountRequest.getMobile()));
        lazyPayAutoDebitRequest.setLazyPayAmountDetails(getLazyPayFormattedAmountDetails(payLaterDebitAmountRequest.getValue(), payLaterDebitAmountRequest.getCurrency()));
        lazyPayAutoDebitRequest.setMerchantTxnId(SWIGGY_ID_ + payLaterDebitAmountRequest.getOrderId());
        lazyPayAutoDebitRequest.setSource(MERCHANT_NAME);
        lazyPayAutoDebitRequest.setPaymentMode(AUTO_DEBIT);
        String signature = lazyPayHelper.getSignatureForTransaction(lazyPayAutoDebitRequest.getMerchantTxnId(), payLaterDebitAmountRequest.getValue());

        //token format: Bearer <user_token>
        String token = BEARER + ' ' + getUserLazyPayToken(payLaterDebitAmountRequest.getUserId());

        if (Utility.isEmpty(token)) {
            return new ErrorResponse(1, "Error while auto debiting amount in lazypay as token is null");
        }

        LazyPayTransactionResponse response = lazyPayClient.debitAmount(signature, token, lazyPayAutoDebitRequest);

        LazyPayErrorDetails lazyPayErrorDetails = new LazyPayErrorDetails();
        if (null != response) {
            if (Utility.isEmpty(response.getErrorCode())) {
                return new SuccessResponse(response);
            }
            lazyPayErrorDetails.setCode(response.getErrorCode());
            lazyPayErrorDetails.setMessage(response.getMessage());
        }

        return new ErrorResponse(lazyPayErrorDetails);
    }

    @Override
    public BaseResponse delink(PayLaterDelinkRequest payLaterDelinkRequest) {
        UserTokenDetailsEntity userTokenDetailsEntity = userTokenDetailsDao.findByUserIdAndPaymentMethodName(payLaterDelinkRequest.getUserId(), LAZYPAY);
        if (null != userTokenDetailsEntity) {
            userTokenDetailsDao.delete(userTokenDetailsEntity);
            return new SuccessResponse("User's LazyPay account is delinked successfully!");
        }

        return new SuccessResponse("User's LazyPay account is already delinked!");
    }

    @Override
    public BaseResponse isLinked(PayLaterIsAccountLinkedRequest payLaterIsAccountLinkedRequest) {

        String token = getUserLazyPayToken(payLaterIsAccountLinkedRequest.getUserId());

        if (Utility.isEmpty(token)) {
            return new ErrorResponse(1, "User account not linked");
        }
        return new SuccessResponse("User account is linked");
    }

    @Override
    public PayLaterRefundResponse refund(PayLaterRefundRequest payLaterRefundRequest) {
        LazyPayRefundRequest lazyPayRefundRequest = new LazyPayRefundRequest();
        lazyPayRefundRequest.setMerchantTxnId(payLaterRefundRequest.getMerchantTxnId());
        lazyPayRefundRequest.setLazyPayAmountDetails(getLazyPayFormattedAmountDetails(payLaterRefundRequest.getAmount(), payLaterRefundRequest.getCurrency()));

        String accessKey = lazyPayHelper.getAccessKey();

        String signature = lazyPayHelper.getSignatureForRefund(payLaterRefundRequest.getMerchantTxnId(), payLaterRefundRequest.getAmount());

        log.info("[LAZYPAY SERVICE] LazyPay Refund Request : {}", Utility.jsonEncode2(lazyPayRefundRequest));

        LazyPayRefundResponse lazyPayRefundResponse = lazyPayClient.refund(signature, accessKey, lazyPayRefundRequest);

        return convertRefundResponseToPayLaterResponse(lazyPayRefundResponse);
    }

    private PayLaterRefundResponse convertRefundResponseToPayLaterResponse(LazyPayRefundResponse lazyPayRefundResponse) {

        PayLaterRefundResponse payLaterRefundResponse = new PayLaterRefundResponse();
        if (null == lazyPayRefundResponse) {
            return null;
        }
        payLaterRefundResponse.setErrorCode(lazyPayRefundResponse.getErrorCode());
        payLaterRefundResponse.setError(lazyPayRefundResponse.getError());
        payLaterRefundResponse.setPath(lazyPayRefundResponse.getPath());
        payLaterRefundResponse.setTxnDateTime(lazyPayRefundResponse.getTxnDateTime());
        payLaterRefundResponse.setAmount(lazyPayRefundResponse.getAmount());
        payLaterRefundResponse.setLpTxnId(lazyPayRefundResponse.getLpTxnId());
        payLaterRefundResponse.setRespMessage(lazyPayRefundResponse.getRespMessage());
        payLaterRefundResponse.setStatus(lazyPayRefundResponse.getStatus());
        payLaterRefundResponse.setMessage(lazyPayRefundResponse.getMessage());
        payLaterRefundResponse.setTimestamp(lazyPayRefundResponse.getTimestamp());

        return payLaterRefundResponse;
    }

    @Override
    public BaseResponse resendOtp(PayLaterResendOtpRequest payLaterResendOtpRequest) {
        LazyPayResendOtpRequest lazyPayResendOtpRequest = new LazyPayResendOtpRequest();
        lazyPayResendOtpRequest.setTxnRefNo(payLaterResendOtpRequest.getTxnRefNo());

        String signature = lazyPayHelper.getSignatureForRegeneratingOtp(payLaterResendOtpRequest.getTxnRefNo());

        LazyPayResendOtpResponse response = lazyPayClient.regenerateOtp(signature, lazyPayResendOtpRequest);


        LazyPayErrorDetails lazyPayErrorDetails = new LazyPayErrorDetails();

        if (null != response) {
            if (Utility.isEmpty(response.getErrorCode())) {
                return new SuccessResponse(response);
            }
            lazyPayErrorDetails.setCode(response.getErrorCode());
            lazyPayErrorDetails.setMessage(response.getMessage());
        }
        return new ErrorResponse(lazyPayErrorDetails);
    }

    @Override
    public List<PayLaterTxnEnquiryResponse> enquiry(PayLaterTxnEnquiryRequest payLaterTxnEnquiryRequest) {
        LazyPayTxnEnquiryRequest lazyPayTxnEnquiryRequest = new LazyPayTxnEnquiryRequest();
        lazyPayTxnEnquiryRequest.setMerchantTxnId(SWIGGY_ID_ + payLaterTxnEnquiryRequest.getMerchantOrderId());
        lazyPayTxnEnquiryRequest.setIsSale(payLaterTxnEnquiryRequest.getIsSale());

        String signature = lazyPayHelper.getSignatureForEnquiry(lazyPayTxnEnquiryRequest.getMerchantTxnId());

        String accessKey = lazyPayHelper.getAccessKey();

        List<LazyPayTxnEnquiryResponse> lazyPayTxnEnquiryResponseList = lazyPayClient.enquireTransaction(signature, accessKey, lazyPayTxnEnquiryRequest);

        return convertTxnResponseToPayLaterResponse(lazyPayTxnEnquiryResponseList);
    }

    private List<PayLaterTxnEnquiryResponse> convertTxnResponseToPayLaterResponse(List<LazyPayTxnEnquiryResponse> lazyPayTxnEnquiryResponseList) {
        List<PayLaterTxnEnquiryResponse> payLaterTxnEnquiryResponseList = new ArrayList<>();

        if (CollectionUtils.isEmpty(lazyPayTxnEnquiryResponseList)) {
            return payLaterTxnEnquiryResponseList;
        }

        for (LazyPayTxnEnquiryResponse lazyPayTxnEnquiryResponse : lazyPayTxnEnquiryResponseList) {
            PayLaterTxnEnquiryResponse payLaterTxnEnquiryResponse = new PayLaterTxnEnquiryResponse();
            payLaterTxnEnquiryResponse.setTimestamp(lazyPayTxnEnquiryResponse.getTimestamp());
            payLaterTxnEnquiryResponse.setMessage(lazyPayTxnEnquiryResponse.getMessage());
            payLaterTxnEnquiryResponse.setAmount(lazyPayTxnEnquiryResponse.getAmount());
            payLaterTxnEnquiryResponse.setLpTxnId(lazyPayTxnEnquiryResponse.getLpTxnId());
            payLaterTxnEnquiryResponse.setRespMessage(lazyPayTxnEnquiryResponse.getRespMessage());
            payLaterTxnEnquiryResponse.setStatus(lazyPayTxnEnquiryResponse.getStatus());
            payLaterTxnEnquiryResponse.setTxnDateTime(lazyPayTxnEnquiryResponse.getTxnDateTime());
            payLaterTxnEnquiryResponse.setTxnType(lazyPayTxnEnquiryResponse.getTxnType());
            payLaterTxnEnquiryResponse.setError(lazyPayTxnEnquiryResponse.getError());
            payLaterTxnEnquiryResponse.setErrorCode(lazyPayTxnEnquiryResponse.getErrorCode());
            payLaterTxnEnquiryResponse.setPath(lazyPayTxnEnquiryResponse.getPath());
            payLaterTxnEnquiryResponseList.add(payLaterTxnEnquiryResponse);
        }
        return payLaterTxnEnquiryResponseList;
    }

    private LazyPayUserDetails getLazyPayFormattedUserDetails(String email, String mobile) {
        return LazyPayUserDetails.builder()
                .email(email)
                .mobile(mobile)
                .build();
    }

    private LazyPayAmountDetails getLazyPayFormattedAmountDetails(String value, String currency) {
        return LazyPayAmountDetails.builder()
                .value(value)
                .currency(currency)
                .build();
    }


    private void saveUserTokenDetails(long userId, LazyPayTokenDetails lazyPayTokenDetails) {
        UserTokenDetailsEntity userTokenEntity = userTokenDetailsDao.findByUserIdAndPaymentMethodName(userId, LAZYPAY);
        if (null != userTokenEntity) {
            userTokenDetailsDao.delete(userTokenEntity);
        }

        userTokenEntity = UserTokenDetailsEntity.builder()
                .token(lazyPayTokenDetails.getAccessToken())
                .refreshToken(lazyPayTokenDetails.getRefreshToken())
                .userId(userId)
                .paymentMethodName(LAZYPAY)
                .expiresOn(getTimeStamp(lazyPayTokenDetails.getExpiresIn()))
                .createdOn(new Timestamp(new java.util.Date().getTime()))
                .build();
        userTokenDetailsDao.save(userTokenEntity);
    }

    private Timestamp getTimeStamp(long expiresIn) {
        long currentTimeInMilliSeconds = System.currentTimeMillis();
        long remainingTimeInMilliSeconds = expiresIn * 1000;
        return new Timestamp(currentTimeInMilliSeconds + remainingTimeInMilliSeconds);
    }

    private String getUserLazyPayToken(long userId) {
        UserTokenDetailsEntity userTokenEntity = userTokenDetailsDao.findByUserIdAndPaymentMethodName(userId, LAZYPAY);
        if (null != userTokenEntity) {
            Timestamp currentTime = new Timestamp(new java.util.Date().getTime());
            if (currentTime.before(userTokenEntity.getExpiresOn())) {
                return userTokenEntity.getToken();
            }
            log.info("[LAZYPAY-SERVICE] Current LazyPay Token for the user {} has expired", userId);
            userTokenDetailsDao.delete(userTokenEntity);
            return "";
        }
        log.info("[LAZYPAY-SERVICE] User {} do not possess lazypay token", userId);
        return "";
    }
}
