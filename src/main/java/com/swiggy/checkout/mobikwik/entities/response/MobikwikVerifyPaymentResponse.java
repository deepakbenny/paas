package com.swiggy.checkout.mobikwik.entities.response;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import com.fasterxml.jackson.annotation.JsonInclude;
import lombok.Getter;
import lombok.Setter;

/**
 * Created by Manish on 29/01/17.
 */
@JsonInclude(JsonInclude.Include.NON_EMPTY)
@JsonIgnoreProperties(ignoreUnknown = true)
@Getter
@Setter
public class MobikwikVerifyPaymentResponse {

    private String amount;
    private String checksum;
    private String statuscode;
    private String orderid;
    private String refid;
    private String statusmessage;
    private String ordertype;
}
