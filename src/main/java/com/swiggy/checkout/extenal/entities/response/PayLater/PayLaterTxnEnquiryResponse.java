package com.swiggy.checkout.extenal.entities.response.PayLater;

import com.fasterxml.jackson.annotation.JsonProperty;
import lombok.Data;

/**
 * Created by chirag.rc on 11/15/17.
 */
@Data
public class PayLaterTxnEnquiryResponse extends PayLaterErrorResponse {
    @JsonProperty("status")
    private String status;

    @JsonProperty("respMessage")
    private String respMessage;

    @JsonProperty("lpTxnId")
    private String lpTxnId;

    @JsonProperty("txnType")
    private String txnType;

    @JsonProperty("txnDateTime")
    private String txnDateTime;

    @JsonProperty("amount")
    private String amount;
}
